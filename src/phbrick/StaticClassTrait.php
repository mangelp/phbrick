<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use RuntimeException;

trait StaticClassTrait
{
    public final function __construct()
    {
        throw new RuntimeException('Cannot create an instance of an static class');
    }
    // This is not needed as the class cannot be instanced and thus there is no instance to be
    // cloned.
    public final function __clone()
    {
        throw new RuntimeException('Cannot clone an static class');
    }
}
