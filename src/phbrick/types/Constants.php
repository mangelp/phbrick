<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use InvalidArgumentException;
use phbrick\StaticClass;

/**
 * Class Constants
 *
 * Wraps PHPs constants providing access to them but also allowing to override constant values.
 *
 * Provide some simple methods to access constants and define overrides using an array when set.
 *
 * @package phbrick\types
 */
class Constants extends StaticClass
{
    /**
     * Array of constants set
     *
     * @var array
     */
    private static $constants = [];

    /**
     * Returns an array with all constant names set within this class but not defined constants.
     */
    public static function getReDefinedConstantNames()
    {
        return array_keys(Constants::$constants);
    }

    /**
     * Gets a constant value if is defined or a default value if not.
     *
     * @param string $name Constant name
     * @param null $defaultValue
     * @return mixed|null
     */
    public static function get($name, $defaultValue = null)
    {
        Constants::assertValidConstantName($name);

        if (isset(Constants::$constants[$name])) {
            return Constants::$constants[$name];
        }
        else if (Constants::isDefined($name)) {
            return constant($name);
        }
        else {
            return $defaultValue;
        }
    }

    /**
     * Gets if a constant is defined.
     *
     * @param string $name Constant name
     * @return bool
     */
    public static function isDefined($name)
    {
        Constants::assertValidConstantName($name);

        return isset(Constants::$constants[$name]) || defined($name);
    }

    /**
     * Gets if a constant is set in the internal array.
     *
     * @param string $name Constant name
     * @return bool
     */
    public static function isReDefined($name)
    {
        Constants::assertValidConstantName($name);

        return isset(Constants::$constants[$name]);
    }

    /**
     * Sets the value of a constant.
     *
     * If the constant is already defined throws an exception.
     *
     * @param string $name Constant name
     * @param $value
     * @return mixed|null The old value if the constant was defined
     */
    public static function set($name, $value)
    {
        $oldValue = null;

        if (constants::isDefined($name)) {
            $oldValue = Constants::get($name);
        }

        Constants::$constants[$name] = $value;
        return $oldValue;
    }

    /**
     * Removes the constant definition from the internal array of constants if defined there.
     *
     * If not does nothing.
     *
     * @param $name
     */
    public static function remove($name)
    {
        Constants::assertValidConstantName($name);

        if (isset(Constants::$constants[$name])) {
            unset(Constants::$constants[$name]);
        }
    }

    /**
     * Asserts that the name of the constant is a non-numeric string.
     * @param string $name Constant name
     */
    public static function assertValidConstantName($name)
    {
        if (!is_string($name) || is_numeric($name)) {
            throw new InvalidArgumentException('Invalid constant name');
        }
    }
}
