<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\types;

use InvalidArgumentException;
use phbrick\exceptions\InvalidFormatException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\StaticClass;

/**
 * Class Floats
 *
 * Strict float scalar handling.
 *
 * @package phbrick\types
 */
class Floats extends StaticClass
{

    /**
     * Tries to convert a value to a float.
     *
     * The value can be a float, an integer, an string of digits or an string that represents a floating value.
     *
     * If the parameter $throwEx is true (default) an exception is thrown if the value cannot be converted to float.
     *
     * If $throwEx is false and the value cannot be converted it returns the parameter $defaultValue that defaults to null.
     *
     * @param float|int|string $value
     * @param bool $throwEx
     * @param mixed $defaultValue
     * @return float
     * @throws InvalidArgumentException If the value can't be converted
     */
    public static function toFloat($value, $throwEx = true, $defaultValue = null)
    {

        if (Floats::isFloat($value)) {
            return (float)$value;
        }
        else if (Ints::isInt($value)) {
            return (float)((int)$value);
        }

        if ($throwEx) {
            if (is_string($value)) {
                throw new InvalidFormatException("Invalid float: $value");
            }
            else {
                throw new InvalidTypeException("float or string", Types::getTypeName($value));
            }
        }

        if ($defaultValue !== null) {
            $defaultValue = Floats::castFloat($defaultValue);
        }

        return (float)$defaultValue;
    }

    /**
     * Returns an string representation of a float value.
     *
     * @param float|int|string $value
     * @return string
     */
    public static function toString($value)
    {
        $value = Floats::castFloat($value);
        return sprintf("%f", $value);
    }

    /**
     * Checks if the string is a float
     *
     * Only floats and strings that can represent a floating value return true
     *
     * Strings with alphabetic chars that PHP's floatval function can convert to floats ignoring the non-digits
     * will not be considered valid floats.
     *
     * @param float|int|string $string
     * @return bool
     */
    public static function isFloat($string)
    {
        return is_float($string) || (is_string($string) && !ctype_digit($string) && is_numeric($string));
    }

    /**
     * Asserts that a given value is an float or an string of digits
     *
     * @param float|int|string $string
     */
    public static function assertFloat($string)
    {
        if (!Floats::isFloat($string)) {
            if (is_string($string)) {
                throw new InvalidFormatException("Invalid float: $string");
            }
            else {
                throw new InvalidTypeException("float or string", Types::getTypeName($string));
            }
        }
    }

    /**
     * Converts an float or string of digits to an float.
     *
     * @param float|int|string $string
     * @return float
     */
    public static function castFloat($string)
    {
        Floats::assertFloat($string);
        return Floats::toFloat($string);
    }
}
