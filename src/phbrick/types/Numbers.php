<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use InvalidArgumentException;
use phbrick\exceptions\IllegalStateException;
use phbrick\exceptions\InvalidFormatException;
use phbrick\exceptions\InvalidTypeException;

/**
 * Class Numbers
 * @package phbrick\types
 */
class Numbers
{
    /**
     * Tries to convert a value to an integer.
     *
     * The value can be an integer, a float, an string of digits or an string that represents a floating value.
     *
     * If the parameter $throwEx is true an exception is thrown if the value cannot be converted
     * to integer. If false and the value cannot be converted it returns the parameter $defaultValue.
     *
     * @param int|float|string $value
     * @param bool $throwEx
     * @param int|float|string $defaultValue
     * @return int|float
     * @throws InvalidArgumentException If the value can't be converted
     */
    public static function toNumber($value, $throwEx = true, $defaultValue = null)
    {

        if (Ints::isInt($value)) {
            return (int)$value;
        }
        else if (Floats::isFloat($value)) {
            // Decimal portion is lot
            return (float)$value;
        }

        if ($throwEx) {
            if (is_string($value)) {
                throw new InvalidFormatException("Invalid number: " . $value);
            }
            else {
                throw new InvalidTypeException("integer, float or string", Types::getTypeName($value));
            }
        }

        if ($defaultValue !== null) {
            if (Ints::isInt($defaultValue)) {
                $defaultValue = Ints::castInt($defaultValue);
            }
            else if (Floats::isFloat($defaultValue)) {
                $defaultValue = Floats::castFloat($defaultValue);
            }
            else {
                throw new InvalidFormatException("Invalid number: " . $defaultValue);
            }
        }

        return $defaultValue;
    }

    /**
     * Returns an string representation of an integer value.
     *
     * @param int|float|string $value
     * @return string
     * @throws IllegalStateException If the value is not convertible to string
     */
    public static function toString($value)
    {
        Numbers::assertNumber($value);

        if (Ints::isInt($value)) {
            return sprintf("%d", Ints::toInt($value));
        }
        else if (Floats::isFloat($value)) {
            return sprintf("%f", Floats::toFloat($value));
        }
        else {
            throw new IllegalStateException("Input value is neither a float or an integer but is numeric: $value");
        }
    }

    /**
     * Checks if the string is an integer
     *
     * Only integers and strings made of digits are considered integers.
     *
     * Strings that contains non-digits are not considered integers even if PHP's intval function can convert them to
     * integers ignoring the non-digits.
     *
     * @param int|float|string $string
     * @return bool
     */
    public static function isNumber($string)
    {
        return is_numeric($string) && (Ints::isInt($string) || Floats::isFloat($string));
    }

    /**
     * Asserts that a given value is an integer, a float or a numeric string.
     *
     * @param int|float|string $string
     * @throws InvalidTypeException If the parameter is not a type that can be converted to an integer.
     * @throws InvalidFormatException If the parameter is an string and is not a valid integer
     */
    public static function assertNumber($string)
    {
        if (!Numbers::isNumber($string)) {
            if (is_string($string)) {
                throw new InvalidFormatException("Invalid integer or float: " . $string);
            }
            else {
                throw new InvalidTypeException("integer, float or string", Types::getTypeName($string));
            }
        }
    }

    /**
     * Converts an integer, float or string of digits to an integer or float.
     *
     * @param int|float|string $string
     * @return int|float
     */
    public static function castNumber($string)
    {
        Numbers::assertNumber($string);
        return Numbers::toNumber($string);
    }

    /**
     * Converts an integer, float or string of digits to a float.
     *
     * @param $string
     * @return float
     */
    public static function castFloat($string)
    {
        return (float)Numbers::castNumber($string);
    }

    /**
     * Converts an integer, float or string of digits to an integer.
     *
     * @param $string
     * @return int
     */
    public static function castInt($string)
    {
        return (int)Numbers::castNumber($string);
    }
}
