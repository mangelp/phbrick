<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\types;

use phbrick\BaseStrictClass;

/**
 * Wraps finfo_* functions into an object.
 */
class MimeInfo extends BaseStrictClass
{
    const MIME_TYPE = 'type';
    const MIME_ENCODING = 'encoding';

    private $fileInfo = null;

    public function __construct()
    {

    }

    public function __destruct()
    {
        if ($this->fileInfo !== null) {
            finfo_close($this->fileInfo);
            $this->fileInfo = null;
        }
    }

    protected function getFileInfoHandler()
    {
        $this->ensureHandler();

        return $this->fileInfo;
    }

    protected function ensureHandler()
    {
        if ($this->fileInfo === null) {
            $this->fileInfo = finfo_open(FILEINFO_SYMLINK | FILEINFO_PRESERVE_ATIME | FILEINFO_MIME);
        }
    }

    /**
     *
     * @param string $mimeString
     * @return array
     */
    protected function processMimeString($mimeString)
    {
        $result = [];
        $mimeParts = explode(';', $mimeString);

        $result[self::MIME_TYPE] = trim($mimeParts[0]);

        $parts = explode('=', $mimeParts[1]);
        $result[self::MIME_ENCODING] = trim($parts[1]);

        return $result;
    }

    /**
     * Get MIME information from a file
     *
     * The returned array format is:
     *
     * array(
     *   'type' => 'MIME type',
     *   'encoding' => 'MIME encoding',
     * );
     *
     * @param string $file File path
     * @return array with the MIME 'type' and 'encoding' of the file as map keys.
     */
    public function getFileMime($file)
    {
        $mimeString = finfo_file($this->getFileInfoHandler(), "$file");

        return $this->processMimeString($mimeString);
    }

    public function getFileMimeType($file)
    {
        return finfo_file($this->getFileInfoHandler(), "$file", FILEINFO_MIME_TYPE);
    }

    public function getFileMimeEncoding($file)
    {
        return finfo_file($this->getFileInfoHandler(), "$file", FILEINFO_MIME_ENCODING);
    }

    public function getDataMime($data)
    {
        $mimeString = finfo_buffer($this->getFileInfoHandler(), $data);

        return $this->processMimeString($mimeString);
    }

    public function getDataMimeType($data)
    {
        return finfo_buffer($this->getFileInfoHandler(), $data, FILEINFO_MIME_TYPE);
    }

    public function getDataMimeEncoding($data)
    {
        return finfo_buffer($this->getFileInfoHandler(), $data, FILEINFO_MIME_ENCODING);
    }
}
