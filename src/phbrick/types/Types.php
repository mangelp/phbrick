<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\types;

use phbrick\exceptions\InvalidTypeException;
use phbrick\exceptions\NotIterableException;
use phbrick\exceptions\TypeException;
use phbrick\ICloneable;
use phbrick\StaticClass;
use phbrick\string\Strings;

class Types extends StaticClass
{

    const TYPE_ARRAY = 'array';
    const TYPE_FLOAT = 'float';
    const TYPE_INT = 'int';
    const TYPE_OBJECT = 'object';
    const TYPE_RESOURCE = 'resource';
    const TYPE_STRING = 'string';
    const TYPE_META_SCALAR = 'scalar';
    const TYPE_META_NUMERIC = 'numeric';
    const TYPE_META_ITERABLE = 'iterable';
    const TYPE_META_MIXED = 'mixed';

    /**
     * Gets the type name or class name
     *
     * @param mixed $var
     * @return string
     */
    public static function getTypeName($var)
    {
        $type = gettype($var);

        if ($type == 'object') {
            $type = 'object::' . get_class($var) . '';
        }

        return $type;
    }

    /**
     * Asserts that the parameter is iterable through a foreach.
     *
     * If the optional parameter $type is given then it will also check that if the iterable is not empty at least the
     * first item contained matches the given type.
     *
     * @param mixed $iterable
     * @param string $type One of the type constants or a fully qualified class name, trait or interface.
     * @throws NotIterableException if not iterable
     * @throws TypeException if the type specified is not null and the first iterable item does not match that type
     */
    public static function assertIterable($iterable, $type = null)
    {
        if (!Types::isIterable($iterable, null)) {
            throw new NotIterableException('Not iterable: ' . Types::getTypeName($iterable));
        }

        if ($type !== null && !Types::isIterable($iterable, $type)) {
            throw new TypeException("Not iterable of type $type");
        }
    }

    /**
     * Gets if the parameter is an array or if it implements \Traversable
     *
     * Also if the parameter $type is specified will return true only when at least the first iterable item matches that
     * type.
     *
     * @param mixed $iterable
     * @param null|string $type Type to check over iterable items if it is iterable
     * @return bool true if the iterable can be iterated and also if the type is null or the first item matches it.
     */
    public static function isIterable($iterable, $type = null)
    {
        if (!is_array($iterable) && !is_a($iterable, '\\Traversable')) {
            return false;
        }

        if ($type !== null) {
            foreach ($iterable as $key => $firstItem) {
                return Types::isType($type, $firstItem);
            }
        }

        return true;
    }

    /**
     * Asserts that the parameter implements ArrayAccess or is an array.
     *
     * @param mixed $array
     * @throws NotIterableException if not iterable
     */
    public static function assertArrayAccess($array)
    {
        if (!is_array($array) && !is_a($array, '\\ArrayAccess')) {
            throw new NotIterableException('Not iterable: ' . Types::getTypeName($array));
        }
    }

    /**
     * Gets if the parameter implements ArrayAccess or is an array.
     *
     * @param mixed $array
     * @return boolean
     */
    public static function isArrayAccess($array)
    {
        return is_array($array)
            || is_a($array, '\\ArrayAccess');
    }

    /**
     * Gets if the string is a numeric integer
     *
     * @param $string
     * @return bool
     */
    public static function isNumeric($string)
    {
        return Numbers::isNumber($string);
    }

    /**
     * Throws an exception if the parameter is not numeric
     *
     * @param $string
     */
    public static function assertNumeric($string)
    {
        Numbers::assertNumber($string);
    }

    /**
     * Casts a numeric value to integer or float or throws an exception if it can be cast
     *
     * @param $string
     * @return float|int
     */
    public static function castNumeric($string)
    {
        return Numbers::castNumber($string);
    }

    /**
     * Gets if the given value matches the given type.
     *
     * Type matching includes simple type conversion, so an scalar will always be of type string (is always convertible)
     * and an string with only digits will be a valid integer.
     *
     * @param string $type Type to check over the $value. One of the TYPE_* constants in Types class or a fully qualified
     * class, interface or trait name.
     * @param mixed $value
     * @return bool True if it is the given type or false if not.
     * @throws TypeException if the given type is an unknown type.
     */
    public static function isType($type, $value)
    {
        Strings::assertString($type);

        switch (strtolower($type)) {
            case Types::TYPE_ARRAY:
                return is_array($value);
            case Types::TYPE_FLOAT:
                return Floats::isFloat($value);
            case Types::TYPE_INT:
                return Ints::isInt($value);
            case Types::TYPE_OBJECT:
                return is_object($value);
            case Types::TYPE_RESOURCE:
                return is_resource($value);
            case Types::TYPE_STRING:
                return Strings::isString($value);
            case Types::TYPE_META_SCALAR:
                return is_scalar($value);
            case Types::TYPE_META_NUMERIC:
                return (Ints::isInt($value) || Floats::isFloat($value));
            case Types::TYPE_META_ITERABLE:
                return Types::isIterable($value);
            case Types::TYPE_META_MIXED:
                return true;
        }

        // The type was none of the above, then it might be a class, interface or trait.
        $isName = preg_match("/[a-zA-Z0-9_\\\\]+/", $type);
        $isClass = $isName && class_exists($type);
        $isInterface = $isName && !$isClass && interface_exists($type);
        $isTrait = $isName && !$isClass && !$isInterface && trait_exists($type);

        if (!$isName || (!$isClass && !$isInterface && !$isTrait)) {
            throw new TypeException("Unknown type $type");
        }

        if (!is_object($value)) {
            return false;
        }
        else if ($isClass || $isInterface) {
            return is_a($value, $type);
        }
        else if ($isTrait) {
            $traits = class_uses($value);
            return in_array($type, $traits);
        }

        return false;
    }

    /**
     * Performs a generic equality comparison between two values of an explicitly provided type and returns if the result
     * of that comparison.
     *
     * @param mixed $a First value to compare
     * @param mixed $b Second value to compare
     * @param string $type Type of the values. Providing the wrong type will yield an
     * @param bool $caseSensitive If true string comparisons are done in a case-sensitive way.
     * @return bool
     * @throws TypeException
     */
    public static function equals($a, $b, $type, $caseSensitive = true)
    {
        if (!Types::isType($type, $a) || !Types::isType($type, $b)) {
            return false;
        }

        switch (strtolower($type)) {
            case Types::TYPE_ARRAY:
                return $a == $b;
            case Types::TYPE_FLOAT:
                $a = Floats::castFloat($a);
                $b = Floats::castFloat($b);
                return $a == $b;
            case Types::TYPE_INT:
                $a = Ints::castInt($a);
                $b = Ints::castInt($b);
                return $a == $b;
            case Types::TYPE_OBJECT:
                return $a == $b;
            case Types::TYPE_RESOURCE:
                return $a == $b;
            case Types::TYPE_STRING:
                $a = Strings::castString($a);
                $b = Strings::castString($b);
                return ($caseSensitive && strcmp($a, $b) == 0)
                    || (!$caseSensitive && strcasecmp($a, $b) == 0);
            case Types::TYPE_META_SCALAR:
                return $a == $b;
            case Types::TYPE_META_NUMERIC:
                return $a == $b;
            case Types::TYPE_META_ITERABLE:
                return $a == $b;
            case Types::TYPE_META_MIXED:
                return $a == $b;
        }

        // The type was none of the above, then it might be a class, interface or trait.
        $isName = preg_match("/[a-zA-Z0-9_\\\\]+/", $type);
        $isClass = $isName && class_exists($type);
        $isInterface = $isName && !$isClass && interface_exists($type);
        $isTrait = $isName && !$isClass && !$isInterface && trait_exists($type);

        if (!$isName || (!$isClass && !$isInterface && !$isTrait)) {
            throw new TypeException("Unknown type $type");
        }

        if (!Types::isType($type, $a)) {
            return false;
        }

        return $a == $b;
    }

    /**
     * checks if an object either implements ICloneable or if it implements __clone method.
     *
     * @param $object
     * @return bool
     */
    public static function isCloneable($object)
    {
        return is_object($object) && ($object instanceof ICloneable || method_exists($object, '__clone'));
    }

    /**
     * Throws an exception if the argument is not cloneable as Types::isCloneable.
     *
     * @param $object
     * @throws InvalidTypeException if Types::isCloneable returns false for this argument
     */
    public static function assertCloneable($object)
    {
        if (!Types::isCloneable($object)) {
            throw new InvalidTypeException("cloneable", Types::getTypeName($object));
        }
    }
}
