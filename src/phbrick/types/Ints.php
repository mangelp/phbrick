<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\types;

use InvalidArgumentException;
use phbrick\exceptions\InvalidFormatException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\StaticClass;
use phbrick\string\Strings;

/**
 * Class Ints
 *
 * Strict integer scalar handling.
 *
 * @package phbrick\types
 */
class Ints extends StaticClass
{

    private static $byteCount = 0;

    /**
     * Gets the number of bytes used to hold integers in current platform
     *
     * @return float|int
     */
    public static function getByteCount()
    {
        if (self::$byteCount > 0) {
            return self::$byteCount;
        }

        self::$byteCount = 1 + (log(PHP_INT_MAX) / log(2));

        return self::$byteCount;
    }

    /**
     * Returns the integer in HEX format
     * @param $integer
     */
    public static function toHex($integer, $prependHash = false)
    {
        $int = self::castInt($integer);

        $hex = sprintf("%x", $int);

        if (strlen($hex) % 2 !== 0) {
            $hex = '0' . $hex;
        }

        if ($prependHash) {
            $hex = '#' . $hex;
        }

        return $hex;
    }

    /**
     * Returns an array of integers that represent the bytes of the input integer
     *
     * @param $integer
     */
    public static function toBytes($integer)
    {
        $int = self::castInt($integer);
        $hex = self::toHex($integer, false);
        $bytes = Strings::chunk($hex, 2);
        return array_map(function($hex) {
            return hexdec($hex);
        }, $bytes);
    }

    /**
     * Tries to convert a value to an integer.
     *
     * The value can be an integer, a float, an string of digits or an string that represents a floating value.
     *
     * If the parameter $throwEx is true an exception is thrown if the value cannot be converted
     * to integer. If false and the value cannot be converted it returns the parameter $defaultValue.
     *
     * @param int|float|string $value
     * @param bool $throwEx
     * @param mixed $defaultValue
     * @return int
     * @throws InvalidArgumentException If the value can't be converted
     */
    public static function toInt($value, $throwEx = true, $defaultValue = null)
    {

        if (Ints::isInt($value)) {
            return (int)$value;
        }
        else if (Floats::isFloat($value)) {
            // Decimal portion is lot
            return (int)((float)$value);
        }

        if ($throwEx) {
            if (is_string($value)) {
                throw new InvalidFormatException("Invalid integer: " . $value);
            }
            else {
                throw new InvalidTypeException("integer or string", Types::getTypeName($value));
            }
        }

        if ($defaultValue !== null) {
            $defaultValue = Ints::castInt($defaultValue);
        }

        return (int)$defaultValue;
    }

    /**
     * Returns an string representation of an integer value.
     *
     * @param int|float|string $value
     * @return string
     */
    public static function toString($value)
    {
        $value = Ints::castInt($value);
        return sprintf("%d", $value);
    }

    /**
     * Checks if the string is an integer
     *
     * Only integers and strings made of digits are considered integers.
     *
     * Strings that contains non-digits are not considered integers even if PHP's intval function can convert them to
     * integers ignoring the non-digits.
     *
     * @param int|float|string $string
     * @return bool
     */
    public static function isInt($string)
    {
        return is_int($string) || (is_string($string) && ctype_digit($string));
    }

    /**
     * Asserts that a given value is an integer or an string of digits
     *
     * @param int|float|string $string
     * @throws InvalidTypeException If the parameter is not a type that can be converted to an integer.
     * @throws InvalidFormatException If the parameter is an string and is not a valid integer
     */
    public static function assertInt($string)
    {
        if (!Ints::isInt($string)) {
            if (is_string($string)) {
                throw new InvalidFormatException("Invalid integer: " . $string);
            }
            else {
                throw new InvalidTypeException("integer or string", Types::getTypeName($string));
            }
        }
    }

    /**
     * Converts an integer or string of digits to an integer.
     *
     * @param int|float|string $string
     * @return int
     */
    public static function castInt($string)
    {
        Ints::assertInt($string);
        return Ints::toInt($string);
    }

    public static function inRange($value, $min, $max)
    {
        $value = Ints::castInt($value);
        $min = Ints::castInt($min);
        $max = Ints::castInt($max);

        return $value >= $min || $value <= $max;
    }

    public static function assertRange($value, $min, $max)
    {
        $value = Ints::castInt($value);

        if (!Ints::inRange($value, $min, $max)) {
            throw new InvalidArgumentException("Value $value is not in range [$min, $max]");
        }

        return $value;
    }
}
