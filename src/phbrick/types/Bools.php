<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use phbrick\exceptions\InvalidFormatException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\StaticClass;

/**
 * Static class to handle conversion between different ways of expressing values
 * that work as booleans.
 */
final class Bools extends StaticClass
{

    /**
     * Values considered to be true
     * @var array
     */
    const TRUE_VALUES = [true, 'true', 1, '1'];

    /**
     * Values considered to be false
     * @var array
     */
    const FALSE_VALUES = [false, 'false', 0, '0'];

    /**
     * Checks if the given value is boolean true
     * @param string|int|bool|array $value
     * @return bool
     */
    public static function isTrue($value)
    {
        if (is_bool($value) && $value) {
            return true;
        }
        else if (!is_scalar($value)) {
            return false;
        }

        if (is_string($value)) {
            $value = strtolower($value);
        }

        $result = in_array($value, Bools::TRUE_VALUES, true);
        return $result;
    }

    /**
     * Checks if the given value is boolean false
     * @param string|int|bool|array $value
     * @return bool
     */
    public static function isFalse($value)
    {
        if (is_bool($value) && !$value) {
            return true;
        }
        else if (!is_scalar($value) || is_array($value)) {
            return false;
        }

        if (is_string($value)) {
            $value = strtolower($value);
        }

        $result = in_array($value, Bools::FALSE_VALUES, true);
        return $result;
    }

    /**
     * Tries to convert a value to a boolean.
     *
     * If the parameter $throwEx is true an exception is thrown if the value cannot be converted
     * to boolean. If false and the value cannot be converted it returns the parameter
     * $defaultValue.
     *
     * @param string|int|bool|array $value
     * @param bool $throwEx
     * @param null $defaultValue
     * @return bool
     */
    public static function toBool($value, $throwEx = true, $defaultValue = null)
    {
        if (is_bool($value)) {
            return $value;
        }
        else if (Bools::isTrue($value)) {
            return true;
        }
        else if (Bools::isFalse($value)) {
            return false;
        }

        if ($throwEx) {
            if (is_string($value)) {
                throw new InvalidFormatException("Invalid boolean: " . $value);
            }
            else {
                throw new InvalidTypeException("integer, boolean or string", Types::getTypeName($value));
            }
        }

        if ($defaultValue !== null) {
            $defaultValue = Bools::castBool($defaultValue);
        }

        return (bool)$defaultValue;
    }

    /**
     * Returns an string representation of a boolean value.
     *
     * @param bool $value
     * @param bool $literal If true will return literal names (true, false), if not will return '1' for true and '0'
     * for false.
     * @return string
     */
    public static function toString($value, $literal = true)
    {
        $value = Bools::castBool($value);

        if ($literal) {
            return $value ? 'true' : 'false';
        }
        else {
            return sprintf("%d", (int)$value);
        }
    }

    /**
     * Checks if the string is a boolean
     *
     * Only booleans and strings that
     *
     * Strings that contains non-digits are not considered booleans even if PHP's boolval function can convert them to
     * booleans ignoring the non-digits.
     *
     * @param bool|int|string $string
     * @return bool
     */
    public static function isBool($string)
    {
        return is_bool($string)
            || (is_scalar($string) && !is_array($string)
                && (Bools::isTrue($string) || Bools::isFalse($string)));
    }

    /**
     * Asserts that a given value is a boolean or an string that can be converted to true/false value
     *
     * @param bool|int|string $string
     */
    public static function assertBool($string)
    {
        if (!Bools::isBool($string)) {
            if (is_string($string)) {
                throw new InvalidFormatException("Invalid boolean: " . $string);
            }
            else {
                throw new InvalidTypeException("boolean or string", Types::getTypeName($string));
            }
        }
    }

    /**
     * Converts a boolean or literal string to boolean
     *
     * @param bool|int|string $string
     * @return bool
     */
    public static function castBool($string)
    {
        Bools::assertBool($string);
        return Bools::toBool($string);
    }
}
