<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use ArrayAccess;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;

/**
 * Mutable multibyte string interface.
 *
 * All methods are fluent and they must return the same instance they are called on when the method
 * does not have an explicit return type.
 *
 * Methods must support negative index positions or positions strings (first, last, middle, ...)
 * apart from positive values. Those indexes are converted to proper indexes before being used.
 *
 * The method IMutableUString::calculateRealIndex performs all the calculations and exception
 * throwing for out-of-bound indexes (after calculation of the real access index).
 *
 * @example
 * // Assume we have an implementation
 * $str = new MutableUStringImpl("My ñ in string", 'UTF-8');
 *
 * $str->toLower()
 *      ->trimEnd("abcdefg")
 *      ->lcFirst()->append("!")
 *      ->prepend("¡")
 *      ->replace(' in ', ' en ');
 *
 * Final value: "¡my ñ en strin!";
 *
 *
 */
interface IMutableUString extends IteratorAggregate, ArrayAccess, Countable
{
    /**
     * Gets the string encoding
     * @return string
     */
    public function getEncoding();

    /**
     * Replaces the string value with the given one
     * @param string $string
     * @return self
     */
    public function setValue($string);

    /**
     * Gets the current string value set.
     * @return string
     */
    public function getValue();

    /**
     * Gets the stored string length
     * @return number
     */
    public function getLength();

    /**
     * Checks if this string is empty
     * @return self
     */
    public function isEmpty();

    /**
     * Splits the current string into an array of strings and returns it.
     *
     * @param string $separator String separator or regex expression.
     * @param bool $notEmpty If true empty elements are removed from the result
     * @return array<IMutableUString>
     */
    public function split($separator, $notEmpty = true);

    /**
     * Replaces an string or regex by a replacement string.
     *
     * @param string $string String or regular expression (set $isRegex to true).
     * @param string $replacement
     * @param bool $isRegex If true the string to search is a regular expression
     * @return self
     */
    public function replace($string, $replacement, $isRegex = false);

    /**
     * Given an index that might be negative or an string returns the index it will try to use.
     *
     * This method also can call the assertion to avoid out of bound access, causing an exception to
     * be thrown in case the index is out of bounds.
     *
     * Negative indexes are converted into positive indexes with the next formula:
     *    $realIndex = $length + $index;
     *
     * So the index of the last string character would be -1 and -$length (given the $length of the
     * string) would be the first character at position 0.
     *
     * Also is supported special strings that are converted into the given positions:
     *  + 'first': 0.
     *  + 'last': $length - 1
     *  + 'mid': $length / 2
     *  + 'before-last': $length - 2
     *  + 'after-first': 1
     *  ' 'after-last': $length
     *
     * @param string|int $index
     * @param bool $forInsertion True if the length of the string is a valid position or false
     * if not (useful for inserting at the end of the array).
     * @param bool $assert
     * @return int|null The accessed index integer or null if the index cannot be calculated.
     * @throws InvalidArgumentException When $assert is true and the assertion fails.
     */
    public function calculateRealIndex($index, $forInsertion = false, $assert = true);

    /**
     * Inserts an string at a given position.
     *
     * Is an alias to replace setting the length of the string to be removed to 0.
     *
     * @param $index
     * @param string $string
     * @return IMutableUString
     * @internal param int $pos
     */
    public function insertAt($index, $string);

    /**
     * Replaces a portion of the string with the given replacement.
     *
     * @param int $index Cannot be negative or greater than the string.
     * @param int $length Number of characters to remove.
     * @param string $replacement
     * @return self
     */
    public function replaceAt($index, $length, $replacement);

    /**
     * Removes a portion of the string.
     *
     * @param int $index
     * @param int $length
     * @return self
     */
    public function removeAt($index, $length);

    /**
     * Reduces the string to the specified portion.
     *
     * Internally it gets the substring with the given parameters and replaces the string value with
     * that result.
     *
     * @param int $index
     * @param int $length
     * @return self
     */
    public function reduceTo($index, $length = null);

    /**
     * Gets the character (code point) at the given position as a PHP string.
     *
     * @param int $index
     * @return string
     */
    public function getAt($index);

    /**
     * Sets the character (code point) at the given position.
     *
     * The parameter $str can be an string with more than one char, but only the first character is used.
     *
     * @param int $index
     * @param string $str String whose first character will be inserted into the given position.
     * @return self
     */
    public function setAt($index, $str);

    /**
     * Gets the position of the given string or false if not
     *
     * @param string $str String to search
     * @param int $offset Position to start the search
     * @param bool $caseSensitive If true the search will be case sensitive, if false it will be
     * case insensitive.
     * @return int|bool The position of the string or false if it is not found.
     */
    public function indexOf($str, $offset = 0, $caseSensitive = true);

    /**
     * Lowercases the string
     *
     * @return self
     */
    public function toLower();

    /**
     * Uppercase the string and returns this instance itself.
     *
     * @return self
     */
    public function toUpper();

    /**
     * Uppercase the first character and returns this instance itself.
     * @return self
     */
    public function ucFirst();

    /**
     * Lowercases the last character
     * @return self
     */
    public function lcFirst();

    /**
     * Gets if the given string leads the current value
     *
     * @param string $string
     * @return bool
     */
    public function startsWith($string);

    /**
     * Gets if the given string trails the current value
     * @param string $string
     * @return bool
     */
    public function endsWith($string);

    /**
     * Trims the given chars from the start and end of the value.
     *
     * @param string $trimChars
     * @return self
     */
    public function trim($trimChars = null);

    /**
     * Trims the given chars from the start of the value
     * @param string $trimChars
     * @return self
     */
    public function trimStart($trimChars = null);

    /**
     * Trims the given chars from the end of the value
     * @param string $trimChars
     * @return self
     */
    public function trimEnd($trimChars = null);

    /**
     * Appends the given string to the current one
     * @param string $string
     * @return self
     */
    public function append($string);

    /**
     * Prepends the given string to the current one
     * @param string $string
     * @return self
     */
    public function prepend($string);

    /**
     * Empties the string
     * @return self
     */
    public function clear();

    /**
     * Returns the string as an array of code points (strings with a single multibyte character).
     *
     * @return array<string>
     */
    public function toArray();

    /**
     * Gets an string bundler with the current string as content.
     *
     * Using the parameter $splitString the bundler will be initialized with the results of the
     * split call, thus returning a bundler with one or more strings.
     *
     * @param $splitString string If set splits the string before passing it to the string bundler
     * instance. Splitting is done calling self::split with the input $splitString parameter.
     * @return StringBundler
     */
    public function toStringBundler($splitString = null);

    /**
     * Returns the current string contents as a PHP string
     * @param null $context
     * @param null $indent
     * @return string
     */
    public function toString($context = null, $indent = null);
}
