<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

/**
 * Interface with a method that returns a non-array scalar or an object that implements __toString
 * method.
 *
 * For example phbrick\Markup\Tag class is a renderable that provides a toString method and is used
 * by other classes that implement this contract to return generated content in the form of objects
 * that might be converted to strings later.
 */
interface IRenderable
{
    /**
     * Generates a non-array scalar or an object that implements __toString.
     *
     * @param mixed $context Optional context for the render method. Usually is empty as its use
     * is specific for each implementation. For example app\View class uses it to pass additional
     * variables to the template being rendered.
     */
    public function render($context = null);
}
