<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\string;

use InvalidArgumentException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\StaticClass;
use phbrick\types\Ints;
use phbrick\types\Types;

class Strings extends StaticClass
{

    /**
     * New line character.
     * @var string
     */
    const LINE_ENDING_LF = "\n";

    /**
     * Carriage return character.
     * @var string
     */
    const LINE_ENDING_CR = "\r";
    /**
     * Carriage return and new line characters.
     * @var string
     */
    const LINE_ENDING_CRLF = "\r\n";

    const CHARS_ARIT = '+-/*%=^';
    const CHARS_ALPHA_LOWER = 'abcdefghijklmnopqrstuvwxyz';
    const CHARS_ALPHA_UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const CHARS_EXTRA = '$%&#@';
    const CHARS_NUM = '0123456789';
    const CHARS_PUNCT = ',.;:?!';
    const CHARS_SEP = '(){}<>[]|\\_';

    const CHARSET_ARIT = 2;
    const CHARSET_ALPHA_LOWER = 4;
    const CHARSET_ALPHA_UPPER = 8;
    const CHARSET_EXTRA = 16;
    const CHARSET_NUM = 32;
    const CHARSET_PUNCT = 64;
    const CHARSET_SEP = 128;
    const CHARSET_ALPHA = 256;
    /**
     * Alphanumeric
     * @var int
     */
    const CHARSET_ALNUM = 512;

    /**
     * Map between each charset and the chars it includes
     * @var array
     */
    private static $charsets = [
        self::CHARSET_ARIT        => self::CHARS_ARIT,
        self::CHARSET_ALPHA_LOWER => self::CHARS_ALPHA_LOWER,
        self::CHARSET_ALPHA_UPPER => self::CHARS_ALPHA_UPPER,
        self::CHARSET_EXTRA       => self::CHARS_EXTRA,
        self::CHARSET_NUM         => self::CHARS_NUM,
        self::CHARSET_PUNCT       => self::CHARS_PUNCT,
        self::CHARSET_SEP         => self::CHARS_SEP,
        self::CHARSET_ALNUM       => self::CHARS_ALPHA_LOWER . self::CHARS_ALPHA_UPPER . self::CHARS_NUM,
    ];

    /**
     * Gets an array with all charsets where the key is the charset constant integer (2 pow for flag use) and the values
     * are the charset characters as an string.
     * @return array<int, string>
     */
    public static function getCharsets()
    {
        return self::$charsets;
    }

    /**
     * Gets an string with all the characters for the given charset flags.
     *
     * If the parameter $charset is a non numeric string it is returned as-is.
     *
     * The parameter $forRegex escapes the non alphanumeric characters when set to true to allow usage in regex expressions.
     *
     * @param int|string $charsets
     * @param bool $forRegex
     * @return string
     */
    public static function getCharsetString($charsets, $forRegex = false)
    {
        $result = '';

        if (is_string($charsets) && !ctype_digit($charsets)) {
            return $charsets;
        }
        else if (is_string($charsets) || is_int($charsets) || is_float($charsets)) {
            $charsets = (int)$charsets;
        }
        else {
            throw new InvalidTypeException("integer or string", Types::getTypeName($charsets));
        }

        foreach (self::$charsets as $flag => $str) {
            if (($flag & $charsets) != $flag) {
                continue;
            }

            if (!$forRegex
                || $flag == self::CHARSET_ALPHA_LOWER
                || $flag == self::CHARSET_ALPHA_UPPER
                || $flag == self::CHARSET_NUM) {

                $result .= $str;
            }
            else {
                $parts = str_split($str);
                $result .= '\\' . implode('\\', $parts);
                unset($parts);
            }
        }

        return $result;
    }

    /**
     * Generates one or more random strings using the specified charset.
     *
     * @param int $size Size of each string
     * @param string|int $charset Charset string to use or flags for the predefined charsets. If not set defaults to
     * alpha (lower and upper) and digits.
     * to use alpha(lower and upper) and digits.
     * @param int $repeat Number of strings to be generated.
     * @return string|array<string>
     */
    public static function random($size, $charset = null, $repeat = 1)
    {
        if (!$charset) {
            $charset = self::CHARSET_ALNUM;
        }

        $size = Ints::castInt($size);
        $repeat = Ints::castInt($repeat);

        if ($size < 1) {
            throw new InvalidArgumentException('Parameter $size must be greater than 0');
        }

        if ($repeat < 1) {
            throw new InvalidArgumentException('Parameter $repeat must be greater than 0');
        }

        $charsetStr = self::getCharsetString($charset);

        if (empty($charsetStr)) {
            throw new InvalidArgumentException('No charset provided');
        }

        $result = [];
        $maxCharPos = strlen($charsetStr) - 1;

        for ($i = 0; $i < $repeat; ++$i) {
            // Create an empty string with the desired size
            $result[$i] = str_repeat(' ', $size);

            for ($j = 0; $j < $size; ++$j) {
                $pos = mt_rand(0, $maxCharPos);
                $result[$i][$j] = $charsetStr[$pos];
            }
        }

        if ($repeat == 1) {
            return $result[0];
        }
        else {
            return $result;
        }
    }

    /**
     * Generates an string of the given size using a predictable order.
     *
     * The generated
     *
     * @param int $size Size of each string
     * @param string|int $charset Charset string to use or flags for the predefined charsets. If not set defaults to
     * alpha (lower and upper) and digits.
     * to use alpha(lower and upper) and digits.
     * @param int $order Order of the sequence item to generate
     * @param int $repeat Number of consecutive sequence strings to be generated.
     * @return string|array<string>
     */
    public static function sequence($size, $order, $charset = null, $repeat = 1)
    {
        if (!$charset) {
            $charset = self::CHARSET_ALNUM;
        }

        $size = Ints::castInt($size);
        $order = Ints::castInt($order);
        $repeat = Ints::castInt($repeat);

        if ($size < 1) {
            throw new InvalidArgumentException('Parameter $size must be greater than 0');
        }

        if ($repeat < 1) {
            throw new InvalidArgumentException('Parameter $repeat must be greater than 0');
        }

        $charsetStr = self::getCharsetString($charset);

        if (empty($charsetStr)) {
            throw new InvalidArgumentException('No charset provided');
        }

        $charsetLength = strlen($charsetStr);
        $result = [];

        for ($repetitionIndex = 0; $repetitionIndex < $repeat; $repetitionIndex++) {

            $string = str_pad("", $size, $charsetStr[0]);

            for ($i = 0; $i < $size; $i++) {
                $length = pow($charsetLength, $i);
                $itemPos = ($order / $length) % $charsetLength;
                $string[$i] = $charset[$itemPos];
            }

            $result[] = $string;
        }

        if ($repeat == 1) {
            return $result[0];
        }
        else {
            return $result;
        }
    }

    /**
     * Gets if the string is either null or an empty string.
     * @param string $str
     * @return boolean
     */
    public static function isNullOrEmpty($str)
    {
        return $str === null || (string)$str == '';
    }

    /**
     * Gets if the string starts with the given string
     * @param string $string
     * @param string $start
     * @return boolean
     */
    public static function startsWith($string, $start)
    {
        $lenStr = strlen($string);
        $lenStart = strlen($start);

        return $lenStr >= $lenStart && substr($string, 0, $lenStart) == $start;
    }

    /**
     * Gets if the string ends with the given string
     * @param string $string
     * @param string $end
     * @return boolean
     */
    public static function endsWith($string, $end)
    {
        $lenStr = strlen($string);
        $lenEnd = strlen($end);

        return $lenStr >= $lenEnd && substr($string, -$lenEnd) == $end;
    }

    /**
     * Checks if the given character is one of the end of line characters.
     *
     * @param $char
     * @return bool
     */
    public static function isLineEndingChar($char)
    {
        return is_string($char)
            && strlen($char) == 1
            && ($char == Strings::LINE_ENDING_CR
                || $char == Strings::LINE_ENDING_CRLF
                || $char == Strings::LINE_ENDING_LF);
    }

    /**
     * Checks if the parameter is an string or an object convertible to string
     *
     * @param $string
     * @return bool
     */
    public static function isString($string)
    {
        return is_string($string) || Renderables::isStringRenderable($string);
    }

    /**
     * Asserts that the parameter is an string or an object convertible to string
     * @param $string
     */
    public static function assertString($string)
    {
        if (!Strings::isString($string)) {
            throw new InvalidTypeException("string", Types::getTypeName($string));
        }
    }

    /**
     * Casts the value to an string and throws an exception if the value is not a valid string.
     * @param $string
     * @return string
     */
    public static function castString($string)
    {
        Strings::assertString($string);
        return (string)$string;
    }

    public static function chunk($string, $length)
    {
        $length = Ints::castInt($length);

        if ($length < 0) {
            throw new InvalidArgumentException('Parameter $length cannot be less than 0');
        }

        $chunks = [];

        if ($length == 0) {
            return $chunks;
        }

        preg_match_all("/.{1,$length}/", $string, $chunks, PREG_SPLIT_NO_EMPTY);
        $chunks = $chunks[0];

        return $chunks;
    }

    public function split($string, $delimiter, $limit = null)
    {

        if (strlen($delimiter) == 0) {
            return $this->toArray();
        }

        if ($limit !== null) {
            $limit = Ints::castInt($limit);
        }

        if ($limit !== null && $limit < 1) {
            return [];
        }

        $result = preg_split("/$delimiter/", $string, $limit);

        return $result;
    }

    public function toArray($string)
    {
        return preg_split('//', $string, null, PREG_SPLIT_NO_EMPTY);
    }
}
