<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

/**
 * Charsets and encodings constants.
 */
class CharsetEncodings
{
    /**
     * English-only charset with one byte per character with printable characters defined from 32
     * to 127 (decimal).
     * @var string
     */
    const ASCII = 'ASCII';
    /**
     * UTF charset with code points encoded from 1 to 4 bytes.
     * @var string
     */
    const UTF_8 = 'UTF-8';
    /**
     * UTF charset with code points encoded from 2 to 4 bytes.
     * @var string
     */
    const UTF_16 = 'UTF-16';
}
