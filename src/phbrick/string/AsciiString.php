<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use InvalidArgumentException;

/**
 * Ascii specific class that uses plain PHP methods to do all the work.
 */
class AsciiString extends UString
{
    /**
     * Initializes the instance and sets the codePage to ASCII
     *
     * @param string $string
     * @param string $codePage IGNORED
     */
    public function __construct($string = null, $codePage = null)
    {
        if ($codePage !== null && $codePage != CharsetEncodings::ASCII) {
            throw new InvalidArgumentException('Only ASCII encoding is allowed');
        }

        parent::__construct($string, CharsetEncodings::ASCII);
    }

    protected function internalStringLength($string)
    {
        return strlen($string);
    }

    public function toUpper()
    {
        return self::instance(strtoupper($this->string));
    }

    public function toLower()
    {
        return self::instance(strtolower($this->string));
    }

    public function reverse()
    {
        $codePoints = array_reverse($this->toArray());

        return self::instance(implode('', $codePoints));
    }

    public function trim()
    {
        return self::instance(trim($this->string));
    }

    public function replace($search, $replace)
    {
        return self::instance(
            str_replace(
                (string)$search,
                (string)$replace,
                $this->string));
    }

    public function replaceSlice($text, $start = 0, $length = null)
    {

        if ($length === null) {
            $length = $this->length - $start;
        }

        return self::instance(
            substr($this->string, 0, $start)
            . (string)$text
            . substr($this->string, $start + $length));
    }

    public function contains($text)
    {
        if (strlen($text) == 0) {
            return true;
        }

        $index = strpos($this->string, (string)$text, 0);

        return $index !== false;
    }

    public function startsWith($needle)
    {
        if (strlen($needle) == 0) {
            return true;
        }

        $len = strlen((string)$needle);

        if ($len > $this->length) {
            return false;
        }
        else if ($len == $this->length) {
            return $needle == $this->string;
        }
        else {
            return strcmp(substr($this->string, 0, $len), $needle) == 0;
        }
    }

    public function endsWith($needle)
    {
        if (strlen($needle) == 0) {
            return true;
        }

        $len = strlen((string)$needle);

        if ($len > $this->length) {
            return false;
        }
        else if ($len == $this->length) {
            return $needle == $this->string;
        }
        else {

            return strcmp(substr($this->string, $this->length - $len), $needle) == 0;
        }
    }

    public function indexOf($needle, $offset = 0)
    {

        if (strlen($needle) == 0) {
            return false;
        }

        $offset = (int)$offset;

        if ($offset < 0) {
            $offset += $this->length;
        }

        return strpos($this->string, (string)$needle, $offset);
    }

    public function lastIndexOf($needle, $offset = 0)
    {

        if (strlen($needle) == 0) {
            return false;
        }

        $offset = (int)$offset;

        if ($offset < 0) {
            $offset += $this->length;
        }

        return strrpos($this->string, (string)$needle, $offset);
    }

    public function chunk($length)
    {
        if ($length < 1) {
            throw new InvalidArgumentException('Chunk length must be greater than 0');
        }

        return str_split($this->string, $length);
    }

    public function repeat($count)
    {
        $count = (int)$count;

        if ($count == 0) {
            return self::instance();
        }
        else if ($count < 0) {
            throw new InvalidArgumentException('Parameter $count cannot be less than 0');
        }

        return self::instance(str_repeat($this->string, $count));
    }

    public function pad($length, $pad = ' ', $mode = STR_PAD_RIGHT)
    {
        $length = (int)$length;

        if ($length < 0) {
            throw new InvalidArgumentException('Parameter $length cannot be less than 0');
        }

        if (empty($pad) || $length == 0 || $length <= $this->length) {
            return self::instance($this);
        }

        $result = null;

        switch ($mode) {
            case STR_PAD_RIGHT:
                // Falls to next case
            case STR_PAD_LEFT:
                // Falls to next case
            case STR_PAD_BOTH:
                $result = self::instance(str_pad($this->string, $length, $pad, $mode));
                break;
            default:
                throw new InvalidArgumentException('Invalid $mode: ' . $mode);
        }

        return $result;
    }

    public function split($delimiter, $limit = null)
    {

        if (strlen($delimiter) == 0) {
            return str_split($this->string);
        }
        else if ($limit !== null && $limit < 1) {
            return [];
        }

        $parts = explode($delimiter, $this->string);
        $result = [];

        $count = 0;
        foreach ($parts as $pos => $str) {
            if ($limit != null && $limit > 0 && $count >= $limit) {
                break;
            }

            $result[$pos] = self::instance($str);
            ++$count;
        }

        return $result;
    }

    public function charAt($index)
    {
        $this->assertInRange($index);

        return self::instance(substr($this->string, $index, 1));
    }

    public function substring($start, $length = null)
    {

        if ($length !== null) {
            return self::instance(substr($this->string, $start, $length));
        }
        else {
            return self::instance(substr($this->string, $start));
        }
    }

    public function toArray()
    {
        return str_split($this->string);
    }
}
