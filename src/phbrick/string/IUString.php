<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use IteratorAggregate;

/**
 * PHP Interface for PHP ustring module class named UString
 *
 * This interface have been copied from the one shown at ustring module github page and modified
 * to fit userland implementation.
 *
 * @see https://github.com/krakjoe/ustring
 * @property int $length The number of unicode code points (read-only)
 */
interface IUString extends IteratorAggregate
{

    /**
     * Shall return the default codePage
     * @return string
     **/
    public static function getDefaultCodePage();

    /**
     * Shall set the default codePage
     * @param $codePage
     * @return void
     */
    public static function setDefaultCodePage($codePage);

    /**
     * Returns an integer with the number of code points contained
     */
    public function getLength();

    /**
     * Shall create a UString with the given value and codePage.
     * If no codePage is provided, the default codePage will be used.
     * The default codePage is set to UTF-8 by default
     * @param null $string
     * @param null $codePage
     * @see UString::setDefaultCodePage
     */
    public function __construct($string = null, $codePage = null);

    /**
     * Shall return a new UString whose characters are converted to upper case
     * @return UString
     **/
    public function toUpper();

    /**
     * Shall return a new UString whose characters are converted to lower case
     * @return UString
     **/
    public function toLower();

    /**
     * Shall return a new UString whose characters have been reversed
     * @return UString
     **/
    public function reverse();

    /**
     * Shall return a new UString with the leading and trailing space removed
     * @return UString
     **/
    public function trim();

    /**
     * Shall return a new UString with every occurrence of $search having been replaced with $replace
     *
     * If $search is empty it will replace nothing.
     *
     * @param string|UString $search
     * @param string|UString $replace
     * @return UString
     **/
    public function replace($search, $replace);

    /**
     * Shall return a new UString with the substring starting at $start and having length $length
     * replaced by $text.
     * @param $text
     * @param int $start Defaults to 0
     * @param int|null $length If not set all characters from start to the end of the string will
     * be replaced.
     * @return UString
     */
    public function replaceSlice($text, $start = 0, $length = null);

    /**
     * Checks if $text is contained in this string
     *
     * @param string|UString $text
     * @return bool true if the text is contained in this string
     **/
    public function contains($text);

    /**
     * Checks if this string starts with $needle
     * @param $needle
     * @return bool
     */
    public function startsWith($needle);

    /**
     * Shall return positively if this ends with $needle
     * @param $needle
     * @return bool
     */
    public function endsWith($needle);

    /**
     * Shall return the index of the first occurrence of $needle, with the search starting at $offset
     * If $offset is less than 1 the offset is calculated from the end of the string as
     * $length + $offset.
     *
     * If $needle is empty returns false.
     *
     * @param $needle
     * @param int $offset
     * @return mixed
     */
    public function indexOf($needle, $offset);

    /**
     * Shall return the index of the last occurrence of $needle, with the search starting at $offset
     *
     * If $offset is less than 1 the offset is calculated from the end of the string as
     * $length + $offset.
     *
     * If $needle is empty returns false.
     *
     * @param $needle
     * @param int $offset
     * @return mixed
     */
    public function lastIndexOf($needle, $offset);

    /**
     * Shall return an array of UString containing chunks of this, each with a maximum length of $length
     * If $length is 0 returns an empty array but if is less than 0 throws an exception.
     * @param $length
     * @return UString[] if $length less than 0
     */
    public function chunk($length);

    /**
     * Shall return a new UString with the contents of this repeated $count times
     * If $count is 0 returns an empty UString but if is less than 0 throws an exception.
     * @param $count
     * @return UString if $count less than 0
     */
    public function repeat($count);

    /**
     * Shall return a new UString padded to a given length with the pad character
     *
     * If $length is 0 or less than the current string length returns a copy of the current string
     * but if $length is less than 0 throws an exception,
     *
     * @param $length
     * @param $pad string Pad string to use. Defaults to space.
     * @param int $mode int Pad mode uses the same STR_PAD_* constants as PHP pad function.
     * @return UString if $length less than 0
     */
    public function pad($length, $pad = ' ', $mode = STR_PAD_RIGHT);

    /**
     * Shall split a UString by the delimiter up to the limit, if specified, returning an array of UStrings
     * @param $delimiter
     * @param null $limit
     * @return UString[]
     */
    public function split($delimiter, $limit = null);

    /**
     * Shall return a new UString containing the single code point at the given index
     * @param $index
     * @return UString
     */
    public function charAt($index);

    /**
     * Shall return a new UString containing a substring of this, starting at $start with a maximum length of $length
     * If $length is not provided shall use UnicodeString::length() - $start to determine the length of the substring to return
     * @param $start
     * @param $length
     * @return UString
     */
    public function substring($start, $length);

    /**
     * Shall return the codePage of this
     * @return string
     **/
    public function getCodePage();
}
