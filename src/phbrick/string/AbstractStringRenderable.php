<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use phbrick\BaseStrictClass;

/**
 * Forces the inheritors to implement a toString method
 */
abstract class AbstractStringRenderable extends BaseStrictClass implements IStringRenderable
{
    use SafeStringRenderTrait;

    public abstract function toString($context = null, $indent = null);
}
