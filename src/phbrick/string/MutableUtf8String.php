<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\string;

use ArrayIterator;
use BadMethodCallException;
use InvalidArgumentException;
use OutOfBoundsException;
use phbrick\BaseStrictClass;

/**
 * Mutable UTF-8 string implementation that uses mbstring to implement the operations.
 *
 * This class uses UTF-8 encoding always and assumes that all provided strings use it too. No
 * encoding conversion is done.
 *
 */
class MutableUtf8String extends BaseStrictClass
    implements IStringAggregator, IMutableUString
{

    use SafeStringRenderTrait;

    /**
     * @var string
     */
    private $encoding = CharsetEncodings::UTF_8;

    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Sets the string encoding
     * @param string $encoding
     * @return $this
     */
    protected function setEncoding($encoding)
    {
        $this->encoding = $encoding;
        return $this;
    }

    private $value = '';

    public function setValue($string)
    {
        $this->value = $string;
        $this->updateLength();

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    private $length = 0;

    public function getLength()
    {
        return $this->length;
    }

    public function isEmpty()
    {
        return $this->length == 0;
    }

    public function __construct($value = '')
    {
        $this->value = $value;
        $this->setEncoding(CharsetEncodings::UTF_8);
        $this->updateLength();
    }

    /**
     * Asserts that the index is inside allowed bounds.
     * @param int $index
     * @param bool $forInsertion
     * @param int $rawIndex
     * @throws BadMethodCallException
     * @throws OutOfBoundsException
     */
    protected function assertIndex($index, $forInsertion = false, $rawIndex = null)
    {
        if ($index === null) {
            throw new BadMethodCallException('Cannot assert null parameter $index');
        }

        if ($index < 0
            || ($forInsertion && ($index > $this->length))
            || (!$forInsertion && ($index >= $this->length))) {

            throw new OutOfBoundsException(
                'Index out of bounds: ' . $index
                . ($rawIndex !== null ? ". Original Value: $rawIndex" : '')
                . '. Length: ' . $this->length
                . '. Insertion: ' . ($forInsertion ? 'true' : 'false'));
        }
    }

    /**
     * Updates the current string length
     */
    protected function updateLength()
    {
        $this->length = mb_strlen($this->value, $this->encoding);
    }

    /**
     * Calculates string length.
     *
     * @param string $str String to calculate the length from
     * @return int
     */
    protected function calculateLength($str)
    {
        return mb_strlen($str, $this->encoding);
    }

    public function count()
    {
        return mb_strlen($this->value, $this->encoding);
    }

    public function split($separator, $notEmpty = true)
    {

        if ($separator === null || $separator == '') {
            $result = [$this->value];
        }
        else {
            $result = mb_split($separator, $this->value);
        }

        if ($notEmpty) {
            foreach ($result as $key => $value) {
                if (empty($value)) {
                    unset($result[$key]);
                }
            }

            $result = array_values($result);
        }

        return $result;
    }

    public function replace($string, $replacement, $isRegex = false)
    {
        if ($isRegex) {
            $this->value = preg_replace("/$string/u", $replacement, $this->value);
        }
        else {
            $this->value = str_replace($string, $replacement, $this->value);
        }

        $this->updateLength();

        return $this;
    }

    public function calculateRealIndex($index, $forInsertion = false, $assert = true)
    {
        $realIndex = null;

        if (is_string($index)) {

            switch ($index) {
                case 'last':
                    $realIndex = $this->length - 1;
                    break;
                case 'first':
                    $realIndex = 0;
                    break;
                case 'mid':
                    $realIndex = (int)($this->length / 2);
                    break;
                case 'after-last':
                    $realIndex = $this->length;
                    break;
                case 'after-first':
                    $realIndex = 1;
                    break;
                case 'before-last':
                    $realIndex = $this->length - 2;
                    break;
                default:
                    throw new InvalidArgumentException(
                        'Invalid string parameter $index: "'
                        . $index . '" is not in ["first", "last", "mid", "after-last", "after-first", "before-last"].');
            }
        }
        else if (is_numeric($index)) {
            $index = (int)$index;

            if ($index < 0) {
                $realIndex = $this->length + (int)$index;
            }
            else {
                $realIndex = (int)$index;
            }
        }
        else {
            throw new InvalidArgumentException('Invalid $index parameter type: ' . gettype($index));
        }

        if ($assert) {
            $this->assertIndex($realIndex, $forInsertion, $index);
        }

        return $realIndex;
    }

    public function insertAt($index, $string)
    {
        $realIndex = $this->calculateRealIndex($index, true);

        $this->value =
            mb_substr($this->value, 0, $realIndex, $this->encoding)
            . $string
            . mb_substr($this->value, $realIndex, null, $this->encoding);

        $this->updateLength();

        return $this;
    }

    public function replaceAt($index, $length, $replacement)
    {

        $realIndex = $this->calculateRealIndex($index);

        $this->value =
            mb_substr($this->value, 0, $realIndex, $this->encoding)
            . $replacement
            . mb_substr($this->value, $realIndex + $length, null, $this->encoding);

        $this->updateLength();

        return $this;
    }

    public function removeAt($index, $length)
    {
        $realIndex = $this->calculateRealIndex($index);

        $this->value =
            mb_substr($this->value, 0, $realIndex, $this->encoding)
            . mb_substr($this->value, $realIndex + $length, null, $this->encoding);

        $this->updateLength();

        return $this;
    }

    public function reduceTo($index, $length = null)
    {
        $realIndex = $this->calculateRealIndex($index);

        $this->value = mb_substr($this->value, $realIndex, $length, $this->encoding);

        $this->updateLength();

        return $this;
    }

    public function getAt($index)
    {
        $realIndex = $this->calculateRealIndex($index);

        return mb_substr($this->value, $realIndex, 1, $this->encoding);
    }

    public function setAt($index, $str)
    {

        $realIndex = $this->calculateRealIndex($index);

        $len = $this->calculateLength($str);

        if ($len > 1) {
            $str = (string)mb_substr($str, 0, 1, $this->encoding);
        }
        else if ($len == 0) {
            throw new InvalidArgumentException("The character to be set is required");
        }

        $this->replaceAt($realIndex, 1, $str);

        return $this;
    }

    public function indexOf($str, $offset = 0, $caseSensitive = true)
    {

        if ($caseSensitive) {
            $result = mb_strpos($this->value, $str, $offset, $this->encoding);
        }
        else {
            $result = mb_stripos($this->value, $str, $offset, $this->encoding);
        }

        return $result;
    }

    public function toLower()
    {
        $this->value = mb_strtolower($this->value, $this->encoding);

        return $this;
    }

    public function toUpper()
    {
        $this->value = mb_strtoupper($this->value, $this->encoding);

        return $this;
    }

    public function ucFirst()
    {
        $this->value =
            mb_strtoupper(
                mb_substr($this->value, 0, 1, $this->encoding),
                $this->encoding)
            . mb_substr($this->value, 1, null, $this->encoding);

        return $this;
    }

    public function lcFirst()
    {
        $this->value =
            mb_strtolower(
                mb_substr($this->value, 0, 1, $this->encoding),
                $this->encoding)
            . mb_substr($this->value, 1, null, $this->encoding);

        return $this;
    }

    public function startsWith($string)
    {
        $len = mb_strlen($string, $this->encoding);

        if ($len > $this->length) {
            return false;
        }
        else if ($len == $this->length) {
            return $string == $this->value;
        }

        return strcmp(
                mb_substr($this->value, 0, $len, $this->encoding),
                $string) == 0;
    }

    public function endsWith($string)
    {
        $len = mb_strlen($string, $this->encoding);

        if ($len > $this->length) {
            return false;
        }

        return strcmp(
                mb_substr($this->value,
                    $this->length - $len,
                    null,
                    $this->encoding),
                $string) == 0;
    }

    public function trim($trimChars = null)
    {
        if ($trimChars) {
            $this->value = trim($this->value, $trimChars);
        }
        else {
            $this->value = trim($this->value);
        }

        return $this;
    }

    public function trimStart($trimChars = null)
    {
        if ($trimChars) {
            $this->value = ltrim($this->value, $trimChars);
        }
        else {
            $this->value = ltrim($this->value);
        }

        return $this;
    }

    public function trimEnd($trimChars = null)
    {
        if ($trimChars) {
            $this->value = rtrim($this->value, $trimChars);
        }
        else {
            $this->value = rtrim($this->value);
        }

        return $this;
    }

    public function append($string)
    {
        $this->value = $this->value . $string;

        $this->updateLength();

        return $this;
    }

    public function prepend($string)
    {
        $this->value = $string . $this->value;

        $this->updateLength();

        return $this;
    }

    public function clear()
    {
        $this->value = '';
        $this->length = 0;

        return $this;
    }

    public function toArray()
    {
        $result = preg_split('//u', $this->value, null, PREG_SPLIT_NO_EMPTY);

        foreach ($result as $pos => $string) {
            $result[$pos] = self::instance($string, $this->encoding);
        }

        return $result;
    }

    public function toStringBundler($splitString = null)
    {
        if ($splitString !== null) {
            return new StringBundler($this->split($splitString));
        }
        else {
            return new StringBundler($this->value);
        }
    }

    public function toString($context = null, $indent = null)
    {
        return (string)$this->value;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->toArray());
    }

    public function offsetGet($index)
    {
        return $this->getAt($index);
    }

    public function offsetSet($index, $str)
    {
        $this->setAt($index, $str);
    }

    public function offsetExists($index)
    {
        $realIndex = $this->calculateRealIndex($index, false, false);
        return is_int($realIndex) && 0 <= $realIndex && $realIndex < $this->length;
    }

    public function offsetUnset($index)
    {
        $this->removeAt($index, 1);
    }
}
