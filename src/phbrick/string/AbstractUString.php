<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use ArrayIterator;
use phbrick\BaseStrictClass;
use phbrick\exceptions\ReadOnlyException;

/**
 * Abstract class for IUString implementations
 */
abstract class AbstractUString
    extends BaseStrictClass
    implements IUStringExtended
{

    /**
     * Default encoding for strings. Defaults to UTF-8
     * @var string
     */
    private static $defaultCodePage = CharsetEncodings::UTF_8;

    /**
     * Gets the default code page in use
     */
    public static function getDefaultCodePage()
    {
        return self::$defaultCodePage;
    }

    /**
     * Sets the default code page in use
     * @param string $codePage
     */
    public static function setDefaultCodePage($codePage)
    {
        self::$defaultCodePage = (string)$codePage;
    }

    /**
     * Map with the previous regex and internal encodings. Only set if these are set at any point.
     * @var array|null
     */
    private static $previousEncodings = null;

    /**
     * Changes regex and internal encodings for mb_string functions. This is done and after
     * that the encoding must be restored at any point. If encoding is not restored subsequent
     * calls to mbstring functions could produce unexpected results for strings whose encoding
     * does not match the one set.
     * @param $encoding
     * @return bool
     */
    protected static function setCurrentEncoding($encoding)
    {
        if (self::$previousEncodings !== null) {
            return false;
        }

        self::$previousEncodings = [
            'regex'    => mb_regex_encoding(),
            'internal' => mb_internal_encoding(),
        ];

        mb_regex_encoding($encoding);
        mb_internal_encoding($encoding);

        return true;
    }

    /**
     * Restore back previous encodings.
     * @return boolean
     */
    protected static function restorePreviousEncoding()
    {
        if (self::$previousEncodings === null) {
            return false;
        }

        mb_regex_encoding(self::$previousEncodings['regex']);
        mb_internal_encoding(self::$previousEncodings['internal']);
        self::$previousEncodings = null;

        return true;
    }

    /**
     * Current number of code points
     * @var int
     */
    protected $length = 0;
    /**
     * Current byte sequence stored
     * @var string
     */
    protected $string = '';
    protected $codePage;

    /**
     * (non-PHPdoc)
     * @see \phbrick\string\IUString::getCodePage()
     */
    public function getCodePage()
    {
        return $this->codePage;
    }

    /**
     * Sets the current code page
     * @param string $codePage
     */
    protected function setCodePage($codePage)
    {
        $this->codePage = (string)$codePage;
    }

    /**
     * (non-PHPdoc)
     * @see \phbrick\string\IUString::getLength()
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Sets the current number of code points
     * @param int $length
     */
    protected function setLength($length)
    {
        $this->length = $length;
    }

    public function __get($propName)
    {
        if ($propName == "length") {
            return $this->getLength();
        }

        return parent::__get($propName);
    }

    public function __set($propName, $value)
    {
        if ($propName == "length") {
            throw new ReadOnlyException("Cannot set read-only property \$$propName");
        }

        return parent::__set($propName, $value);
    }

    /**
     * AbstractUString constructor.
     * @param null $string
     * @param null $codePage
     */
    public function __construct($string = null, $codePage = null)
    {
        if ($string !== null) {
            if ($string instanceof IUString) {
                /**
                 * @type IUString
                 */
                $this->codePage = $string->getCodePage();
            }

            $this->string = (string)$string;
        }

        if ($codePage !== null) {
            $this->setCodePage($codePage);
        }
        else {
            $this->setCodePage(self::$defaultCodePage);
        }

        $this->setLength($this->internalStringLength($string));
    }

    protected abstract function internalStringLength($string);

    /**
     * Gets if the current string is empty
     * @return bool
     */
    public function isEmpty()
    {
        return $this->length == 0;
    }

    public function getIterator()
    {
        $codePoints = $this->toArray();

        return new ArrayIterator($codePoints);
    }

    public function __toString()
    {
        return $this->string;
    }
}

