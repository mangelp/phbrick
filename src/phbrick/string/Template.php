<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use Exception;
use phbrick\exceptions\FileAccessException;
use phbrick\exceptions\RenderableException;

/**
 * Renderable template that includes a file with a set of parameters defined.
 */
class Template extends AbstractStringRenderable
{

    protected static $allowedExtensions = ['phtml', 'php'];

    /**
     * @var array
     */
    private $params = [];

    /**
     * Gets the parameters to be exported as variables before loading the template
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Sets the parameters to be exported as variables before loading the template
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @var string
     */
    private $file = null;

    /**
     * Gets the absolute full path to the phtml file to be loaded
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets the absolute full path to the phtml file to be loaded
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Initializes the file to load and the set of parameters to be defined
     * @param string $file
     * @param array $params
     */
    public function __construct($file = null, array $params = null)
    {
        if ($file) {
            $this->setFile($file);
        }

        if ($params) {
            $this->setParams($params);
        }
    }

    public function toString($context = null, $indent = null)
    {

        $params = $this->params;

        if (is_array($context)) {
            $params = array_merge($context);
        }
        else if (is_a($context, '\Traversable')) {
            foreach ($context as $key => $value) {
                if (!is_numeric($key)) {
                    $params[$key] = $value;
                }
            }
        }

        return self::includeTemplate($this->file, $params);
    }

    /**
     * Loads a .phtml file declaring all the key-value pairs in the $___params parameter as declared
     * variables when the file is loaded.
     *
     * @param string $___templateFile Absolute path to the file to be loaded.
     * @param array $___params Array of key/value pairs to be declared before the file loads
     * @return string When the file load throws an exception.
     * @throws FileAccessException
     */
    public static function includeTemplate($___templateFile, array $___params = [])
    {

        if (!is_file($___templateFile)) {
            throw new FileAccessException("File does not exists or is not a file: $___templateFile");
        }

        $ext = strtolower(trim(pathinfo($___templateFile, PATHINFO_EXTENSION)));

        if (!in_array($ext, self::$allowedExtensions)) {
            throw new RenderableException('Cannot load a template file that does not have .phtml extension');
        }

        // Include the file inside an anonymous function to ensure the included file does not see
        // the current context.
        $LoadFileCallback = function($___params, $___templateFile) {
            extract($___params);
            include($___templateFile);
        };

        $loadedContents = null;

        try {
            ob_start();

            $LoadFileCallback($___params, $___templateFile);

            $loadedContents = ob_get_contents();
        } catch (Exception $ex) {
            throw new RenderableException(
                "Template file loading throwed an unhandled exception", 0, $ex);
        } finally {
            ob_end_clean();
        }

        return $loadedContents;
    }
}
