<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\string;

use Exception;
use InvalidArgumentException;
use phbrick\markup\Tag;
use phbrick\StaticClass;
use phbrick\types\Types;
use RuntimeException;

final class Renderables extends StaticClass
{

    /**
     * Checks if the input parameter is an scalar, a callable or an object that provides a method named __toString,
     * toString or render.
     *
     * A render method is not supposed to return an string but an object that implements __toString or toString.
     *
     * Callables are considered renderables as they, hopefully, might return an string or an object that implements
     * __toString, toString or render.
     *
     * @param mixed $target
     * @return boolean
     */
    public static function isRenderable($target)
    {
        return Renderables::isStringRenderable($target)
            || (is_object($target) && method_exists($target, 'render'));
    }

    /**
     * Checks if the input parameter is an scalar, a callable or an object that provides a method named __toString,
     * or toString.
     *
     * Callables are considered renderables as they, hopefully, might return an string or an object that implements
     * __toString or toString when executed.
     *
     * @param mixed $target
     * @return boolean
     */
    public static function isStringRenderable($target)
    {
        return is_scalar($target)
            || is_callable($target)
            || (is_object($target)
                && (method_exists($target, '__toString')
                    || method_exists($target, 'toString')));
    }

    /**
     * Logs using error_log
     *
     * @param string $msg
     * @param string $method
     * @param string $targetName
     * @param mixed $target
     */
    protected static function log(string $msg, string $method, string $targetName, $target)
    {
        $name = self::cls() . '@' . $method;

        $logMsg = sprintf("%s: %s", $name, $msg);

        if ($target) {
            $type = gettype($target);

            if ($type == 'object') {
                $type = get_class($target);
            }

            $logMsg .= sprintf('. Caused by parameter %s of type %s', $targetName, $type);
        }

        error_log($logMsg);
    }

    /**
     * Calls the method Renderables::toString but catching all exceptions.
     *
     * Exceptions produced are logged.
     *
     * @param mixed $renderable
     * @param ?array $context
     * @return string|null An string produced by the renderable or null if it fails
     */
    public static function safeToString($renderable, ?array $context = null)
    {
        $result = null;

        try {
            $result = Renderables::toString($renderable, $context);
        } catch (Exception $ex) {
            self::log('Renderable threw an exception: ' . $ex->getMessage(), __METHOD__, '$renderable', $renderable);
        }

        return $result;
    }

    /**
     * Calls the method Renderables::render but catching all exceptions.
     *
     * If the render fails it returns null.
     *
     * Exceptions produced are logged.
     *
     * @param mixed $renderable
     * @param ?array $context
     * @return string
     */
    public static function tryRender($renderable, ?array $context = null)
    {
        $result = null;

        try {
            $result = self::render($renderable, $context);
        } catch (Exception $ex) {

            self::log('Renderable threw an exception: ' . $ex->getMessage(), __METHOD__, '$renderable', $renderable);
        }

        return $result;
    }

    /**
     * Generates an string from the given renderable. This method does not returns objects, only
     * strings.
     *
     * @param mixed $renderable
     * @param ?array $context
     * @return string
     */
    public static function toString($renderable, ?array $context = null): string
    {
        return (string)Renderables::render($renderable, $context);
    }

    /**
     * Generates an object that can be converted to an string or an string itself.
     *
     * If the $renderable is the name of a global function it will be returned without being
     * executed as before checking if is callable we check if is an scalar and we return it.
     *
     * @param mixed $renderable
     * @param ?array $context
     * @return StringBundler|string|Tag If some input argument is not valid
     * @internal param bool $throwEx If true an exception will be thrown if a string
     * cannot be returned.
     * @see \phbrick\string\Renderables::isRenderable
     * @see \phbrick\string\Renderables::isStringRenderable
     */
    public static function render($renderable, ?array $context = null)
    {
        if ($renderable === null) {
            return null;
        }

        $result = null;

        if (is_scalar($renderable)) {
            // Scalar renderable
            $result = (string)$renderable;
        }
        else if (is_callable($renderable)) {

            try {
                $result = $renderable();
            } catch (Exception $ex) {
                $exMsg = $ex->getMessage();
                throw new RuntimeException(
                    'Cannot produce an string from callable' . ($exMsg ? ": $exMsg" : ''), 0, $ex);
            }

            if (!Renderables::isStringRenderable($result)) {
                throw new InvalidArgumentException('Callable parameter $renderable returned invalid result not convertible to an string of type ' . Types::getTypeName($result));
            }
        }
        else if (is_object($renderable)) {

            if (method_exists($renderable, 'toString')) {
                $result = $renderable->toString($context);
                $methodName = 'toString';
            }
            else if (method_exists($renderable, 'render')) {
                $result = $renderable->render($context);
                $methodName = 'render';
            }
            else if (method_exists($renderable, '__toString')) {
                $result = $renderable->__toString();
                $methodName = '__toString';
            }
            else {
                throw new InvalidArgumentException(
                    'Invalid renderable object of class ' . get_class($renderable) . '. It cannot produce an string.');
            }

            if (!Renderables::isStringRenderable($result)) {
                throw new InvalidArgumentException('Class ' . get_class($renderable) . ' method ' . $methodName . ' returned invalid result of type ' . Types::getTypeName($result) . ' not convertible to an string.');
            }
        }
        else {
            throw new InvalidArgumentException(
                'Invalid $renderable argument type ' . gettype($renderable) . '. It cannot produce an string.');
        }

        return $result;
    }
}
