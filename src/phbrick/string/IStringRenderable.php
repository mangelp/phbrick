<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

/**
 * Interface that provides two methods. One is __toString and the other is toString($context).
 * The former is a magic method and the later is a regular method that receives a context and
 * an indentation parameter.
 *
 * Implements of this interface might use or not the parameters of the toString method. But must
 * always implement it assuming that no parameter might be provided.
 */
interface IStringRenderable
{
    public function toString($context = null, $indent = null);

    public function __toString();
}
