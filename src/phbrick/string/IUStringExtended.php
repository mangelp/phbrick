<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

/**
 * Extends base IUString interface with more methods
 */
interface IUStringExtended extends IUString
{

    /**
     * Returns an array with all the code points
     */
    public function toArray();

    /**
     * Gets if the current string is empty (zero code points)
     * @return bool
     */
    public function isEmpty();
}
