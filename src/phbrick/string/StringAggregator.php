<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\string;

/**
 * Simple aggregator of strings into a single string
 */
class StringAggregator implements IStringAggregator
{

    private $string = '';

    public function append($string)
    {
        $this->string .= $string;
    }

    public function prepend($string)
    {
        $this->string = $string . $this->string;
    }

    public function toString($context = null, $indent = null)
    {
        return $this->string;
    }

    public function __toString()
    {
        return $this->string;
    }
}
