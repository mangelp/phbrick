<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\string;

use RuntimeException;

/**
 * Trait for classes that has a render method that returns either an object convertible to string
 * (implements toString or __toString) or an scalar that can be cast to string (strings included).
 *
 * This trait is compatible with SafeStringRenderTrait method as the later will call the method that
 * implements the former.
 */
trait RenderableToStringTrait
{

    public function toString(array $context = null, $indent = null)
    {
        $render = $this->render($context);

        // We don't care too much about the return type, if it is an object we check if we would
        // call toString or __toString, but if not cast it to an string.

        if (is_object($render)) {
            if (method_exists($render, '__toString')) {
                return (string)$render;
            }
            else if (method_exists($render, 'toString')) {
                return (string)$render->toString($context, $indent);
            }
            else {
                throw new RuntimeException("Must implement either toString or __toString methods");
            }
        }
        else {
            return (string)$render;
        }
    }
}
