<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\string;

use Exception;

trait SafeStringRenderTrait
{
    public function __toString()
    {
        $result = '';

        try {
            if (method_exists($this, 'toString')) {
                $result = Renderables::safeToString($this);
            }
            else if (method_exists($this, 'render')) {
                $result = (string)Renderables::tryRender($this);
            }
        } catch (Exception $ex) {
            error_log(get_called_class()
                . ': SafeStringRendererTrait::__toString: Exception thrown: '
                . $ex->getMessage());
        }

        if (!is_string($result)) {
            $result = (string)$result;
        }

        return $result;
    }
}
