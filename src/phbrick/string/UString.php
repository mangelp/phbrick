<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use Exception;
use InvalidArgumentException;
use OutOfBoundsException;
use RuntimeException;

/**
 * Multibyte string implementation for IUString interface that mimics the behaviour of an
 * experimental PHP 7 extension called ustring.
 *
 * The $length property is implemented using magic __get method and a proper getter and setter are
 * in place (the setter is protected).
 *
 * When using this class bear in mind that is a userland implementation more slow and with a bigger
 * memory footprint than the ustring module class. Also this class implements some extra features.
 *
 * @see https://github.com/krakjoe/ustring
 *
 * @property int $length The number of unicode code points (read-only)
 **/
class UString extends AbstractUString implements IUStringExtended
{

    /**
     *
     * @param UString $string
     * @return UString
     */
    public static function cast(UString $string)
    {
        return $string;
    }

    public function __construct($string = null, $codePage = null)
    {
        if (empty($codePage)) {
            $codePage = self::getDefaultCodePage();
        }

        parent::__construct($string, $codePage);
    }

    protected function internalStringLength($string)
    {
        return mb_strlen($string, $this->codePage);
    }

    public function toUpper()
    {
        return new UString(
            mb_strtoupper($this->string, $this->codePage),
            $this->codePage);
    }

    public function toLower()
    {
        return new UString(
            mb_strtolower(
                $this->string,
                $this->codePage),
            $this->codePage);
    }

    public function reverse()
    {
        $codePoints = array_reverse($this->toArray());

        return new UString(
            implode('', $codePoints),
            $this->codePage);
    }

    public function trim()
    {
        return new UString(
            trim($this->string),
            $this->codePage);
    }

    public function replace($search, $replace)
    {
        return new UString(
            str_replace(
                (string)$search,
                (string)$replace,
                $this->string),
            $this->codePage);
    }

    public function replaceSlice($text, $start = 0, $length = null)
    {

        if ($length === null) {
            $length = $this->length - $start;
        }

        return new UString(
            mb_substr($this->string, 0, $start, $this->codePage)
            . (string)$text
            . mb_substr($this->string, $start + $length, null, $this->codePage),
            $this->codePage);
    }

    public function contains($text)
    {
        if (strlen($text) == 0) {
            return true;
        }

        $index = mb_strpos(
            $this->string, (string)$text, 0, $this->codePage);

        return $index !== false && $index >= 0;
    }

    public function startsWith($needle)
    {
        if (strlen($needle) == 0) {
            return true;
        }

        $len = mb_strlen((string)$needle, $this->codePage);

        if ($len > $this->length) {
            return false;
        }
        else if ($len == $this->length) {
            return $needle == $this->string;
        }
        else {
            return strcmp(
                    mb_substr($this->string, 0, $len, $this->codePage),
                    $needle) == 0;
        }
    }

    public function endsWith($needle)
    {
        if (strlen($needle) == 0) {
            return true;
        }

        $len = mb_strlen((string)$needle, $this->codePage);

        if ($len > $this->length) {
            return false;
        }
        else if ($len == $this->length) {
            return $needle == $this->string;
        }
        else {

            return strcmp(
                    mb_substr($this->string, $this->length - $len, null, $this->codePage),
                    $needle) == 0;
        }
    }

    public function indexOf($needle, $offset = 0)
    {

        if (strlen($needle) == 0) {
            return false;
        }

        $offset = (int)$offset;

        if ($offset < 0) {
            $offset += $this->length;
        }

        return mb_strpos(
            $this->string, (string)$needle, $offset, $this->codePage);
    }

    public function lastIndexOf($needle, $offset = 0)
    {

        if (strlen($needle) == 0) {
            return false;
        }

        $offset = (int)$offset;

        if ($offset < 0) {
            $offset += $this->length;
        }

        return mb_strrpos(
            $this->string, (string)$needle, $offset, $this->codePage);
    }

    public function chunk($length)
    {
        $length = (int)$length;
        $chunks = [];

        if ($length == 0) {
            return $chunks;
        }
        else if ($length < 0) {
            throw new InvalidArgumentException('Parameter $length cannot be less than 0');
        }

        $total = $this->length;
        $current = 0;

        while ($current < $total) {
            $chunk = $this->substring($current, $length);
            $chunks[] = $chunk;
            $current += $chunk->length;
        }

        return $chunks;
    }

    public function repeat($count)
    {
        $count = (int)$count;

        if ($count == 0) {
            return new UString();
        }
        else if ($count < 0) {
            throw new InvalidArgumentException('Parameter $count cannot be less than 0');
        }

        return new UString(
            str_repeat($this->string, $count),
            $this->codePage);
    }

    public function pad($length, $pad = ' ', $mode = STR_PAD_RIGHT)
    {
        $length = (int)$length;

        if ($length < 0) {
            throw new InvalidArgumentException('Parameter $length cannot be less than 0');
        }

        $pad = new UString($pad);

        if ($pad->isEmpty() || $length == 0 || $length <= $this->length) {
            return new UString($this);
        }

        // First repeat $pad as many times as we can taking all the chars from it.

        $targetLength = $length - $this->length;
        $repeatCount = (int)floor($targetLength / $pad->length);

        $appendPad = $pad->repeat($repeatCount);

        // Now complete the pad to the desired target length
        $reminderPad = $pad->substring(0, $targetLength - $appendPad->length);
        $appendPad = self::instance($reminderPad . (string)$appendPad, $this->codePage);

        $result = null;

        switch ($mode) {
            case STR_PAD_RIGHT:
                $result = new UString(
                    $this->string . $appendPad->string,
                    $this->codePage);
                break;
            case STR_PAD_LEFT:
                $result = new UString(
                    $appendPad->string . $this->string,
                    $this->codePage);
                break;
            case STR_PAD_BOTH:
                $half = (int)$appendPad->length / 2;
                $result = new UString(
                    $appendPad->substring(0, $half)
                    . $this->string
                    . $appendPad->substring($half),
                    $this->codePage);
                break;
            default:
                throw new InvalidArgumentException('Invalid $mode: ' . $mode);
        }

        return $result;
    }

    /**
     * @param $delimiter
     * @param int|null $limit
     * @return array|null|UString[]|string[]
     */
    public function split($delimiter, $limit = null)
    {

        if (strlen($delimiter) == 0) {
            return $this->toArray();
        }
        else if ($limit !== null && $limit < 1) {
            return [];
        }

        self::setCurrentEncoding($this->codePage);

        $result = null;

        try {
            if ($limit === null) {
                $result = mb_split($delimiter, $this->string);
            }
            else {
                $result = mb_split($delimiter, $this->string, $limit);
            }
        } catch (Exception $ex) {
            self::restorePreviousEncoding();
            throw new RuntimeException("Split error", 0, $ex);
        }

        self::restorePreviousEncoding();

        foreach ($result as $pos => $str) {
            $result[$pos] = self::instance($str, $this->codePage);
        }

        return $result;
    }

    protected function assertInRange($index)
    {
        if ($index < 0 || $index > $this->length) {
            throw new OutOfBoundsException("Index $index is out of [0, {$this->length}]");
        }
    }

    public function charAt($index)
    {
        $this->assertInRange($index);

        return new UString(
            mb_substr($this->string, $index, 1, $this->codePage));
    }

    public function substring($start, $length = null)
    {
        return new UString(
            mb_substr($this->string, $start, $length, $this->codePage));
    }

    public function toArray()
    {
        $codePoints = [];

        for ($i = 0; $i < $this->length; ++$i) {
            $codePoints[$i] = mb_substr($this->string, $i, 1, $this->codePage);
        }

        return $codePoints;
    }
}
