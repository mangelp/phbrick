<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\string;

use InvalidArgumentException;

/**
 * UTF-8 specific derived class from self that optimizes several methods.
 *
 * This class extends and optimizes several methods for UTF-8 encoding.
 */
class UTF8String extends UString
{

    /**
     * Initialize a new string. The codePage set here is ignored and is internally set always to
     * UTF-8
     *
     * @param string $string
     * @param string $codePage IGNORED
     */
    public function __construct($string = null, $codePage = null)
    {
        if ($codePage !== null && $codePage != CharsetEncodings::UTF_8) {
            throw new InvalidArgumentException('Only UTF-8 encoding is allowed');
        }

        parent::__construct($string, CharsetEncodings::UTF_8);
    }

    public function chunk($length)
    {
        $length = (int)$length;
        $chunks = [];

        if ($length == 0) {
            return $chunks;
        }
        else if ($length < 0) {
            throw new InvalidArgumentException('Parameter $length cannot be less than 0');
        }

        preg_match_all("/.{1,$length}/u", $this->string, $chunks, PREG_SPLIT_NO_EMPTY);
        $chunks = $chunks[0];

        foreach ($chunks as $pos => $str) {
            $chunks[$pos] = self::instance($str);
        }

        return $chunks;
    }

    public function split($delimiter, $limit = null)
    {

        if (strlen($delimiter) == 0) {
            return $this->toArray();
        }
        else if ($limit !== null && $limit < 1) {
            return [];
        }

        $result = preg_split("/$delimiter/u", $this->string, $limit);

        foreach ($result as $pos => $str) {
            $result[$pos] = self::instance($str);
        }

        return $result;
    }

    public function toArray()
    {
        $codePoints = preg_split('//u', $this->string, null, PREG_SPLIT_NO_EMPTY);

        return $codePoints;
    }
}
