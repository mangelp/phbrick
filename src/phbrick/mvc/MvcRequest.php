<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use phbrick\BaseStrictClass;
use phbrick\string\Strings;

/**
 * Class MvcRequest
 *
 * Request data handed to a request dispatcher that will decide what controller will use to handle it.
 *
 * @package phbrick\mvc
 */
class MvcRequest extends BaseStrictClass implements IMvcRequest
{
    /**
     * Request parameters
     *
     * @var MvcParameters
     */
    private $params = null;
    /**
     * Virtual URI that defines the target of the request
     * @var string
     */
    private $targetUri = null;

    /**
     * @return MvcParameters
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param MvcParameters $params
     */
    public function setParams(MvcParameters $params)
    {
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getTargetUri()
    {
        return $this->targetUri;
    }

    /**
     * @param string $targetUri
     */
    public function setTargetUri($targetUri)
    {
        $this->targetUri = Strings::castString($targetUri);
    }

    /**
     * Request constructor.
     *
     * Initializes the target for the request and an empty collection of parameters.
     * @param $targetUri
     */
    public function __construct($targetUri)
    {
        $this->setTargetUri($targetUri);
        $this->setParams(new MvcParameters([]));
    }
}
