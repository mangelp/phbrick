<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use phbrick\collection\ReadOnlyArrayMap;
use phbrick\string\Strings;
use phbrick\types\Bools;
use phbrick\types\Floats;
use phbrick\types\Ints;

/**
 * Class MvcParameters
 *
 * Map where keys and values are strings.
 *
 * @package phbrick\mvc
 */
class MvcParameters extends ReadOnlyArrayMap
{
    public function __construct($iterable, $readOnly = false)
    {
        parent::__construct($iterable, $readOnly);
    }

    /**
     * Gets a parameter as a boolean value
     *
     * @param string $paramName
     * @param string $defaultValue
     * @return bool|null
     */
    public function getBool($paramName, $defaultValue = null)
    {
        if (!isset($this->storage[$paramName])) {
            return $defaultValue;
        }

        return Bools::toBool($this->storage[$paramName], false, $defaultValue);
    }

    /**
     * Gets a parameter as an integer
     *
     * @param string $paramName
     * @param string $defaultValue
     * @return int|null
     */
    public function getInt($paramName, $defaultValue = null)
    {
        if (!isset($this->storage[$paramName])) {
            return $defaultValue;
        }

        return Ints::toInt($this->storage[$paramName], false, $defaultValue);
    }

    /**
     * Gets a parameter as a float
     *
     * @param string $paramName
     * @param string $defaultValue
     * @return float|null
     */
    public function getFloat($paramName, $defaultValue = null)
    {
        if (!isset($this->storage[$paramName])) {
            return $defaultValue;
        }

        return Floats::toFloat($this->storage[$paramName], false, $defaultValue);
    }

    /**
     * Gets a parameter as an object.
     *
     * If the parameter value is not of type object the default value is returned instead. So this
     * method what it really does is to return the value only if it is a class instance.
     *
     * @param string $paramName
     * @param object $defaultValue
     * @return object
     */
    public function getObject($paramName, $defaultValue = null)
    {
        if (!isset($this->storage[$paramName])) {
            return $defaultValue;
        }

        if (!is_object($this->storage[$paramName])) {
            return $defaultValue;
        }

        return $this->storage[$paramName];
    }

    /**
     * Gets a parameter as a given class instance.
     *
     * This method does the same as getObject does, but it also checks that the object instance
     * matches a given class name.
     *
     * @param string $paramName
     * @param $className
     * @param object $defaultValue
     * @return object
     */
    public function getClassInstance($paramName, $className, $defaultValue = null)
    {
        if (!isset($this->storage[$paramName])) {
            return $defaultValue;
        }

        if (!is_object($this->storage[$paramName]) || !is_a($this->storage[$paramName], $className)) {
            return $defaultValue;
        }

        return $this->storage[$paramName];
    }

    /**
     * Gets a portion of this map filtering pairs by key using a prefix and returns a new MvcParameters instance.
     *
     * The prefix is removed from the keys in the returned instance.
     *
     * @param string $prefix Prefix to filter parameters
     * @param bool $readOnly Makes the returned collection to be unmodifiable if is set to true.
     * @return MvcParameters
     */
    public function getSubsetByPrefix($prefix, $readOnly = null)
    {
        $prefixedItems = $this->apply(function($key, $value) use ($prefix) {
            if (Strings::startsWith($key, $prefix)) {
                return [substr($key, strlen($prefix)) => $value];
            }
            else {
                return false;
            }
        });

        $newInstance = new MvcParameters($prefixedItems, $readOnly);

        return $newInstance;
    }
}
