<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use phbrick\collection\ArrayMap;

/**
 * Class MvcResult
 *
 * Models the result of a request
 *
 * @package phbrick\mvc
 */
class MvcResult implements IMvcResult
{
    /**
     * Whether the request results represents a success (true) or a failure (false).
     * @var bool
     */
    private $success = true;
    /**
     * Collection of error messages generated. If the $success flag is true this collection must be empty.
     * @var ArrayMap
     */
    private $errors = null;
    /**
     * Data returned by the controller after the execution of the method that handled the request
     * @var mixed
     */
    private $data = null;

    /**
     * @return ArrayMap
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param ArrayMap $errors
     */
    public function setErrors($errors)
    {
        $this->errors->clear();
        $this->errors->setAll($errors);
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->errors->isEmpty();
    }

    /**
     * @param bool $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * Obtains the data returned by the controller method that was executed
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function __construct()
    {
        $this->errors = new ArrayMap();
    }
}
