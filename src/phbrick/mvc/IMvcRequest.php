<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

interface IMvcRequest
{
    /**
     * Gets the parameters of the request
     * @return MvcParameters
     */
    public function getParams();

    /**
     * Sets the parameters of the request
     * @param MvcParameters $params
     */
    public function setParams(MvcParameters $params);

    /**
     * @return string
     */
    public function getTargetUri();

    /**
     * @param string $targetUri
     */
    public function setTargetUri($targetUri);
}
