<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

interface IMvcRequestHandler
{
    /**
     * @param IMvcRequestHandler $requestHandler
     * @return IMvcRequestHandler
     */
    public static function castRequestHandler(IMvcRequestHandler $requestHandler);

    /**
     * Gets the unique handler name
     *
     * @return string
     */
    public function getName();

    /**
     * Checks if the handlers matches the request and can be used to handle it
     *
     * @param IMvcRequest $request
     * @return mixed
     */
    public function handles(IMvcRequest $request);

    /**
     * Handles the request within the context and returns a result
     *
     * @param IMvcRequest $request
     * @param IRequestContext|null $context
     * @return IMvcResult
     */
    public function handle(IMvcRequest $request, IRequestContext $context = null);

    /**
     * Called over the handler after it have been added to a dispatcher
     * @param IMvcDispatcher $dispatcher
     */
    public function onHandlerAdded(IMvcDispatcher $dispatcher);

    /**
     * Called over the handler after it have been removed from a dispatcher
     * @param IMvcDispatcher $dispatcher
     */
    public function onHandlerRemoved(IMvcDispatcher $dispatcher);
}
