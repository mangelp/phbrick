<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

/**
 * Interface IRequestDispatcher
 *
 * Models the operations to be implemented by a dispatcher that receives a request object with all the parameters and provides the result of the execution of the request by the most suitable controller.
 *
 * @package phbrick\mvc
 */
interface IMvcDispatcher
{
    /**
     * Must be called before the first dispatch method call and only once.
     *
     * This method initializes the dispatcher implementation.
     */
    public function initialize();

    /**
     * Registers a handler for requests
     *
     * @param IMvcRequestHandler $handler
     * @return mixed
     */
    public function registerHandler(IMvcRequestHandler $handler);

    /**
     * Checks if a handler with the given name is already registered.
     * @param $name
     * @return mixed
     */
    public function isRegisteredHandler($name);

    /**
     * Removes a handler by its name.
     *
     * @param $handlerName
     * @return mixed
     */
    public function unregisterHandler($handlerName);

    /**
     * Dispatches a request using the context information provided.
     *
     * This method must construct a request object and produce a request result in return
     * @param IMvcRequest $request
     * @param IRequestContext $context
     * @return MvcResult
     */
    public function dispatch(IMvcRequest $request, IRequestContext $context = null);
}
