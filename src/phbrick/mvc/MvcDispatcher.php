<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use Exception;
use phbrick\BaseStrictClass;

/**
 * Class MvcDispatcher
 * @package phbrick\mvc
 */
class MvcDispatcher extends BaseStrictClass implements IMvcDispatcher
{

    private $handlers = [];

    /**
     * Must be called before the first dispatch method call and only once.
     *
     * This method initializes the dispatcher implementation.
     */
    public function initialize()
    {
    }

    public function registerHandler(IMvcRequestHandler $handler)
    {
        $this->handlers[$handler->getName()] = $handler;
    }

    public function isRegisteredHandler($name)
    {
        return isset($this->handlers[$name]);
    }

    public function unregisterHandler($handlerName)
    {
        unset($this->handlers[$handlerName]);
    }

    /**
     * Dispatches a request using the context information provided.
     *
     * This method must construct a request object and produce a request result in return
     * @param IMvcRequest $request
     * @param IRequestContext $context
     * @return MvcResult
     * @throws MvcException If there is no handler for the request target or if the handler throws an exception when called.
     */
    public function dispatch(IMvcRequest $request, IRequestContext $context = null)
    {
        $selectedHandler = null;

        foreach ($this->handlers as $key => $value) {
            if (isset($this->handlers[$key]) && $this->handlers[$key]->handles($request)) {
                $selectedHandler = $this->handlers[$key];
            }
        }

        if (!$selectedHandler) {
            throw new MvcException("No handler found for target " . $request->getTargetUri(), MvcErrorCodes::E_CONTROLLER_NOT_FOUND);
        }

        try {
            $result = $selectedHandler->handle($request, $context);
        } catch (Exception $exception) {
            throw new MvcException("Unhandled exception on request handling", MvcErrorCodes::E_INTERNAL_ERROR, $exception);
        }

        return $result;
    }
}
