<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use Exception;
use phbrick\types\Ints;
use RuntimeException;

/**
 * Class MvcException
 * @package phbrick\mvc
 */
class MvcException extends RuntimeException
{

    /**
     * MvcException constructor.
     *
     * @param int $code one of the MvcErrorCodes constants
     * @param string $message Error message
     * @param Exception|null $previous Previous exception
     */
    public function __construct($message, $code, Exception $previous = null)
    {
        $code = Ints::castInt($code);
        MvcErrorCodes::assertValue($code);

        if (empty($message)) {
            $message = "Error " . $code;
        }

        parent::__construct($message, $code, $previous);
    }
}
