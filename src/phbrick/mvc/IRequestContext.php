<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use phbrick\collection\ArrayMap;

/**
 * Interface IRequestContext
 *
 * Models contextual information passed along the request and shared between all request handlers executed.
 *
 * @package phbrick\mvc
 */
interface IRequestContext
{
    /**
     * Map of key-value pairs associated with the request context
     * @return ArrayMap
     */
    public function getProperties();
}
