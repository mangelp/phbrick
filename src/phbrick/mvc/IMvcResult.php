<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\mvc;

use phbrick\collection\ArrayMap;
use Traversable;

interface IMvcResult
{
    /**
     * Obtains the full map of defined errors.
     *
     * If the success flag is true this collection must be empty
     *
     * @return ArrayMap
     */
    public function getErrors();

    /**
     * Sets all errors and replaces existing ones.
     *
     * The success flag must be updated to false when this method is used with a non-empty parameter and must be set to
     * true when this method is used with an empty collection.
     *
     * @param array|Traversable $errors
     */
    public function setErrors($errors);

    /**
     * Obtains the data returned by the controller method that was executed
     * @return mixed
     */
    public function getData();

    /**
     * @param mixed $data
     */
    public function setData($data);

    /**
     * Gets if the request succeeded or if there was any kind of error.
     *
     * Must return true only if there are no errors or false otherwise.
     *
     * @return bool
     */
    public function isSuccess();
}
