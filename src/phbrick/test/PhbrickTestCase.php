<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\test;

use PHPUnit\Framework\TestCase;

/**
 * Class PhbrickTestCase
 * @package phbrick\test
 */
class PhbrickTestCase extends TestCase
{
    protected function beforeTest()
    {

    }

    protected function afterTest()
    {

    }

    protected final function setUp(): void
    {
        parent::setUp();
        $this->beforeTest();
    }

    protected final function tearDown(): void
    {
        parent::tearDown();
        $this->afterTest();
    }
}
