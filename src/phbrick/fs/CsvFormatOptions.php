<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\fs;

use phbrick\BaseStrictClass;
use phbrick\string\Strings;

/**
 * Defines all CSV format options and columns of the CSV file to read/write it as a set of records
 * that might be modeled as a class.
 *
 */
class CsvFormatOptions extends BaseStrictClass
{
    /**
     * @var string
     */
    private $fieldDelimiter = ';';

    /**
     * Gets
     * @return string
     */
    public function getFieldDelimiter()
    {
        return $this->fieldDelimiter;
    }

    /**
     * Sets
     * @param string $fieldDelimiter
     */
    public function setFieldDelimiter($fieldDelimiter)
    {
        $this->fieldDelimiter = $fieldDelimiter;
    }

    /**
     * @var string
     */
    private $textDelimiter = '"';

    /**
     * Gets
     * @return string
     */
    public function getTextDelimiter()
    {
        return $this->textDelimiter;
    }

    /**
     * Sets
     * @param string $textDelimiter
     */
    public function setTextDelimiter($textDelimiter)
    {
        $this->textDelimiter = $textDelimiter;
    }

    /**
     * @var int
     */
    private $maxLineLength = -1;

    /**
     * Gets the maximum number of chars a line might have
     * @return int
     */
    public function getMaxLineLength()
    {
        return $this->maxLineLength;
    }

    /**
     * Sets the maximum number of chars a line might have
     * @param int $maxLineLength
     */
    public function setMaxLineLength($maxLineLength)
    {
        $this->maxLineLength = $maxLineLength;
    }

    /**
     * @var string
     */
    private $escapeCharacter = '\\';

    /**
     * Gets
     * @return string
     */
    public function getEscapeCharacter()
    {
        return $this->escapeCharacter;
    }

    /**
     * Sets
     * @param string $escapeCharacter
     */
    public function setEscapeCharacter($escapeCharacter)
    {
        $this->escapeCharacter = $escapeCharacter;
    }

    /**
     * @var string
     */
    private $newLine = Strings::LINE_ENDING_LF;

    /**
     * Gets
     * @return string
     */
    public function getNewLine()
    {
        return $this->newLine;
    }

    /**
     * Sets
     * @param string $newLine
     */
    public function setNewLine($newLine)
    {
        $this->newLine = $newLine;
    }

    /**
     * @var array
     */
    private $columns = null;

    /**
     * Gets an array with the name of the csv columns
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Sets an array with the name of the csv columns
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @var int
     */
    private $skipLines = null;

    /**
     * Gets the number of lines to skip from the file start
     * @return int
     */
    public function getSkipLines()
    {
        return $this->skipLines;
    }

    /**
     * Sets the number of lines to skip from the file start
     * @param int $skipLines
     */
    public function setSkipLines($skipLines)
    {
        $this->skipLines = $skipLines;
    }

    /**
     * @var string
     */
    private $recordClass = null;

    /**
     * Gets fully qualified name of a class that must be instanced from each retrieved record.
     * @return string
     */
    public function getRecordClass()
    {
        return $this->recordClass;
    }

    /**
     * Sets fully qualified name of a class that must be instanced from each retrieved record.
     * @param string $recordClass
     */
    public function setRecordClass($recordClass)
    {
        $this->recordClass = $recordClass;
    }
}
