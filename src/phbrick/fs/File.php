<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\fs;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\exceptions\FileAccessException;
use phbrick\exceptions\InvalidOperationException;
use phbrick\exceptions\PathException;
use phbrick\types\Types;
use Traversable;

/**
 * Models file operations as an object.
 *
 * All file modes supported are binary and proper exceptions are thrown when an invalid operation
 * is performed over a file. For example writing to a read-only file with throw an exception.
 *
 * This class has a flag that allows automatically opening the file whenever it is needed to perform
 * an operation. The file is open using the current mode set and is left open until explicitly
 * closed.
 *
 */
class File extends BaseStrictClass
{

    /**
     * Read-only binary
     * @var string
     */
    const MODE_READ = 'rb';
    /**
     * Read-write binary
     * @var string
     */
    const MODE_READ_WRITE = 'r+b';
    /**
     * Overwrite-only binary
     * @var string
     */
    const MODE_WRITE_REPLACE = 'wb';
    /**
     * Overwrite-read binary
     * @var string
     */
    const MODE_WRITE_REPLACE_READ = 'w+b';
    /**
     * Append-only binary
     * @var string
     */
    const MODE_WRITE_APPEND = 'ab';
    /**
     * Append-read binary
     * @var string
     */
    const MODE_WRITE_APPEND_READ = 'a+b';
    /**
     * Overwrite-only binary if not exists
     * @var string
     */
    const MODE_WRITE_NOT_EXISTS = 'xb';
    /**
     * Overwrite-read binary if not exists
     * @var string
     */
    const MODE_WRITE_NOT_EXISTS_READ = 'x+b';

    private static $modes = [
        self::MODE_READ,
        self::MODE_READ_WRITE,
        self::MODE_WRITE_REPLACE,
        self::MODE_WRITE_REPLACE_READ,
        self::MODE_WRITE_APPEND,
        self::MODE_WRITE_APPEND_READ,
        self::MODE_WRITE_NOT_EXISTS,
        self::MODE_WRITE_NOT_EXISTS_READ,
    ];

    /**
     * Default read chunk size of 1024 bytes. You can provide a different one in read calls or set
     * it as a property for the given File instance.
     * @var int
     */
    const DEFAULT_CHUNK_SIZE = 1024;

    /**
     * @var FsPath
     */
    private $path = null;

    /**
     * Gets file path
     * @return FsPath
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets file path
     * @param FsPath $path
     */
    public function setPath(FsPath $path)
    {

        if ($this->isOpen()) {
            throw new InvalidOperationException('Cannot change the path, the file is open');
        }

        $this->path = $path;
    }

    /**
     * @var resource
     */
    private $handle = null;

    /**
     * Gets the file descriptor handle.
     *
     * @return resource
     */
    protected function getHandle()
    {
        return $this->handle;
    }

    /**
     * Sets the file descriptor handle
     * @param resource $handle
     */
    protected function setHandle($handle)
    {
        $this->handle = $handle;
    }

    /**
     * @var string
     */
    private $mode = null;

    /**
     * Gets the open mode of the file
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Sets the open mode of the file
     * @param string $mode
     */
    protected function setMode($mode)
    {
        if (!in_array($mode, self::$modes)) {
            throw new InvalidArgumentException('Invalid mode: ' . $mode);
        }

        $this->mode = $mode;
    }

    /**
     * Controls if the file is automatically open when needed or not.
     * @var bool
     */
    private $autoOpen = false;

    /**
     * Gets if the file is automatically open if any read/write/seek operation is executed and the
     * file handle is closed.
     *
     * @return bool
     */
    public function isAutoOpen()
    {
        return $this->autoOpen;
    }

    /**
     * Sets if the file is automatically open if any read/write/seek operation is executed and the
     * file handle is closed.
     *
     * @param bool $autoOpen
     */
    public function setAutoOpen($autoOpen)
    {
        $this->autoOpen = (bool)$autoOpen;
    }

    /**
     * @var int
     */
    private $readChunkSize = self::DEFAULT_CHUNK_SIZE;

    /**
     * Gets the amount of bytes to try to read in each operation by default
     *
     * @return int
     * @see File::DEFAULT_CHUNK_SIZE
     */
    public function getReadChunkSize()
    {
        return $this->readChunkSize;
    }

    /**
     * Sets the amount of bytes to try to read in each operation by default
     * @param int $readChunkSize
     * @see File::DEFAULT_CHUNK_SIZE
     */
    public function setreadChunkSize($readChunkSize)
    {
        $value = (int)$readChunkSize;

        if ($value < 1) {
            throw new InvalidArgumentException(
                'Cannot set chunk size to ' . print_r($readChunkSize, true)
                . ', the minimum allowed value is 1');
        }

        $this->readChunkSize = $value;
    }

    /**
     * Initializes the path and mode of the file.
     *
     * The mode is used as a default mode to open the file.
     *
     * @param string $path
     * @param string $mode
     */
    public function __construct($path = null, $mode = null)
    {
        if ($path !== null) {
            $path = FsPath::parsePath($path);
            $this->setPath($path);
        }

        if ($mode != null) {
            $this->setMode($mode);
        }
    }

    /**
     * Implements the destructor to ensure the file handle is closed
     */
    public function __destruct()
    {
        if ($this->isOpen()) {
            $this->close();
        }

        $this->handle = null;
        $this->path = null;
    }

    /**
     * Gets if the file is open.
     */
    public function isOpen()
    {
        return $this->handle !== null;
    }

    private $modificationCount = null;

    /**
     * Opens the file.
     *
     * If the file is already open it throws an exception and if the file cannot be found or open
     * it throws another exception.
     *
     * @param string $mode
     * @return bool If there is no mode set
     * open the file.
     * @throws FileAccessException
     */
    public function open($mode = null)
    {
        if ($this->isOpen()) {
            return false;
        }

        if ($mode !== null) {
            $this->setMode($mode);
        }

        if ($this->mode === null) {
            throw new FileAccessException('Cannot open a file without a mode');
        }

        $this->assertPathIsValidFile();

        $handle = fopen($this->path->toString(), $this->mode);

        if ($handle) {
            $this->handle = $handle;
            $this->modificationCount = $this->path->getModificationCount();
            return true;
        }

        return false;
    }

    /**
     * Closes and opens again the file in the specified mode.
     * @param string $mode
     * @return boolean
     */
    public function reOpen($mode = null)
    {
        $this->close();

        return $this->open($mode);
    }

    /**
     * Checks that the current path is a file and that we can open it with the proper mode.
     * @throws PathException
     */
    protected function assertPathIsValidFile()
    {
        if (!$this->path->isFile()
            && $this->canModeRead($this->mode)
            && !$this->canModeWrite($this->mode)) {
            throw new PathException('The path is not a file: ' . $this->path->toString());
        }
    }

    /**
     * Checks if the current mode allows to read from the file.
     * @param string $mode
     * @return bool
     */
    protected function canModeRead($mode)
    {
        $canRead = in_array($mode, [
            self::MODE_READ,
            self::MODE_READ_WRITE,
            self::MODE_WRITE_APPEND_READ,
            self::MODE_WRITE_NOT_EXISTS_READ,
            self::MODE_WRITE_REPLACE_READ,
        ]);

        return $canRead;
    }

    /**
     * Asserts that the current mode is set to a valid mode to read from the file or throws an
     * exception if not.
     *
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     */
    protected function assertModeAllowsReading()
    {
        if (!$this->canModeRead($this->mode)) {
            throw new InvalidOperationException('File open mode ' . $this->mode . ' cannot be used for reading');
        }
    }

    /**
     * Checks if the current mode allows to write to the file.
     * @param string $mode
     * @return bool
     */
    protected function canModeWrite($mode)
    {
        $canWrite = in_array($mode, [
            self::MODE_READ_WRITE,
            self::MODE_WRITE_APPEND,
            self::MODE_WRITE_APPEND_READ,
            self::MODE_WRITE_NOT_EXISTS,
            self::MODE_WRITE_NOT_EXISTS_READ,
            self::MODE_WRITE_REPLACE,
            self::MODE_WRITE_REPLACE_READ,
        ]);

        return $canWrite;
    }

    /**
     * Asserts that the current mode is set to a valid mode to write to a file or throws an
     * exception if not.
     *
     * @throws InvalidOperationException if the current mode does not allow writing to the file
     */
    protected function assertModeAllowsWriting()
    {

        if (!self::canModeWrite($this->mode)) {
            throw new InvalidOperationException('File open mode ' . $this->mode . ' cannot be used for writing');
        }
    }

    /**
     * Asserts that the current file open mode will not append to an existing file while writing
     * to it
     * @throws FileAccessException
     */
    protected function assertModeWillNotAppend()
    {
        if ($this->mode == self::MODE_WRITE_APPEND
            || $this->mode == self::MODE_WRITE_APPEND_READ) {

            throw new FileAccessException('Invalid operation over file open with append mode');
        }
    }

    /**
     * Checks if the file is open. If it is not open and the autoOpen flag is set then the open()
     * method is called.
     *
     * If the file is not open and the autoOpen flag is not set then an exception is thrown.
     *
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     */
    protected function checkIsOpen()
    {
        if (!$this->handle && !$this->autoOpen) {
            throw new FileAccessException('File not open');
        }
        else if (!$this->handle) {
            $this->open();
        }
        else if ($this->modificationCount != $this->path->getModificationCount()) {
            throw new InvalidOperationException('File path have been modified while the file was open');
        }
    }

    /**
     * Closes the file handler.
     */
    public function close()
    {
        if ($this->handle) {
            fclose($this->handle);
            $this->handle = null;
            return true;
        }

        return false;
    }

    /**
     * Moves the file pointer to the specified position.
     *
     * This method uses the same parameters as fseek does.
     *
     * This operation is available only if the file have not been open in append mode.
     *
     * @param int $offset
     * @param int $whence Defaults to SEEK_SET (offset from file start).
     * @return number
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     * @see fseek
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();
        $this->assertModeWillNotAppend();

        if (is_string($offset) && $offset == 'start') {
            $offset = 0;
            $whence = SEEK_SET;
        }
        else if (is_string($offset) && $offset == 'end') {
            $offset = 0;
            $whence = SEEK_END;
        }

        $offset = (int)$offset;

        switch ($whence) {
            case SEEK_CUR:
                break;
            case SEEK_END:
                if ($offset > 0) {
                    throw new InvalidArgumentException('Cannot seek from file end with a positive offset: ' . $offset);
                }
                break;
            case SEEK_SET:
                if ($offset < 0) {
                    throw new InvalidArgumentException('Cannot seek from file start with a negative offset: ' . $offset);
                }
                break;
            default:
                throw new InvalidArgumentException(
                    'Invalid $whence value. Must be one of the PHP constants SEEK_SET, SEEK_CUR, SEEK_END');
        }

        $result = fseek($this->handle, $offset, $whence);

        if ($result == -1) {
            throw new FileAccessException('Failed seek to ' . $offset . ' from ' . $whence);
        }

        return $result;
    }

    /**
     * Rewinds the file pointer to the beginning of the file.
     *
     * This operation is available only if the file have not been open in append mode.
     *
     * @return boolean
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     */
    public function rewind()
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();
        $this->assertModeWillNotAppend();

        return rewind($this->handle);
    }

    /**
     * Gets the file pointer current offset.
     *
     * This operation is available only if the file have not been open in append mode.
     *
     * @return int
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     */
    public function getOffset()
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();
        $this->assertModeWillNotAppend();

        return ftell($this->handle);
    }

    /**
     * Checks if the file
     * @return boolean
     */
    public function isEof()
    {
        return $this->isOpen() && feof($this->handle);
    }

    /**
     * Writes up to $maxBytes from $data to the file.
     *
     * @param string $data Bytes to write to the file
     * @param int $maxBytes Maximum number of bytes to write.
     * @return int number of bytes written
     * @throws FileAccessException If the write operation fails or if it could not write up to
     * $maxBytes bytes.
     */
    protected function internalWrite($data, $maxBytes)
    {
        // Always provide the length to avoid
        $maxBytes = (int)$maxBytes;
        $len = strlen($data);

        if ($maxBytes < 1) {
            $maxBytes = $len;
        }

        // Nothing to write
        if ($len == 0) {
            return 0;
        }

        $byteCount = fwrite($this->handle, $data, $maxBytes);

        if ($byteCount === false) {
            throw new FileAccessException('Failed to write ' . $len . ' bytes');
        }
        // If we wrote less bytes than expected throw an exception
        else if ($byteCount != $maxBytes && $maxBytes <= $len) {
            throw new FileAccessException('Cannot write requested ' . $len . ' bytes, only wrote ' . $byteCount . ' bytes');
        }

        return $byteCount;
    }

    /**
     * Writes CSV fields to a file using PHP fputcsv function.
     *
     * Returns the number of characters written.
     *
     * @param array $csvFields
     * @param string $delimiter
     * @param string $enclosure
     * @return int
     * @throws FileAccessException
     */
    protected function internalWriteCsvLine(array $csvFields, $delimiter = null, $enclosure = null)
    {

        // Properly call the function following declared parameters. Null parameters cannot be used
        // as they are not valid values and are not replaced by default values

        if ($delimiter && $enclosure) {
            $writtenChars = fputcsv($this->handle, $csvFields, $delimiter, $enclosure);
        }
        else if ($delimiter) {
            $writtenChars = fputcsv($this->handle, $csvFields, $delimiter);
        }
        else {
            $writtenChars = fputcsv($this->handle, $csvFields);
        }

        if ($writtenChars === false) {
            throw new FileAccessException('Failed to write CSV line');
        }

        return $writtenChars;
    }

    /**
     * Writes data to the file.
     *
     * If the file is not open it will be open if the autoOpen flag is set to true or throw an
     * Exception if is set to false.
     *
     * Asserts that the current file operation mode is compatible with write operations.
     *
     * @param string $data
     * @param int $maxBytes
     * @return int Number of bytes written
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow writing to the file
     */
    public function write($data, $maxBytes = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        return $this->internalWrite($data, $maxBytes);
    }

    /**
     * Writes all the data from the array values into the file.
     *
     * If the file is not open it will be open if the autoOpen flag is set to true or throw an
     * Exception if is set to false.
     *
     * Asserts that the current file operation mode is compatible with write operations.
     *
     * @param array|Traversable $data
     * @return int the total count of bytes written to the file
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow writing to the file
     * @see File::write
     */
    public function writeAll($data)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();
        $totalWrote = 0;

        if (is_string($data)) {
            $data = [$data];
        }

        if (is_array($data) || is_a($data, '\\Traversable')) {
            foreach ($data as $key => $dataItem) {
                if (!is_string($dataItem)) {
                    throw new InvalidArgumentException('Cannot write type ' . Types::getTypeName($dataItem) . ' with key ' . $key . ' to file');
                }
                $totalWrote += $this->internalWrite($dataItem, strlen($dataItem));
            }
        }
        else {
            throw new InvalidArgumentException('Cannot write type ' . Types::getTypeName($data) . ' to file');
        }

        return $totalWrote;
    }

    /**
     * Reads up to $maxBytes from the file.
     * @param int $maxBytes Maximum number of bytes to read
     * @return string Bytes of data read
     * @throws FileAccessException If the file pointer is at the end of the file or the read
     * operation fails.
     * @throws InvalidArgumentException If $maxBytes is less than 1
     */
    protected function internalRead($maxBytes)
    {
        $maxBytes = (int)$maxBytes;

        if ($maxBytes < 1) {
            throw new InvalidArgumentException('Invalid number of bytes to read. Must be greater than 0');
        }

        if (feof($this->handle)) {
            throw new FileAccessException('End of file reached');
        }

        $readData = fread($this->handle, $maxBytes);

        if ($readData === false) {
            throw new FileAccessException('Cannot read from file');
        }

        return $readData;
    }

    /**
     * @param int|null $maxLineLength Maximum length of the line to be read or null to ignore it
     * @return bool|string The line read or false if not line could be read
     * @throws FileAccessException If the read operation fails
     */
    protected function internalReadLine($maxLineLength)
    {
        if ($maxLineLength !== null) {
            $maxLineLength = (int)$maxLineLength;
        }

        if ($maxLineLength !== null && $maxLineLength < 1) {
            throw new InvalidArgumentException('Invalid maximum line length. Must be greater than 0');
        }

        if (feof($this->handle)) {
            // Users must check for end of file before trying to read
            throw new FileAccessException('End of file reached');
        }

        if ($maxLineLength !== null) {
            $readData = fgets($this->handle, $maxLineLength);
        }
        else {
            $readData = fgets($this->handle);
        }

        if ($readData === false && !feof($this->handle)) {
            // This is an unexpected error
            throw new FileAccessException('Failed to read from file');
        }
        else if ($readData === false) {
            // The file pointer was at the end of the file but a last line could not be read :?
            return false;
        }

        return $readData;
    }

    /**
     * Reads a line from a CSV file using PHP fgetcsv function.
     *
     * @param string $maxLineLength
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return array
     * @throws FileAccessException
     * @throws InvalidArgumentException
     */
    protected function internalReadCsvLine($maxLineLength = null, $delimiter = null, $enclosure = null, $escape = null)
    {
        if ($maxLineLength !== null) {
            $maxLineLength = (int)$maxLineLength;
        }

        if ($maxLineLength !== null && $maxLineLength < 1) {
            throw new InvalidArgumentException('Invalid maximum line length. Must be greater than 0');
        }

        if (feof($this->handle)) {
            throw new FileAccessException('End of file reached');
        }

        // Properly call the function following declared parameters. Null parameters cannot be used
        // as they are not valid values and are not replaced by default values

        if ($delimiter && $enclosure && $escape) {
            $readData = fgetcsv($this->handle, $maxLineLength, $delimiter, $enclosure, $escape);
        }
        else if ($delimiter && $enclosure) {
            $readData = fgetcsv($this->handle, $maxLineLength, $delimiter, $enclosure);
        }
        else if ($delimiter) {
            $readData = fgetcsv($this->handle, $maxLineLength, $delimiter);
        }
        else {
            $readData = fgetcsv($this->handle, $maxLineLength);
        }

        return $readData;
    }

    /**
     * Reads data from a file.
     *
     * If the file is not open it will be open if the autoOpen flag is set to true or throw an
     * Exception if is set to false.
     *
     * Asserts that the current file operation mode is compatible with read operations.
     *
     * @param int $maxBytes Maximum number of bytes to read. This parameter is required and must
     * be an integer greater or equal to 1 (one byte read).
     * @return string data read
     * @throws FileAccessException if the file read operation fails
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidArgumentException if $maxBytes is less than 1
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     */
    public function read($maxBytes)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();

        return $this->internalRead($maxBytes);
    }

    /**
     * Reads all the data from the file and returns it as an array of all data chunks read.
     *
     *
     * @param int $chunkSize Size of the chunks to be read. Defaults to File::DEFAULT_CHUNK_SIZE
     * @return array of data chunks read as strings
     * @throws FileAccessException if the file read operation fails
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow reading from the file
     */
    public function readAll($chunkSize = null)
    {

        $this->checkIsOpen();
        $this->assertModeAllowsReading();

        $chunkSize = (int)$chunkSize;

        if ($chunkSize === null || (int)$chunkSize < 1) {
            // We will not further check this value as the setter already does it and by default
            // is set to self::DEFAULT_CHUNK_SIZE
            $chunkSize = $this->getReadChunkSize();
        }

        $data = [];

        while (!feof($this->handle)) {
            $data[] = $this->internalRead($chunkSize);
        }

        return $data;
    }

    /**
     * Flushes all pending writes to the file on disk if the file is open.
     *
     * If the file is not open it will be open if the autoOpen flag is set to true or throw an
     * Exception if is set to false.
     *
     * Asserts that the current file operation mode is compatible with read operations.
     *
     * @return True if the operation succeeds or false if not
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow writing to the file
     */
    public function flush()
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        return fflush($this->handle);
    }

    /**
     * Calls fstat over the file pointer and returns it.
     *
     * If the file is not open it will be open if the autoOpen flag is set to true or throw an
     * Exception if is set to false.
     *
     * @return array
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     */
    public function stat()
    {
        $this->checkIsOpen();

        $stat = fstat($this->handle);

        return $stat;
    }

    /**
     * Truncates file to given size.
     *
     * If the size if greater than current file size it will be filled with NULL bytes.
     *
     * @param int $size Positive integer size
     * @return bool
     * @throws InvalidArgumentException If $size is less than 0
     * @throws FileAccessException If the file is not open and autoOpen is not set to true.
     * @throws InvalidOperationException if the current mode does not allow writing to the file
     */
    public function truncate($size)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        $size = (int)$size;

        if ($size < 0) {
            throw new InvalidArgumentException('Cannot truncate file with negative size: ' . $size);
        }

        $truncated = ftruncate($this->handle, $size);

        return $truncated;
    }
}

