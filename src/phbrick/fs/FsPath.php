<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\fs;

use DirectoryIterator;
use InvalidArgumentException;
use OutOfBoundsException;
use phbrick\collection\IdentifierSequence;
use phbrick\exceptions\DirectoryAccessException;
use phbrick\exceptions\FileAccessException;
use phbrick\exceptions\InvalidOperationException;
use phbrick\string\Strings;
use phbrick\types\MimeInfo;

/**
 * Models the path to a filesystem entry.
 */
class FsPath extends IdentifierSequence
{

    /**
     * @param FsPath $fsPath
     * @return FsPath
     */
    public static function castFsPath(FsPath $fsPath)
    {
        return $fsPath;
    }

    const DIRECTORY_SEPARATOR = '/';
    const PARENTREF = '..';
    const THISREF = '.';

    const MODE_RWX = 'rwx';
    const MODE_RW = 'rw';
    const MODE_RX = 'rx';
    const MODE_R = 'r';
    const MODE_W = 'w';
    const MODE_X = 'x';

    const TYPE_FILE = 'file';
    const TYPE_FOLDER = 'folder';
    const TYPE_NONE = 'none';

    /**
     * @var bool
     */
    private $resolveParentFolders = true;

    /**
     * Gets if parent folders notation is followed or removed
     * @return bool
     */
    public function isResolveParentFolders()
    {
        return $this->resolveParentFolders;
    }

    /**
     * Sets if parent folders notation is followed or removed
     * @param bool $resolveParentFolders
     */
    public function setResolveParentFolders($resolveParentFolders)
    {
        $this->resolveParentFolders = (bool)$resolveParentFolders;
    }

    /**
     * @var string
     */
    private $root = null;

    /**
     * Gets a root for the path
     * @return string
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Sets a root for the path
     * @param string $root
     */
    public function setRoot($root)
    {
        if ($root !== null) {
            $root = trim($root, "\r\n\t ");
        }

        if (strlen($root) > 1) {
            $root = rtrim($root, $this->getSeparator());
        }

        $this->root = $root;
    }

    /**
     * Gets the type of path that is either file, folder or none
     * @return string
     */
    public function getPathType()
    {

        $fileName = $this->toString();

        if (is_dir($fileName)) {
            return self::TYPE_FOLDER;
        }
        else if (is_file($fileName)) {
            return self::TYPE_FILE;
        }
        else {
            return self::TYPE_NONE;
        }
    }

    /**
     * Sets the directory separator character.
     *
     * By default is set to /
     *
     * @param string $separator
     * @see \phbrick\collection\IdentifierSequence::setSeparator()
     */
    public function setSeparator($separator)
    {
        if (strlen($separator) != 1 || empty($separator)) {
            throw new InvalidArgumentException(
                'Separator must be a non-empty string with only one character');
        }

        $oldSeparator = $this->getSeparator();

        if ($oldSeparator == $separator) {
            return;
        }

        parent::setSeparator($separator);

        if (!empty($this->root)) {
            $this->root = str_replace($oldSeparator, $separator, $this->root);
        }
    }

    public function __construct($path = null, $separator = self::DIRECTORY_SEPARATOR)
    {

        $this->setSeparator($separator);

        parent::__construct($path);
    }

    /**
     * Replaces path delimiter and dots sequences by single dots and single delimiter to avoid
     * paths that specify parent folders.
     *
     * @param string|array $sequence
     * @return array the sanitized segments of the path as an array
     */
    protected function sanitizeSequence($sequence)
    {
        $sequence = parent::sanitizeSequence($sequence);

        $sequenceLen = count($sequence);

        // Iterate each position and process .. and . references after trimming up each sequence
        // segment
        for ($pos = 0; $pos < $sequenceLen; $pos++) {

            // In PHP arrays numerical indexes work like map keys, if we unset an index the rest
            // will not be affected, so iterating all indexes is OK while removing some of them.

            $segment = trim($sequence[$pos], "\r\n\t " . $this->getSeparator());
            $sequence[$pos] = $segment;

            if ($segment == self::THISREF
                || ($segment == self::PARENTREF && !$this->resolveParentFolders)) {
                unset($sequence[$pos]);
            }
            else if ($segment == self::PARENTREF && $this->resolveParentFolders && $pos == 0) {
                throw new OutOfBoundsException('Tried to follow parent folder reference on root path segment');
            }
            else if ($segment == self::PARENTREF && $this->resolveParentFolders) {
                unset($sequence[$pos]);
                unset($sequence[$pos - 1]);
            }
        }

        // reset indexes
        $sequence = array_values($sequence);

        return $sequence;
    }

    protected function prepareSequence($sequence, $sanitize = true)
    {
        $preparedSequence = parent::prepareSequence($sequence, $sanitize);

        // $sequence must be an iterable, so using square bracket syntax to get the first element
        // or  character works the same for both
        if (!empty($sequence) && (is_a($sequence, '\\phbrick\\fs\\FsPath') || $sequence[0] == $this->getSeparator())) {
            $this->setRoot($this->getSeparator());
        }

        return $preparedSequence;
    }

    public function toString()
    {
        $result = '';

        if (!empty($this->root)) {
            $result .= rtrim($this->root, $this->getSeparator()) . $this->getSeparator();
        }

        $result .= parent::toString();

        return $result;
    }

    public function toArray()
    {
        $parts = parent::toArray();

        if (!empty($this->root)) {
            array_splice($parts, 0, 0, ['']);
        }

        return $parts;
    }

    /**
     * Gets if the current path is empty
     *
     * @return bool
     * @see \phbrick\collection\AbstractCollection::isEmpty()
     */
    public function isEmpty()
    {
        return parent::isEmpty() && empty($this->root);
    }

    /**
     * Gets if the current path exists as a file, directory or symbolic link.
     * @return bool
     */
    public function exists()
    {
        return file_exists($this->toString());
    }

    /**
     * Gets if the current path is a file
     * @return bool
     */
    public function isFile()
    {
        return is_file($this->toString());
    }

    /**
     * Gets if the current path is a directory
     * @return bool
     */
    public function isDirectory()
    {
        return is_dir($this->toString());
    }

    /**
     * Gets if the current path is a symbolic link
     * @return bool
     */
    public function isSymbolicLink()
    {
        return is_link($this->toString());
    }

    /**
     * Gets if the given permissions are allowed on the path.
     * @param string $mode one of the MODE_* constants or any combination of letters 'rwx'.
     * @return bool
     */
    public function isAllowedTo($mode)
    {
        $permissions = str_split($mode);
        $allowed = true;
        $fileName = $this->toString();

        foreach ($permissions as $permission) {
            if ($permission == 'r') {
                $allowed = $allowed && is_readable($fileName);
            }
            else if ($permission == 'w') {
                $allowed = $allowed && is_writable($fileName);
            }
            else if ($permission == 'x') {
                $allowed = $allowed && is_executable($fileName);
            }
        }

        return $allowed;
    }

    /**
     * Gets the last portion of the path, that is the name of the current directory or file this
     * path points to.
     * @return string
     * @example The base name of /home/user/file.txt is file.txt
     */
    public function getBaseName()
    {
        return pathinfo($this->toString(), PATHINFO_BASENAME);
    }

    /**
     * Gets the parent path.
     * @return mixed
     * @example The directory name of /home/user/file.txt is /home/user
     */
    public function getDirName()
    {
        return pathinfo($this->toString(), PATHINFO_DIRNAME);
    }

    /**
     * Gets the last portion of the path without the extension.
     *
     * If the path is a directory this might return the same as FsPath::getDirName() if it does not
     * contains any dots
     *
     * @return string
     * @example The file name of /home/user/file
     */
    public function getFileName()
    {
        return pathinfo($this->toString(), PATHINFO_FILENAME);
    }

    /**
     * Gets the extension of the last path portion.
     *
     * If the filename does not have any dots then this function returns an empty string
     * @return string
     */
    public function getExtension()
    {
        return pathinfo($this->toString(), PATHINFO_EXTENSION);
    }

    /**
     * Gets the mime information from the path
     * @return array A map with two keys ('type' and 'encoding') with the MIME type and encoding of
     * the file.
     * @throws InvalidOperationException If the path is empty, the path does not exists or the
     * path is a folder.
     */
    public function getMime()
    {
        if ($this->isEmpty()) {
            throw new InvalidOperationException('Cannot get MIME information from an empty path');
        }

        $pathType = $this->getPathType();

        if ($pathType == self::TYPE_NONE) {
            throw new InvalidOperationException('Cannot get MIME information from a not existent filesystem path');
        }

        if ($pathType == self::TYPE_FOLDER) {
            throw new InvalidOperationException('Cannot get MIME information from a folder path');
        }

        $mimeInfo = new MimeInfo();

        $mime = $mimeInfo->getFileMime($this->toString());

        return $mime;
    }

    /**
     * Renames the current path base name to the one given while keeping the current directory.
     *
     * @param string $baseName New base name.
     * @return bool
     * @throws InvalidOperationException If the current path is empty
     * @throws InvalidArgumentException If the input base name is empty or contains the directory
     * separator.
     */
    public function renameBasename($baseName)
    {

        if ($this->isEmpty()) {
            throw new InvalidOperationException('Cannot rename an empty path');
        }
        else if (empty($baseName)) {
            throw new InvalidArgumentException('The new base name cannot be empty');
        }
        else if (strstr($baseName, $this->getSeparator()) !== false) {
            throw new InvalidArgumentException(
                'The base name must not contain the directory separator character');
        }

        $newPath = $this->getDirName() . $this->getSeparator() . $baseName;

        return $this->rename($newPath);
    }

    /**
     * Renames the current path directory to the one given while keeping the current base name.
     *
     * @param $directory
     * @return bool If the current path is empty
     * @internal param string $newName New directory name
     */
    public function renameDirectoryName($directory)
    {
        if ($this->isEmpty()) {
            throw new InvalidOperationException('Cannot rename an empty path');
        }
        else if (empty($directory)) {
            throw new InvalidArgumentException('The new directory cannot be empty');
        }

        $newPath = $directory . $this->getSeparator() . $this->getBaseName();

        return $this->rename($newPath);
    }

    /**
     * Moves the current path to another path.
     *
     * @param string $newPath
     * @return boolean
     * @throws InvalidOperationException If the current path is empty
     */
    public function rename($newPath)
    {
        if ($this->isEmpty()) {
            throw new InvalidOperationException('Cannot rename an empty path');
        }

        $oldPath = $this->toString();
        $renamed = rename($oldPath, $newPath);

        if ($renamed) {
            $preparedSequence = $this->prepareSequence($newPath, true);
            $this->clear();
            $this->initialize($preparedSequence);
        }

        return $renamed;
    }

    /**
     * Returns the parent path (the directory name) of this path as a new FsPath instance.
     * @return FsPath
     */
    public function getParentPath()
    {
        if ($this->isEmpty()) {
            throw new InvalidOperationException('An empty path has no parent');
        }

        return new FsPath(pathinfo($this->toString(), PATHINFO_BASENAME));
    }

    /**
     * Deletes the file or directory this path refers to.
     *
     * In case a directory is not empty this method will throw an exception if recursive flag is
     * not set to true.
     *
     * This method throws an exception if the path is not writable and thus can not be deleted.
     *
     * @param bool $recursive If true directory contents are first tried to be removed before
     * trying to remove it.
     * @throws DirectoryAccessException
     * @throws FileAccessException
     */
    public function delete($recursive = false)
    {
        if ($this->isEmpty()) {
            throw new InvalidOperationException('Cannot delete empty path');
        }

        $fName = $this->toString();

        // We can remove a non-writable file if the operation is forced
        if (!$this->isAllowedTo(self::MODE_W)) {
            if ($this->isFile()) {
                throw new FileAccessException('Cannot remove write-protected path ' . $fName);
            }
            else if ($this->isDirectory()) {
                throw new DirectoryAccessException('Cannot remove write-protected path ' . $fName);
            }
        }

        if ($this->isSymbolicLink() || $this->isFile()) {
            $unlinked = unlink($fName);

            if (!$unlinked) {
                throw new FileAccessException('Cannot remove path ' . $fName);
            }
        }
        else if ($this->isDirectory()) {
            $iterator = new DirectoryIterator($fName);

            // The parent folder is writable

            foreach ($iterator as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }

                if (($fileInfo->isFile() || $fileInfo->isLink()) && !$fileInfo->isWritable()) {
                    throw new FileAccessException('Cannot remove write-protected path ' . $fileInfo->getRealPath());
                }
                else if (($fileInfo->isFile() || $fileInfo->isLink()) && !unlink($fileInfo->getRealPath())) {
                    throw new FileAccessException('Cannot remove path ' . $fileInfo->getRealPath());
                }
                else if ($fileInfo->isDir() && !$fileInfo->isWritable()) {
                    throw new DirectoryAccessException('Cannot remove write-protected path ' . $fileInfo->getRealPath());
                }
                else if ($fileInfo->isDir()) {
                    $subPath = self::instance($fileInfo->getRealPath());
                    $subPath->delete($recursive);
                }
            }

            rmdir($fName);
        }
    }

    /**
     * Creates an empty file for the current path
     *
     * @param int $modePermissions Octal permissions for the new file
     * @return boolean true
     * @throws FileAccessException
     * @throws InvalidOperationException
     */
    public function createFile($modePermissions = null)
    {
        if ($this->exists()) {
            throw new InvalidOperationException('Cannot create a file over an existent path');
        }

        if (!is_writable($this->getDirName())) {
            throw new FileAccessException('Cannot create file, parent directory is not writable');
        }

        if (!touch($this->toString())) {
            throw new FileAccessException('Cannot create file: ' . $this);
        }

        if (!chmod($this->toString(), $modePermissions)) {
            throw new FileAccessException('Cannot set file permissions');
        }

        return true;
    }

    /**
     * Creates an empty folder for the current path
     * @param int $modePermissions Octal permissions for the new folder
     * @return bool
     * @throws DirectoryAccessException
     * @throws FileAccessException
     */
    public function createFolder($modePermissions = null)
    {
        if ($this->exists()) {
            throw new InvalidOperationException('Cannot create a file over an existent path');
        }

        if (!is_writable($this->getDirName())) {
            throw new DirectoryAccessException('Cannot create file, parent directory is not writable');
        }

        if (!mkdir($this->toString(), $modePermissions, true)) {
            throw new FileAccessException('Cannot create directory: ' . $this);
        }

        return true;
    }

    /**
     * Changes the file/directory modification and access time stamps.
     *
     * @param int $modifiedTimeStamp
     * @param int $accessTimeStamp
     * @return boolean
     */
    public function touch($modifiedTimeStamp = null, $accessTimeStamp = null)
    {
        $fName = $this->toString();

        return touch($fName, (int)$modifiedTimeStamp, (int)$accessTimeStamp);
    }

    /**
     * Parses a path string and returns an FsPath instance
     * @param string $path
     * @return FsPath
     */
    public static function parsePath($path)
    {
        $result = new FsPath($path);

        return $result;
    }

    /**
     * Create temporal directory
     *
     * If no $targetFolder value is set then the PHP tmp system folder will be used.
     *
     * @param string $prefix Prefix to append to the generated folder name
     * @param string $targetFolder Target folder to create the temporal directory
     * @param int|number $perms Octal-mode permissions for folder. Defaults to 0700
     * @return FsPath If the temporal folder path cannot be created
     */
    public static function createTmpDir($prefix = null, $targetFolder = null, $perms = 0700)
    {

        if (empty($targetFolder)) {
            $targetFolder = sys_get_temp_dir();
        }

        $name = $prefix
            . Strings::random(8, Strings::CHARSET_ALNUM);
        $limit = 5;
        while (--$limit > 0 && is_dir("$targetFolder/$name")) {
            $name = $prefix
                . Strings::random(8, Strings::CHARSET_ALNUM);
        }

        if ($limit == 0) {
            throw new InvalidOperationException('Could not create a temporal directory');
        }

        if (!is_dir("$targetFolder/$name")) {
            mkdir("$targetFolder/$name", $perms, true);
        }

        /** @var FsPath $new */
        $new = self::instance("$targetFolder/$name");
        return FsPath::castFsPath($new);
    }

    /**
     * Creates a temporal file name.
     *
     * @param string $prefix
     * @param string $targetFolder
     * @return FsPath the path
     */
    public static function createTmpFile($prefix = null, $targetFolder = null)
    {

        if (empty($targetFolder)) {
            $targetFolder = sys_get_temp_dir();
        }

        $name = tempnam($targetFolder, $prefix);
        $limit = 5;
        while (--$limit > 0 && !$name) {
            $name = tempnam($targetFolder, $prefix);
        }

        if (!is_file("$name")) {
            mkdir("$name", 0750, true);
        }

        /** @var FsPath $new */
        $new = self::instance($name);
        return FsPath::castFsPath($new);
    }
}
