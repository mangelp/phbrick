<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\fs;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\exceptions\FileAccessException;
use phbrick\exceptions\NotIterableException;
use phbrick\exceptions\TypeException;
use phbrick\types\Types;
use Traversable;

/**
 * Models both read/write operations over a CSV file using plain arrays or an object model for each
 * CSV line.
 */
class CsvFile extends BaseStrictClass
{

    /**
     * File to read/write CSV lines
     * @var File
     */
    private $textFile = null;

    /**
     * @var CsvFormatOptions
     */
    private $options = null;

    /**
     * Gets CSV file format options
     * @return CsvFormatOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets CSV file format options
     * @param CsvFormatOptions $options
     */
    public function setOptions(CsvFormatOptions $options)
    {
        $this->options = $options;
    }

    /**
     * Gets the current file path or null.
     *
     * @return FsPath|null
     */
    public function getPath()
    {
        if ($this->textFile) {
            return $this->textFile->getPath();
        }

        return null;
    }

    public function __construct($path, $mode, CsvFormatOptions $options = null)
    {
        if (is_string($path)) {
            $path = FsPath::parsePath($path);
        }
        else if (is_a($path, 'phbrick\\fs\\FsPath')) {
            $path = FsPath::castFsPath($path);
        }
        else {
            throw new InvalidArgumentException('Path must be an string or an FsPath instance');
        }

        $this->assertValidMode($mode);

        if ($options) {
            $this->setOptions($options);
        }

        $this->textFile = new TextFile($path, $mode);
        $this->textFile->setAutoOpen(true);
    }

    /**
     * Opens the CSV file. If it is already open it does nothing.
     */
    public function open()
    {
        if ($this->textFile) {
            $this->textFile->open();
        }
    }

    /**
     * Closes the CSV file. If it is already closed it does nothing.
     */
    public function close()
    {
        if ($this->textFile) {
            $this->textFile->close();
        }
    }

    public function reOpen($mode)
    {
        if ($this->textFile) {
            $this->textFile->close();
            $this->textFile->open($mode);
        }
    }

    public function isOpen()
    {
        if ($this->textFile) {
            return $this->textFile->isOpen();
        }
        else {
            return false;
        }
    }

    public function isEof()
    {
        if ($this->textFile) {
            return $this->textFile->isEof();
        }
        else {
            return false;
        }
    }

    public function __destruct()
    {
        if ($this->textFile) {
            $this->textFile->close();
            $this->textFile = null;
        }
    }

    /**
     * Asserts that the mode is one of the supported ones. This class does not support modes that
     * allow to read and write at the same time.
     *
     * @param string $mode
     * @throws InvalidArgumentException If the mode is invalid
     */
    protected function assertValidMode($mode)
    {
        switch ($mode) {
            case File::MODE_READ_WRITE:
                // Fall back to next option
            case File::MODE_WRITE_APPEND_READ:
                // Fall back to next option
            case File::MODE_WRITE_NOT_EXISTS_READ:
                // Fall back to next option
            case File::MODE_WRITE_REPLACE_READ:
                throw new InvalidArgumentException('CsvFile allows only READ or WRITE modes, but not both.');
        }
    }

    /**
     * Asserts that the current mode is "read"
     * @throws InvalidArgumentException
     */
    protected function assertModeRead()
    {
        if ($this->textFile->getMode() == File::MODE_READ) {
            throw new InvalidArgumentException('Current file mode cannot read');
        }
    }

    /**
     * Asserts that the current mode is "write"
     * @throws InvalidArgumentException
     */
    protected function assertModeWrite()
    {
        if ($this->textFile->getMode() != File::MODE_READ) {
            throw new InvalidArgumentException('Current file mode cannot write');
        }
    }

    /**
     * Asserts that an options object is set
     * @throws InvalidArgumentException
     */
    protected function assertOptions()
    {
        if (!$this->options) {
            throw new InvalidArgumentException('Missing Csv file format options');
        }
    }

    /**
     * Gets if the CSV file is in read mode.
     * This class only allows to write or read from a CSV file, but not both.
     */
    public function isReadMode()
    {
        return $this->textFile
            && $this->textFile->getMode() == File::MODE_READ;
    }

    /**
     * Gets if the CSV file is in write mode.
     * This class only allows to write or read from a CSV file, but not both.
     */
    public function isWriteMode()
    {
        return $this->textFile
            && $this->textFile->getMode() != File::MODE_READ;
    }

    /**
     * Reads a single record from CSV file
     * @return boolean|array
     */
    public function read()
    {
        $this->assertOptions();

        if ($this->textFile->isEof()) {
            return false;
        }

        $csvLine = $this->textFile->readCsvLine(
            $this->options->getMaxLineLength(),
            $this->options->getFieldDelimiter(),
            $this->options->getTextDelimiter(),
            $this->options->getEscapeCharacter()
        );

        if (!$csvLine) {
            return false;
        }

        $record = $this->createRecord($csvLine);

        return $record;
    }

    /**
     * Reads all records from the CsvFile
     * @return array All read records or an empty array
     */
    public function readAll()
    {
        $this->assertOptions();

        if ($this->textFile->isEof()) {
            return [];
        }

        $data = [];

        while (!$this->textFile->isEof()) {
            $csvLine = $this->textFile->readCsvLine(
                $this->options->getMaxLineLength(),
                $this->options->getFieldDelimiter(),
                $this->options->getTextDelimiter(),
                $this->options->getEscapeCharacter()
            );

            if (!$csvLine) {
                break;
            }

            $data[] = $this->createRecord($csvLine);
        }

        return $data;
    }

    /**
     * Writes a record to the CSV file
     * @param array|object $record
     * @return boolean true if the record was written or false if not
     * @throws FileAccessException
     */
    public function write($record)
    {
        $this->assertOptions();

        $csvLineFields = $this->createCsvLine($record);

        $result = $this->textFile->writeCsvLine(
            $csvLineFields,
            $this->options->getFieldDelimiter(),
            $this->options->getTextDelimiter()
        );

        if ($result !== false) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Writes all the given elements to the CSV file.
     *
     * @param array|Traversable $iterable
     * @return bool|int False if no record was written or the number of records written
     * @throws FileAccessException
     * @throws NotIterableException
     * @throws TypeException
     */
    public function writeAll($iterable)
    {
        $this->assertOptions();

        Types::assertIterable($iterable);
        $counter = 0;

        foreach ($iterable as $data) {
            if ($this->write($data)) {
                ++$counter;
            }
        }

        if ($counter == 0) {
            return false;
        }
        else {
            return $counter;
        }
    }

    /**
     * Creates a record from a tuple of values read from the CSV file.
     *
     * Allows to model records as instances of a class or process the read values before being
     * used.
     *
     * @param array $csvLine
     * @return array|object
     */
    protected function createRecord(array $csvLine)
    {
        return $csvLine;
    }

    /**
     * Creates an array with the tuple of values to be written to the CSV file.
     *
     * Allows to model records as instances of a class or process the values before writing them
     * to the CSV file.
     *
     * @param array|object $record
     * @return array|object
     */
    protected function createCsvLine($record)
    {
        return $record;
    }
}

