<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\fs;

use InvalidArgumentException;
use phbrick\exceptions\FileAccessException;
use phbrick\string\Strings;
use phbrick\types\Types;
use Traversable;

/**
 * File resource that stores text that can be read or written.
 *
 * Extends File class and adds text operations.
 */
class TextFile extends File
{

    /**
     * @var int|null
     */
    private $maxLineLength = null;

    /**
     * Gets the maximum line length to read
     * @return int|null
     */
    public function getMaxLineLength()
    {
        return $this->maxLineLength;
    }

    /**
     * Sets the maximum line length to read
     * @param int|null $maxLineLength
     */
    public function setMaxLineLength($maxLineLength)
    {
        if ($maxLineLength !== null) {
            $maxLineLength = (int)$maxLineLength;
        }

        if ($maxLineLength !== null && $maxLineLength < 1) {
            throw new InvalidArgumentException('Invalid maximum line length. Must be greater than 0');
        }

        $this->maxLineLength = $maxLineLength;
    }

    /**
     * Reads a line of text with an optional maximum length.
     *
     * Returns the string read or false if no line could be read.
     *
     * Line breaks are removed from the line before is returned.
     *
     * @param int $maxLineLength
     * @return string|false
     * @throws FileAccessException
     */
    public function readLine($maxLineLength = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();

        if ($this->isEof()) {
            return false;
        }

        if ($maxLineLength === null) {
            $maxLineLength = $this->maxLineLength;
        }

        $line = $this->internalReadLine($maxLineLength);

        if ($line === false) {
            // File end found while trying a new read
            return false;
        }

        $line = rtrim($line, "\n\r");

        return $line;
    }

    /**
     * Reads all the remaining lines in the file until EOF is reached.
     *
     * If there are no more lines to be read returns an empty array.
     *
     * @param int $maxLineLength
     * @return array Lines read
     * @throws FileAccessException
     */
    public function readAllLines($maxLineLength = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();
        $result = [];
        $line = null;

        if ($maxLineLength === null) {
            $maxLineLength = $this->maxLineLength;
        }

        while (!$this->isEof()) {
            $line = $this->internalReadLine($maxLineLength);
            $line = rtrim($line, "\n\r");
            $result[] = $line;

            $line = null;
        }

        return $result;
    }

    /**
     * Reads a CSV line from the CSV file.
     *
     * Returns false if there is no more lines to be read.
     *
     * @param string $maxLineLength
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return bool|array
     * @throws FileAccessException
     */
    public function readCsvLine($maxLineLength = null, $delimiter = null, $enclosure = null, $escape = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();

        if ($this->isEof()) {
            return false;
        }

        if ($maxLineLength === null) {
            $maxLineLength = $this->maxLineLength;
        }

        return $this->internalReadCsvLine($maxLineLength, $delimiter, $enclosure, $escape);
    }

    /**
     * Reads all the remaining lines from a CSV file.
     *
     * If there are no more lines to be read it returns an empty array.
     *
     * @param string $maxLineLength
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return array
     * @throws FileAccessException
     */
    public function readAllCsvLines($maxLineLength = null, $delimiter = null, $enclosure = null, $escape = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsReading();

        $result = [];
        $line = null;

        if ($maxLineLength === null) {
            $maxLineLength = $this->maxLineLength;
        }

        while (!$this->isEof()) {
            $line = $this->internalReadCsvLine($maxLineLength, $delimiter, $enclosure, $escape);

            if ($line === false) {
                break;
            }
            else {
                $result[] = $line;
            }

            $line = null;
        }

        return $result;
    }

    /**
     * Writes a line to the file. It always appends the given line ending.
     *
     * @param string $line
     * @param string $lineEnding
     * @return int number of characters (bytes) written
     * @throws FileAccessException
     */
    public function writeLine($line, $lineEnding = Strings::LINE_ENDING_LF)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        if (!Strings::isLineEndingChar($lineEnding)) {
            throw new InvalidArgumentException('Invalid line ending character: ' . $lineEnding);
        }

        if (!is_string($line)) {
            throw new InvalidArgumentException('Invalid type for $line, it must be an string');
        }

        return $this->internalWrite($line . $lineEnding, strlen($line) + 1);
    }

    /**
     * Writes lines to the file. It always appends the given line ending after each line.
     *
     * @param array|Traversable $lines
     * @param string $lineEnding
     * @return int If $lines is not iterable or if the line ending is invalid
     * @throws FileAccessException
     */
    public function writeAllLines($lines, $lineEnding = Strings::LINE_ENDING_LF)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        if (!Strings::isLineEndingChar($lineEnding)) {
            throw new InvalidArgumentException('Invalid line ending character: ' . $lineEnding);
        }

        if (!Types::isIterable($lines)) {
            throw new InvalidArgumentException('Parameter $lines is not iterable');
        }

        $total = 0;

        foreach ($lines as $line) {
            $result = $this->internalWrite($line . "\n", strlen($line) + 1);

            if ($result === false) {
                throw new FileAccessException('Could not write to file');
            }

            $total += $result;
        }

        return $total;
    }

    /**
     * @param array $csvLineFields
     * @param null $delimiter
     * @param null $enclosure
     * @return int
     * @throws FileAccessException
     */
    public function writeCsvLine(array $csvLineFields, $delimiter = null, $enclosure = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        if (!is_array($csvLineFields)) {
            throw new InvalidArgumentException('Invalid type for $csvLineFields, it must be an array');
        }

        $result = $this->internalWriteCsvLine($csvLineFields, $delimiter, $enclosure);

        if ($result === false) {
            throw new FileAccessException('Could not write CSV fields to file');
        }

        return $result;
    }

    /**
     * @param $csvLines
     * @param null $delimiter
     * @param null $enclosure
     * @return int
     * @throws FileAccessException
     */
    public function writeAllCsvLines($csvLines, $delimiter = null, $enclosure = null)
    {
        $this->checkIsOpen();
        $this->assertModeAllowsWriting();

        if (!Types::isIterable($csvLines)) {
            throw new InvalidArgumentException('Parameter $csvLines is not iterable');
        }

        $total = 0;

        foreach ($csvLines as $csvFields) {
            $result = $this->internalWriteCsvLine($csvFields, $delimiter, $enclosure);

            if ($result === false) {
                throw new FileAccessException('Could not write CSV fields to file');
            }

            $total += $result;
        }

        return $total;
    }
}
