<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font\tool\faExtractor;

use phbrick\BaseStrictClass;
use phbrick\exceptions\FileAccessException;
use phbrick\fs\File;
use phbrick\fs\FsPath;
use phbrick\fs\TextFile;
use phbrick\string\Strings;

/**
 * Class CssFileReader implements a quick CSS file reader that only extracts simple rules from well-formatted files.
 *
 * This class is not a CSS file parser.
 *
 * The file must have a blank line between consecutive rules.
 * The selector must be in the same line as the opening curly bracket.
 * Each css rule block must contain one declaration per line.
 * The ending curly bracket must be on a new empty line.
 * Each rule selector must be on a new line.
 * Lines with any of the start or end multi-line comment chars are totally ignored.
 *
 * @package phbrick\font\tool\faExtractor
 */
class CssFileReader extends BaseStrictClass
{

    const REGEX_SELECTOR = "/^[.@#]?([a-zA-Z\-\_\:\.\@\# \>]+)[ \t]?[\{,]$/";
    const REGEX_DECLARATION = "/^[\r\n\t ]*([a-zA-Z0-9\_\-]+)[\t ]*:[\t ]*([^;]+);[\}]?$/";

    /**
     * @var FsPath
     */
    private $cssFile;
    /**
     * @var TextFile
     */
    private $file = null;
    /**
     * @var int
     */
    private $lineNumber = -1;

    /**
     * @return FsPath
     */
    public function getCssFile()
    {
        return $this->cssFile;
    }

    /**
     * @param FsPath $cssFile
     */
    protected function setCssFile($cssFile)
    {
        $this->cssFile = new FsPath($cssFile);
    }

    /**
     * @return int
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    public function __construct($cssFile)
    {
        if (!file_exists($cssFile)) {
            throw new FileAccessException("File not found: $cssFile");
        }

        $this->setCssFile($cssFile);
    }

    public function open()
    {
        if ($this->file === null) {
            $this->file = new TextFile($this->cssFile);
        }

        if (!$this->file->isOpen()) {
            $this->file->open(File::MODE_READ);
            $this->lineNumber = 0;
        }
    }

    public function close()
    {
        if ($this->file !== null) {
            $this->file->close();
            $this->file = null;
            $this->lineNumber = -1;
        }
    }

    public function isOpen()
    {
        return $this->file !== null && $this->file->isOpen();
    }

    /**
     * Reads a rule from the css file discarding all comments.
     *
     * @return CssRule|null The read css rule or null if a complete rule could not be read and the file end is reached
     */
    public function readRule()
    {
        if (!$this->isOpen()) {
            $this->open();
        }

        $cssRule = new CssRule();
        $inBlock = false;
        $multiLineCommentStarted = false;

        while (!$this->file->isEof()) {

            $line = $this->file->readLine();

            if ($line === false) {
                // No more lines
                break;
            }

            $line = trim($line);
            $this->lineNumber++;

            if (Strings::isNullOrEmpty($line)) {
                continue;
            }

            $multiCommentLineStart = strpos($line, '/*');
            $multiCommentLineEnd = strpos($line, '*/');

            if ($multiLineCommentStarted && $multiCommentLineEnd === false) {
                // A multi-line comment is open, continue until a closing is found
                continue;
            }
            else if ($multiLineCommentStarted && $multiCommentLineEnd !== false) {
                // take the rest of the line after the multi-line commend end
                $line = substr($line, $multiCommentLineEnd + 2);
                // Disable multi-line comment mode
                $multiLineCommentStarted = false;
                // If a multi-line comment start is found before the end, discard it
                $multiCommentLineStart = false;
                // The multi-line comment end was "consumed", set to false to not consider it again
                $multiCommentLineEnd = false;

                if (Strings::isNullOrEmpty($line)) {
                    continue;
                }
            }

            if ($multiCommentLineStart !== false && $multiCommentLineEnd !== false) {
                // Remove the comment from the line
                $line = trim(substr($line, 0, $multiCommentLineStart)
                    . substr($line, $multiCommentLineEnd + 2));
            }
            else if ($multiCommentLineStart !== false) {
                // multi-line comment start
                $line = trim(substr($line, 0, $multiCommentLineStart));
                $multiLineCommentStarted = true;
            }
            else if ($multiCommentLineEnd !== false) {
                // Bogus multi-line comment found, ignore whatever goes before it in this line
                $line = trim(substr($line, $multiCommentLineEnd + 2));
            }

            if (Strings::isNullOrEmpty($line)) {
                continue;
            }

            $isSelector = preg_match(self::REGEX_SELECTOR, $line);
            $isDeclaration = preg_match(self::REGEX_DECLARATION, $line);
            $isEndBlock = $line == '}';
            $isStartBlock = $line == '{';
            $hasBlockStart = Strings::endsWith($line, '{');
            $hasBlockEnd = Strings::endsWith($line, '}');

            if (!$inBlock) {
                if ($isSelector) {
                    // Selector found, add it and update the inBlock flag if there is an open curly brace at the end
                    $cssRule->addSelector($line, $this->lineNumber);
                    $inBlock = $hasBlockStart;
                }
                else if ($isStartBlock && $cssRule->hasSelectors()) {
                    $inBlock = true;
                }
                else {
                    // Invalid format, restart
                    $cssRule->clear();
                }
            }
            else {
                if ($isDeclaration) {
                    $cssRule->addDeclaration($line, $this->lineNumber);
                    $inBlock = !$hasBlockEnd;
                }
                else if ($isEndBlock) {
                    $inBlock = false;
                }
                else {
                    // Not a declaration, invalid block
                    $cssRule->clear();
                    $inBlock = false;
                }

                // If the block end is reached and the css rule is not empty we have a valid rule, otherwise restart
                if (!$inBlock && $cssRule->hasDeclarations()) {
                    return $cssRule;
                }
                else if (!$inBlock) {
                    $cssRule->clear();
                }
            }
        }

        return null;
    }
}
