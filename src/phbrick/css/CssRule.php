<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font\tool\faExtractor;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\collection\IdentifierSequence;
use phbrick\string\SafeStringRenderTrait;
use phbrick\string\StringAggregator;
use phbrick\string\Strings;
use phbrick\types\Ints;

/**
 * Class CssRule models a simple container for a CSS rule read from a file.
 *
 * @package phbrick\font\tool\faExtractor
 */
class CssRule extends BaseStrictClass
{
    use SafeStringRenderTrait;

    private $selectors = [];
    private $declarations = [];
    private $lineNumbers = [];

    /**
     * @return IdentifierSequence[]
     */
    public function getSelectors()
    {
        return $this->selectors;
    }

    /**
     * @param IdentifierSequence[] $selectors
     */
    protected function setSelectors($selectors)
    {
        $this->selectors = $selectors;
    }

    public function addSelector($selector, $lineNumber)
    {
        $selector = trim($selector, "\r\n\t ,{");
        $this->selectors[] = new IdentifierSequence($selector, ' ', true);
        $this->addLineNumber($lineNumber);
    }

    /**
     * @return array
     */
    public function getDeclarations()
    {
        return $this->declarations;
    }

    public function getDeclaration($name)
    {
        if (isset($this->declarations[$name])) {
            return $this->declarations[$name];
        }

        return null;
    }

    /**
     * @param array $declarations
     */
    protected function setDeclarations($declarations)
    {
        $this->declarations = $declarations;
    }

    public function addDeclaration($declaration, $lineNumber = null)
    {
        $string = trim($declaration, "\r\n\t ;");
        $pos = strpos($string, ":");

        if ($pos === false || $pos < 2) {
            throw new InvalidArgumentException("Invalid declaration: $declaration");
        }

        $name = trim(substr($string, 0, $pos), "\t \"''");
        $value = trim(substr($string, $pos + 1), "\t \"''");

        if (Strings::isNullOrEmpty($name) || Strings::isNullOrEmpty($value)) {
            throw new InvalidArgumentException("Invalid declaration: $declaration");
        }

        $this->declarations[$name] = $value;
        $this->addLineNumber($lineNumber);
    }

    /**
     * @return array
     */
    public function getLineNumbers()
    {
        return $this->lineNumbers;
    }

    public function addLineNumber($lineNumber = null)
    {
        if ($lineNumber === null) {
            $lineNumber = count($this->lineNumbers) + 1;
        }
        $this->lineNumbers[] = Ints::castInt($lineNumber);
    }

    public function clear()
    {
        $this->lineNumbers = [];
        $this->declarations = [];
        $this->selectors = [];
    }

    /**
     * @return bool
     */
    public function hasSelectors()
    {
        return count($this->selectors) != 0;
    }

    /**
     * @return bool
     */
    public function hasDeclarations()
    {
        return count($this->declarations) != 0;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return !$this->hasDeclarations() && !$this->hasSelectors();
    }

    public function hasDeclaration($name)
    {
        return isset($this->declarations[$name]);
    }

    public function toString()
    {
        $bundler = new StringAggregator();
        $bundler->append(implode("\n, ", $this->selectors));
        $bundler->append(" {");

        foreach ($this->declarations as $name => $value) {
            $hasSpace = strpos($value, " ") !== false;
            $bundler->append("\n\t$name: ");

            if ($hasSpace) {
                $bundler->append("\"$value\"");
            }
            else {
                $bundler->append($value);
            }
        }

        return $bundler->toString();
    }
}
