<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup;

use phbrick\string\AbstractStringRenderable;
use phbrick\string\SafeStringRenderTrait;

/**
 * Models a markup document with a content and a declaration.
 * @param array $context Context to use when rendering
 */
abstract class Document extends AbstractStringRenderable
{

    use SafeStringRenderTrait;

    /**
     * @var bool
     */
    private $indentMarkup = false;

    /**
     * Gets if the generated markup must be indented.
     * @return bool
     */
    public function isIndentMarkup()
    {
        return $this->indentMarkup;
    }

    /**
     * Sets if the generated markup must be indented.
     * @param bool $indentMarkup
     */
    public function setIndentMarkup($indentMarkup)
    {
        $this->indentMarkup = (bool)$indentMarkup;
    }
}
