<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup;

use phbrick\collection\AttributesTrait;
use phbrick\string\AbstractStringRenderable;
use phbrick\string\IStringAggregator;
use phbrick\string\SafeStringRenderTrait;
use Traversable;

/**
 * Models a markup element that helps rendering markup but does not implements a navigable DOM.
 *
 * Each markup element is made up of a name, attributes and a content that can be an array of
 * another contents, other markup elements or scalar values.
 *
 * Content and attribute values must always be convertible to string to avoid errors.
 */
class Tag extends AbstractStringRenderable
{

    use AttributesTrait, SafeStringRenderTrait;

    /**
     * @param Tag $tag
     * @return Tag
     */
    public static function castTag(Tag $tag)
    {
        return $tag;
    }

    const VOID = 'void';

    /**
     * The tag may have a content or not and can have any other markup inside it.
     * Open and close markup will be rendered.
     * @example
     * <regularMarkup attr1="value1">
     *     Hello
     *     <regularMarkup2></regularMarkup2>
     * </regularMarkup>
     * @var string
     */
    const TYPE_REGULAR = 'regular';
    /**
     * The tag cannot hold any content and only the open markup will be rendered
     * the same way is rendered when the tag is terminal or regular, but no
     * closing markup will be rendered.
     * @example
     * <emptyRegularMarkup file="some">
     * @var string
     */
    const TYPE_EMPTY_REGULAR = 'empty-regular';
    /**
     * The tag cannot hold any content and only the open markup will be rendered
     * adding the markup end delimiter char before the markup end char.
     * @example
     * <empty foo="var"/>
     * @var string
     */
    const TYPE_EMPTY = 'empty';

    /**
     * @var string
     */
    private $name = self::VOID;

    /**
     * Gets the html tag name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the tag name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @var string
     */
    private $tagType = self::TYPE_REGULAR;

    /**
     * Gets the type of tag
     * @return string
     */
    public function getTagType()
    {
        return $this->tagType;
    }

    /**
     * Sets the type of tag
     * @param string $tagType
     */
    public function setTagType($tagType)
    {
        $this->tagType = $tagType;
    }

    /**
     * @var array
     */
    protected $content = [];

    /**
     * Gets the current tag content
     * @return Tag[]|Traversable
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the current tag content
     * @param mixed $content
     */
    public function setContent($content)
    {
        if (empty($content)) {
            $this->content = [];
        }
        else if (is_array($content) || is_a($content, '\\Traversable')) {
            $this->content = [];

            foreach ($content as $key => $item) {
                $this->content[] = $item;
            }
        }
        else {
            $this->content = [$content];
        }
    }

    public function __construct(
        $name = self::VOID,
        array $attributes = [],
        $type = self::TYPE_REGULAR)
    {

        $this->setTagType($type);

        if (!empty($name)) {
            $this->setName($name);
        }
        else {
            $this->setName(self::VOID);
        }

        if (!empty($attributes)) {
            $this->setAttrs($attributes);
        }

        $this->init();
    }

    protected function init()
    {
    }

    protected function beforeRender()
    {
    }

    protected function afterRender()
    {
    }

    /**
     * Gets if the tag name is set to void, so it will not render any open/close markup, only
     * its content if any.
     *
     * @return boolean
     */
    public function isVoid()
    {
        return empty($this->name) || strcasecmp($this->name, self::VOID) == 0;
    }

    /**
     * Gets if this tag is absolutely empty and will not render anything.
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->isVoid()
            && $this->isContentEmpty()
            && !$this->hasAnyAttrs();
    }

    /**
     * Gets if this markup does not have a content.
     * This has nothing to do with attributes
     * @return bool
     */
    public function isContentEmpty()
    {
        return empty($this->content);
    }

    /**
     * Empties this tag instance content (not attributes).
     */
    public function clearContent()
    {
        $this->content = [];
    }

    /**
     * Generates a string representation of this tag an all his children.
     *
     * This method is merely a wrapper that calls render but without the string bundler parameter.
     *
     * This method always returns an string.
     *
     * @param string $context
     * @param string $indent
     *
     * @return string
     */
    public function toString($context = null, $indent = null)
    {
        if (empty($context)) {
            $context = [];
        }

        if ($indent) {
            $context['indent'] = $indent;
        }

        $aggregator = $this->render($context);

        return $aggregator->toString();
    }

    /**
     * Renders the tag as strings and return them as an instance of an implementation of
     * IStringAggregator.
     *
     * @param null $context
     * @return IStringAggregator
     */
    public function render($context = null)
    {
        if (empty($context)) {
            $context = [];
        }

        $renderer = $this->createRenderer($context);
        $aggregator = $this->internalRender($context, $renderer);

        return $aggregator;
    }

    /**
     * Internal renderer method that generates an string.
     *
     * @param array|string $context
     * @param TagRenderer $renderer
     * @return IStringAggregator
     */
    protected function internalRender(array $context = [], TagRenderer $renderer = null)
    {

        $this->beforeRender();

        $aggregator = $renderer->render($this, $context);

        $this->afterRender();

        return $aggregator;
    }

    /**
     * Creates a renderer for this tag.
     *
     * The only reason this tag is abstract is this tiny method. to have __toString() working we
     * need to create a markup renderer and that markup renderer will be absolutely dependent upon
     * the concrete markup type.
     *
     * @param mixed $context
     * @param mixed $indent
     * @return TagRenderer
     */
    protected function createRenderer($context = null, $indent = null)
    {
        $renderer = new TagRenderer();

        if ($indent === null && isset($context['indent'])) {
            $indent = Indent::createFromContext($context);
        }

        $renderer->setIndent($indent);

        return $renderer;
    }
}
