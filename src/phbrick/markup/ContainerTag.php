<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup;

use InvalidArgumentException;
use phbrick\string\Renderables;

/**
 * Extends the base tag class with extra methods to handle contained elements as a collection
 */
class ContainerTag extends Tag
{

    /**
     * @param ContainerTag $tag
     * @return ContainerTag
     */
    public static function castContainerTag(ContainerTag $tag)
    {
        return $tag;
    }

    public function __construct(
        $name = self::VOID,
        array $attributes = [],
        $content = null,
        $type = self::TYPE_REGULAR)
    {

        if (is_array($content)
            || is_a($content, '\\ArrayObject')
            || is_a($content, '\\phbrick\\collection\\BaseCollectionArray')) {

            foreach ($content as $key => $value) {
                $this->addContent($value);
            }
        }
        else {
            $this->addContent($content);
        }

        parent::__construct($name, $attributes, $type);
    }

    public function isContentEmpty()
    {
        return $this->content === null || $this->content === ''
            || (is_array($this->content) && empty($this->content));
    }

    /**
     * Clears the contents of the tag
     */
    public function clearContent()
    {
        $this->content = [];
    }

    /**
     * Adds a new content item to the tag contents
     * @param mixed $content If is an array or an IteratorAggregate it will be iterated and the
     * extracted elements added one by one.
     * @throws InvalidArgumentException If the type of the content cannot be used as a renderable
     */
    public function addContent($content)
    {

        if ($content === null || $content === '') {
            return;
        }

        if (!is_array($this->content)) {
            if (empty($this->content)) {
                $this->content = [];
            }
            else {
                $this->content = [$this->content];
            }
        }

        $isRenderable = Renderables::isRenderable($content);
        $isObject = is_object($content);
        $isArray = !$isObject && is_array($content);
        $isTraversable = $isObject && is_a($content, '\\Traversable');
        $isTag = $isObject && is_a($content, '\\phbrick\\markup\\Tag');

        if ($isRenderable || $isTag) {
            $this->content[] = $content;
        }
        else if ($isArray || $isTraversable) {

            foreach ($content as $key => $value) {
                $this->content[] = $value;
            }
        }
        else {

            if ($isObject) {
                throw new InvalidArgumentException(
                    'Cannot add instance of ' . get_class($content) . ' as tag content.');
            }
            else {
                throw new InvalidArgumentException(
                    'Cannot add ' . gettype($content) . ' as tag content.');
            }
        }
    }

    /**
     * Gets if the tag has any child content.
     *
     * Child content can be any content inside it, not only tag instances.
     *
     * @return bool True if the tag has children or false if not
     */
    public function hasChildren()
    {
        if (!is_array($this->content)
            || (count($this->content) == 0
                && !is_array($this->content[0])
                && !is_object($this->content[0]))) {

            return false;
        }

        return true;
    }

    /**
     * Retrieves the first tag with a matching tag name.
     *
     * @param string $name
     * @return boolean|ContainerTag
     */
    public function findTagByName($name)
    {
        if (!is_array($this->content)) {
            return false;
        }

        $result = false;

        foreach ($this->content as $content) {
            if (!is_object($content) || !is_a($content, 'phbrick\markup\ContainerTag')) {
                continue;
            }

            $tag = self::castContainerTag($content);

            if ($tag->getName() == $name) {
                $result = $tag;
                break;
            }
            else {
                $findResult = $tag->findTagByName($name);

                if ($findResult) {
                    $result = $findResult;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Creates a new tag and returns it with the current tag added as the last of his contents.
     *
     * @param string $name
     * @param array $attributes
     * @param string $content
     * @param string $type
     * @return ContainerTag
     */
    public function wrap(
        $name = self::VOID,
        array $attributes = [],
        $content = null,
        $type = self::TYPE_REGULAR)
    {

        /** @var ContainerTag $new */
        $new = self::instance($name, $attributes, $content, $type);
        $wrap = ContainerTag::castContainerTag($new);
        $wrap->addContent($this);

        return $wrap;
    }

    public function count()
    {
        if (is_array($this->content)) {
            return count($this->content);
        }
        else if ($this->content !== null && $this->content !== "") {
            return 1;
        }
        else {
            return 0;
        }
    }
}
