<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\markup;

use ArrayAccess;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\string\IStringAggregator;
use phbrick\string\Renderables;
use phbrick\string\StringAggregator;
use phbrick\types\Types;

/**
 * Converts markup to string
 */
class TagRenderer extends BaseStrictClass
{

    /**
     * @param TagRenderer $tagRenderer
     * @return TagRenderer
     */
    public static function castTagRenderer(TagRenderer $tagRenderer)
    {
        return $tagRenderer;
    }

    const DELIMITER_NONE = 'none';
    const DELIMITER_OPEN_BEGIN = 'open-begin';
    const DELIMITER_OPEN_END = 'open-end';
    const DELIMITER_CLOSE_BEGIN = 'close-begin';
    const DELIMITER_CLOSE_END = 'close-end';
    const DELIMITER_ATTR_PREFIX = 'attr-prefix';
    const DELIMITER_ATTR_SUFFIX = 'attr-suffix';
    const DELIMITER_ATTR_ASSIGN = 'attr-assign';
    const DELIMITER_ATTR_VALUE_WRAP = 'attr-value-wrap';

    /**
     * @var IStringAggregator
     */
    private $aggregator = null;

    /**
     * Gets Aggregator object that will hold all the generated strings
     * @return IStringAggregator
     */
    public function getAggregator()
    {
        return $this->aggregator;
    }

    /**
     * Sets Aggregator object that will hold all the generated strings
     * @param IStringAggregator $aggregator
     */
    public function setAggregator($aggregator)
    {
        $this->aggregator = $aggregator;
    }

    /**
     * @var Indent
     */
    private $indent = null;

    /**
     * Gets Markup indentation to prettify output
     * @return Indent
     */
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * Sets Markup indentation to prettify output
     * @param Indent $indent
     */
    public function setIndent(Indent $indent)
    {
        $this->indent = $indent;
    }

    /**
     * @var array
     */
    private $delimiters = [];

    /**
     * Gets markup delimiters
     * @return array
     */
    public function getDelimiters()
    {
        return $this->delimiters;
    }

    /**
     * Sets markup delimiters
     * @param array $delimiters
     */
    public function setDelimiters(array $delimiters)
    {
        $this->delimiters = $delimiters;
    }

    /**
     * Sets the value for a delimiter. If the value is an array it must contain a value for each
     * tag type. If the value is not an array it must be an string with the delimiter value for
     * all tag types.
     *
     * @param string $delimiter
     * @param string|array $value
     */
    public function setDelimiter($delimiter, $value)
    {
        $this->delimiters[$delimiter] = $value;
    }

    /**
     * Gets stored delimiter strings or false if not stored yet.
     *
     * @param string $delimiter
     * @return boolean|string
     */
    public function getDelimiter($delimiter)
    {
        if (!isset($this->delimiters[$delimiter])) {
            return false;
        }

        return $this->delimiters[$delimiter];
    }

    public function getDelimiterType($delimiter, $type)
    {
        if (!isset($this->delimiters[$delimiter])) {
            return false;
        }
        else if (!is_array($this->delimiters[$delimiter])) {
            return $this->delimiters[$delimiter];
        }
        else if (!isset($this->delimiters[$delimiter][$type])) {
            return false;
        }
        else {
            return $this->delimiters[$delimiter][$type];
        }
    }

    public function setDelimiterType($delimiter, $type, $value)
    {
        if (!isset($this->delimiters[$delimiter])) {
            $this->delimiters[$delimiter] = [];
        }
        else if (!is_array($this->delimiters[$delimiter])) {
            throw new InvalidArgumentException('Delimiter is not an array: ' . $delimiter . '-' . $type);
        }

        $this->delimiters[$delimiter][$type] = $value;
    }

    public function __construct()
    {

    }

    protected function beforeRender(Tag $tag, array $context = [])
    {
    }

    protected function afterRender(Tag $tag, array $context = [])
    {
    }

    /**
     * Renderer method that generates an string from the markup.
     *
     * @param Tag $tag
     * @param array $context
     * @return IStringAggregator String generated. Same as $renderer->getAggregator()
     */
    public function render(Tag $tag, array $context = [])
    {

        $this->beforeRender($tag, $context);

        // Ensure we have an aggregator and indentation

        $aggregator = $this->getAggregator();

        if (!$aggregator) {
            $aggregator = $this->createAggregator($context);
            $this->setAggregator($aggregator);
        }

        $indent = $this->getIndent();

        if (!$indent) {
            $indent = $this->createIndent($context);
            $this->setIndent($indent);
        }

        // Render the tag, first the opening and then the content and closing. If there is no
        // opening we do not render anything else
        if ($this->renderOpenTag($tag, $context)) {
            // If the tag has any content we render it with the proper indentation
            if (!$tag->isContentEmpty()) {
                $indent->inc();
                $this->renderContentTag($tag, $context);
                $indent->dec();
            }

            $this->renderCloseTag($tag, $context);
        }

        $this->afterRender($tag, $context);

        return $this->getAggregator();
    }

    /**
     * Given an indentation prepares it and returns an Indent class instance.
     *
     * @param array $context
     * @return Indent|null Indent instance that represents the current indent or null if no indent
     */
    protected function createIndent(array $context)
    {
        $indent = Indent::createEmpty();

        if (isset($context['indent']) && is_a($context['indent'], 'phbrick\\string\\Indent')) {
            $indent = $context['indent'];
        }
        else if (isset($context['indent'])) {
            if (is_string($context['indent'])) {
                $indentStr = $context['indent'];
                $indentLevel = strlen($context['indent']);
                $indent->setIndentString($indentStr);
                $indent->setLevel($indentLevel);
            }
            else if (is_numeric($context['indent'])) {
                $indentStr = ' ';
                $indentLevel = (int)$context['indent'];
                $indent->setIndentString($indentStr);
                $indent->setLevel($indentLevel);
            }
            else {
                throw new InvalidArgumentException("Invalid indent " . Types::getTypeName($context['indent']));
            }
        }

        if (isset($context['indentStr'])) {
            $indent->setIndentString($context['indentStr']);
        }

        if (isset($context['indentLevel'])) {
            $indent->setLevel($context['indentLevel']);
        }

        if (isset($context['indentBreak'])) {
            $indent->setLineBreak($context['indentBreak']);
        }

        return $indent;
    }

    /**
     * Creates the instance of the object that will hold the generated contents.
     * @param array $context
     * @return IStringAggregator
     */
    protected function createAggregator(array $context = null)
    {
        $aggregator = new StringAggregator();

        return $aggregator;
    }

    /**
     * Renders the opening of the tag and adds it to the current aggregator
     *
     * Returns true if something was added or false if not.
     *
     * @param Tag $tag
     * @param array $context
     * @return bool True if the tag opening is rendered or false if not (void tag)
     */
    public function renderOpenTag(Tag $tag, array $context = [])
    {
        if ($tag->isVoid()) {
            // Void and content-empty tags are not rendered
            return !$tag->isContentEmpty();
        }

        $aggregator = $this->getAggregator();

        // Indent the open tag and write the beginning and the name
        $aggregator->append($this->getIndent());
        $aggregator->append($this->getOpenMarkupStart($tag));
        $aggregator->append($tag->getName());

        // Check if it has any attributes, if so render them. The attribute renderer takes care of
        // prefixing them by an space to separate from the tag name
        if ($tag->hasAnyAttrs()) {
            $this->renderTagAttributes($tag, $context);
        }

        // Write the ending of the open tag
        $aggregator->append($this->getOpenMarkupEnd($tag));

        // Always return true as the tag was not void and the name was written to aggregator
        return true;
    }

    /**
     * Generates an string with all tag attributes along his values with the proper format.
     *
     * Attributes that starts with an underscore or attributes that have an empty value are skipped.
     *
     * @param Tag $tag
     * @param array $context
     * @return bool True if at least one attribute was rendered or false if not.
     */
    public function renderTagAttributes(Tag $tag, array $context = [])
    {
        $rendered = false;
        $order = 0;
        foreach ($tag->getAttrs() as $name => $value) {
            $attributeString = $this->renderTagAttribute($name, $value, $order, $context);

            if (!empty($attributeString)) {
                // Append an space before each rendered attribute
                $this->getAggregator()->append(' ' . $attributeString);
                $rendered = true;
            }
        }

        return $rendered;
    }

    /**
     * Generates an string with a single attribute.
     * @param string $name
     * @param mixed $value
     * @param int $order
     * @param array $context
     * @return bool
     */
    public function renderTagAttribute($name, $value, $order, array $context = [])
    {

        if ($this->isEmptyTagAttribute($name, $value)) {
            return false;
        }

        $valueWrap = (string)$this->getAttrValueWrap();
        $attrAssign = (string)$this->getAttrAssign();
        $attrPrefix = (string)$this->getAttrPrefix();
        $attrSuffix = (string)$this->getAttrSuffix();

        if (Types::isIterable($value)) {
            $valueStr = '';
            foreach ($value as $part) {
                $valueStr .= $part;
            }

            $value = $valueStr;
            unset($valueStr);
        }
        else {
            $value = (string)$value;
        }

        return $attrPrefix . $name . $attrAssign . $valueWrap . $value . $valueWrap . $attrSuffix;
    }

    protected function isEmptyTagAttribute($name, $value)
    {
        // Skip private attributes or those empty or without name
        if (empty($name) || $name[0] == '_' || $value == '' || $value === null) {
            return true;
        }

        return false;
    }

    /**
     * Renders the contents of the tag and adds them to the current aggregator
     *
     * Returns true if something was added (tag not empty) or false if not (tag has no content).
     *
     * @param Tag $tag
     * @param array $context
     * @return bool If some content was added to the bundler returns true, if not it will return
     * false.
     */
    public function renderContentTag(Tag $tag, array $context = [])
    {

        if ($tag->isContentEmpty()) {
            return false;
        }

        $rendered = false;

        $content = $tag->getContent();

        if (is_array($content) || is_a($content, '\Traversable')) {

            foreach ($content as $pos => $item) {
                if ($this->renderContentItem($pos, $item, $context)) {
                    $rendered = true;
                }
            }
        }
        else if ($this->renderContentItem(0, $content, $context)) {
            $rendered = true;
        }

        return $rendered;
    }

    /**
     * Renders a single item in the contents of this tag
     *
     * @param $pos
     * @param mixed $item
     * @param array $context
     * @return bool
     * @internal param IStringAggregator $aggregator
     * @internal param string $indent
     */
    public function renderContentItem($pos, $item, array $context = [])
    {

        if (empty($item)) {
            return false;
        }

        $indent = $this->getIndent();

        if (is_scalar($item) && !is_array($item)) {
            $this->getAggregator()->append("$indent$item");
        }
        else if (is_a($item, 'phbrick\markup\Tag')) {
            $this->render($item, $context);
        }
        else {

            $renderedContent = Renderables::render($item,
                [
                    'pos'    => $pos,
                    'indent' => $indent,
                ]);

            $this->getAggregator()->append("$indent$renderedContent");
        }

        return true;
    }

    /**
     * Renders the closing of the tag and adds it to the current aggregator
     *
     * Returns true if something was added or false if not.
     *
     * @param Tag $tag
     * @param array $context
     * @return bool True if the tag closing is rendered or false if not (void tag)
     */
    public function renderCloseTag(Tag $tag, array $context = [])
    {
        // Void tags does not have a close tag
        if ($tag->isVoid()) {
            return false;
        }

        $aggregator = $this->getAggregator();

        // Write indentation before the tag
        $aggregator->append($this->getIndent());
        // Write the end tag
        $aggregator->append($this->getCloseMarkupStart($tag));
        $aggregator->append($tag->getName());
        $aggregator->append($this->getCloseMarkupEnd($tag));
        // Always return true as the tag was not void and the name was written to aggregator
        return true;
    }

    protected function getOpenMarkupStart(Tag $tag)
    {
        return $this->getDelimiterType(self::DELIMITER_OPEN_BEGIN, $tag->getTagType());
    }

    protected function getOpenMarkupEnd(Tag $tag)
    {
        return $this->getDelimiterType(self::DELIMITER_OPEN_END, $tag->getTagType());
    }

    protected function getCloseMarkupStart(Tag $tag)
    {
        return $this->getDelimiterType(self::DELIMITER_CLOSE_BEGIN, $tag->getTagType());
    }

    protected function getCloseMarkupEnd(Tag $tag)
    {
        return $this->getDelimiterType(self::DELIMITER_CLOSE_END, $tag->getTagType());
    }

    protected function getAttrPrefix()
    {
        return $this->getDelimiterType(self::DELIMITER_ATTR_PREFIX, self::DELIMITER_NONE);
    }

    protected function getAttrSuffix()
    {
        return $this->getDelimiterType(self::DELIMITER_ATTR_SUFFIX, self::DELIMITER_NONE);
    }

    protected function getAttrAssign()
    {
        return $this->getDelimiterType(self::DELIMITER_ATTR_ASSIGN, self::DELIMITER_NONE);
    }

    protected function getAttrValueWrap()
    {
        return $this->getDelimiterType(self::DELIMITER_ATTR_VALUE_WRAP, self::DELIMITER_NONE);
    }

    /**
     * Creates a new TagRenderer instance initialized with the given context.
     *
     * The keys used from the context are:
     *  + tagRenderer: One of TagRenderer instance, Class name or array-like. Where each one is:
     *    - TagRenderer instance: Instance to be cloned.
     *    - Class name: Name of the class to initialize the renderer instance.
     *    - Array-like: Array or ArrayAccess implementation with all the TagRenderer delimiters to
     *      be used.
     *  + tagRendererDelimiters: Array or ArrayAccess implementation with all the TagRenderer
     *    delimiters to be used. These delimiters are set after the tagRenderer key contents are
     *    used.
     *
     * @param array|ArrayAccess $context
     * @return TagRenderer
     */
    public static function createFromContext($context = null)
    {
        /** @var TagRenderer $renderer */
        $renderer = self::instance();

        if (empty($context)) {
            return TagRenderer::castTagRenderer($renderer);
        }

        $hasRendererMapKey = isset($context['tagRenderer']);

        if ($hasRendererMapKey && is_string($context['tagRenderer']) && class_exists($context['tagRenderer'])) {
            $clsName = $context['tagRenderer'];
            $renderer = new $clsName();
        }
        else if ($hasRendererMapKey && is_a($context['tagRenderer'], self::cls())) {
            $renderer = clone $context['tagRenderer'];
        }
        else if ($hasRendererMapKey && Types::isArrayAccess($context['tagRenderer'])) {
            $renderer->setDelimiters($context['tagRenderer']);
        }

        if (isset($context['tagRendererDelimiters'])) {
            $renderer->setDelimiters($context['tagRendererDelimiters']);
        }

        return $renderer;
    }
}
