<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html;

use InvalidArgumentException;
use phbrick\string\IStringRenderable;
use phbrick\string\SafeStringRenderTrait;

/**
 * Models an (x)html document type declaration.
 */
class Doctype implements IStringRenderable
{

    use SafeStringRenderTrait;

    const DOCTYPE_HTML_5 = 'html-5';
    const DOCTYPE_HTML_401_TRAN = 'html-4.0.1-tran';
    const DOCTYPE_HTML_401_STRICT = 'html-4.0.1-strict';
    const DOCTYPE_HTML_401_FRAMESET = 'html-4.0.1-frameset';
    const DOCTYPE_XHTML_10_STRICT = 'xhtml-1.0-strict';
    const DOCTYPE_XHTML_10_TRAN = 'xhtml-1.0-tran';
    const DOCTYPE_XHTML_10_FRAMESET = 'xhtml-1.0-frameset';

    const DEFAULT_DOCTYPE_STRING = '<!DOCTYPE html>';

    public static $Doctypes = [
        self::DOCTYPE_HTML_401_FRAMESET,
        self::DOCTYPE_HTML_401_STRICT,
        self::DOCTYPE_HTML_401_TRAN,
        self::DOCTYPE_HTML_5,
        self::DOCTYPE_XHTML_10_FRAMESET,
        self::DOCTYPE_XHTML_10_STRICT,
        self::DOCTYPE_XHTML_10_TRAN,
    ];

    private $value = self::DOCTYPE_HTML_5;

    /**
     * Sets the document type string
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the document type string.
     * @param string $value Document type string
     * @throws InvalidArgumentException
     */
    public function setValue($value)
    {
        if (!in_array($value, self::$Doctypes)) {
            throw new InvalidArgumentException('Invalid value ' . $value);
        }

        $this->value = $value;
    }

    public function __construct($doctype = null)
    {
        if ($doctype) {
            $this->setValue($doctype);
        }
    }

    public function isHtml()
    {
        return $this->value == self::DOCTYPE_HTML_401_FRAMESET
            || $this->value == self::DOCTYPE_HTML_401_STRICT
            || $this->value == self::DOCTYPE_HTML_401_TRAN
            || $this->value == self::DOCTYPE_HTML_5;
    }

    public function isXhtml()
    {
        return $this->value == self::DOCTYPE_XHTML_10_FRAMESET
            || $this->value == self::DOCTYPE_XHTML_10_STRICT
            || $this->value == self::DOCTYPE_XHTML_10_TRAN;
    }

    public function isEdge()
    {
        return $this->value == self::DOCTYPE_HTML_5;
    }

    /**
     * @param mixed|null $context
     * @return string
     */
    public function render($context = null)
    {
        $type = 'html PUBLIC ';
        $dtd = '-//W3C//DTD ';
        $url = 'http://www.w3.org/TR/';
        $content = null;

        switch ($this->value) {
            case self::DOCTYPE_HTML_401_FRAMESET:
                $type = 'HTML PUBLIC ';
                $dtd .= 'HTML 4.01 Frameset//EN';
                $url .= 'html4/frameset.dtd';
                break;
            case self::DOCTYPE_HTML_401_STRICT:
                $type = 'HTML PUBLIC ';
                $dtd .= 'HTML 4.01//EN';
                $url .= 'html4/strict.dtd';
                break;
            case self::DOCTYPE_HTML_401_TRAN:
                $type = 'HTML PUBLIC ';
                $dtd .= 'HTML 4.01 Transitional//EN';
                $url .= 'html4/loose.dtd';
                break;
            case self::DOCTYPE_XHTML_10_FRAMESET:
                $dtd .= 'XHTML 1.0 Frameset//EN';
                $url .= 'xhtml1/DTD/xhtml1-frameset.dtd';
                break;
            case self::DOCTYPE_XHTML_10_STRICT:
                $dtd .= 'XHTML 1.0 Strict//EN';
                $url .= 'xhtml1/DTD/xhtml1-strict.dtd';
                break;
            case self::DOCTYPE_XHTML_10_TRAN:
                $dtd .= 'XHTML 1.0 Transitional//EN';
                $url .= 'xhtml1/DTD/xhtml1-transitional.dtd';
                break;
            case self::DOCTYPE_HTML_5:
            default:
                $dtd = null;
                $url = null;
                $type = 'html';
                break;
        }

        return '<!DOCTYPE ' . $type . ($dtd ? " \"$dtd\"" : '') . ($url ? " \"$url\"" : '') . '>';
    }

    public function toString($context = null, $indent = null)
    {
        return (string)$this->render($context);
    }
}
