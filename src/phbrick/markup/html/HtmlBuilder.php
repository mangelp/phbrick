<?php

namespace phbrick\markup\html;

use phbrick\exceptions\IllegalStateException;
use phbrick\string\RenderableToStringTrait;
use phbrick\string\Strings;

class HtmlBuilder
{
    use RenderableToStringTrait;

    const TAGS = [
        'a',
        'abbr',
        'acronym',
        'address',
        'applet',
        'area',
        'article',
        'aside',
        'audio',
        'b',
        'base',
        'basefont',
        'bdo',
        'big',
        'blockquote',
        'body',
        'br',
        'button',
        'canvas',
        'caption',
        'center',
        'cite',
        'code',
        'col',
        'colgroup',
        'datalist',
        'dd',
        'del',
        'dfn',
        'div',
        'dl',
        'dt',
        'em',
        'embed',
        'fieldset',
        'figcaption',
        'figure',
        'font',
        'footer',
        'form',
        'frame',
        'frameset',
        'head',
        'header',
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'hr',
        'html',
        'i',
        'iframe',
        'img',
        'input',
        'ins',
        'kbd',
        'label',
        'legend',
        'li',
        'link',
        'main',
        'map',
        'mark',
        'meta',
        'meter',
        'nav',
        'noscript',
        'object',
        'ol',
        'optgroup',
        'option',
        'p',
        'param',
        'pre',
        'progress',
        'q',
        's',
        'samp',
        'script',
        'section',
        'select',
        'small',
        'source',
        'span',
        'strike',
        'strong',
        'style',
        'sub',
        'sup',
        'table',
        'tbody',
        'td',
        'textarea',
        'tfoot',
        'th',
        'thead',
        'time',
        'title',
        'tr',
        'u',
        'ul',
        'var',
        'video',
        'wbr',
    ];

    /**
     * @var HtmlTag
     */
    private $root = null;
    /**
     * @var HtmlTag
     */
    private $last = null;

    public function __construct($name = null, $attrs = [], $content = [], $type = HtmlTag::TYPE_REGULAR)
    {
        if (!is_array($content) && $content !== null && $content !== "") {
            $content = [$content];
        }

        if ($name instanceof HtmlTag) {
            $this->root = HtmlTag::castHtmlTag($name);
        }
        else if ($name !== null) {
            $this->root = new HtmlTag($name, $attrs, $content, $type);
        }
        else {
            $this->root = new HtmlTag();
        }

        $this->last = $this->root;
    }

    /**
     * @return HtmlTag
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return HtmlTag
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Adds a new tag and returns the current builder.
     *
     * @param $name
     * @param array $attrs
     * @param array $content
     * @param string $type
     * @return $this
     */
    public function add($name, $attrs = [], $content = [], $type = HtmlTag::TYPE_REGULAR)
    {
        if (!is_array($content) && $content !== null && $content !== "") {
            $content = [$content];
        }

        if ($name instanceof HtmlTag) {
            $tag = HtmlTag::castHtmlTag($name);
        }
        else {
            $tag = new HtmlTag($name, $attrs, $content, $type);
        }

        $this->last->addContent($tag);

        return $this;
    }

    public function render()
    {
        if ($this->root !== null) {
            return $this->root->render();
        }
        else {
            return "";
        }
    }

    /**
     * Creates a new tag, adds it to the builder and returns it.
     *
     * @param $name
     * @param array $attrs
     * @param array $content
     * @param string $type
     * @return HtmlTag
     */
    public function create($name, $attrs = [], $content = [], $type = HtmlTag::TYPE_REGULAR)
    {
        if (!is_array($content) && $content !== null && $content !== "") {
            $content = [$content];
        }

        $tag = self::tag($name, $attrs, $content, $type);

        $this->last->addContent($tag);

        return $tag;
    }

    public function __call($name, $arguments)
    {
        $attributes = [];
        $content = null;
        $type = HtmlTag::TYPE_REGULAR;

        if (isset($arguments['attrs'])) {
            $attributes = $arguments['attrs'];
        }
        else if (isset($arguments['attributes'])) {
            $attributes = $arguments['attributes'];
        }
        else if (isset($arguments[0])) {
            $attributes = $arguments[0];
        }

        if (isset($arguments['content'])) {
            $content = $arguments['content'];
        }
        else if (isset($arguments[1])) {
            $content = $arguments[1];
        }

        if (isset($arguments['type'])) {
            $type = $arguments['type'];
        }
        else if (isset($arguments[2])) {
            $type = $arguments[2];
        }

        $tag = self::tag($name, $attributes, $content, $type);

        $this->last->addContent($tag);

        return $tag;
    }

    /**
     * @param $name
     * @param $arguments
     * @return HtmlTag
     */
    public static function __callStatic($name, $arguments)
    {
        $attributes = [];
        $content = null;
        $type = HtmlTag::TYPE_REGULAR;

        if (isset($arguments['attrs'])) {
            $attributes = $arguments['attrs'];
        }
        else if (isset($arguments['attributes'])) {
            $attributes = $arguments['attributes'];
        }
        else if (isset($arguments[0])) {
            $attributes = $arguments[0];
        }

        if (isset($arguments['content'])) {
            $content = $arguments['content'];
        }
        else if (isset($arguments[1])) {
            $content = $arguments[1];
        }

        if (isset($arguments['type'])) {
            $type = $arguments['type'];
        }
        else if (isset($arguments[2])) {
            $type = $arguments[2];
        }

        return self::tag($name, $attributes, $content, $type);
    }

    /**
     * @param $name
     * @return mixed
     */
    private static function prepareTag($name)
    {
        if (is_string($name)) {
            $name = self::filterTagName($name);
            self::assertValidTagName($name);
        }

        return $name;
    }

    /**
     * @param $name
     * @return bool
     */
    public static function isValidTagName($name)
    {
        return array_search($name, self::TAGS, true) !== false;
    }

    private static function assertValidTagName($name)
    {
        if ($name !== HtmlTag::VOID && !self::isValidTagName($name)) {
            throw new IllegalStateException("Invalid tag name: $name");
        }
    }

    /**
     * @param string $name
     * @return string
     */
    public static function filterTagName($name)
    {
        return strtolower(trim($name, "\r\n\t\0 %"));
    }

    /**
     * @param $name
     * @param array $attrs
     * @param array $content
     * @param string $type
     * @return HtmlTag
     */
    public static function tag($name = HtmlTag::VOID, $attrs = [], $content = [], $type = HtmlTag::TYPE_REGULAR)
    {
        if (!is_array($content) && $content !== null && $content !== "") {
            $content = [$content];
        }

        if (is_array($name)) {
            $attrs = [];
            $content = null;
            $type = HtmlTag::TYPE_REGULAR;
            $arguments = $name;
            $name = HtmlTag::VOID;

            if (isset($arguments['name'])) {
                $name = $arguments['name'];
            }

            if (isset($arguments['attrs'])) {
                $attrs = $arguments['attrs'];
            }
            else if (isset($arguments['attributes'])) {
                $attrs = $arguments['attributes'];
            }
            else if (isset($arguments[0])) {
                $attrs = $arguments[0];
            }

            if (isset($arguments['content'])) {
                $content = $arguments['content'];
            }
            else if (isset($arguments[1])) {
                $content = $arguments[1];
            }

            if (isset($arguments['type'])) {
                $type = $arguments['type'];
            }
            else if (isset($arguments[2])) {
                $type = $arguments[2];
            }
        }

        $name = self::prepareTag($name);

        return new HtmlTag($name, $attrs, $content, $type);
    }

    /**
     * Adds child tags to the current tag and returns this current tag.
     *
     * The children array must be an array of HtmlTag instances, tag names or a map of tag names and tag children
     *
     * @param array|string $children
     * @param HtmlTag $tag
     * @return HtmlTag
     */
    public static function children($tag, ...$childrens)
    {
        if (empty($childrens) || empty($tag)) {
            return $tag;
        }

        foreach ($childrens as $children) {
            if (is_string($children)) {
                $children = array_map('trim', explode(',', $children));
            }
            else if (is_array($children) && isset($children['name'])) {
                $tag->addContent(self::tag($children));
                continue;
            }

            foreach ($children as $key => $child) {
                if (is_int($key) && $child instanceof HtmlTag) {
                    $tag->addContent($child);
                }
                else if (is_int($key) && is_string($child)) {
                    if ($child[0] === '%') {
                        self::parseTag($child, $tag);
                    }
                    else {
                        $tag->addContent(HtmlBuilder::tag($child));
                    }
                }
                else if (is_int($key) && is_array($child)) {
                    self::children($tag, $child);
                }
                else if (is_string($key) && is_string($child)) {
                    /**
                     * @var HtmlTag $tag
                     */
                    $newTag = HtmlBuilder::tag($key);
                    self::children($newTag, $child);
                    $tag->addContent($newTag);
                }
                else if (is_string($key) && is_array($child)) {

                    /**
                     * @var HtmlTag $newTag
                     */
                    $newTag = self::tag($child);

                    if (isset($child['attrs']) && is_array($child['attrs'])) {
                        $newTag->setAttrs($child['attrs']);
                    }

                    if (isset($child['content'])) {
                        $newTag->setContent($child['content']);
                    }

                    if (isset($child['type'])) {
                        $newTag->setTagType($child['type']);
                    }

                    if (isset($child['children'])) {
                        $newTag->children($child['children']);
                    }

                    $tag->addContent($newTag);
                }
            }
        }

        return $tag;
    }

    /**
     * Creates a new tag only with the given name
     *
     * The tag type is inferred from whether the name ends with a single slash / (empty-regular) or by a double slash
     * // (empty).
     *
     * @param $name
     * @return HtmlTag
     */
    public static function tagFromName($name)
    {
        $type = HtmlTag::TYPE_REGULAR;

        if (Strings::endsWith($name, '//')) {
            $type = HtmlTag::TYPE_EMPTY;
        }
        else if (Strings::endsWith($name, '/')) {
            $type = HtmlTag::TYPE_EMPTY_REGULAR;
        }

        $tagName = self::prepareTag(trim($name, '/'));

        $newTag = new HtmlTag($tagName, [], null, $type);

        return $newTag;
    }

    /**
     * Parses a compact representation of a tag
     *
     * The representation must start by % and each attribute be separated by a |
     *
     * The last part must be the tag content. This representation allow to specify children, but all children is
     * appended to the previous tag.
     *
     * @param string $tagStr
     * @param HtmlTag $parent Parent to add the newly created tag
     * @return HtmlTag
     */
    public static function parseTag($tagStr, $parent = null)
    {
        // %div|class=block|title=foo title|style=width:20px;height:12px;|foo text node|%span|class=text-label|label text node

        if (empty($tagStr)) {
            return null;
        }

        if ($tagStr[0] !== '%') {
            throw new IllegalStateException("Invalid string to parse, it does not start with % <<$tagStr>>");
        }

        $nextPos = strpos($tagStr, '|');

        if ($nextPos === false) {
            $newTag = self::tagFromName($tagStr);
            return $newTag;
        }

        $newTag = null;
        $lastPos = 0;
        $index = -1;
        $attributes = true;

        while ($nextPos !== false) {
            $nextLength = $nextPos - $lastPos;
            $nextPart = substr($tagStr, $lastPos, $nextLength);
            $lastPos = $nextPos + 1;
            $nextPos = strpos($tagStr, '|', $lastPos);
            $index++;

            if ($index === 0) {
                $newTag = self::tagFromName($nextPart);
                continue;
            }

            $eqPos = strpos($nextPart, '=');

            if ($eqPos === false) {
                $attributes = false;
            }

            if ($attributes) {
                $attrName = substr($nextPart, 0, $eqPos);
                $attrValue = substr($nextPart, $eqPos + 1);
                $newTag->setAttr($attrName, $attrValue);
            }
            else if (strlen($nextPart) > 0 && $nextPart[0] === '%') {
                // The next part is a new tag
                self::parseTag(substr($tagStr, $lastPos - $nextLength - 1), $newTag);
                // Move all indexes to the end
                $nextPos = false;
                $lastPos = strlen($tagStr);
                $nextPart = '';
            }
            else {
                $newTag->addContent($nextPart);
            }
        }

        if ($lastPos < strlen($tagStr)) {
            $nextPart = substr($tagStr, $lastPos);
            $eqPos = strpos($nextPart, '=');

            if ($eqPos === false) {
                $newTag->addContent(substr($tagStr, $lastPos));
            }
            else {
                $attrName = substr($nextPart, 0, $eqPos);
                $attrValue = substr($nextPart, $eqPos + 1);
                $newTag->setAttr($attrName, $attrValue);
            }
        }

        if ($parent !== null) {
            $parent->addContent($newTag);
        }

        return $newTag;
    }
}
