<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html;

use phbrick\markup\ContainerTag;
use phbrick\markup\Indent;
use phbrick\markup\Tag;

/**
 * Models an HTML tag extending the base tag class with html-specific details.
 *
 */
class HtmlTag extends ContainerTag
{

    /**
     * @param HtmlTag $htmlTag
     * @return HtmlTag
     */
    public static function castHtmlTag(HtmlTag $htmlTag)
    {
        return $htmlTag;
    }

    /**
     * Gets the id attribute value if set or null if not set.
     * @return string
     */
    public function getId()
    {
        return $this->getAttr('id', null);
    }

    /**
     * Sets
     * @param string $id
     */
    public function setId($id)
    {
        $this->setAttr('id', $id);
    }

    public function __construct(
        $name = self::VOID,
        array $attributes = [],
        $content = null,
        $type = self::TYPE_REGULAR)
    {
        parent::__construct($name, $attributes, $content, $type);
    }

    protected function createRenderer($context = null, $indent = null)
    {
        if (!$indent) {
            $indent = Indent::createFromContext($context);
        }

        $tagRenderer = HtmlTagRenderer::createFromContext($context);
        $tagRenderer->setIndent($indent);

        return $tagRenderer;
    }

    /**
     * Retrieves the first tag with a matching attribute named 'id'.
     *
     * This method does a deep-first search over all tags and ignores non-tag contents.
     *
     * @param string $id
     * @return Tag|bool
     */
    public function findTagById($id)
    {
        $contents = $this->getContent();

        if (!is_array($this->content) || empty($id)) {
            return false;
        }

        $result = false;

        foreach ($contents as $content) {
            if (!is_object($content) || !is_a($content, 'phbrick\markup\Tag')) {
                continue;
            }

            $tag = Tag::castTag($content);

            if ($tag->getAttr('id', null) == $id) {
                $result = $content;
            }
            else if ($tag instanceof HtmlTag) {
                $tag = HtmlTag::castHtmlTag($tag);
                $result = $tag->findTagById($id);
            }

            if ($result) {
                break;
            }
        }

        return $result;
    }

    public function __call($methodName, $methodArgs)
    {
        $tag = call_user_func_array([HtmlBuilder::class, $methodName], $methodArgs);

        $this->addContent($tag);

        return $tag;
    }

    /**
     * Adds child tags to the current tag and returns this current tag.
     *
     * The children array must be an array of HtmlTag instances, tag names or a map of tag names and tag children
     *
     * @param mixed ...$childrens
     * @return $this
     */
    public function children(...$childrens)
    {
        HtmlBuilder::children($this, ...$childrens);
        return $this;
    }
}
