<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html;

use InvalidArgumentException;

trait CssClassTrait
{
    /**
     * @var array
     */
    private $cssClasses = [];

    /**
     * Gets array of css class names to apply to the html head tag
     * @return array
     */
    public function getCssClasses()
    {
        return $this->cssClasses;
    }

    /**
     * Sets array of css class names to apply to the html head tag
     * @param array|string $cssClasses
     */
    public function setCssClasses($cssClasses)
    {
        if (is_string($cssClasses)) {
            $this->cssClasses = explode(' ', $cssClasses);
        }
        else if (is_array($cssClasses)) {
            $this->cssClasses = $cssClasses;
        }
        else {
            throw new InvalidArgumentException('Argument $cssClasses must be an array or an string');
        }
    }

    /**
     * Adds a css class to apply to the html head tag
     * @param array|string $cssClass
     * @throws InvalidArgumentException
     */
    public function addCssClass($cssClass)
    {
        if (is_string($cssClass)) {
            $cssClass = explode(' ', $cssClass);
        }

        if (!is_array($cssClass)) {
            throw new InvalidArgumentException('Argument $cssClass must be an array or an string');
        }

        $this->cssClasses = array_unique(array_merge($this->cssClasses, $cssClass));
    }
}
