<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html;

use phbrick\string\RenderableToStringTrait;
use phbrick\string\SafeStringRenderTrait;

/**
 * Models the body tag for an html document so it can be used as the base to
 * build up a document layout.
 *
 * The body instance is not an HtmlTag-like instance, is a custom one with some
 * other different properties to limit what can be done with it.
 */
class HtmlBody
{

    use CssClassTrait, SafeStringRenderTrait, RenderableToStringTrait;

    /**
     * @var HtmlDocument
     */
    private $document = null;

    /**
     * Gets the parent document
     * @return HtmlDocument
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Sets the parent document
     * @param HtmlDocument $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @var array
     */
    private $content = [];

    /**
     * Gets an array with the contents of the body
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Adds a content to the body
     * @param HtmlTag|string $content
     */
    public function addContent($content)
    {
        $this->content[] = $content;
    }

    public function __construct()
    {
    }

    /**
     *
     * @param array $context
     * @return HtmlTag
     */
    public function render(array $context = null)
    {

        $body = new HtmlTag('body');
        $cssClasses = $this->getCssClasses();

        if (!empty($cssClasses)) {
            $body->setAttr('class', $cssClasses);
        }

        $body->addContent($this->content);

        return $body;
    }
}
