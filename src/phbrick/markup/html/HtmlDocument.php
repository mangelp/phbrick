<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html;

use phbrick\markup\Document;
use phbrick\markup\html\header\HtmlHeader;
use phbrick\markup\Indent;

/**
 * Models an html document as a composition of two other components: a header
 * and a body.
 */
class HtmlDocument extends Document
{

    const XHTML_NMSP = 'http://www.w3.org/1999/xhtml';

    use CssClassTrait;

    /**
     * @var Doctype
     */
    private $doctype = null;

    /**
     * Gets  the document doctype declaration
     * @return Doctype
     */
    public function getDoctype()
    {
        return $this->doctype;
    }

    /**
     * Sets  the document doctype declaration
     * @param Doctype $doctype
     */
    public function setDoctype(Doctype $doctype)
    {
        $this->doctype = $doctype;
    }

    /**
     * @var array
     */
    private $namespaces = [];

    /**
     * Gets an array map with the xml namespaces to use in the document
     * @return array
     */
    public function getNamespaces()
    {
        return $this->namespaces;
    }

    /**
     * Sets an array map with the xml namespaces to use in the document
     * @param array $namespaces
     * @internal param array $xmlNamespaces
     */
    public function setNamespaces(array $namespaces)
    {
        $this->namespaces = $namespaces;
    }

    /**
     * Sets a new namespace for a given prefix
     * @param string $prefix
     * @param string $namespace
     */
    public function setNamespace($prefix, $namespace)
    {
        $this->namespaces[$prefix] = $namespace;
    }

    /**
     * Removes the given namespace by its prefix.
     * @param string $prefix
     */
    public function removeNamespace($prefix)
    {
        if (isset($this->namespaces[$prefix])) {
            unset($this->namespaces[$prefix]);
        }
    }

    /**
     * Checks if a given namespace is defined by its prefix
     * @param string $prefix
     * @return bool
     */
    public function hasNamespace($prefix)
    {
        return isset($this->namespaces[$prefix]);
    }

    /**
     * @var HtmlHeader
     */
    private $header = null;

    /**
     * Gets the instance that models the html header
     * @return HtmlHeader
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Sets the instance that models the html header
     * @param HtmlHeader $header
     */
    public function setHeader(HtmlHeader $header)
    {
        $this->header = $header;
    }

    /**
     * @var HtmlBody
     */
    private $body = null;

    /**
     * Gets the object that models the document body tag along his contents
     * @return HtmlBody
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Sets the object that models the document body tag along his contents
     * @param HtmlBody $body
     */
    public function setBody(HtmlBody $body)
    {
        $this->body = $body;
    }

    /**
     * @var bool
     */
    private $useXml = false;

    /**
     * Gets if this document is an XML document
     * @return bool
     */
    public function isUseXml()
    {
        return $this->useXml;
    }

    /**
     * Sets if this document is an XML document
     * @param bool $useXml
     */
    public function setUseXml($useXml)
    {
        $this->useXml = (bool)$useXml;
    }

    public function __construct($doctype = null, $lang = null)
    {
        $this->doctype = new Doctype($doctype);

        $this->header = new HtmlHeader();
        $this->header->setDocument($this);

        if ($lang) {
            $this->header->setLang($lang);
        }

        $this->body = new HtmlBody();
        $this->body->setDocument($this);
    }

    /**
     * Renders the HtmlDocument.
     * @param array $context
     * @return HtmlTag Tag containing all the document parts.
     */
    public function render(array $context = null)
    {
        // Wrap everything inside a void tag
        $document = new HtmlTag();

        if ($this->isUseXml()) {
            $document->addContent($this->renderXmlDeclaration($context));
        }

        $document->addContent($this->renderDoctypeDeclaration($context));

        $html = new HtmlTag('html');

        $cssClasses = $this->getCssClasses();

        if (!empty($cssClasses)) {
            $html->setAttr('class', $cssClasses);
        }

        $document->addContent($html);

        if (!empty($this->namespaces)) {
            foreach ($this->namespaces as $nmspPrefix => $nmspValue) {
                if ($nmspPrefix == 'default') {
                    $html->attr('xmlns', $nmspValue);
                }
                else {
                    $html->attr('xmlns:' . $nmspPrefix, $nmspValue);
                }
            }
        }

        $html->addContent($this->renderDocumentHeader($context));
        $html->addContent($this->renderDocumentBody($context));

        return $document;
    }

    /**
     * Renders the xml document declaration.
     * The encoding defaults to UTF-8.
     * @param array $context
     * @return HtmlTag|string
     */
    protected function renderXmlDeclaration(array $context = null)
    {
        $encoding = 'UTF-8';

        if ($this->header) {
            $encoding = strtoupper($this->header->getCharset());
        }

        return new HtmlTag(
            HtmlTag::VOID, [],
            '<?xml version="1.0" encoding="' . $encoding . '"?>');
    }

    /**
     * Renders the doctype declaration. Defaults to the html5 doctype.
     * @param array $context
     * @return HtmlTag|string
     */
    protected function renderDoctypeDeclaration(array $context = null)
    {
        if ($this->doctype) {
            return $this->doctype->render();
        }
        else {
            return Doctype::DEFAULT_DOCTYPE_STRING;
        }
    }

    /**
     * Renders the document header.
     *
     * @param array $context
     * @return HtmlTag|string
     */
    protected function renderDocumentHeader(array $context = null)
    {
        if ($this->header) {
            return $this->header->render();
        }
        else {
            return new HtmlTag('head');
        }
    }

    /**
     * Renders the document body.
     *
     * @param array $context
     * @return HtmlTag|string
     */
    protected function renderDocumentBody(array $context = null)
    {
        if ($this->body) {
            return $this->body->render();
        }
        else {
            return new HtmlTag('body');
        }
    }

    /**
     * (non-PHPdoc)
     * @param null $context
     * @param null $indent
     * @return string
     * @see \phbrick\markup\Document::toString()
     */
    public function toString($context = null, $indent = null)
    {
        if ($indent === null && $this->isIndentMarkup()) {
            $indent = Indent::createFromContext($context);
        }

        $html = $this->render($context);
        return $html->toString($context, $indent);
    }
}
