<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\markup\html;

use phbrick\markup\Tag;
use phbrick\markup\TagRenderer;

/**
 * Renders HTML tags
 */
class HtmlTagRenderer extends TagRenderer
{
    public function __construct()
    {
        parent::__construct();

        $this->setDelimiters([
            self::DELIMITER_OPEN_BEGIN      => '<',
            self::DELIMITER_OPEN_END        => [
                Tag::TYPE_REGULAR       => '>',
                Tag::TYPE_EMPTY         => '/>',
                Tag::TYPE_EMPTY_REGULAR => '>',
            ],
            self::DELIMITER_CLOSE_BEGIN     => '</',
            self::DELIMITER_CLOSE_END       => '>',
            self::DELIMITER_ATTR_ASSIGN     => '=',
            self::DELIMITER_ATTR_VALUE_WRAP => '"',
        ]);
    }

    public function renderTagAttribute($name, $value, $order, array $context = [])
    {

        if (is_array($value)) {
            switch ($name) {
                case 'class':
                    $value = $this->renderClassAttribute($value, $context);
                    break;
                case 'style':
                    $value = $this->renderStyleAttribute($value, $context);
                    break;
            }
        }

        return parent::renderTagAttribute($name, $value, $order, $context);
    }

    protected function renderClassAttribute(array $class, array $context = [])
    {
        return trim(implode(' ', $class));
    }

    protected function renderStyleAttribute(array $style, array $context = [])
    {
        $result = [];

        foreach ($style as $key => $value) {
            $result [] = "$key:$value;";
        }

        return implode(' ', $result);
    }

    public function renderCloseTag(Tag $tag, array $context = [])
    {
        switch ($tag->getTagType()) {
            case Tag::TYPE_EMPTY:
            case Tag::TYPE_EMPTY_REGULAR:
                return false;
            default:
                return parent::renderCloseTag($tag, $context);
        }
    }

    public function renderContentTag(Tag $tag, array $context = [])
    {
        switch ($tag->getTagType()) {
            case Tag::TYPE_EMPTY:
            case Tag::TYPE_EMPTY_REGULAR:
                return false;
            default:
                return parent::renderContentTag($tag, $context);
        }
    }
}

