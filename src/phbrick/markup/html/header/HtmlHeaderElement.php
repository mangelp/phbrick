<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup\html\header;

use BadMethodCallException;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\collection\AttributesTrait;
use phbrick\markup\html\HtmlTag;
use phbrick\markup\Tag;
use phbrick\string\SafeStringRenderTrait;

/**
 * Models an HTML header element like a meta tag, a link tag, a script tag, ...
 */
class HtmlHeaderElement extends BaseStrictClass
{

    use AttributesTrait, SafeStringRenderTrait;

    /// Html element names allowed in header
    const ELEM_LINK = 'link';
    const ELEM_META = 'meta';
    const ELEM_SCRIPT = 'script';
    const ELEM_STYLE = 'style';
    const ELEM_NONE = 'none';

    private static $names = [
        self::ELEM_META,
        self::ELEM_LINK,
        self::ELEM_SCRIPT,
        self::ELEM_STYLE,
    ];

    /**
     * Gets an array with all allowed html element names.
     * @return array
     */
    public static function getAllNames()
    {
        return self::$names;
    }

    // Specific header types with direct methods to add them
    const ETYPE_FAVICON_ICON = 'favicon-icon';
    const ETYPE_FAVICON_SHORTCUT = 'favicon-shortcut-icon';
    const ETYPE_HTTP = 'http-equiv';
    const ETYPE_LINK = 'link';
    const ETYPE_META = 'meta';
    const ETYPE_META_PROPERTY = 'meta-property';
    const ETYPE_SCRIPT_INLINE = 'script-inline';
    const ETYPE_SCRIPT_LINK = 'script-link';
    const ETYPE_STYLE_INLINE = 'style-inline';
    const ETYPE_STYLE_LINK = 'style-link';
    const ETYPE_NONE = 'none';

    private static $types = [
        self::ETYPE_FAVICON_ICON,
        self::ETYPE_FAVICON_SHORTCUT,
        self::ETYPE_HTTP,
        self::ETYPE_LINK,
        self::ETYPE_META,
        self::ETYPE_META_PROPERTY,
        self::ETYPE_SCRIPT_INLINE,
        self::ETYPE_SCRIPT_LINK,
        self::ETYPE_STYLE_INLINE,
        self::ETYPE_STYLE_LINK,
    ];

    /**
     * Gets all predefined header element types
     */
    public static function getAllTypes()
    {
        return self::$types;
    }

    /**
     *
     * @param HtmlHeaderElement $element
     * @return HtmlHeaderElement
     */
    public static function cast(HtmlHeaderElement $element)
    {
        return $element;
    }

    /**
     * @var string
     */
    private $type = self::ETYPE_NONE;

    /**
     * Gets the type of header element
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type of header element
     * @param $type
     * @internal param string $elemType
     */
    protected function setType($type)
    {
        $type = strtolower($type);

        if (!in_array($type, self::getAllTypes())) {
            throw new InvalidArgumentException('Unknown HTML header element type: ' . $type);
        }

        $this->type = $type;
    }

    /**
     * @var int
     */
    private $weight = 0;

    /**
     * Gets the sorting weight of this element
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Sets the sorting weight of this element
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @var string
     */
    private $group = null;

    /**
     * Gets the group this header belongs to
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Sets the group this header belongs to
     * @param string $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @var string
     */
    private $content = null;

    /**
     * Gets the header content if it has any
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the tag content if it is defined for the tag type.
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    public function __construct(
        $type = self::ELEM_NONE,
        array $attrs = [],
        $group = null,
        $weight = 0)
    {

        $this->setType($type);

        if (!empty($attrs)) {
            $this->setAttrs($attrs);
        }

        if ($group) {
            $this->setGroup($group);
        }

        if ($weight) {
            $this->setWeight($weight);
        }
    }

    /**
     * Gets if this element is empty by not having attributes nor content.
     */
    public function isEmpty()
    {
        return empty($this->attrs)
            && empty($this->content);
    }

    /**
     * Returns one of the allowed tags for a given predefined header element type
     * @param string $type
     * @return null|string
     * @throws BadMethodCallException
     */
    protected function getTagNameForType($type)
    {
        $tagName = null;

        switch ($type) {
            case self::ETYPE_FAVICON_ICON:
            case self::ETYPE_FAVICON_SHORTCUT:
            case self::ETYPE_LINK:
            case self::ETYPE_STYLE_LINK:
                $tagName = self::ELEM_LINK;
                break;
            case self::ETYPE_HTTP:
            case self::ETYPE_META:
            case self::ETYPE_META_PROPERTY:
                $tagName = self::ELEM_META;
                break;
            case self::ETYPE_SCRIPT_INLINE:
            case self::ETYPE_SCRIPT_LINK:
                $tagName = self::ELEM_SCRIPT;
                break;
            case self::ETYPE_STYLE_INLINE:
                $tagName = self::ELEM_STYLE;
                break;
            default:
                throw new BadMethodCallException('Invalid element type: ' . $this->getType());
        }

        return $tagName;
    }

    /**
     * Gets the allowed tag closing type for a given predefined header element type.
     * @param string $type
     * @return null|string
     * @throws BadMethodCallException
     */
    protected function getTagTypeForType($type)
    {
        $tagType = null;

        switch ($type) {
            case self::ETYPE_FAVICON_ICON:
            case self::ETYPE_FAVICON_SHORTCUT:
            case self::ETYPE_LINK:
            case self::ETYPE_STYLE_LINK:
            case self::ETYPE_HTTP:
            case self::ETYPE_META:
            case self::ETYPE_META_PROPERTY:
                $tagType = Tag::TYPE_EMPTY_REGULAR;
                break;
            case self::ETYPE_SCRIPT_LINK:
            case self::ETYPE_SCRIPT_INLINE:
            case self::ETYPE_STYLE_INLINE:
                $tagType = Tag::TYPE_REGULAR;
                break;
            default:
                throw new BadMethodCallException('Invalid element type: ' . $this->getType());
        }

        return $tagType;
    }

    /**
     * Gets the attributes that are defined for a given predefined header element type
     *
     * @param string $type Header element type
     * @return array<string, null> An array where the keys are the attribute names and the values
     * are all set to the given attribute value, a default value or null if is required but not set.
     */
    protected function getTypeAttributes($type)
    {
        $attributes = [];

        switch ($type) {
            case self::ETYPE_FAVICON_ICON:
                $attributes = [
                    'href' => null,
                    'rel'  => 'icon',
                    'type' => 'image/x-icon',
                ];
                break;
            case self::ETYPE_FAVICON_SHORTCUT:
                $attributes = [
                    'href' => null,
                    'rel'  => 'shortcut icon',
                ];
                break;
            case self::ETYPE_HTTP:
                $attributes = [
                    'content'    => null,
                    'http-equiv' => null,
                ];
                break;
            case self::ETYPE_LINK:
                $attributes = [
                    'href'  => null,
                    'rel'   => null,
                    'type'  => null,
                    'title' => null,
                ];
                break;
            case self::ETYPE_META:
                $attributes = [
                    'value' => null,
                    'name'  => null,
                ];
                break;
            case self::ETYPE_META_PROPERTY:
                $attributes = [
                    'content'  => null,
                    'property' => null,
                ];
                break;
            case self::ETYPE_SCRIPT_INLINE:
                $attributes = [
                    'type' => 'text/javascript',
                ];
                break;
            case self::ETYPE_SCRIPT_LINK:
                $attributes = [
                    'src'   => null,
                    'type'  => 'text/javascript',
                    'async' => null,
                ];
                break;
            case self::ETYPE_STYLE_INLINE:
                $attributes = [
                    'type'  => 'text/css',
                    'media' => 'all',
                ];
                break;
            case self::ETYPE_STYLE_LINK:
                $attributes = [
                    'href'  => null,
                    'type'  => 'text/css',
                    'rel'   => 'stylesheet',
                    'media' => 'all',
                ];
                break;
        }

        ksort($attributes);

        return $this->getAttrs($attributes);
    }

    /**
     * Gets the attributes that uniquely identify an element of a given type.
     *
     * Those attributes are used to identify equal elements to avoid duplications.
     * @param $type
     * @return array
     */
    protected function getIdentityAttributes($type)
    {
        $attributes = [];

        switch ($type) {
            case self::ETYPE_FAVICON_ICON:
                $attributes = [
                    'href', 'rel', 'type',
                ];
                break;
            case self::ETYPE_FAVICON_SHORTCUT:
                $attributes = [
                    'href', 'rel',
                ];
                break;
            case self::ETYPE_HTTP:
                $attributes = [
                    'content', 'http-equiv',
                ];
                break;
            case self::ETYPE_LINK:
                $attributes = [
                    'href', 'rel', 'type',
                ];
                break;
            case self::ETYPE_META:
                $attributes = [
                    'value', 'name',
                ];
                break;
            case self::ETYPE_META_PROPERTY:
                $attributes = [
                    'content', 'property',
                ];
                break;
            case self::ETYPE_SCRIPT_INLINE:
                $attributes = [
                    'type',
                ];
                break;
            case self::ETYPE_SCRIPT_LINK:
                $attributes = [
                    'src', 'type',
                ];
                break;
            case self::ETYPE_STYLE_INLINE:
                $attributes = [
                    'type', 'media',
                ];
                break;
            case self::ETYPE_STYLE_LINK:
                $attributes = [
                    'href', 'type', 'rel', 'media',
                ];
                break;
        }

        $result = $this->getAttrs($attributes);

        ksort($result);

        return $result;
    }

    /**
     * Check if the other element has the same values as this one.
     * @param HtmlHeaderElement $other
     * @return boolean
     */
    public function equals(HtmlHeaderElement $other)
    {
        if ($this->getType() == $other->getType()
            && $this->matchAttrs($other->getAttrs())
            && $this->getContent() == $other->getContent()) {

            return true;
        }

        return false;
    }

    public function render($context = [], $indent = null)
    {
        $tagName = $this->getTagNameForType($this->type);
        $tagType = $this->getTagTypeForType($this->type);

        $tag = new HtmlTag($tagName, $this->getAttrs(), $this->getContent(), $tagType);

        return $tag;
    }
}
