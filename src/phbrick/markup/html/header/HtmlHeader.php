<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\markup\html\header;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\markup\html\HtmlDocument;
use phbrick\markup\html\HtmlTag;
use phbrick\string\SafeStringRenderTrait;

/**
 * Models all the document header parts including the title and all header tags.
 *
 * Added headers can be added into an specific group. Headers can have its own weight and also there is a weight for
 * groups. This allows for having an specific order in headers when they are rendered.
 *
 * Headers can be added with a custom id to identify them or with a generated id. The generated id is a hash function
 * that uses the properties and the name of the concrete header tag to generate an unique id. If a header is added with
 * a duplicated id then it might return an exception based upon the allowed header multiplicity or simply append the
 * value to the existing header.
 *
 */
class HtmlHeader extends BaseStrictClass
{

    use SafeStringRenderTrait;

    /**
     * @var HtmlDocument
     */
    private $document = null;

    /**
     * Gets the parent document
     * @return HtmlDocument
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Sets the parent document
     * @param HtmlDocument $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @var string
     */
    private $lang = null;

    /**
     * Gets the document language. IE: es, en.
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Sets the document language. IE: es, en.
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @var string
     */
    private $charset = 'utf-8';

    /**
     * Gets the document charset. Defaults to UTF-8.
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * Sets the document charset. Defaults to UTF-8.
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @var array
     */
    private $title = [];

    /**
     * Gets the document title
     * @return array
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the title as an string.
     * @param string $separator
     * @return string
     */
    public function getTitleString($separator = ' - ')
    {
        return implode($separator, $this->title);
    }

    /**
     * Sets the document title components replacing the current ones
     * @param array|string $title
     */
    public function setTitle($title)
    {
        if (!is_array($title)) {
            $title = [$title];
        }

        $this->title = $title;
    }

    /**
     * Adds another component to the title.
     * @param string $title
     */
    public function addTitle($title)
    {
        $this->title[] = (string)$title;
    }

    /**
     * @var array
     */
    private $elements = [];

    /**
     * Gets the elements added to the header
     * @return array
     */
    public function getElements()
    {
        return $this->elements;
    }

    private $elementCount = 0;

    public function getElementCount()
    {
        return $this->elementCount;
    }

    /**
     * Sets the elements added to the header
     * @param array $elements
     */
    protected function setElements($elements)
    {
        $this->elements = $elements;
    }

    /**
     * @var HtmlHeaderGroups
     */
    private $groups = null;

    /**
     * Gets the groups for organizing elements
     * @return HtmlHeaderGroups
     */
    public function getGroups()
    {
        return $this->groups;
    }

    protected function setGroups(HtmlHeaderGroups $groups)
    {
        $this->groups = $groups;
    }

    /**
     *
     * @param HtmlDocument $document
     * @param HtmlHeaderGroups $groups
     */
    public function __construct(HtmlDocument $document = null, HtmlHeaderGroups $groups = null)
    {
        if ($document) {
            $this->setDocument($document);
        }

        if ($groups === null) {
            $groups = new HtmlHeaderGroups();
            $groups->setHeader($this);
        }

        $this->setGroups($groups);
    }

    /**
     * Adds an element to the header.
     *
     * @param HtmlHeaderElement $elem
     * @param null $group
     */
    public function addElement(HtmlHeaderElement $elem, $group = null)
    {

        if ($this->hasElement($elem)) {
            throw new InvalidArgumentException('Cannot add the same element instance twice');
        }

        $this->elements[] = $elem;
        $this->elementCount++;

        if (empty($group)) {
            $group = 'default';
        }

        if (!$this->getGroups()->hasGroup($group)) {
            $this->getGroups()->addGroup($group);
        }

        $this->getGroups()->addToGroup($elem, $group);
    }

    /**
     * Creates and adds a new element to the header.
     *
     * @param string $elemType Name of the element type to be added
     * @param array $attributes Attributes of the element. Empty by default.
     * @param mixed $content Content of the element if supported by it. Defaults to null.
     * @param null $group
     * @return HtmlHeaderElement The element created and added.
     * @internal param mixed $orderKey Custom value to sort headers with.
     */
    public function add($elemType, array $attributes = [], $content = null, $group = null)
    {
        $elem = new HtmlHeaderElement($elemType, $attributes, $group);

        if ($content) {
            $elem->setContent($content);
        }

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Removes all the elements that match the parameters passed in and returns them with the same
     * keys they had in the internal collection.
     *
     * @param string $elemType
     * @param array $attributes
     * @param null $content
     * @param null $group
     * @return array the elements removed
     */
    public function remove($elemType = null, array $attributes = [], $content = null, $group = null)
    {
        $elements = $this->findElements($elemType, $attributes, $content, $group, true);

        foreach ($elements as $pos => $elem) {
            $elem = HtmlHeaderElement::cast($elem);
            $this->removeElement($elem);
        }

        return $elements;
    }

    /**
     * Removes a header element.
     *
     * It can also remove all those elements that matches the equals comparison.
     *
     * @param HtmlHeaderElement $searchElem
     * @param bool|string $same If true will compare references instead of using equals. When false use
     * equals.
     * @param bool|string $allMatching If true and $same is false will remove every instance if not it
     * will break the loop after removing the first one.
     */
    public function removeElement(HtmlHeaderElement $searchElem, $same = false, $allMatching = true)
    {
        foreach ($this->elements as $pos => $headerElem) {
            if (($same && $searchElem == $headerElem)
                || (!$same && $searchElem->equals($headerElem))) {

                array_splice($this->elements, $pos, 1, []);
                --$this->elementCount;

                $this->getGroups()->removeFromGroup($headerElem, true);

                if (!$same && !$allMatching) {
                    break;
                }
            }
        }
    }

    /**
     * Checks if there is any element that matches the given parameters.
     *
     * @param string $type
     * @param array $attributes
     * @return bool
     * @internal param string $name
     */
    public function has($type = null, array $attributes = [])
    {
        $elements = $this->findElements($type, $attributes, false);

        if (empty($elements)) {
            return false;
        }

        return true;
    }

    /**
     * Check if a given element instance is already added.
     * @param HtmlHeaderElement $elemToFind
     * @return boolean
     */
    public function hasElement(HtmlHeaderElement $elemToFind)
    {
        foreach ($this->elements as $pos => $elem) {
            if ($elemToFind === $elem) {
                return true;
            }
        }

        return false;
    }

    /**
     * Find elements that matches the properties specified and returns them.
     *
     * @param string $elemType Type of element (one of ETYPE_* constants)
     * @param array $attributes Element attributes to match
     * @param null $content
     * @param null $group
     * @param bool $multiple
     * @return array
     */
    public function findElements($elemType = null, array $attributes = [], $content = null, $group = null, $multiple = true)
    {
        $foundElements = [];

        foreach ($this->elements as $key => $elem) {
            $elem = HtmlHeaderElement::cast($elem);

            if (!empty($elemType) && $elem->getType() != $elemType) {
                continue;
            }

            if (!empty($attributes)
                && !$elem->matchAttrs($attributes)) {
                continue;
            }

            if (!empty($content)
                && $elem->getContent() != $content) {
                continue;
            }

            if (!empty($group)
                && $elem->getGroup() != $group) {
                continue;
            }

            $foundElements[] = $elem;

            if (!$multiple) {
                break;
            }
        }

        return $foundElements;
    }

    /**
     * Adds a new meta element with a property attribute to the header.
     *
     * @param $property
     * @param string $content Property content
     * @param string $group
     * @return HtmlHeaderElement
     * @internal param string $name Property name
     */
    public function addMetaProperty($property, $content, $group = null)
    {

        $elem = new HeaderMetaProperty($property, $content);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Adds a new meta element with a name attribute to the header.
     * @param string $name
     * @param string $value
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addMetaName($name, $value, $group = null)
    {
        $elem = new HeaderMeta($name, $value);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Adds a new meta element with a http-equiv attribute to the header.
     *
     * @param $httpEquiv
     * @param string $content
     * @param string $group
     * @return HtmlHeaderElement
     * @internal param string $name
     */
    public function addMetaHttp($httpEquiv, $content, $group = null)
    {
        $elem = new HeaderHttp($httpEquiv, $content);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Adds a link element
     * @param string $href
     * @param string $rel
     * @param string $type
     * @param string $title
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addLink($href, $rel, $type = null, $title = null, $group = null)
    {

        $elem = new HeaderLink($href, $rel, $type, $title);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Adds a favicon icon.
     *
     * @param string $href Favicon url
     * @param string $type Favicon mime type. Defaults to image/x-icon
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addFaviconIcon($href, $type = "image/x-icon", $group = null)
    {

        $elem = new HeaderFaviconIcon($href, $type);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     *
     * @param string $href
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addFaviconShortcutIcon($href, $group = null)
    {
        $elem = new HeaderFaviconShortcut($href);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     *
     * @param string $src
     * @param bool $async
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addScriptLink($src, $async = false, $group = null)
    {
        $elem = new HeaderScriptLink($src, $async);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     *
     * @param string $scriptCode
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addScript($scriptCode, $group = null)
    {
        $elem = new HeaderScriptInline($scriptCode);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     *
     * @param string $href
     * @param string $media
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addStyleLink($href, $media = 'all', $group = null)
    {
        $elem = new HeaderStyleLink($href, $media);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     *
     * @param string $styleCode
     * @param string $media
     * @param string $group
     * @return HtmlHeaderElement
     */
    public function addStyle($styleCode, $media = 'all', $group = null)
    {
        $elem = new HeaderStyleInline($styleCode, $media);

        $this->addElement($elem, $group);

        return $elem;
    }

    /**
     * Generates a markup instance per each header element and adds them
     * into an html tag instance.
     *
     * @param array $context
     * @return HtmlTag
     */
    public function render(array $context = null)
    {
        $head = new HtmlTag('head');
        $title = new HtmlTag('title', [], $this->getTitleString());

        $head->addContent($title);

        foreach ($this->elements as $pos => $elem) {
            $elem = HtmlHeaderElement::cast($elem);
            $head->addContent($elem);
        }

        return $head;
    }

    public function toString(array $context = null, $indent = null)
    {
        $html = $this->render($context);
        return $html->toString($context, $indent);
    }
}
