<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup\html\header;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use RuntimeException;

/**
 * Manages grouping and sorting of HtmlHeaderElement instances and groups that will contain them.
 */
class HtmlHeaderGroups extends BaseStrictClass
{
    private $groups = [];

    /**
     * Gets all the defined groups.
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @var callback
     */
    private $groupComparator = null;

    /**
     * Gets the comparison callable to sort groups
     * @return callable
     */
    public function getGroupComparator()
    {
        return $this->groupComparator;
    }

    /**
     * Sets the comparison callable to sort groups
     * @param callable $groupComparator
     */
    public function setGroupComparator(callable $groupComparator)
    {
        $this->groupComparator = $groupComparator;
    }

    /**
     * @var callable
     */
    private $headerElemComparator = null;

    /**
     * Gets the comparison callable to sort headers inside a group
     * @return callable
     */
    public function getHeaderElemComparator()
    {
        return $this->headerElemComparator;
    }

    /**
     * Sets the comparison callable to sort headers inside a group
     * @param callable $headerElemComparator
     */
    public function setHeaderElemComparator(callable $headerElemComparator)
    {
        $this->headerElemComparator = $headerElemComparator;
    }

    /**
     * @var HtmlHeader
     */
    private $header = null;

    /**
     * Gets the HtmlHeader instance that is using this group manager
     * @return HtmlHeader
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Sets the HtmlHeader instance that is using this group manager
     * @param HtmlHeader $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    public function __construct(callable $groupComparator = null, callable $headerElemComparator = null)
    {

        if ($groupComparator) {
            $this->setGroupComparator($groupComparator);
        }

        if ($headerElemComparator) {
            $this->setHeaderElemComparator($headerElemComparator);
        }
    }

    public function isEmpty()
    {
        return empty($this->groups);
    }

    /**
     * Asserts that the group is defined.
     *
     * @param string $group
     * @throws InvalidArgumentException
     */
    protected function assertGroupDefined($group)
    {
        if (!isset($this->groups[$group])) {
            throw new InvalidArgumentException('Group is not defined: ' . $group);
        }
    }

    /**
     * Adds a new group with the given weight and sorts all the groups to keep them sorted.
     *
     * @param string $group Name of the group
     * @param int $weight Weight for ordering the group. Defaults to 0.
     * @param bool $noSort If true groups will not be sorted. If false (default) sorts them.
     *
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function addGroup($group, $weight = 0, $noSort = false)
    {

        if (isset($this->groups[$group])) {
            throw new InvalidArgumentException('Cannot overwrite existing group ' . $group);
        }

        if (!is_string($group)) {
            throw new InvalidArgumentException('Parameter $group must be an string');
        }

        $this->createGroup($group, $weight);

        if (!$noSort) {
            $this->sortGroups();
        }
    }

    /**
     * Creates a new group with the given name and adds it to the groups map.
     *
     * @param string $group
     * @param int $weight
     * @param array $headers
     */
    protected function createGroup($group, $weight, array $headers = [])
    {
        $this->groups[$group] = [
            'name'    => $group,
            'weight'  => (int)$weight,
            'headers' => $headers,
        ];
    }

    /**
     * Removes a given group moving all the headers to an alternative group.
     *
     * @param string $group Group to be removed
     * @param string $moveHeadersToGroup Group to move the headers to if set.
     * @throws InvalidArgumentException If the group to remove does not exists or if the group to
     * move the headers to does not exists.
     */
    public function removeGroup($group, $moveHeadersToGroup = null)
    {
        $this->assertGroupDefined($group);

        if ($moveHeadersToGroup !== null && !isset($this->groups[$moveHeadersToGroup])) {
            throw new InvalidArgumentException('Group to move headers is not defined: ' . $moveHeadersToGroup);
        }

        $headers = $this->groups[$group]['headers'];

        unset($this->groups[$group]);

        if ($moveHeadersToGroup) {
            $this->addToGroup($headers, $moveHeadersToGroup);
        }
    }

    /**
     * Gets if the group exists
     * @param string $group
     * @return bool
     */
    public function hasGroup($group)
    {
        return isset($this->groups[$group]);
    }

    /**
     * Adds headers to a given group.
     * @param HtmlHeaderElement|array $headers
     * @param string $group
     */
    public function addToGroup($headers, $group)
    {
        $this->assertGroupDefined($group);

        if (!is_array($headers)) {
            $headers = [$headers];
        }

        foreach ($headers as $pos => $header) {
            $header = HtmlHeaderElement::cast($header);
            $this->groups[$group]['headers'][] = $header;
            $header->setGroup($group);
        }

        $this->sortHeaders($group);
    }

    /**
     * Sorts all headers of a group using a callable
     *
     * If no callable is set it will use the default header comparator if set, but if it is also
     * not set it will use a default comparison using the header weight.
     *
     * @param string $group
     * @param callable $comparator
     */
    protected function sortHeaders($group, callable $comparator = null)
    {

        if (!isset($this->groups[$group]) || empty($this->groups[$group]['header'])) {
            return;
        }

        if (!$comparator && $this->headerElemComparator) {
            $comparator = $this->headerElemComparator;
        }

        if (!$comparator) {
            $comparator = function(HtmlHeaderElement $headerA, HtmlHeaderElement $headerB) {
                return $headerA->getWeight() - $headerB->getWeight();
            };
        }

        usort($this->groups[$group]['header'], $comparator);
    }

    /**
     * Sorts all groups using a callable as comparator.
     *
     * If no callable is set it will use the default group comparator if set, but if it is also
     * not set it will use a default comparison using the group weight.
     * @param callable $comparator
     */
    protected function sortGroups(callable $comparator = null)
    {

        if (!$comparator && $this->groupComparator) {
            $comparator = $this->groupComparator;
        }

        if (!$comparator) {
            $comparator = function($groupA, $groupB) {
                return $groupA['weight'] - $groupB['weight'];
            };
        }

        uasort($this->groups, $comparator);
    }

    /**
     * Gets all the headers set for a given group
     * @param string $group
     * @return mixed
     * @throws InvalidArgumentException If the group doesn't exists
     */
    public function getGroupHeaders($group)
    {
        $this->assertGroupDefined($group);

        return $this->groups[$group]['headers'];
    }

    /**
     * Removes a given header from his group.
     *
     * All the group headers will be iterated and those that match the criteria removed.
     *
     * @param HtmlHeaderElement $header
     * @param bool $same If true will remove only the same instance, if false will use equals
     * to determine if it will be removed or not.
     * @throws InvalidArgumentException
     */
    public function removeFromGroup(HtmlHeaderElement $header, $same = false)
    {

        $headerGroup = $header->getGroup();

        if (empty($headerGroup)) {
            throw new InvalidArgumentException('The header element does not have the group name set');
        }

        foreach ($this->groups[$header->getGroup()]['headers'] as $pos => $headerElem) {
            if (($same && $header === $headerElem)
                || (!$same && $header->equals($headerElem))) {

                array_splice($this->groups[$header->getGroup()]['headers'], $pos, 1, []);
            }
        }
    }

    /**
     * Removes all headers from all groups they might be set.
     * @param array|HtmlHeaderElement $headers
     * @param bool $same If true will remove only the same instance, if false will use equals
     * to determine if it will be removed or not.
     */
    public function removeFromAllGroups($headers, $same = false)
    {

        if (!is_array($headers)) {
            $headers = [$headers];
        }

        foreach ($headers as $key => $header) {
            $this->removeFromGroup($header, $same);
        }
    }

    /**
     * Sorts all the headers inside each group and sorts all the groups.
     */
    public function sort()
    {
        $this->sortGroups();

        foreach ($this->groups as $groupName => $groupDef) {
            $this->sortHeaders($groupName);
        }
    }

    /**
     * Returns all the groups
     * @return mixed
     */
    public function getSortedElements()
    {
        $this->sort();

        $headers = [];

        foreach ($this->groups as $pos => $group) {
            $headers[] = $group['headers'];
        }

        $elements = call_user_func_array(
            'array_merge',
            $headers);

        return $elements;
    }
}
