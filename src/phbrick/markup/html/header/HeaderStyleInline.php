<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup\html\header;

class HeaderStyleInline extends HtmlHeaderElement
{

    public function __construct($content = null, $media = 'all')
    {
        parent::__construct(self::ETYPE_STYLE_INLINE, [
            'media' => $media,
            'type'  => 'text/css',
        ]);

        if ($content) {
            $this->setContent($content);
        }
    }
}
