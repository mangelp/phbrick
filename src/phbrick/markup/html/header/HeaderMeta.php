<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup\html\header;

class HeaderMeta extends HtmlHeaderElement
{

    public function __construct($name = null, $value = null)
    {
        parent::__construct(self::ETYPE_META, [
            'name'    => $name,
            'content' => $value,
        ]);
    }
}
