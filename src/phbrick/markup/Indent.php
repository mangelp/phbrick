<?php

namespace phbrick\markup;

use ArrayAccess;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Types;

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

/**
 * Models markup indentation generator.
 *
 * By defaults indents with a line break followed by by a tab.
 */
class Indent extends BaseStrictClass
{

    const INDENT_TAB = "\t";
    const INDENT_SPACE = " ";

    const LINE_BREAK_LF = "\n";
    const LINE_BREAK_CR = "\r";
    const LINE_BREAK_CRLF = "\r\n";

    use SafeStringRenderTrait;

    /**
     * @var int
     */
    private $level = 0;

    /**
     * Gets the current indentation level. It starts at 0
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Sets the current indentation level. It starts at 0
     * @param int $level
     */
    public function setLevel($level)
    {
        if (!is_int($level) || $level < 0) {
            throw new InvalidArgumentException('Parameter $level must be an integer greater than or equals to 0');
        }

        $this->level = (int)$level;
    }

    /**
     * @var string
     */
    private $indentString = self::INDENT_TAB;

    /**
     * Gets the string used to pad and indent and that is also repeated with each increment of indentation.
     * @return string
     */
    public function getIndentString()
    {
        return $this->indentString;
    }

    /**
     * Sets the string used to pad and indent and that is also repeated with each increment of indentation.
     * @param string $indentString
     */
    public function setIndentString($indentString)
    {
        $this->indentString = $indentString;
    }

    /**
     * @var string
     */
    private $lineBreak = self::LINE_BREAK_LF;

    /**
     * Gets the indent leading line break
     * @return string
     */
    public function getLineBreak()
    {
        return $this->lineBreak;
    }

    /**
     * Sets the indent leading line break
     * @param string $lineBreak
     */
    public function setLineBreak($lineBreak)
    {
        $this->lineBreak = $lineBreak;
    }

    /**
     * Increases indentation level
     * @return Indent self
     */
    public function inc()
    {
        ++$this->level;

        return $this;
    }

    /**
     * Decreases indentation level.
     *
     * Cannot decrease past 0
     *
     * @return Indent self
     */
    public function dec()
    {
        $this->level = $this->level > 0 ?
            --$this->level :
            0;

        return $this;
    }

    /**
     *
     * @param string $indentString
     * @param number $level
     * @param string $lineBreak
     */
    public function __construct($indentString = null, $level = null, $lineBreak = null)
    {
        if ($indentString !== null) {
            $this->setIndentString($indentString);
        }

        if ($level !== null) {
            $this->setLevel($level);
        }

        if ($lineBreak !== null) {
            $this->setLineBreak($lineBreak);
        }
    }

    /**
     * Generates the indentation string
     * @param mixed $context
     * @return string
     */
    public function render($context = null)
    {
        return $this->lineBreak
            . str_repeat($this->indentString, $this->level);
    }

    /**
     * @param Indent $indent
     * @return Indent
     */
    public static function cast(Indent $indent)
    {
        return $indent;
    }

    /**
     * Creates a new Indent instance that is empty
     * @return Indent
     */
    public static function createEmpty()
    {
        return new Indent('', 0, '');
    }

    /**
     * Creates an indent initializing it from the context.
     *
     * The context can contain the next keys:
     *  + indentString: Indentation string repeated for each level.
     *  + indentLevel: Indentation level (indentation depth).
     *  + indentLineBreak: Line break chars to prepend to each indentation.
     *  + indent: Can be one of Indent instance, array, string or integer. Where each one is:
     *    - Indent instance: Its properties are copied over the new one.
     *    - Array: An array that can contain indent properties: indentString, indentLevel, indentLineBreak,
     *    - String: Indentation string.
     *    - Integer: Indentation level. It is assumed that the indentation string is an space.
     *
     * If an Indent instance is provided through the context its properties will be copied over
     * the new one before looking for them in the context.
     * @param ArrayAccess|array $context
     * @return Indent
     * @example
     * // Both indents has the same indent string
     * $indent1 = Indent::createFromContext(['indent' => 'x']);
     * $indent2 = Indent::createFromContext(['indentString' => 'x']);
     *
     * // Initialize everything from array
     * $indent1 = Indent::createFromContext(['indent' => [
     *       'indentString' => "Y",
     *       'indentLevel' => "2",
     *       'indentLineBreak' => "X",
     *   ]]);
     * $indent2 = Indent::createFromContext([
     *       'indentString' => "Y",
     *       'indentLevel' => "2",
     *       'indentLineBreak' => "X",
     *   ]);
     *
     * // Alter a provided indentation instance
     * $indent2 = Indent::createFromContext([
     *       'indent' => $indent1,
     *       'indentLevel' => "4",
     *   ]);
     */
    public static function createFromContext($context = null)
    {
        $indent = self::createEmpty();

        if (empty($context)) {
            return $indent;
        }

        Types::assertArrayAccess($context);

        $hasIndentInContext = isset($context['indent']);
        $cls = self::cls();

        if ($hasIndentInContext && is_string($context['indent'])) {
            $indent->setIndentString($context['indent']);
        }
        else if ($hasIndentInContext && is_numeric($context['indent'])) {
            $indent->setLevel((int)$context['indent']);
            $indent->setIndentString(Indent::INDENT_SPACE);
        }
        else if ($hasIndentInContext && is_a($context['indent'], $cls)) {
            $indentToCopy = self::cast($context['indent']);
            $indent->setLevel($indentToCopy->getLevel());
            $indent->setIndentString($indentToCopy->getIndentString());
            $indent->setLineBreak($indentToCopy->getLineBreak());
        }
        else if ($hasIndentInContext && Types::isArrayAccess($context['indent'])) {
            // Maybe the context contains an array with all indent properties
            $context = $context['indent'];
        }

        if (isset($context['indentString']) && is_string($context['indentString'])) {
            $indent->setIndentString($context['indentString']);
        }

        if (isset($context['indentLevel']) && is_numeric($context['indentLevel'])) {
            $indent->setLevel((int)$context['indentLevel']);
        }

        if (isset($context['indentLineBreak']) && is_string($context['indentLineBreak'])) {
            $indent->setLineBreak($context['indentLineBreak']);
        }

        return $indent;
    }
}
