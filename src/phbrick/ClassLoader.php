<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * (c) 2014-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use InvalidArgumentException;
use phbrick\classLoader\NsObject;
use phbrick\classLoader\NsPrefix;
use phbrick\classLoader\NsPrefixGroup;
use phbrick\classLoader\NsPrefixOptions;
use RuntimeException;

/**
 * Loader for namespaced classes and interfaces, non PSR-compliant.
 *
 * This loader does not supports non-namespaced classes, it does not uses the
 * include path and is compatible with phar archives.
 *
 * To configure loading for a given library a namespace prefix must be
 * registered with all the information needed for class loading.
 *
 * A namespace prefix is made up of all namespace identifiers starting with the
 * root namespace identifier (usually the vendor as per PSR-1/2/4
 * recommendations) and followed by one ore more sub-namespaces. For example
 * foo\var or vendor\name\space.
 *
 * Namespaces prefixes are case-sensitive, but registering a namespace that only
 * differs in case with another registered namespace will cause an error and an
 * exception will be thrown.
 *
 * Namespaces must contain only ASCII letters (both cases), numbers and
 * underscores but take into account that if the namespace does not have any
 * namespace separators "\" the presence of underscores will make it to be
 * handled as a pseudo-namespaced name (namespace that uses underscores to
 * separate namespace components like ZendFramework-1 does).
 *
 * Registering prefixes with the same root but different sub-namespace
 * sequences is supported to allow setting concrete loading options for a
 * concrete sub-namespace sequence or loading different namespaces with the same
 * root from different locations.
 *
 * For example you could register Vendor1\Foo to a path and Vendor1\Foo\Bar to
 * another path or add two paths to Vendor1\Foo, The idea is to allow different
 * libraries to provide classes for the same namespaces and have an specific
 * load order or options for them.
 *
 * Using alternative namespace separators is allowed if properly configured so
 * a namespace prefix of "foo.var" would be registered as "foo\var".
 * This class loader allows using non-standard namespace separators like
 * underscores, dots and slashes, allowing to configure this as both a default
 * behaviour or as a configuration key for each registered prefix.
 *
 * For each registered namespace prefix a set of options can be set to customize
 * the behavior of class loading when the namespace prefixes match. The
 * concrete list of options can be checked in the documentation of
 * addPrefix($namespacePrefix, $options) method, but some of these options are:
 *   + Specify one ore more root path(s) to look for the class file.
 *   + Delegate into another class loader or use a class path mapper that will
 *     return one or more full paths to load the class from.
 *   + Use a different file extension for classes (like .inc) or use more
 *     than one file extension when trying to locate the files, ej: .php, .inc.
 *   + Specify the removal of a portion from the beginning of the class
 *     namespace before converting it to a path.
 *
 * When a class loading is requested all registered prefixes that match the
 * beginning of the namespace are collected and used for class loading. Those
 * with the longest number of components are checked first, as they are more
 * specific, and last the more generic and shorter ones. If no namespace prefix
 * matches a given fully qualified class name (FQCN) the autoload operation
 * ends and fails (with a false result or with an exception).
 *
 * There is an strict flag that controls whether an exception is thrown when
 * a class cannot be loaded but the prefix is registered. It is not thrown in
 * the autoload handler, but internally the method that does the loading throws
 * it and is cached and silenced in the autoload method.
 *
 * This class is also prepared to autoload the library that contains it
 * registering the root of the current namespace and the path to load classes
 * from (based in the path to this same script).
 *
 * This class loader makes some assumptions to load PSR-0 and PSR-4 classes
 * by looking at the presence of backslashes and underscores in the fully
 * qualified class name. If it contains at least one backslash it is a PSR-4
 * class name and underscores are not replaced, but if it contains at least
 * one underscore it assumes it is a PSR-0 pseudo-namespaced class name and
 * replaces underscores by path separators.
 *
 * If neither underscores or backslashes are found the class loading fails
 * directly as this implementations depends totally in the existence of a
 * registered (pseudo)namespace prefix that leads to the configuration for
 * autoloading.
 *
 * @see https://wiki.php.net/rfc/splclassloader
 * @see https://gist.github.com/221634
 * @see http://www.php-fig.org/psr/psr-0
 * @see http://www.php-fig.org/psr/psr-4
 *
 * @author Miguel Angel Pérez <mangelp[ATT]gmail[DOTT]com>
 *
 */
class ClassLoader
{

    const DEFAULT_FILE_EXTENSION = 'php';

    /**
     *
     * @var ClassLoader
     */
    private static $instance = null;

    /**
     * Default class loader options.
     * @type NsPrefixOptions
     */
    private $options;

    /**
     * Gets the default class loader options set for new prefixes
     * @return NsPrefixOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the default class loader options set for new prefixes
     * @param NsPrefixOptions $options
     */
    public function setOptions(NsPrefixOptions $options)
    {
        $this->options = $options;
    }

    /**
     * Next numeric order for a new added prefix.
     *
     * This is a variable that increments with each prefix addition to keep
     * the insertion order and give an unique id for each configured prefix.
     * @var int
     */
    protected $nextPrefixAddOrder = 0;

    /**
     * Strict mode flag. In strict mode this class will throw exceptions anytime
     * a class or interface cannot be loaded, but only if the class or interface
     * belongs to a registered namespace prefix.
     * @var bool
     */
    private $strict = false;

    /**
     * Gets if the loader will throw exceptions when a class expected to be
     * loaded by it is not found.
     * @return bool
     */
    public function isStrict()
    {
        return $this->strict;
    }

    /**
     * Sets if the loader will throw exceptions when a class expected to be
     * loaded by it is not found.
     * @param bool $strict
     */
    public function setStrict($strict)
    {
        $this->strict = $strict;
    }

    /**
     * Path to register by default with current namespace.
     * This will allow us to load phbrick classes.
     * @var string
     */
    private $defaultPath = null;

    /**
     * Gets the path to register by default to load phbrick classes.
     * @return string
     */
    public function getDefaultPath()
    {
        return $this->defaultPath;
    }

    /**
     * Sets the path to register by default to load phbrick classes.
     * Only sub-classes can set this property value.
     * @param string $path
     */
    protected function setDefaultPath($path)
    {
        $this->defaultPath = $path;
    }

    /**
     * Map of prefix roots as array keys and mapping data as values.
     *
     * The prefixes are kept in insertion order.
     *
     * This field is set by the constructor to an array or an a ArrayAccess
     * implementation with support for string keys.
     *
     * The storage of only the root of each prefix allows to group registered
     * items by it and simplify search and prefix management operations.
     *
     * @see \phbrick\ClassLoader::addPrefix
     * @see \phbrick\ClassLoader::removePrefix
     * @see \phbrick\ClassLoader::hasPrefix
     * @var array<string, array>
     */
    private $prefixes = [];

    /**
     * Returns the current set of prefixes.
     *
     * @return array<string, array>
     */
    public function getPrefixes()
    {
        return $this->prefixes;
    }

    /**
     * Gets if this class have been registered for autoloading.
     * @return boolean
     */
    public function isAutoloadRegistered()
    {
        return $this->autoloadRegistered;
    }

    /**
     * Flag set to true when this class is registered as an autoload callback.
     * @var bool
     */
    private $autoloadRegistered = false;

    /**
     * Avoid executing initDefaults twice
     *
     * @var bool
     */
    private $initDefaultsCalled = false;

    /**
     * Gets if init defaults was already called.
     * It cannot be called twice.
     * @return boolean
     */
    public function isInitDefaultsCalled()
    {
        return $this->initDefaultsCalled;
    }

    /**
     * @var callable
     */
    private $fallbackLoader = null;

    /**
     * Gets a callable that will be used when loading fails
     * @return callable
     */
    public function getFallbackLoader()
    {
        return $this->fallbackLoader;
    }

    /**
     * Sets a callable that will be used when loading fails
     * @param callable $fallbackLoader
     */
    public function setFallbackLoader($fallbackLoader)
    {
        if (!is_callable($fallbackLoader)) {
            throw new InvalidArgumentException('Argument $fallbackLoader is not a valid callable');
        }

        $this->fallbackLoader = $fallbackLoader;
    }

    /**
     * Initializes a new instance.
     *
     * If the $defaultPath parameter is provided it will be used to register by default the current namespace prefix
     * for loading its classes from the default path.
     *
     * If not set the path will be inferred from the path to this class, but if set to false (bool) then no default
     * prefix will be registered for class loading.
     *
     * @param string|false|null $defaultPath Default path to register for
     * current namespace loading.
     * @param string $defaultExtension
     */
    public final function __construct($defaultPath = null, $defaultExtension = self::DEFAULT_FILE_EXTENSION)
    {
        $this->defaultPath = $defaultPath;
        $this->loadDependencies();
        $this->options = new NsPrefixOptions();
        $this->options->setExtensions($defaultExtension);

        $this->init();
    }

    /**
     * Loads required classes that are used by this implementation
     */
    protected function loadDependencies()
    {
        $basePath = __DIR__;
        $dependencies = [
            'BaseStrictClass',
            'classLoader\\NsObject',
            'classLoader\\NsPrefixGroup',
            'classLoader\\NsPrefixOptions',
            'classLoader\\NsPrefix',
        ];

        foreach ($dependencies as $dependency) {
            if (!class_exists("\\phbrick\\$dependency", false)) {
                require_once($basePath . '/' . str_replace('\\', '/', "$dependency.php"));
            }
        }
    }

    /**
     * Custom instance initialization.
     * This method is called at the end of __construct()
     */
    protected function init()
    {
    }

    /**
     * Registers the minimum required prefix to load classes that belong to the
     * same namespace as this one as long as they have the same namespace root
     * and location.
     *
     * If the default path property is set to false then this method does
     * nothing, but if is set to an empty string or null, then the base
     * path to this library will be inferred from the __FILE__ constant.
     * When set to a path will be used as is, without checking whether is
     * valid or not. This way inheritors can use their __FILE__ path.
     *
     * @return bool False when already called or disabled, true when a default
     * prefix is configured.
     */
    public function initDefaults()
    {

        if ($this->initDefaultsCalled) {
            return false;
        }

        $this->initDefaultsCalled = true;

        // Library path is not configurable, is inferred from the
        // path to this file and the name of the full namespace

        $defaultPath = $this->getInternalDefaultPath();

        if (!$defaultPath) {
            // Default path disabled, no defaults will be initialized
            return false;
        }

        // Use the namespace to get the prefix from it.
        $cls = get_called_class();
        $pos = strrpos($cls, NsObject::NS_SEP);
        $namespace = substr($cls, 0, $pos);
        $namespace = explode(NsObject::NS_SEP, $namespace);

        $this->addPrefix($namespace[0], $defaultPath);

        return true;
    }

    /**
     * Destructor implementation.
     *
     * Unregisters autoload.
     */
    public final function __destruct()
    {
        if ($this->autoloadRegistered) {
            try {
                $this->unregisterAutoload();
            } catch (RuntimeException $ex) {
                // Don't throw exceptions on destruct
            }
        }
    }

    /**
     * Gets the path to configure by default for loading classes in the same
     * namespace as this loader instance.
     *
     * This method returns the current value of the default path property if set to any, but if not it will return the
     * path of the parent of the folder that holds this file of the path to the folder that contains the file for this
     * class.
     *
     * If this method returns a false or empty value no defaults will be configured. If it returns a path it will be
     * used as the base path to register a class loader using the namespace of this class as prefix (without the class
     * name, obviously) and assuming this class is under the vendor folder so removing the class name leaves the prefix
     * for autoloading any other classes.
     *
     * @return string
     * @see self::classFile
     */
    protected function getInternalDefaultPath()
    {
        $defaultPath = $this->getDefaultPath();

        if ($defaultPath) {
            return $defaultPath;
        }

        $classFile = self::classFile();

        $defaultPath = dirname(dirname($classFile));

        return $defaultPath;
    }

    /**
     * Remove this loader from autoload stack.
     *
     * @return bool True if this instance was registered for autoloading or
     * false if it was not registered.
     * @throws RuntimeException When the autoload unregister call fails
     */
    public function unregisterAutoload()
    {
        if (!$this->autoloadRegistered) {
            return false;
        }

        if (!spl_autoload_unregister([$this, 'autoloadClass'])) {
            throw new RuntimeException("ClassLoader: Can't unregister autoload handler");
        }

        $this->autoloadRegistered = false;

        return true;
    }

    /**
     * Adds a new prefix to the map of prefixes.
     *
     * The prefix must be made up of at least one component and can start by a namespace separator character followed
     * by zero or more components separated by the namespace separator character. Before processing the prefix it will
     * be normalized trimming all whitespace and backslash characters from both ends of the prefix.
     * Underscores are preserved at the end of the prefix as they are used to detect if the namespace is either a PSR-0
     * pseudo namespace or a PSR-4 one.
     * If after trimming the prefix it does not contains any namespace separator but ends with an underscore and there
     * isn't set an option to tell what type of namespace is it is assumed to be a PSR-0 pseudo-namespace.
     *
     * When registering a prefix the loader behaviour can be adapted by using the $options parameter to pass in one or
     * more options.
     *
     * If the options parameter is an string is supposed to be a semicolon-separated list of paths to search for
     * classes.
     *
     * If the options parameter is an array is supposed to be a map with one or more of the next keys:
     *    - loaders: Custom loader as a callable that receives the input class
     *      name to be loaded as parameter and returns a boolean with the result
     *      of the class loading.
     *      If this option is set all the rest are ignored. The return value is
     *      used to determine if the loader succeeded or not, assuming that only
     *      succeeds if it returns a boolean true value (exact type and value)
     *      as any other return value would be taken as a fail.
     *
     *    - paths: One (string) or more (array<string>) absolute filesystem paths
     *      to search when loading a class. If is only an string it can have
     *      one or more paths separated by semicolons, but if is an array each
     *      string value must be only a path.
     *
     *    - extensions: One(string) or more file extensions(array<string>) to be
     *      appended to each generated candidate class file name to try to load
     *      the class from. If is only an string it can be a semicolon-separated
     *      list of file extensions, but if it is an array then each string
     *      value must be a single extension. Always without leading dots.
     *
     *    - mapper: Callable that receives the FQCN components and the
     *      configured prefix paths and returns one or more paths to try to
     *      load the class from. Returned paths must be absolute or start by
     *      an scheme (like phar://, file:// and even C:/).
     *
     *    - removePrefix: If set to a non-false value the first part of the
     *      class paths generated from the FQCN will be altered by removing a
     *      portion of it depending on the type of this option:
     *
     *        + string: If the class path starts by this string then it will
     *          be removed. If empty nothing is done.
     *
     *        + int: Number of path components to remove from the beginning of
     *          the class path. If less than 0 nothing is done.
     *
     *        + bool: If true then the registered prefix will be trimmed from
     *          the beginning of the class path if found. If false nothing is
     *          done.
     *
     *    - pseudoNamespaced: If true then the prefix is a pseudo-namespace
     *      and underscores will be replaced by directory separators when
     *      building the class path. if false underscores will not be touched.
     *
     *    - order: Specific order for the prefix configured. This order acts
     *      like a grouping factor. Prefixes are first sorted by order and then
     *      the default sorting is applied for all prefixes with the same order.
     *
     * @param string $prefix Namespace prefix to be registered
     * @param string|array $options Prefix options.
     * @return boolean True if added or false if the prefix was already set.
     * @throws RuntimeException If the prefix is found already in any internal structure (was not properly removed or added)
     * or if the prefix contains non-ANSI chars.
     * @throws InvalidArgumentException If options is not either an array or an string
     * @example
     *   $loader->addPrefix('\My\Prefix', '/class/path1;/class/path2');
     *
     *   $loader->addPrefix('\My\Prefix', array('/class/path1', '/class/path2'));
     *
     *   $loader->addPrefix('My_Prefix_', '/path/to');
     *
     *   $loader->addPrefix('\My\Prefix', array(
     *      'path' => '/my/lib/',
     *      'extensions' => 'inc;lib;php',
     *      'pseudoNamespace' => false,
     *   ));
     *
     *   $loader->addPrefix('My_Prefix_', array(
     *      'path' => '/my/lib/',
     *      'extensions' => 'inc;lib;php',
     *      'pseudoNamespace' => true,
     *   ));
     *
     */
    public function addPrefix($prefix, $options)
    {

        $nsOptions = new NsPrefixOptions(
            $this->nextPrefixAddOrder,
            $options,
            $this->options);

        // Increase the counter for the next added prefix
        ++$this->nextPrefixAddOrder;

        // If the option pseudoNamespaced was set use it, if not pass a null to allow autodetection
        $type = null;

        if ($nsOptions->isPseudoNamespaced()) {
            $type = NsObject::TYPE_PSEUDO_NAMESPACED;
        }
        else if ($nsOptions->isOptionSet('pseudoNamespaced')) {
            $type = NsObject::TYPE_NAMESPACED;
        }

        $nsObject = new NsObject(
            $prefix,
            false,
            $type);

        $nsPrefixGroup = $this->getPrefixGroup($nsObject->getVendor());

        if ($nsPrefixGroup && $nsPrefixGroup->has($nsObject)) {
            // Already exists
            return false;
        }
        else if (!$nsPrefixGroup) {
            // Initialize default data structure per vendor and set order to be inverse to put longer prefixes first
            $nsPrefixGroup = new NsPrefixGroup($nsObject->getVendor(), null, true);
            $this->prefixes[$nsPrefixGroup->getVendor()] = $nsPrefixGroup;
        }

        $nsPrefixGroup->add($nsObject, $nsOptions);

        return true;
    }

    /**
     * Gets the group of prefixes for a given vendor.
     *
     * @param $vendorName
     * @return NsPrefixGroup
     */
    public function getPrefixGroup($vendorName)
    {
        $result = null;

        foreach ($this->prefixes as $vendor => $nsPrefixGroup) {
            if ($vendor == $vendorName) {
                $result = $nsPrefixGroup;
                break;
            }
        }

        return $result;
    }

    /**
     * Gets if a prefix group exists by its vendor name
     * @param string $vendorName
     * @return bool
     */
    public function hasPrefixGroup($vendorName)
    {
        return isset($this->prefixes[$vendorName]);
    }

    /**
     * Completely removes a vendor and all associated prefixes.
     *
     * @param string $vendorName
     * @return bool True if the vendor is found and removed and false if not.
     */
    public function removePrefixGroup($vendorName)
    {
        if (isset($this->prefixes[$vendorName])) {
            unset($this->prefixes[$vendorName]);
            return true;
        }

        return false;
    }

    /**
     * Checks if a given prefix have been already registered.
     *
     * Prefixes are normalized before being added and they contain part of the
     * namespace to be resolved.
     *
     * @param string $prefix
     * @return bool
     */
    public function hasPrefix($prefix)
    {
        $nsObject = new NsObject(
            $prefix,
            false);

        $prefixGroup = $this->getPrefixGroup($nsObject->getVendor());

        if ($prefixGroup) {
            return $prefixGroup->has($nsObject);
        }

        return false;
    }

    /**
     * Removes a prefix.
     * Returns true if the prefix is removed and false if not (it did not
     * existed).
     *
     * If the prefix is only the vendor portion of the namespace then this
     * method does the same as removePrefixGroup().
     * @param string $prefix
     * @return bool
     */
    public function removePrefix($prefix)
    {
        $nsObject = new NsObject(
            $prefix,
            false);

        $prefixGroup = $this->getPrefixGroup($nsObject->getVendor());

        if ($prefixGroup) {
            return $prefixGroup->remove($nsObject);
        }

        return false;
    }

    /**
     * Returns an ordered array of prefix strings for the given vendor.
     *
     * @param string $vendorName
     * @return array Array of registered sub-namespace paths for the given
     * vendor. If there are no prefixes or the vendor doesn't exists it returns
     * an empty array.
     */
    public function getVendorPrefixes($vendorName)
    {

        $prefixGroup = $this->getPrefixGroup($vendorName);
        $result = [];

        if ($prefixGroup) {
            foreach ($prefixGroup->getPrefixes() as $prefix) {
                /* @type NsPrefix $prefix */
                $result[] = $prefix->toString();
            }
        }

        return $result;
    }

    /**
     * Registers the current loader for autoloading our libraries appending
     * it to the end of the autoload stack
     * @return bool True if registered or false if already registered
     * @throws RuntimeException when the autoload registration fails.
     */
    public function registerAutoload()
    {

        if ($this->autoloadRegistered) {
            // Already registered
            return false;
        }

        if (!spl_autoload_register([$this, 'autoloadClass'], true, false)) {
            throw new RuntimeException("ClassLoader: Can't register autoload handler");
        }

        $this->autoloadRegistered = true;

        return true;
    }

    /**
     * Loads one or more classes if their prefixes were mapped properly.
     * When exception throwing is enabled the method throws an exception for
     * the first class whose namespace is properly registered and is not found
     * in the matching file.
     *
     * Those class names without a matching prefix are simply ignored. But
     * class names that match a prefix are resolved and if not found an
     * exception is thrown if strict mode is enabled or a result value of
     * false is returned.
     *
     * @param string|array<string> $classNames
     * @param bool|null $strict If true an exception if thrown if the class can't be
     * loaded from any prefix or any other bad condition. If false no exception will
     * be thrown, but if null then the value will be taken from the class field with
     * the same name.
     * @return int Number of classes loaded if strict mode is not set to true
     * or if all classes where found (in that case this will be the total of
     * classes in the input argument).
     * @throws RuntimeException If an unhandled exception is thrown by underlying code.
     */
    public function loadClasses($names, $strict = null)
    {
        if (is_string($names)) {
            $names = [$names];
        }
        else if (is_a($names, '\Traversable')) {
            $result = [];

            foreach ($names as $name) {
                $result[] = $name;
            }

            $names = $result;
        }

        $result = 0;

        foreach ($names as $name) {

            if (empty($name) || $this->existsFqn($name)) {
                continue;
            }

            $loaded = $this->loadClass($name, $strict);

            if ($loaded) {
                ++$result;
            }
        }

        return $result;
    }

    /**
     * Method used for autoloading a single class name.
     *
     * Returns true if the class was autoloaded or false if not, but this
     * method won't throw any exception at all (unhandled exceptions are
     * caught and silenced) so if autoloading code is failing no class
     * will be loaded by this autoloader, but other autoloaders will have
     * the chance to try to load the class.
     *
     * @param string $name
     * @return bool True if the class was autoloaded or false if not
     */
    public function autoloadClass($name)
    {
        $result = false;

        try {
            $result = $this->loadClass($name);
        } catch (RuntimeException $ex) {
            // Autoload must not throw exceptions
            error_log(
                "phbrick\\ClassLoader::autoloadClass: " . get_class($ex)
                . ': ' . $ex->getMessage());
        }

        return $result;
    }

    /**
     * Loads a class by his prefix searching all the paths given for the prefix.
     *
     * @param NsObject|string $name
     * @param bool $strict
     * @return bool True if the class is already loaded or is loaded by this method
     * or false if not.
     */
    public function loadClass($name, $strict = null)
    {

        if ($this->existsFqn($name)) {
            // Already loaded, go go go!
            return true;
        }

        if ($strict === null) {
            $strict = $this->strict;
        }

        $notLoadedMsg = 'Class or interface not found ' . $name;
        // @var NsObject
        $nsObject = new NsObject($name, true);

        // If the class to be loaded is made only of one component we either return false or use
        // the fallback loader if set.

        if ($nsObject->isRoot() && $this->fallbackLoader) {
            $callable = $this->fallbackLoader;
            return $callable($nsObject, $strict);
        }
        else if ($nsObject->isRoot()) {
            return false;
        }

        $prefixGroup = $this->getPrefixGroup($nsObject->getVendor());

        if (!$prefixGroup) {
            // If the vendor is not found we return false to let the next loader try it.
            return false;
        }

        $nsPrefixes = $prefixGroup->getPrefixes();

        if (empty($nsPrefixes)) {
            // There are no path prefixes for the given class name to be loaded and cannot be loaded.

            if ($strict) {
                throw new RuntimeException($notLoadedMsg . '. There is no configuration for the given path');
            }

            return false;
        }

        $classLoaded = false;

        foreach ($nsPrefixes as $nsPrefix) {
            $classLoaded = $this->loadClassFromPrefix($nsObject, $nsPrefix);

            if ($classLoaded) {
                break;
            }
        }

        if (!$classLoaded && $strict) {
            throw new RuntimeException($notLoadedMsg . '. No prefix can load the given class');
        }

        return $classLoaded;
    }

    /**
     * Tries to load a class from an already defined prefix in the prefix map.
     *
     * @param NsObject $nsObject
     * @param NsPrefix $nsPrefix
     * @return bool True if the class was loaded or false if not.
     */
    protected function loadClassFromPrefix(NsObject $nsObject, NsPrefix $nsPrefix)
    {

        $loaded = false;
        $fqn = $nsObject->toString();
        $nsOptions = $nsPrefix->getOptions();

        if ($nsOptions->hasLoaders()) {
            return $this->loadClassFromLoaderCallbacks($nsPrefix, $fqn);
        }

        $filePaths = $nsPrefix->generateFilePaths($nsObject);

        foreach ($filePaths as $candidateFile) {
            // Return the first path found to have an existing file that might
            // contain the class definition.
            if (file_exists($candidateFile) && !is_dir($candidateFile)) {

                include($candidateFile);

                if ($this->existsFqn($fqn)) {
                    $loaded = true;
                    break;
                }
            }
        }

        return $loaded;
    }

    protected function existsFqn($fqn)
    {
        return class_exists($fqn, false)
            || interface_exists($fqn, false)
            || trait_exists($fqn, false);
    }

    /**
     * Gets the current loader instance or creates a new one.
     * @return ClassLoader
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            $currentClass = get_called_class();
            self::$instance = new $currentClass();
        }

        return self::$instance;
    }

    /**
     * Gets the file this class is defined in by using late static binding to call a protected static method that
     * returns the __FILE__ constant for the concrete class.
     *
     * @return string
     */
    public static function classFile()
    {
        $class = get_called_class();
        $method = '____currentClassFile';

        // Call the static method to return the class's file full path.
        return $class::$method();
    }

    /**
     * Internal method that returns the path to this file, but not to inheritor's file.
     * @return string
     */
    protected static function ____currentClassFile()
    {
        return __FILE__;
    }

    /**
     * Returns a value of this same type to help code-completion on IDEs.
     *
     * @param ClassLoader $instance
     * @return ClassLoader
     */
    public static function cast(ClassLoader $instance)
    {
        return $instance;
    }

    /**
     * Static alias for calling load($classes, $strict) in the currently
     * defined instance.
     *
     * As this method uses getInstance if no current instance exists it will
     * cause the creation of a new one.
     *
     * @param string|array $classes
     * @param bool $strict
     * @return bool True if all classes are loaded or false if not and $strict
     * is not true.
     */
    public static function using($classes, $strict = false)
    {
        return self::getInstance()->loadClass($classes, $strict);
    }

    /**
     * Initializes and sets the default loader instance. If a loader is provided
     * then that loader is used.
     * Also can enable autoloading if the $autoload flag is set.
     * @param ClassLoader $loader
     * @param bool $autoload
     * @return ClassLoader
     * @throws RuntimeException
     */
    public static function registerLoader(ClassLoader $loader = null, $autoload = false)
    {
        if (self::$instance !== null) {
            throw new RuntimeException("ClassLoader: Loader instance already set");
        }

        if ($loader !== null) {
            self::$instance = $loader;
        }

        if ($autoload) {
            self::getInstance()->registerAutoload();
        }

        return self::getInstance();
    }

    /**
     * @param NsPrefix $nsPrefix
     * @param string $fqn
     * @return bool
     */
    protected function loadClassFromLoaderCallbacks(NsPrefix $nsPrefix, $fqn)
    {
        $loaded = false;

        foreach ($nsPrefix->getOptions()->getLoaders() as $loader) {
            call_user_func_array($loader, ['class' => $fqn]);

            if ($this->existsFqn($fqn)) {
                $loaded = true;
                break;
            }
        }

        return $loaded;
    }
}
