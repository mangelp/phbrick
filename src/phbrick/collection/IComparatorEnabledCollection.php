<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

/**
 * Operations for collections that support the use of a comparator to look for items
 */
interface IComparatorEnabledCollection
{

    /**
     * Gets the comparator in use.
     *
     * The comparator can be set using a setter or the constructor.
     * @return callable the comparator or null if is not set
     * @internal param callable|IComparator $comparator
     */
    public function getComparator();

    /**
     * Sets the comparator.
     *
     * @param callable|IComparator|null $comparator the comparator to set. If is null then clears
     * the current comparator being set.
     */
    public function setComparator(callable $comparator);
}
