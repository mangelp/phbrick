<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use InvalidArgumentException;
use phbrick\exceptions\InvalidKeyException;
use phbrick\types\Types;
use Traversable;

class AbstractArrayMap extends AbstractCollection implements IMap
{

    use ComparatorEnabledCollectionTrait;

    /**
     * Initializes the collection elements from another collection
     * @param array|Traversable $iterable
     */
    protected function initialize($iterable)
    {
        if ($iterable === null) {
            return;
        }

        Types::assertIterable($iterable);

        foreach ($iterable as $key => $element) {
            $this->set($key, $element);
        }
    }

    /**
     * Checks that the key is valid.
     *
     * Checks that is not empty and that is an string or an object that implements hashCode method.
     *
     * If the $notExists parameter is true then it must also check that the key does not exists
     * already.
     *
     * @param string $key
     * @param bool $notExists
     * @throws InvalidKeyException when the keys does not match the validity criteria.
     */
    protected function assertValidKey($key, $notExists = false)
    {
        if ($key === null || !is_string($key) || strlen($key) == 0) {
            throw new InvalidKeyException('The key must be a non-empty string');
        }

        if ($notExists && isset($this->storage[$key])) {
            throw new InvalidKeyException("The key is already defined: $key");
        }
    }

    /**
     * Searches all keys whose paired value is equal to the given element up to the given limit.
     *
     * @param mixed $element
     * @param string $limit
     * @return array
     * @throws InvalidArgumentException
     */
    protected function internalFindAllKeysOf($element, $limit = null)
    {
        $this->assertValidValue($element);

        $keys = [];
        $count = 0;

        if ($this->size == 0) {
            return $keys;
        }

        if ($limit === null) {
            $limit = $this->size;
        }
        else {
            $limit = (int)$limit;
        }

        if ($limit <= 0) {
            throw new InvalidArgumentException('Invalid limit, it cannot be less than 1');
        }

        $comparator = $this->createComparator($this->getComparator());

        foreach ($this->storage as $key => $value) {
            if ($count >= $limit) {
                break;
            }

            if ($comparator($this->storage[$key], $element) == 0) {
                $keys[] = $key;
                ++$count;
            }
        }

        return $keys;
    }

    public function set($key, $element)
    {
        $this->assertValidValue($element);
        $this->assertValidKey($key);

        if (!isset($this->storage[$key])) {
            ++$this->size;
            ++$this->modificationCount;
        }

        $this->storage[$key] = $element;
    }

    public function setAll($iterable, $overwrite = true)
    {
        Types::assertIterable($iterable);

        foreach ($iterable as $key => $element) {
            if ($overwrite || !isset($this->storage[$key])) {
                $this->set($key, $element);
            }
        }
    }

    public function get($key)
    {
        $this->assertValidKey($key, false);

        return $this->storage[$key];
    }

    public function has($key)
    {
        $this->assertValidKey($key, false);

        return isset($this->storage[$key]);
    }

    public function hasElement($element)
    {
        $result = $this->internalFindAllKeysOf($element, 1);

        return !empty($result);
    }

    public function keys()
    {
        return array_keys($this->storage);
    }

    public function values()
    {
        return array_values($this->storage);
    }

    public function keyOf($element)
    {

        $result = $this->internalFindAllKeysOf($element, 1);

        if (!empty($result)) {
            return $result[0];
        }
        else {
            return null;
        }
    }

    public function allKeysOf($element)
    {

        $result = $this->internalFindAllKeysOf($element);

        return $result;
    }

    public function remove($key)
    {
        if (isset($this->storage[$key])) {
            unset($this->storage[$key]);
            --$this->size;
            ++$this->modificationCount;
        }
    }

    public function removeAll($iterable)
    {
        Types::assertIterable($iterable);

        foreach ($iterable as $element) {
            $this->remove($element);
        }
    }

    /**** \ArrayAccess ***
     * @param mixed $key
     * @return mixed
     */

    public function offsetGet($key)
    {
        return $this->get($key);
    }

    public function offsetSet($key, $element)
    {
        $this->set($key, $element);
    }

    public function offsetUnset($key)
    {
        $this->remove($key);
    }

    public function offsetExists($key)
    {
        return $this->has($key);
    }
}
