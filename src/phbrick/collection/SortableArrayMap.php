<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

/**
 * Implementation of sorting support by keys and/or values of a map like PHP sort operations do.
 *
 */
class SortableArrayMap extends ArrayMap implements ISortable
{
    private $sortModificationCount = -1;

    public function isSorted()
    {
        return $this->sortModificationCount == $this->modificationCount;
    }

    protected function setSorted()
    {
        $this->sortModificationCount = $this->modificationCount;
    }

    /**
     * Sorts the current collection elements by key.
     *
     * @param callable $comparator
     * @param null $invertSortOrder
     * @see \phbrick\collection\ISortable::sort()
     */
    public function sort(callable $comparator = null, $invertSortOrder = null)
    {

        $this->sortByMode(SortModes::BY_KEY_KEEP_VALUE, $comparator, $invertSortOrder);
    }

    public function sortByMode($sortMode, callable $comparator = null, $invertSortOrder = null)
    {

        if ($comparator === null) {
            $comparator = $this->getComparator();
        }

        $comparator = $this->createComparator($comparator, $invertSortOrder);

        switch ($sortMode) {
            case SortModes::BY_VALUE_NO_KEEP_KEY:
                $keys = array_keys($this->storage);
                $values = array_values($this->storage);

                if ($comparator !== null) {
                    usort($values, $comparator);
                }
                else if (!$invertSortOrder) {
                    sort($values);
                }
                else {
                    rsort($values);
                }

                $this->storage = array_combine($keys, $values);

                break;
            case SortModes::BY_VALUE_KEEP_KEY:
                if ($comparator !== null) {
                    uasort($this->storage, $comparator);
                }
                else if (!$invertSortOrder) {
                    asort($this->storage);
                }
                else {
                    arsort($this->storage);
                }

                break;
            case SortModes::NONE:
                /* NO BREAK */
            case SortModes::BY_KEY_KEEP_VALUE:
                if ($comparator != null) {
                    uksort($this->storage, $comparator);
                }
                else if (!$invertSortOrder) {
                    ksort($this->storage, $comparator);
                }
                else {
                    krsort($this->storage, $comparator);
                }

                break;
            case SortModes::BY_KEY_NO_KEEP_VALUE:
                $values = array_values($this->storage);
                $keys = array_keys($this->storage);

                if ($comparator != null) {
                    usort($keys, $comparator);
                }
                else if (!$invertSortOrder) {
                    sort($keys, $comparator);
                }
                else {
                    rsort($keys, $comparator);
                }

                $this->storage = array_combine($keys, $values);

                break;
        }

        ++$this->modificationCount;

        $this->setSorted();
    }

    public function getDefaultSortMode()
    {
        return SortModes::BY_KEY_KEEP_VALUE;
    }

    public function shuffle()
    {
        shuffle($this->storage);
        ++$this->modificationCount;
    }
}
