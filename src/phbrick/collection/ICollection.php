<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\collection;

use Countable;

/**
 * Basic collection of key-value pairs where the key cannot be null.
 *
 * Models basic modification operations.
 *
 */
interface ICollection extends Countable, IIterable
{

    /**
     * Returns the current size of the collection
     * @return int
     */
    public function getSize();

    /**
     * Returns an array with all the elements of this collection in the same order they are stored
     * internally.
     *
     * @return array
     */
    public function toArray();

    /**
     * Empties this collection
     */
    public function clear();
}
