<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;
use InvalidArgumentException;
use phbrick\types\Types;

/**
 * Abstract collection for array-like collections that does not implement IList or IMap interfaces
 * but also does not extend \ArrayObject to provide an array-like behaviour.
 */
abstract class AbstractArrayCollection extends AbstractCollection implements ArrayAccess, IArray
{
    use ArrayAccessTrait;

    protected function initialize($iterable)
    {

        if (empty($iterable)) {
            return;
        }

        if (!Types::isIterable($iterable)) {
            throw new InvalidArgumentException(
                'Cannot initialize collection from a non-iterable type ' . Types::getTypeName($iterable));
        }

        foreach ($iterable as $index => $indexValue) {
            $this->add($indexValue, $index);
        }
    }

    public function get($index)
    {
        return $this->storage[$index];
    }

    public function has($index)
    {
        return isset($this->storage[$index]);
    }

    public function add($element, $index = null)
    {
        $this->assertValidValue($element);

        if ($index !== null) {
            $this->storage[$index] = $element;
        }
        else {
            $this->storage[] = $element;
        }

        $this->size++;
        $this->modificationCount++;
    }

    public function set($index, $element)
    {
        $this->assertValidValue($element);

        $this->storage[$index] = $element;
    }
}
