<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use Exception;
use phbrick\exceptions\InvalidCallbackException;

/**
 * IMap implementation that uses an array for internal storage.
 */
class ArrayMap extends AbstractArrayMap implements IFilterable, IMap, IComparatorEnabledCollection
{

    /**
     * @param ArrayMap $arrayMap
     * @return ArrayMap
     */
    public static function castArrayMap(ArrayMap $arrayMap)
    {
        return $arrayMap;
    }

    /**
     * Filters the current collection elements and returns them into a new collection keeping the
     * current elements unaltered.
     *
     * When the criteria returns a collection of elements the values are appended to the result
     * without checking the type of the keys.
     *
     * @param callable $filterCriteria
     * @return ArrayMap
     * @see \phbrick\collection\IFilterable::filter()
     */
    public function apply(callable $filterCriteria)
    {
        /** @var ArrayMap $map */
        $map = self::instance();
        $result = ArrayMap::castArrayMap($map);

        $this->internalFilter($this->storage, $filterCriteria, $result);

        return $result;
    }

    /**
     * Filters the current collection elements directly altering the collection.
     *
     * When the criteria returns a collection of elements the values are appended to the result
     * without checking the type of the keys.
     *
     * @param callable $filterCriteria
     * @return void
     * @see  \phbrick\collection\IFilterable::apply()
     * @see  \phbrick\collection\ArrayList::filter()
     */
    public function alter(callable $filterCriteria)
    {
        $storage = $this->storage;
        $this->storage = [];
        $this->size = 0;

        $this->internalFilter($storage, $filterCriteria, $this);
    }

    protected function internalAddAllMatchingElementsToArray($element, array &$indexes, $offset, $limit)
    {
        $comparator = $this->createComparator();
        $count = 0;

        do {
            if ($comparator($this->storage[$offset], $element) == 0) {
                $indexes[] = $offset;
                ++$count;
            }
        } while ($count < $limit && ++$offset < $this->size);
    }

    /**
     * Filters the array of elements and adds them into the given $result parameter
     *
     * @param array $storage Plain array used for data storage.
     * @param callable $filterCriteria
     * @param AbstractArrayMap $result
     * @throws InvalidCallbackException If the $filterCriteria callable does not return a proper
     * value of it throws an exception.
     */
    protected function internalFilter(array $storage, callable $filterCriteria, AbstractArrayMap $result)
    {
        foreach ($storage as $key => $value) {

            try {
                $filterResult = $filterCriteria($key, $value);
            } catch (Exception $ex) {
                throw new InvalidCallbackException('Callback throwed an exception: ' . $ex->getMessage(), 0, $ex);
            }

            $isCollection = false;

            if (is_int($filterResult) || is_float($filterResult) || is_bool($filterResult)) {
                $match = (bool)$filterResult;
            }
            else if (is_numeric($filterResult)) {
                $match = (bool)((float)$filterResult);
            }
            else if (is_array($filterResult) || is_a($filterResult, '\\Traversable')) {
                $match = true;
                $isCollection = true;
            }
            else {
                throw new InvalidCallbackException(
                    'Expected a return type of array, int, float or bool but got ' . gettype($filterResult));
            }

            if ($match && !$isCollection) {
                // Append to the result. When there is a match that returns a boolean true value
                // we ignore the provided key and add the value
                $result->set($key, $value);
            }
            else if ($match && $isCollection) {
                // Append to the result. If is not bool then is a traversable and we add all
                // map elements
                $result->setAll($filterResult);
            }
        }
    }
}
