<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

/**
 * Sorting operations. This interface requires than an order between elements is defined and a way
 * of sorting them is provided using either a default comparator or a provided one.
 *
 * Implementors must define an iteration order such that repeated iterations must yield the same
 * sequence of elements (assuming each element can be uniquely identified).
 *
 * Once the sort method is called the sort status is retained until there is some modification of
 * the collection that might alter the sorting of elements.
 */
interface ISortable
{

    /**
     * Sorts elements using the provided comparator parameter, the comparator property if set or a
     * default comparison implementation using some default comparison logic.
     *
     * If there is no comparator and the collection contains objects or arrays there is no guarantee
     * that the collection iteration order might be changed at all or that the current iteration
     * order might keep.
     *
     * When an IList impl. is sorted index association is not kept and when an IMap impl. is sorted
     * the key association is kept and the map is sorted by the value associated to each key.
     *
     * @param callable|string $comparator
     * @param bool $invertSortOrder If true sort order is inverted (multiply comparison result by -1).
     * This is optional and might not be supported by all implementations.
     */
    public function sort(callable $comparator = null, $invertSortOrder = null);

    /**
     * Sorts the collection using the specified mode.
     *
     * This method is optional and implementations does not have to support it. Implementations
     * might simply call ISortable::sort($comparator) or throw a RuntimeException if the sort mode
     * is not supported.
     *
     * For those modes that might sort keys/indexes they must not use the comparator (if any) and
     * must use default sorting but honor sorting order inversion.
     *
     * @param string $sortMode one of phbrick\collection\SortModes constant.
     * @param callable $comparator
     * @param null $invertSortOrder
     */
    public function sortByMode($sortMode, callable $comparator = null, $invertSortOrder = null);

    /**
     * Gets the default sorting mode used by the collection. If the collection does not use one
     * of the defined sorting modes (a custom one) it must return a customized name for it that does
     * not match any of the defined ones.
     */
    public function getDefaultSortMode();

    /**
     * Randomly changes elements order.
     *
     * When applied over an ordered map must change elements order, but not key associations.
     * Over a list it must change elements order and not keep key association.
     *
     * Calling this method marks the instance as not sorted, obviously.
     */
    public function shuffle();

    /**
     * Checks if the current collection sort flag is set.
     *
     * @return bool
     */
    public function isSorted();
}
