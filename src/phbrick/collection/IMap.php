<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;
use phbrick\exceptions\InvalidKeyException;
use phbrick\exceptions\NullValueException;
use Traversable;

/**
 * Collection of key-value pairs without order.
 *
 * A comparator might be needed if the elements of the map are non-scalars or arrays as by default
 * any comparison must use basic operations.
 * The comparator must return 0 when both compared values/objects are considered to be equal.
 *
 */
interface IMap extends ArrayAccess, ICollection
{

    /**
     * Gets the value associated with the key if it exists.
     *
     * @param mixed $key
     * @return mixed
     * @throws InvalidKeyException If the key is invalid or there is no element for that key.
     */
    public function get($key);

    /**
     * Checks if a given key exists.
     *
     * @param mixed $key
     * @return boolean
     */
    public function has($key);

    /**
     * Sets a value for a given key. If the key does not exists it adds it and if it exists
     * replaces the associated value.
     *
     * The key and value cannot be null and in that case a NullValueException must be thrown.
     *
     * @param mixed $key
     * @param mixed $element
     * @throws InvalidKeyException If the key is invalid.
     * @throws NullValueException if either the key or the value are null.
     */
    public function set($key, $element);

    /**
     * Sets all the elements of the given map.
     *
     * @param array|Traversable $map
     * @param bool $overwrite If true elements with the same key will be overwritten, if false
     * they will be skipped.
     * @throws InvalidKeyException If the key is invalid.
     * @throws NullValueException if any key or value are null.
     */
    public function setAll($map, $overwrite = true);

    /**
     * Search and return the first key whose value is equal to the one passed.
     *
     * If the element is not found returns null.
     *
     * @param mixed $element
     * @return string|null The key or null if no key was found.
     */
    public function keyOf($element);

    /**
     * Finds all keys whose value is the one specified
     * @param mixed $element
     * @return array<string> An array with all the keys found
     */
    public function allKeysOf($element);

    /**
     * Removes the element associated with the key and the key.
     *
     * @param mixed $key
     * @throws InvalidKeyException If the key is invalid or there is no element for that key.
     */
    public function remove($key);

    /**
     * Removes all the elements with the given keys from the iterable.
     *
     * The iterable is iterated and all values are used as the keys to be removed.
     *
     * @param array|Traversable $iterable Keys to be removed
     * @throws InvalidKeyException If some key is invalid or there is no element for that key.
     */
    public function removeAll($iterable);

    /**
     * Gets an array with all the keys
     */
    public function keys();

    /**
     * Gets an array with all the values
     */
    public function values();
}
