<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

/**
 * Compare two values (scalar or object) and returns a result as an integer.
 *
 * Implementors qualify as callables as this interface forces implementation of __invoke method.
 */
interface IComparator
{
    /**
     * Compares $former with $later and return an integer > 1 if the order of $former goes after
     * $later, < 1 if the order of $former goes before $later and == 0 if both orders are the same.
     * @param mixed $former
     * @param mixed $later
     */
    public function compare($former, $later);

    /**
     * Implementation of Invoke that makes implementors callable.
     * @param mixed $former
     * @param mixed $later
     */
    public function __invoke($former, $later);
}
