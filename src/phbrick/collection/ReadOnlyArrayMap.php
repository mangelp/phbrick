<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\collection;

use phbrick\IReadOnly;
use phbrick\ReadOnlyTrait;

/**
 * Class ReadOnlyArrayMap extends ArrayMap providing support for read-only collection.
 *
 * @package phbrick\collection
 */
class ReadOnlyArrayMap extends ArrayMap implements IReadOnly
{
    use ReadOnlyTrait;

    public function __construct($iterable = null, $readOnly = false)
    {
        parent::__construct($iterable);
        $this->setReadOnly($readOnly);
    }

    /**
     * @param mixed $key
     * @param mixed $element
     */
    public function set($key, $element)
    {
        $this->assertReadOnly();
        return parent::set($key, $element);
    }

    /**
     * @param mixed $key
     */
    public function remove($key)
    {
        $this->assertReadOnly();
        return parent::remove($key);
    }

    /**
     * @param callable $filterCriteria
     * @return void
     */
    public function alter(callable $filterCriteria)
    {
        $this->assertReadOnly();
        parent::alter($filterCriteria);
    }
}
