<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;
use ArrayIterator;
use phbrick\BaseStrictClass;
use phbrick\exceptions\NullValueException;
use Traversable;

abstract class AbstractCollection extends BaseStrictClass implements ICollection
{

    /**
     * @var string
     */
    protected $size = 0;

    /**
     * Gets
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    protected $modificationCount = 0;

    /**
     * Gets the number of modificationCount to this collection.
     *
     * Modifications are counted as basic operations like adding, removing or replacing elements.
     *
     * This is useful to have when developing an iterator that might avoid concurrent modificationCount
     *
     * @return int
     */
    public function getModificationCount()
    {
        return $this->modificationCount;
    }

    /**
     * Member that holds the real storage for elements. Usually a play array or an SPL collection.
     *
     * Is initialized by default to an empty array.
     *
     * @var ArrayAccess|array
     */
    protected $storage = [];

    /**
     * Initializes the collection with the given elements
     *
     * @param array|Traversable $iterable
     */
    public function __construct($iterable = null)
    {
        $this->storage = $this->createStorage();

        $this->initialize($iterable);
    }

    /**
     * Creates the data type that will hold the elements of the collection.
     *
     * By default we store the elements into a plain array.
     *
     * Changing the result type of this method requires changing the implementation of many others
     * that use array operations.
     *
     * @return array|ArrayAccess
     */
    protected function createStorage()
    {
        $storage = [];

        return $storage;
    }

    /**
     * Initializes the collection elements from another collection
     * @param array|Traversable $iterable
     */
    protected abstract function initialize($iterable);

    /**
     * Asserts that the value is valid for this collection.
     *
     * By default only checks that is not null as our collections does not allow null values.
     *
     * Inheritors must override this method to enforce any restriction over the allowed map values.
     *
     * @param mixed $value
     * @throws NullValueException
     */
    protected function assertValidValue($value)
    {
        if ($value === null) {
            throw new NullValueException('The value cannot be null');
        }
    }

    public function clear()
    {
        $this->size = 0;
        $this->storage = $this->createStorage();
        ++$this->modificationCount;
    }

    public function isEmpty()
    {
        return $this->size == 0;
    }

    public function toArray()
    {
        return $this->storage;
    }

    /**** \Countable ****/

    public function count()
    {
        return $this->getSize();
    }

    /**** \IteratorAggregate ****/

    public function getIterator()
    {
        return new ArrayIterator($this->storage);
    }
}
