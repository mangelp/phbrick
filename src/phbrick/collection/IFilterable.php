<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use phbrick\exceptions\InvalidCallbackException;
use phbrick\exceptions\NullValueException;

/**
 * Methods that allow to reduce or increase the elements of a collection or alter them.
 */
interface IFilterable
{
    /**
     * Filters the elements of the current collection and returns them as a new instance of the
     * same collection class.
     *
     * This method returns a new collection and leaves the current one unmodified. Non-scalar
     * collection values are not cloned so reference values will be shared between collections.
     *
     * The $filterCriteria callable signature is:
     *   callback(mixed $key, mixed $element): null|bool|int|array;
     *
     * The callback is called over each collection element and for each one the callback must return
     * a value of type integer, float, boolean or array, throwing an exception of type
     * \InvalidCallbackException if the returned value is of any other type.
     *
     * By each type the filter method implementation must to the following:
     *  + null: Must behave as if a boolean false value was returned.
     *  + integer and float: They are converted to bool with a cast, and the behaviour for boolean
     *    values apply.
     *  + boolean: If true the $element is added to the result collection. If false it is skipped.
     *  + array: An array of elements that must be appended to the result in the same iteration
     *    order. The elements must follow any type restrictions the collection might have (if any).
     *    The callback might return as many elements as it wants. Returning an empty array is like
     *    returning boolean false.
     *
     * Note1: This interface allows the callback first parameter to be either an integer or an
     * string, but implementors that only use one of these values must allow only the desired one.
     *
     * Note2: If the collection is only a list of items with order then the callback returned
     * elements will be appended to the resulting collection or inserted, but if they are inserted
     * out of the current range an exception must be thrown.
     *
     * @param callable $callback Callback that decides what elements will be returned
     * @return IIterable An instance of the same class with the filtered elements
     * @throws InvalidCallbackException if the callback returns an invalid result
     * @throws NullValueException if any element added is null
     */
    public function apply(callable $callback);

    /**
     * This method behaves identically than IFilterable::filter but it replaces the current
     * collection elements by the ones filtered, removes any other elements and returns nothing.
     *
     * @param callable $filterCallback
     * @return IIterable An instance of the same class with the filtered elements
     * @see \phbrick\collection\IFilterable::apply()
     */
    public function alter(callable $filterCallback);
}
