<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use InvalidArgumentException;
use OutOfBoundsException;
use phbrick\exceptions\TypeException;
use phbrick\types\Types;
use Traversable;

abstract class AbstractArrayList extends AbstractCollection implements IList
{

    use ArrayAccessTrait;

    /**
     * Initializes the collection elements from another collection
     * @param array|Traversable $iterable
     */
    protected function initialize($iterable)
    {
        if ($iterable === null) {
            return;
        }

        Types::assertIterable($iterable);

        foreach ($iterable as $element) {
            $this->add($element);
        }
    }

    /**
     * Checks if the given key is a valid index.
     *
     * The $allowCollectionSize flags allows the index to be equal to the collection size. If not
     * that index will cause an \OutOfBoundsException.
     *
     * @param int|string $index list index
     * @param bool $allowCollectionSize
     * @throws TypeException
     */
    protected function assertValidIndex($index, $allowCollectionSize = false)
    {
        if ($index === null) {
            return;
        }

        if (!is_int($index) && !is_float($index) && !is_numeric($index)) {
            throw new TypeException("Invalid key type, it must be numeric");
        }

        $index = (int)$index;
        $limit = $allowCollectionSize ?
            $this->size :
            $this->size - 1;

        if ($index < 0 || $index > $limit) {

            if ($allowCollectionSize || $this->size > 0) {
                throw new OutOfBoundsException("Invalid index $index, must be in the range [0, $limit]");
            }
            else {
                throw new OutOfBoundsException("Invalid index $index, the collection is empty");
            }
        }
    }

    protected function internalFindAllIndexesOf($element, $offset = null, $limit = null)
    {
        $this->assertValidValue($element);

        if ($offset !== null) {
            $offset = (int)$offset;
            $this->assertValidIndex($offset, false);
        }
        else {
            $offset = 0;
        }

        $indexes = [];

        if ($this->size == 0) {
            return $indexes;
        }

        if ($limit === null) {
            $limit = $this->size;
        }
        else {
            $limit = (int)$limit;
        }

        if ($limit < 0) {
            throw new InvalidArgumentException('Invalid limit, it cannot be less than 0');
        }

        $this->internalAddAllMatchingElementsToArray($element, $indexes, $offset, $limit);

        return $indexes;
    }

    /**
     * Adds all elements that match a certain equality predicate to the given element
     *
     * @param mixed $element
     * @param array $indexes
     * @param int $offset
     * @param int $limit
     */
    protected abstract function internalAddAllMatchingElementsToArray($element, array &$indexes, $offset, $limit);

    public function add($element, $index = null)
    {
        $this->assertValidValue($element);

        if ($index !== null) {
            $index = (int)$index;
            $this->assertValidIndex($index, true);
        }
        else {
            $index = $this->size;
        }

        if ($index == $this->size) {
            $this->storage[] = $element;
        }
        else {
            array_splice($this->storage, $index, 0, [$element]);
        }

        ++$this->size;
        ++$this->modificationCount;
    }

    public function addAll($iterable, $offset = null)
    {
        Types::assertIterable($iterable);

        $index = $this->size;

        if ($offset != null) {
            $index = (int)$offset;
        }

        $this->assertValidIndex($index, true);
        $elements = [];
        $elementsSize = 0;

        foreach ($iterable as $key => $element) {
            $this->assertValidValue($element);
            $elements[] = $element;
            ++$elementsSize;
        }

        array_splice($this->storage, $index, 0, $elements);
        $this->size += $elementsSize;
        ++$this->modificationCount;
    }

    public function allIndexesOf($element, $offset = null)
    {
        return $this->internalFindAllIndexesOf($element, $offset);
    }

    public function get($index)
    {
        $this->assertValidIndex($index, false);

        return $this->storage[$index];
    }

    public function has($index)
    {
        $index = (int)$index;
        return $index >= 0 && $index < $this->size;
    }

    public function indexOf($element, $offset = null)
    {

        $result = $this->internalFindAllIndexesOf($element, $offset, 1);

        if (count($result) != 0) {
            return $result[0];
        }
        else {
            return -1;
        }
    }

    public function remove($element)
    {
        $index = $this->indexOf($element);

        if ($index != -1) {
            $this->removeAt($index);
        }

        return $index;
    }

    public function removeAt($index)
    {
        $this->assertValidIndex($index);
        array_splice($this->storage, $index, 1, []);
        --$this->size;
        ++$this->modificationCount;
    }

    public function removeAll($iterable)
    {
        Types::assertIterable($iterable);

        foreach ($iterable as $element) {
            $this->remove($element);
        }
    }

    public function removeAllAt($iterable)
    {
        Types::assertIterable($iterable);
        $indexes = [];

        foreach ($iterable as $index) {
            $this->assertValidIndex($index);
            $indexes[] = $index;
        }

        if (empty($indexes)) {
            return;
        }

        rsort($indexes);

        foreach ($indexes as $index) {
            array_splice($this->storage, $index, 1, []);
        }

        ++$this->modificationCount;
        $this->size -= count($indexes);
    }

    public function subList($index, $length)
    {
        $this->assertValidIndex($index);

        $result = self::instance(array_slice($this->storage, $index, $length));

        return $result;
    }

    public function replace($index, $length, $iterable = null)
    {
        $this->assertValidIndex($index);

        $array = Arrays::toArray($iterable);
        $result = array_splice($this->storage, $index, $length, $array);
        $this->size = count($this->storage);

        return self::instance($result);
    }

    public function set($index, $element)
    {
        $this->assertValidIndex($index, true);
        $this->assertValidValue($element);

        $this->storage[$index] = $element;
        ++$this->modificationCount;

        // When we set to the end we are adding a new element
        if ($index == $this->size) {
            ++$this->size;
        }
    }

    public function toArray()
    {
        return $this->storage;
    }
}
