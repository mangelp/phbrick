<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

/**
 * Trait that implements \ArrayAccess for classes that implement IArray interface
 */
trait ArrayAccessTrait
{
    /**** \ArrayAccess ***/

    /**
     * @param int|string $index
     * @return mixed
     */
    public function offsetGet($index)
    {
        return $this->get($index);
    }

    /**
     * @param int|string $index
     * @param $value
     */
    public function offsetSet($index, $value)
    {
        if ($index === null) {
            $this->add($value);
        }
        else {
            $this->set($index, $value);
        }
    }

    /**
     * @param int|string $index
     */
    public function offsetUnset($index)
    {
        $this->removeAt($index);
    }

    /**
     * @param int|string $index
     * @return mixed
     */
    public function offsetExists($index)
    {
        return $this->has($index);
    }
}
