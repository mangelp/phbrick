<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use Exception;
use phbrick\exceptions\InvalidCallbackException;
use phbrick\exceptions\NotIterableException;
use phbrick\exceptions\TypeException;
use phbrick\types\Types;
use Traversable;

/**
 * IList implementation that uses an array as internal storage.
 */
class ArrayList extends AbstractArrayList implements ISortable, IFilterable, IList, IComparatorEnabledCollection
{
    use ComparatorEnabledCollectionTrait;

    /**
     * @param ArrayList $arrayList
     * @return ArrayList
     */
    public static function castArrayList(ArrayList $arrayList)
    {
        return $arrayList;
    }

    private $sortModificationCount = -1;

    public function isSorted()
    {
        return $this->sortModificationCount == $this->modificationCount;
    }

    protected function setSorted()
    {
        $this->sortModificationCount = $this->modificationCount;
    }

    public function __construct($iterable = null, callable $comparator = null)
    {
        parent::__construct($iterable);
        if ($comparator) {
            $this->setComparator($comparator);
        }
    }

    protected function internalAddAllMatchingElementsToArray($element, array &$indexes, $offset, $limit)
    {
        $comparator = $this->createComparator();
        $count = 0;

        do {
            if ($comparator($this->storage[$offset], $element) == 0) {
                $indexes[] = $offset;
                ++$count;
            }
        } while ($count < $limit && ++$offset < $this->size);
    }

    /**
     * Sorts the current ILIst elements.
     *
     * Element index association is not kept.
     *
     * @param callable $comparator
     * @param null $invertSortOrder
     * @see \phbrick\collection\ISortable::sort()
     */
    public function sort(callable $comparator = null, $invertSortOrder = null)
    {

        $this->sortByMode($this->getDefaultSortMode(), $comparator, $invertSortOrder);
    }

    public function sortByMode($sortMode, callable $comparator = null, $invertSortOrder = null)
    {

        if ($comparator === null) {
            $comparator = $this->getComparator();
        }

        $comparator = $this->createComparator($comparator, $invertSortOrder);

        switch ($sortMode) {
            case SortModes::NONE:
                /* NO BREAK */
            case SortModes::BY_VALUE_NO_KEEP_KEY:
                if ($comparator !== null) {
                    usort($this->storage, $comparator);
                }
                else if (!$invertSortOrder) {
                    sort($this->storage);
                }
                else {
                    rsort($this->storage);
                }

                break;
            case SortModes::BY_VALUE_KEEP_KEY:
                if ($comparator !== null) {
                    uasort($this->storage, $comparator);
                }
                else if (!$invertSortOrder) {
                    asort($this->storage);
                }
                else {
                    arsort($this->storage);
                }

                break;
            case SortModes::BY_KEY_KEEP_VALUE:
                if ($comparator != null) {
                    uksort($this->storage, $comparator);
                }
                else if (!$invertSortOrder) {
                    ksort($this->storage, $comparator);
                }
                else {
                    krsort($this->storage, $comparator);
                }

                break;
            case SortModes::BY_KEY_NO_KEEP_VALUE:
                $values = array_values($this->storage);
                $keys = array_keys($this->storage);

                if ($comparator != null) {
                    usort($keys, $comparator);
                }
                else if (!$invertSortOrder) {
                    sort($keys, $comparator);
                }
                else {
                    rsort($keys, $comparator);
                }

                $this->storage = array_combine($keys, $values);

                break;
        }

        ++$this->modificationCount;

        $this->setSorted();
    }

    public function getDefaultSortMode()
    {
        return SortModes::BY_VALUE_NO_KEEP_KEY;
    }

    public function shuffle()
    {
        shuffle($this->storage);
        ++$this->modificationCount;
    }

    /**
     * Filters the current collection elements and returns them into a new collection keeping the
     * current elements unaltered.
     *
     * When the criteria returns a collection of elements the values are appended to the result
     * without checking the type of the keys.
     *
     * @param callable $filterCriteria
     * @return ArrayList
     * @see \phbrick\collection\IFilterable::filter()
     */
    public function apply(callable $filterCriteria)
    {
        /** @var $result ArrayList */
        $result = self::instance();

        $this->internalFilter($this->storage, $filterCriteria, $result);

        return $result;
    }

    /**
     * Filters the current collection elements directly altering the collection.
     *
     * When the criteria returns a collection of elements the values are appended to the result
     * without checking the type of the keys.
     *
     * @param callable $filterCriteria
     * @return void
     * @see  \phbrick\collection\IFilterable::apply()
     * @see  \phbrick\collection\ArrayList::filter()
     */
    public function alter(callable $filterCriteria)
    {
        $storage = $this->storage;
        $this->storage = [];
        $this->size = 0;

        $this->internalFilter($storage, $filterCriteria, $this);
        unset($storage);
    }

    /**
     * Filters the array of elements and adds them into the given $result parameter
     *
     * @param array $storage Plain array used for data storage.
     * @param callable $filterCriteria
     * @param AbstractArrayList $result
     * @throws InvalidCallbackException If the $filterCriteria callable does not return a proper
     * value of it throws an exception.
     */
    protected function internalFilter(array $storage, callable $filterCriteria, AbstractArrayList $result)
    {
        foreach ($storage as $index => $value) {

            try {
                $filterResult = $filterCriteria($index, $value);
            } catch (Exception $ex) {
                throw new InvalidCallbackException('Callback throwed an exception: ' . $ex->getMessage(), 0, $ex);
            }

            $isCollection = false;

            if (is_int($filterResult) || is_float($filterResult) || is_bool($filterResult)) {
                $match = (bool)$filterResult;
            }
            else if (is_numeric($filterResult)) {
                $match = (bool)((float)$filterResult);
            }
            else if (is_array($filterResult) || is_a($filterResult, '\\Traversable')) {
                $match = true;
                $isCollection = true;
            }
            else {
                throw new InvalidCallbackException(
                    'Expected a return type of array, int, float or bool but got ' . gettype($filterResult));
            }

            if ($match && !$isCollection) {
                // Append to the result. When there is a match that returns a boolean true value
                // we ignore the provided key and add the value
                $result->add($value);
            }
            else if ($match && $isCollection) {
                // Append to the result. If is not bool then is a traversable and we add all
                // elements
                $result->addAll($filterResult);
            }
        }
    }

    /**
     * Merges all elements from the input iterable with the current ones in this collection and
     * returns them as a new instance.
     *
     * Works like array_merge but with iterable collections.
     *
     * @param array|Traversable $iterable
     * @return ArrayList
     * @throws NotIterableException
     * @throws TypeException
     */
    public function merge($iterable)
    {
        Types::assertIterable($iterable);
        $elements = Arrays::toArray($iterable);
        $elements = array_merge($this->storage, $elements);

        /** @var ArrayList $result */
        $result = self::instance($elements);
        $result = ArrayList::castArrayList($result);
        return $result;
    }
}
