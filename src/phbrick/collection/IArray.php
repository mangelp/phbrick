<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use InvalidArgumentException;
use OutOfBoundsException;

/**
 * Array-like operations derived from \ArrayAccess interface.
 */
interface IArray
{

    /**
     * Gets an element by its index
     * @param int $index
     * @throws OutOfBoundsException If the provided index is less than 0 or greater than or equal
     * to the current collection size.
     */
    public function get($index);

    /**
     * Checks if a given index is valid within the collection
     * @param int $index
     * @return bool true if the index is valid or false if not
     */
    public function has($index);

    /**
     * Adds a new element to the collection at the end of it or inserts it into a given index.
     *
     * @param mixed $element Element to be added
     * @param int $index If set inserts the element at that position displacing the rest of
     * elements after it.
     * @throws OutOfBoundsException If the provided index is less than 0 or greater than the
     * current collection size.
     * @throws InvalidArgumentException When $element is null
     */
    public function add($element, $index = null);

    /**
     * Replaces the element at the given index with the one provided.
     *
     * If $index is equals to the size of the collection it will behave as add($element) as it will
     * append the element to the end of the collection.
     *
     * @param int $index
     * @param mixed $element
     * @throws OutOfBoundsException If the provided index is less than 0 or greater than or equal
     * to the current collection size.
     * @throws InvalidArgumentException When $element is null
     */
    public function set($index, $element);
}
