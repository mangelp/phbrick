<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\collection;

use ArrayAccess;
use Countable;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\exceptions\ReadOnlyException;
use phbrick\types\Types;
use RuntimeException;

/**
 * Hierarchical configuration collection.
 *
 * Stores a set of key-value pairs of configurations and manages those options
 * providing ways of retrieving them, retrieving subsets with limited access
 * or without the ability of updating them.
 *
 * Internally all options are stored using a key and a value into a single array.
 * The keys are made up of components separated by dots. A subset of keys can
 * be retrieved searching them by a prefix and return them wrapped into a new
 * configuration object instance and with restrictions like to be readonly or
 * restrict updating.
 *
 * This class also implements PHP's interfaces ArrayAccess and Countable, so
 * it can be easily managed with standard array-like code.
 *
 */
class Config extends BaseStrictClass implements ArrayAccess, Countable
{

    /**
     * Main configuration storage.
     * @var array
     */
    private $config = [];

    /**
     * Configuration object wrapped by this one.
     * @var Config
     */
    private $wrappedConfig = null;

    /**
     *
     * @return Config
     */
    public function getWrappedConfig()
    {
        return $this->wrappedConfig;
    }

    /**
     * If this flag is set to true the configuration cannot be updated.
     * @var bool
     */
    private $readOnly = false;

    /**
     * Sets if the configuration is read-only.
     */
    public function setReadOnly()
    {
        $this->readOnly = true;
    }

    /**
     * Gets if the configuration is read-only.
     * @return boolean
     */
    public function isReadOnly()
    {
        return $this->readOnly;
    }

    private $strict = true;

    /**
     * Sets strict mode.
     * In strict mode missing configurations cause exceptions.
     * Mostly useful for debugging.
     * @param bool $value
     */
    public function setStrict($value = true)
    {
        $this->strict = $value;
    }

    /**
     * Gets if strict mode is enabled.
     * In strict mode missing configurations cause exceptions.
     * @return boolean
     */
    public function isStrict()
    {
        return $this->strict;
    }

    /**
     * Gets the root of the keys.
     * This root is appended to each key accessed so it can effectively restrict
     * access to only a subset of the configuration.
     * @var string
     */
    private $keyRoot = '';

    /**
     * Gets the key root.
     * The key root cannot be set directly.
     * @return string
     */
    public function getKeyRoot()
    {
        return $this->keyRoot;
    }

    /**
     *
     * @param array|Config $config
     * @param string $keyRoot
     */
    public function __construct($config = null, $keyRoot = '')
    {
        $keyRoot = trim($keyRoot, '.');

        if (is_array($config)) {
            $this->config = $config;
            $this->keyRoot = $keyRoot;
        }
        else if (is_a($config, 'phbrick\collection\Config')) {
            if ($config->wrappedConfig) {
                $this->wrappedConfig = $config->wrappedConfig;
            }
            else {
                $this->wrappedConfig = $config;
            }

            $this->keyRoot = $config->keyRoot;

            if (!empty($keyRoot) && !empty($this->keyRoot)) {
                $this->keyRoot .= '.' . ltrim($keyRoot, '.');
            }
            else if (empty($keyRoot) && !empty($this->keyRoot)
                || !empty($keyRoot) && empty($this->keyRoot)) {

                $this->keyRoot .= $keyRoot;
            }

            $this->readOnly = $config->isReadOnly();
        }
        else if ($config !== null) {
            throw new InvalidArgumentException(
                'Invalid configuration argument type: ' . gettype($config));
        }
        else {
            $this->keyRoot = $keyRoot;
        }
    }

    /* [## Interface impls start */

    /* \ArrayAccess impl */

    /**
     * @param mixed $offset
     * @return mixed
     * @internal param $offset
     */
    public function offsetExists($offset)
    {
        return $this->hasConfig($offset);
    }

    /**
     * @param mixed $offset
     * @return mixed
     * @throws RuntimeException
     * @internal param $offset
     */
    public function offsetGet($offset)
    {
        return $this->getConfig($offset);
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @internal param $offset
     * @internal param $value
     */
    public function offsetSet($offset, $value)
    {
        $this->setConfig($offset, $value);
    }

    /**
     * @param mixed $offset
     * @throws RuntimeException
     * @internal param $offset
     */
    public function offsetUnset($offset)
    {
        $this->unsetConfig($offset);
    }

    /* \Countable impl */

    /**
     * Count implementation.
     * (non-PHPdoc)
     * @see Countable::count()
     */
    public function count()
    {
        return $this->calculateSize();
    }

    /* Interface implementations end ##] */

    /**
     * Gets the total number of keys this configuration stores independently
     * of the key root.
     */
    public function getTotalSize()
    {
        if ($this->wrappedConfig) {
            return $this->wrappedConfig->getTotalSize();
        }

        return count($this->config);
    }

    public function calculateSize()
    {
        $config = null;

        if ($this->wrappedConfig) {
            $config = $this->wrappedConfig->config;
        }
        else {
            $config = $this->config;
        }

        if (empty($this->keyRoot)) {
            return count($config);
        }

        $count = 0;
        $len = strlen($this->keyRoot);

        foreach ($config as $key => $value) {
            if (substr($key, 0, $len) == $this->keyRoot) {
                ++$count;
            }
        }

        return $count;
    }

    /**
     * Gets all keys as an array
     * @return array
     */
    public function getKeys()
    {

        $config = $this->getConfigByKeyRoot();

        return array_keys($config);
    }

    /**
     * Gets all values as an array
     * @return array
     */
    public function getValues()
    {
        $config = $this->getConfigByKeyRoot();

        return array_values($config);
    }

    /**
     * Gets all configuration pairs as an array
     * @return array
     */
    public function toArray()
    {
        return $this->getConfigByKeyRoot();
    }

    protected function getConfigByKeyRoot($keyRoot = null)
    {
        if ($keyRoot === null) {
            $keyRoot = $this->keyRoot;
        }

        if ($this->wrappedConfig) {
            return $this->wrappedConfig->getConfigByKeyRoot($keyRoot);
        }

        if (empty($keyRoot)) {
            return $this->config;
        }

        $len = strlen($keyRoot) + 1;
        $keyRootPortion = $keyRoot . '.';
        $result = [];

        foreach ($this->config as $key => $value) {
            if (substr($key, 0, $len) == $keyRootPortion) {
                $key = substr($key, $len);
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Prefixes the key with the current key root.
     * If the key starts already with the key root it will not be prefixed.
     * @param string $key
     * @return string
     */
    protected function getKey($key)
    {
        $key = trim($key, '.');

        if (!empty($this->keyRoot)) {

            if (substr($key, 0, strlen($this->keyRoot)) != $this->keyRoot) {
                $key = $this->keyRoot . '.' . $key;
            }
        }

        return $key;
    }

    /**
     * Gets a configuration key using a default value if it is not found.
     * When using this method use an strict comparison to test the result if
     * you don't provide a default value to avoid confusions between FALSE and
     * NULL values.
     *
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     * @throws RuntimeException If strict mode is enabled and the key is not found
     * in configuration and a default value is not provided.
     */
    public function getConfig($key, $defaultValue = null)
    {
        $key = $this->getKey($key);

        if ($this->wrappedConfig) {
            return $this->wrappedConfig->getConfig($key, $defaultValue);
        }

        if (isset($this->config[$key])
            || array_key_exists($key, $this->config)) {

            return $this->config[$key];
        }

        if ($this->strict && $defaultValue === null) {
            throw new RuntimeException(
                "Access to undefined key: $key "
                . "under root '{$this->keyRoot}' "
                . "without a default value set");
        }

        return $defaultValue;
    }

    /**
     * Sets the configuration key.
     *
     * @param string $key
     * @param mixed $value
     * @return Config
     * @throws ReadOnlyException When the configuration is read only
     */
    public function setConfig($key, $value)
    {

        $key = $this->getKey($key);

        if ($this->wrappedConfig) {
            return $this->wrappedConfig->setConfig($key, $value);
        }

        if ($this->readOnly) {
            throw new ReadOnlyException("Configuration is read only");
        }

        $this->config[$key] = $value;

        return $this;
    }

    /**
     * Checks if a configuration key exists.
     * @param string $key
     * @param bool $notNull
     * @return bool
     */
    public function hasConfig($key, $notNull = false)
    {
        $key = $this->getKey($key);

        if ($this->wrappedConfig) {
            $hasIt = $this->wrappedConfig->hasConfig($key, $notNull);
        }
        else if ($notNull) {
            $hasIt = isset($this->config[$key]);
        }
        else {
            $hasIt = isset($this->config[$key])
                || array_key_exists($key, $this->config);
        }

        return $hasIt;
    }

    public function unsetConfig($key)
    {
        $key = $this->getKey($key);

        if ($this->wrappedConfig) {
            return $this->wrappedConfig->unsetConfig($key);
        }

        $hasIt = isset($this->config[$key])
            || array_key_exists($key, $this->config);

        if ($hasIt) {
            unset($this->config[$key]);
        }
        else if ($this->strict) {
            throw new RuntimeException(
                "Unset of undefined key: $key "
                . "under root '{$this->keyRoot}'");
        }
        else {
            return false;
        }

        return true;
    }

    /**
     * Loads the configuration from a file as a PHP array.
     *
     * @param string $filePath
     * @param string $configArrayName
     * @throws RuntimeException If the file doesn't exists or is not readable or if
     * the specified array variable name isn't found after loading the file.
     */
    public function loadFromPHPFile($filePath, $configArrayName = 'config')
    {
        if (!file_exists($filePath)) {
            throw new RuntimeException("File $filePath don't exists.");
        }

        if (!is_readable($filePath)) {
            throw new RuntimeException("Can't open $filePath for reading.");
        }

        if (is_dir($filePath)) {
            throw new RuntimeException("Cannot load configuration from a directory");
        }

        if (ob_start()) {
            try {
                include($filePath);
            } catch (RuntimeException $ex) {
                error_log(__METHOD__ . '[Exception ' . get_class($ex) . ']: ' . $ex->getMessage());
            } finally {
                ob_end_clean();
            }
        }

        if (!isset($$configArrayName) || !is_array($$configArrayName)) {
            throw new RuntimeException("Configuration array not found");
        }

        foreach ($$configArrayName as $key => $value) {
            $value = $this->replaceConfigKeys($value);
            $this->setConfig($key, $value);
        }
    }

    public function getConfigKeyReplacement($configKey)
    {
        $key = trim($configKey, '{}');

        $value = $this->getConfig($key, $this);

        if ($value === $this) {
            // Not found
            return $configKey;
        }

        return $value;
    }

    /**
     * Replaces all configuration keys found in the string in the form ${keyName}
     * by their values.
     *
     * Configuration keys found are not passed through getKey and used as is.
     *
     * @param String $string
     * @param array $extraVariables
     * @return String
     */
    public function replaceConfigKeys($string, array $extraVariables = [])
    {
        if (strpos($string, '{') === false) {
            return $string;
        }

        $callback = null;
        $cfgInstance = $this;

        if (empty($extraVariables)) {

            $callback = function($configKey) use ($cfgInstance) {
                if (is_array($configKey)) {
                    $configKey = $configKey[0];
                }

                return $cfgInstance->getConfigKeyReplacement($configKey);
            };
        }
        else {
            $callback = function($configKey) use ($cfgInstance, $extraVariables) {
                if (is_array($configKey)) {
                    $configKey = $configKey[0];
                }

                $value = $cfgInstance->getConfigKeyReplacement($configKey);

                if ($value == $configKey
                    && (isset($extraVariables[$configKey])
                        || array_key_exists($configKey, $extraVariables))) {

                    $value = $extraVariables[$configKey];
                }

                return $value;
            };
        }

        $replacedString = preg_replace_callback(
            "/\{[a-z\.A-Z0-9]+\}/", $callback, $string);

        return $replacedString;
    }

    /**
     * Empties this configuration and removes any root prefix.
     */
    public function clear()
    {
        $this->wrappedConfig = null;
        $this->keyRoot = '';
        $this->config = [];
    }

    /**
     * Gets if this configuration is empty.
     */
    public function isEmpty()
    {
        if ($this->wrappedConfig) {
            return $this->wrappedConfig->isEmpty();
        }

        return empty($this->config);
    }

    /**
     * Returns a new instance that returns keys automatically prefixed with
     * the specified root value.
     * If the root is empty removes it.
     * @param string $root
     * @param bool $readonly If true/false it will be set in the child class if
     * the value in the parent class is false. If not will inherit the value
     * true.
     * @param null $strict
     * @return Config
     */
    public function wrap($root = null, $readonly = null, $strict = null)
    {

        /** @var Config $cfg */
        $cfg = self::instance($this, $root);

        if ($this->readOnly) {
            // Inherit readonly value
            $cfg->readOnly = true;
        }
        else if ($readonly !== null) {
            $cfg->readOnly = (bool)$readonly;
        }

        if ($strict === null) {
            $cfg->strict = $this->strict;
        }
        else {
            $cfg->strict = (bool)$strict;
        }

        return $cfg;
    }

    public function setAll($config, $overwriteExisting = true)
    {
        Types::assertIterable($config);

        foreach ($config as $cKey => $cValue) {
            if ($overwriteExisting || !$this->hasConfig($cKey)) {
                $this->setConfig($cKey, $cValue);
            }
        }
    }
}
