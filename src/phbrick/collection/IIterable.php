<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use IteratorAggregate;

/**
 * Iterable collections must implement the provided operations
 */
interface IIterable extends IteratorAggregate
{

    /**
     * Gets if the collection is empty
     */
    public function isEmpty();

    /**
     * Gets the number of modifications to this collection.
     *
     * Modifications are incremented when the collection is changed.
     *
     * This is needed for iterators that might avoid concurrent modifications of a collection while
     * it is being iterated.
     *
     * Implementors must properly count all modifications to the collection for this counting to be
     * of use. After a modification operation the collection modification count must be greater
     * than the count before the modification and therefore the difference must be greater or equal
     * than 1.
     *
     * @return int
     */
    public function getModificationCount();
}
