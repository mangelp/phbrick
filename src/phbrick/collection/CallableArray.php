<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick\collection;

use Exception;
use InvalidArgumentException;
use RuntimeException;
use Traversable;

/**
 * Array implementation that only contains callables and provides an interface
 * to handle them and also to call them.
 */
class CallableArray extends ArrayList
{

    const POSITION_FIRST = 'first';
    const POSITION_MIDDLE = 'middle';
    const POSITION_LAST = 'last';

    /**
     *
     * @param iterable<callable> $callables
     */
    public function __construct($callables = null)
    {
        parent::__construct($callables, function($a, $b) {
            if ($this->isSameCallable($a, $b)) {
                return 0;
            }
            else if (spl_object_hash($a) > spl_object_hash($b)) {
                return 1;
            }
            else {
                return -1;
            }
        });
    }

    protected function assertValidValue($value)
    {
        if (!is_callable($value)) {
            throw new InvalidArgumentException('Invalid value type: ' . gettype($value) . '. Expected callable.');
        }
    }

    /**
     * Adds a new callable
     *
     * @param callable $callable
     * @param int|string $position
     * @return int|null|string The insertion position
     */
    public function add($callable, $position = null)
    {

        if (is_string($position)) {
            switch (strtolower($position)) {
                case self::POSITION_FIRST:
                    $position = 0;
                    break;
                case self::POSITION_MIDDLE:
                    $position = (int)round(((float)$this->size) / 2, 0, PHP_ROUND_HALF_DOWN);
                    break;
                case self::POSITION_LAST:
                    $position = $this->size;
                    break;
            }
        }

        $this->assertValidIndex($position, true);
        parent::add($callable, $position);

        return $position;
    }

    /**
     * Removes a callable.
     *
     * @param callable $callable
     * @return boolean True if it is found and removed or false otherwise
     */
    public function removeIfFound($callable)
    {

        $result = $this->remove($callable);

        if ($result == -1) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Checks if there is a callable that matches the given one
     * @param callable $callable
     * @return boolean
     */
    public function contains(callable $callable)
    {
        $pos = $this->indexOf($callable);

        return $pos !== -1;
    }

    /**
     * Compares two callables to see if they seem to be the same callable.
     *
     * @param callable|array|Traversable $callable
     * @param callable|array|Traversable $existingCallable
     * @return boolean
     */
    protected function isSameCallable($callable, $existingCallable)
    {

        $type = gettype($callable);

        if ($type != gettype($existingCallable)) {
            return false;
        }

        $same = false;

        if ($type == 'string'
            && $callable == $existingCallable) {

            $same = true;
        }
        elseif ($type == 'array'
            && count($callable) == count($existingCallable)
            && isset($callable[0])
            && $callable[0] == $existingCallable[0]
            && isset($callable[1])
            && $callable[1] == $existingCallable[1]) {

            $same = true;
        }
        elseif (is_object($callable)
            && $callable == $existingCallable) {

            $same = true;
        }

        return $same;
    }

    /**
     * Executes all callables using the input arguments array for parameters.
     * If any of them throws an exception the exceptionHandler callback would
     * be executed if defined, but if not the exception is rethrown.
     *
     * @param array $args
     * @param callable $exceptionHandler
     */
    public function call(array $args = [], callable $exceptionHandler = null)
    {
        foreach ($this->storage as $index => $callable) {
            try {
                call_user_func_array($callable, $args);
            } catch (Exception $ex) {
                if (is_callable($exceptionHandler)) {
                    call_user_func_array($exceptionHandler, [$ex, $args]);
                }
                else {
                    throw new RuntimeException(
                        'Unhandled exception  ' . get_class($ex) . ': ' . $ex->getMessage(),
                        0, $ex);
                }
            }
        }
    }
}
