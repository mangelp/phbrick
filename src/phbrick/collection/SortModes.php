<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use phbrick\StaticClass;

/**
 * Sort modes enumeration.
 */
class SortModes extends StaticClass
{
    /**
     * Sort by key and do not keep value association
     * @var string
     */
    const BY_KEY_NO_KEEP_VALUE = 'key-nokeep-value';
    /**
     * Sort by key and keep value association
     * @var string
     */
    const BY_KEY_KEEP_VALUE = 'key-keep-value';
    /**
     * Sort by value and do not keep key association.
     *
     * @var string
     */
    const BY_VALUE_NO_KEEP_KEY = 'value-nokeep-key';
    /**
     * Sort by value and keep key association.
     * @var string
     */
    const BY_VALUE_KEEP_KEY = 'value-keep-key';
    /**
     * No mode set
     * @var string
     */
    const NONE = '';
}
