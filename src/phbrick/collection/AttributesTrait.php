<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\collection;

use InvalidArgumentException;

/**
 * Trait with operations to handle an array of key-value pairs that represent attributes
 */
trait AttributesTrait
{

    protected $attrs = [];

    /**
     * Gets if there are any attributes defined
     * @return boolean
     */
    public function hasAnyAttrs()
    {
        return !empty($this->attrs);
    }

    /**
     * Returns the value associated with the attribute name or null if not defined.
     * @param string $name
     * @param mixed $defaultValue
     * @return mixed
     */
    public function getAttr($name, $defaultValue = null)
    {
        if (isset($this->attrs[$name])) {
            return $this->attrs[$name];
        }

        return $defaultValue;
    }

    /**
     * Gets all the given attributes using the value as the default value if they are not set.
     *
     * If the $keysAndDefaultValues map contains integer keys then for each of them the value is
     * assumed to be the attribute key and will be returned only if it is set (as there is no default
     * value for it).
     *
     * @param array $keysAndDefaultValues Map with attribute names as keys and default values as
     * values.
     * @param bool|string $onlyIfExists If true non-existing attributes are not returned despite having
     * a default value for them.
     * @return array
     */
    public function getAttrs(array $keysAndDefaultValues = null, $onlyIfExists = false)
    {

        if (empty($keysAndDefaultValues)) {
            return $this->attrs;
        }

        $result = [];

        foreach ($keysAndDefaultValues as $attrKey => $attrValue) {

            if (is_string($attrKey)
                && (($onlyIfExists && $this->hasAttr($attrKey))
                    || !$onlyIfExists)) {

                $result[$attrKey] = $this->getAttr($attrKey, $attrValue);
            }
            else if (is_int($attrKey) && $this->hasAttr($attrValue)) {
                $result[$attrValue] = $this->getAttr($attrValue);
            }
        }

        return $result;
    }

    /**
     * Adds an attribute if it does not already exists.
     * @param string $name Name of the attribute
     * @param mixed $value Attribute value
     * @return boolean True if added (did not existed) or false if not (already exists).
     */
    public function addAttr($name, $value)
    {
        if (!isset($this->attrs[$name])) {
            $this->attrs[$name] = $value;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Adds attributes only if they don't exist.
     *
     * Returns the number of attributes added.
     *
     * @param array $attrs
     * @return int
     */
    public function addAttrs(array $attrs)
    {
        $count = 0;

        foreach ($attrs as $attrKey => $attrValue) {
            $added = $this->addAttr($attrKey, $attrValue);
            $count += $added ? 1 : 0;
        }

        return $count;
    }

    /**
     * Sets the attribute name with the given value
     * @param string $name
     * @param mixed $value
     */
    public function setAttr($name, $value)
    {
        $this->attrs[$name] = $value;
    }

    /**
     * Sets all the given attributes with the given values
     * @param array $map
     * @param bool $onlyIfExists If true will only update exiting attributes if false attributes
     * not defined will be defined.
     * @throws InvalidArgumentException
     */
    public function setAttrs(array $map, $onlyIfExists = false)
    {

        foreach ($map as $attrKey => $attrValue) {
            if (is_int($attrKey)) {
                throw new InvalidArgumentException('Found non-allowed integer key. Use only string keys with this method');
            }

            if (($attrValue === null)
                || ($onlyIfExists && !isset($this->attrs[$attrKey]))) {
                continue;
            }

            $this->setAttr($attrKey, $attrValue);
        }
    }

    /**
     * Checks if the attribute exists.
     *
     * An attribute exists if the key is set with a non-null value.
     *
     * @param string $name
     * @return bool If the attribute exists or not.
     */
    public function hasAttr($name)
    {
        if (empty($name)) {
            return false;
        }

        return isset($this->attrs[$name]);
    }

    /**
     * Combined polymorphic setter/getter.
     *
     * When the first parameter $nameOrMap is an array then the behaviour depends upon the second
     * parameter $valueOrGetterFlag. If is a boolean true then the method returns the attributes
     * using keys as names and value as default values is some is not set. If it is a boolean false
     * or null then the method sets the attributes.
     *
     * When the first parameter is string and the second parameter is not set then retrieves the
     * value.
     * When the first parameter is string but the second parameter is set then sets the value.
     *
     * @param $nameOrMap
     * @param null $valueOrGetterFlag
     * @return mixed|null Returns a map of key/value pairs, a single value or null if the operation
     * was only a set.
     * @example
     * $obj = ObjWithAttr();
     * // Sets attributes a and b
     * $obj->attr(array('a' => 1, 'b' => 2));
     * $obj->attr(array('a' => 1, 'b' => 2), false);
     * // Gets attributes a and b using the second parameter as a flag to determine it is getting
     * // values
     * $obj->attr(array('a' => 1, 'b' => 2), true);
     * // Gets a
     * $obj->attr('a');
     * // Sets b
     * $obj->attr('b', 'default');
     *
     * @internal param array|string $name
     * @internal param null|string $value
     */
    public function attr($nameOrMap, $valueOrGetterFlag = null)
    {
        if (is_string($nameOrMap)) {
            if ($valueOrGetterFlag === null) {
                return $this->getAttr($nameOrMap);
            }
            else {
                $this->setAttr($nameOrMap, $valueOrGetterFlag);
            }

            return null;
        }

        if (!is_array($nameOrMap)) {
            throw new InvalidArgumentException('Parameter $nameOrMap must be either a map or an string');
        }

        if ($valueOrGetterFlag === null || $valueOrGetterFlag === false) {
            $this->setAttrs($nameOrMap);
        }
        else if ($valueOrGetterFlag === true) {
            $this->getAttrs($nameOrMap);
        }

        return null;
    }

    /**
     * Gets if the element matches the given attributes.
     *
     * @param array $attributes
     * @return boolean
     */
    public function matchAttrs(array $attributes)
    {
        foreach ($attributes as $attrName => $attrValue) {
            if (!isset($this->attrs[$attrName]) || $this->attrs[$attrName] != $attrValue) {
                return false;
            }
        }

        return true;
    }
}
