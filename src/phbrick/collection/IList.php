<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;
use InvalidArgumentException;
use OutOfBoundsException;
use Traversable;

/**
 * Collection of elements with a defined order that can be randomly accessed by that order.
 *
 * All the operations keep the indexes continuous, instead of allowing certain indexes to be
 * removed like with native PHP arrays.
 *
 * A comparator might be needed if the elements of the list are non-scalars or arrays as by default
 * any comparison must use basic operations.
 * The comparator must return 0 when both compared values/objects are considered to be equal.
 *
 */
interface IList extends ArrayAccess, ICollection, IArray
{

    /**
     * Adds all elements from the $iterable source into the current collection from a given position.
     *
     * Elements are inserted in the same order they are iterated. Keys are ignored.
     *
     * @param array|Traversable $iterable
     * @param int $offset Position where to start inserting the new elements.
     * @throws OutOfBoundsException If the provided offset is less than 0 or greater than the
     * current collection size.
     * @throws InvalidArgumentException When any element is null
     */
    public function addAll($iterable, $offset = null);

    /**
     * Returns the index of the first occurrence of $element in the collection starting the search
     * at the given $offset.
     *
     * This method iterates collection elements in ascending order by index until it finds the
     * element or the end of the collection.
     *
     * @param mixed $element
     * @param int $offset Index to start searching the element
     * @return int the index of the element or -1 if not found.
     * @throws InvalidArgumentException When $element is null
     * @throws OutOfBoundsException If the provided offset is less than 0 or greater or equal than
     * the current collection size.
     */
    public function indexOf($element, $offset = null);

    /**
     * Returns all the indexes of $element in the collection starting the search at the given
     * $offset.
     *
     * This method iterates collection elements in ascending order by index until it finds the
     * end of the collection.
     *
     * @param mixed $element
     * @param int $offset Index to start searching the element
     * @return array<int>
     * @throws InvalidArgumentException When $element is null
     * @throws OutOfBoundsException If the provided offset is less than 0 or greater than the
     * current collection size.
     */
    public function allIndexesOf($element, $offset = null);

    /**
     * Removes the given element if it is found and returns the index of the element removed or -1
     * if no element was found.
     *
     * @param mixed $element
     * @return int the index of the element or -1
     * @throws InvalidArgumentException When $element is null
     */
    public function remove($element);

    /**
     * Removes the element at the given index.
     * @param int $index
     * @throws OutOfBoundsException If the provided index is less than 0 or greater or equal than
     * the current collection size.
     */
    public function removeAt($index);

    /**
     * Removes all the elements found in the iterable.
     *
     * @param array|Traversable $iterable
     * @throws InvalidArgumentException When any element in the iterable is null
     */
    public function removeAll($iterable);

    /**
     * Removes all the elements in the iterable using them as indexes.
     *
     * @param array|Traversable $iterable
     */
    public function removeAllAt($iterable);

    /**
     * Returns a sublist of the current list as a new IList.
     *
     * @param int $pos
     * @param int $length
     * @return IList a new instance with the elements
     */
    public function subList($pos, $length);

    /**
     * Replaces a portion of the elements with the elements of an iterable and returns the replaced
     * elements as a new instance.
     *
     * The number of elements in the iterable can be any (0, 1, .., 100) as long as they fit with
     * any type constraints implementors must use over elements. If the iterable is null then the
     * elements are simply removed.
     *
     * @param int $pos
     * @param int $length
     * @param array|Traversable $iterable
     * @return IList the replaced portion as a new instance
     */
    public function replace($pos, $length, $iterable = null);
}
