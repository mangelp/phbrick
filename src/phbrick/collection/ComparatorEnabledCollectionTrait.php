<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use InvalidArgumentException;
use phbrick\types\Types;

trait ComparatorEnabledCollectionTrait
{
    /**
     * @var callable
     */
    private $comparator = null;

    /**
     * Gets the comparator for collection elements
     * @return callable
     */
    public function getComparator()
    {
        return $this->comparator;
    }

    /**
     * Sets the comparator for collection elements
     * @param callable $comparator
     */
    public function setComparator(callable $comparator)
    {
        $this->comparator = $comparator;
    }

    /**
     * Default comparator that uses basic comparison operators.
     *
     * @param mixed $a
     * @param mixed $b
     * @return int Comparison integer (> 0, < 0, == 0)
     */
    protected function defaultComparison($a, $b)
    {
        $cmp = null;

        if ($a == $b) {
            $cmp = 0;
        }
        else if ($a > $b) {
            $cmp = 1;
        }
        else {
            $cmp = -1;
        }

        return $cmp;
    }

    /**
     * Always returns a valid comparator.
     *
     * If a comparator is provided it will use it, but if not it will try to use the default
     * comparator returned by ComparatorEnabledCollectionTrait::defaultComparison
     *
     * @param string $comparator
     * @param bool $invertSort
     * @return callable|IComparator
     * @throws InvalidArgumentException when there is a comparator provided that is not a valid
     * comparator.
     */
    protected function createComparator($comparator = null, $invertSort = false)
    {
        if ($comparator === null && $this->getComparator() === null) {
            $comparator = [$this, 'defaultComparison'];
        }
        else if ($comparator === null) {
            $comparator = $this->getComparator();
        }

        if (!is_callable($comparator)
            && !is_a($comparator, 'phbrick\\collections\\IComparator')) {

            throw new InvalidArgumentException('Invalid comparator ' . Types::getTypeName($comparator));
        }

        if ($invertSort) {
            $newComparator = function($a, $b) use ($comparator) {
                $cmp = $comparator($a, $b) * -1;
                return $cmp;
            };

            return $newComparator;
        }
        else {
            return $comparator;
        }
    }
}

