<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use InvalidArgumentException;
use phbrick\string\SafeStringRenderTrait;
use phbrick\string\Strings;
use phbrick\types\Bools;
use phbrick\types\Types;
use Traversable;

/**
 * Models a sequence of identifiers or names separated by a given string.
 *
 * The default separator is an empty string.
 *
 */
class IdentifierSequence extends AbstractArrayCollection
{

    use SafeStringRenderTrait;

    /**
     * @var string
     */
    private $separator = '';

    /**
     * Gets the identifier delimiter string
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * Sets the identifier delimiter string
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = Strings::castString($separator);
    }

    /**
     * @var bool
     */
    private $caseSensitive = true;

    /**
     * Gets if the identifiers are case sensitive or not. By default is case sensitive.
     * @return bool
     */
    public function isCaseSensitive()
    {
        return $this->caseSensitive;
    }

    /**
     * Sets if the identifiers are case sensitive or not
     * @param bool $caseSensitive
     */
    public function setCaseSensitive($caseSensitive)
    {
        $this->caseSensitive = Bools::castBool($caseSensitive);
    }

    /**
     * IdentifierSequence constructor.
     *
     * Initializes the sequence and the separator.
     *
     * @param string|array|Traversable|null $sequence
     * @param string|null $separator
     * @param bool|null $caseSensitive
     */
    public function __construct($sequence = null, $separator = null, $caseSensitive = null)
    {
        if ($caseSensitive !== null) {
            $this->setCaseSensitive($caseSensitive);
        }

        if ($separator !== null) {
            $this->setSeparator($separator);
        }

        if ($sequence !== null) {
            $sequence = $this->prepareSequence($sequence);
        }

        parent::__construct($sequence);
    }

    /**
     * Normalizes a sequence converting it to an array of sanitized identifiers.
     *
     * @param string|array|Traversable $sequence
     * @param bool $sanitize
     * @return array
     */
    protected function prepareSequence($sequence, $sanitize = true)
    {
        $preparedSequence = null;

        if (is_string($sequence)) {
            $preparedSequence = explode($this->separator, trim($sequence, $this->separator));
        }
        else if (Types::isIterable($sequence)) {
            $preparedSequence = Arrays::toArray($sequence);
        }
        else {
            throw new InvalidArgumentException(
                'Cannot add identifier components from type ' . Types::getTypeName($sequence));
        }

        if ($sanitize) {
            $preparedSequence = $this->sanitizeSequence($preparedSequence);
        }

        return $preparedSequence;
    }

    /**
     * Reviews the sequence and asserts it is a valid sequence and that each identifier is also
     * valid.
     *
     * This method can alter the sequence and always must return the sequence to be used.
     *
     * @param array|\Traversable $sequence
     * @return array the sanitized identifiers of the sequence as an array
     */
    protected function sanitizeSequence($sequence)
    {
        return $sequence;
    }

    protected function assertValidValue($value)
    {

        parent::assertValidValue($value);

        if (!is_string($value) && !Types::isIterable($value)) {
            throw new InvalidArgumentException('Invalid value type ' . Types::getTypeName($value));
        }
    }

    public function toString()
    {
        return implode($this->separator, $this->storage);
    }
}
