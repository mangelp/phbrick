<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\collection;

use Countable;
use Exception;
use InvalidArgumentException;
use OutOfBoundsException;
use OutOfRangeException;
use phbrick\exceptions\InvalidCallbackException;
use phbrick\exceptions\InvalidOperationException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\exceptions\TypeException;
use phbrick\StaticClass;
use phbrick\types\Bools;
use phbrick\types\Ints;
use phbrick\types\Types;

/**
 * Generic operations over arrays.
 *
 * Some operations apply only to integer-indexed arrays.
 *
 * @static
 */
final class Arrays extends StaticClass
{
    /**
     * Shift direction left
     * @var string
     */
    const SHIFT_LEFT = 'l';

    /**
     * Shift direction right
     * @var string
     */
    const SHIFT_RIGHT = 'r';

    /**
     * @param array $array
     * @return array
     */
    public static function castArray(array $array)
    {
        return $array;
    }

    /**
     * Converts several types into an array.
     *
     * Arrays are returned as is, Traversable implementations are iterated, strings are returned as an array of
     * chars, objects are returned as a map of field=>value, null is returned as an empty array and
     * any other value throws an exception.
     *
     * This method always returns an array or throws an exception
     *
     * @param mixed $value Value to be converted to an array
     * @return array An array created from the argument
     * @throws InvalidArgumentException if the input argument cannot be converted to an array.
     */
    public static function toArray($value)
    {

        if (is_array($value)) {
            $result = $value;
        }
        else if (is_object($value)) {
            $result = Arrays::objectToArray($value);
        }
        else {
            $result = Arrays::scalarToArray($value);
        }

        return $result;
    }

    /**
     * Converts an scalar into an array.
     *
     * If the scalar value is not an scalar or null it will throw an exception.
     *
     * @param mixed $scalar
     * @return array
     * @throws InvalidArgumentException If the input argument is not an scalar.
     */
    public static function scalarToArray($scalar)
    {
        if (!is_scalar($scalar) && !is_null($scalar)) {
            throw new InvalidArgumentException('Not an scalar: ' . Types::getTypeName($scalar));
        }

        if ($scalar === null) {
            $result = [];
        }
        else if (is_string($scalar)) {
            $result = str_split($scalar);
        }
        else {
            $result = [$scalar];
        }

        return $result;
    }

    /**
     * Converts an object to an array.
     *
     * Traversable implementations are iterated and the key/value pairs are added to the result.
     *
     * If the class of object implements ArrayAccess and Countable it will be iterated using an index from 0 to the size
     * returned by count.
     *
     * If the object has a toArray method it will be called without parameters and the result will be returned if it is
     * an array, if not an IllegalArgumentException exception will be thrown with the class of the object warning about
     * the invalid toArray implementation.
     *
     * In any other case the result of get_object_vars will be returned.
     *
     * @param object $object
     * @return array
     * @throws InvalidArgumentException If the input argument cannot be converted to array or is not an object.
     */
    public static function objectToArray($object)
    {
        if (!is_object($object)) {
            throw new InvalidArgumentException('Not a class instance: ' . Types::getTypeName($object));
        }

        $result = [];

        if (is_a($object, '\\Traversable')) {
            foreach ($object as $vKey => $vValue) {
                $result[$vKey] = $vValue;
            }
        }
        else if (is_a($object, '\\ArrayAccess') && is_a($object, '\\Countable')) {
            /** @var Countable $object */
            $size = $object->count();

            for ($index = 0; $index < $size; $index++) {
                $result[$index] = $object[$index];
            }
        }
        else if (method_exists($object, 'toArray')) {
            $result = $object->toArray();

            if (!is_array($result)) {
                throw new InvalidArgumentException('Invalid toArray implementation in ' . Types::getTypeName($object));
            }
        }
        else {
            $result = get_object_vars($object);
        }

        return $result;
    }

    /**
     * Apply a callback over each array key-value pair and return the result as a new array.
     *
     * The callback can filter the elements returned or even alter the key/value pairs or add new
     * ones. The callback signature is the same as described in IFilterable::apply interface.
     *
     * @param array $array
     * @param callable $callable Callback that filters and alters the key/value pairs.
     * @param bool $preserveNumericKeys If true numeric keys are preserved, but if false they will
     * be reset.
     * @return array
     * @throws OutOfBoundsException If the index is out of array limits
     */
    public static function apply(array $array, callable $callable = null, $preserveNumericKeys = false)
    {
        $result = [];

        if (!$callable) {
            return $array;
        }

        foreach ($array as $key => $value) {
            try {
                $filterResult = $callable($key, $value);
            } catch (Exception $ex) {
                throw new InvalidCallbackException('Callback throwed an exception: ' . $ex->getMessage(), 0, $ex);
            }

            $match = null;

            if (is_int($filterResult) || is_float($filterResult) || is_bool($filterResult)) {
                $match = (bool)$filterResult;
                $filterResult = [$key => $value];
            }
            else if (is_numeric($filterResult)) {
                $match = (bool)((float)$filterResult);
                $filterResult = [$key => $value];
            }
            else if (is_array($filterResult) || Types::isIterable($filterResult)) {
                $match = true;
            }
            else {
                throw new InvalidCallbackException(
                    'Expected a return type of array, int, float or bool but got '
                    . gettype($filterResult));
            }

            if ($match) {
                // Append to the result the modified value or a set of new values
                foreach ($filterResult as $filterKey => $filterValue) {
                    if (is_string($filterKey) || (is_int($filterKey) && $preserveNumericKeys)) {
                        $result[$filterKey] = $filterValue;
                    }
                    else {
                        $result[] = $filterValue;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Checks if the items of an array are of the given type.
     *
     * @param array $array
     * @param string $type Type to check
     * @param bool $quick If true only the first and last elements are checked
     * @return bool
     * @throws TypeException
     */
    public static function isOfType(array $array, $type, $quick = true)
    {
        $count = count($array);

        if ($count == 0) {
            return false;
        }

        if ($quick) {
            $first = reset($array);
            return Types::isType($type, $first) && ($count == 1 || Types::isType($type, end($array)));
        }
        else {
            foreach ($array as $key => $value) {
                if (!Types::isType($type, $value)) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * Gets if the indexes of an array are of type string
     *
     * @param array $array
     * @param bool $quick If true only the first and last element keys are checked
     * @return bool
     */
    public static function isStringIndexed(array $array, $quick = true)
    {
        return Arrays::isIndexType($array, true, $quick);
    }

    /**
     * Gets if indexes of an array are of type int
     *
     * @param array $array
     * @param bool $quick If true only the first and last element keys are checked
     * @return bool
     */
    public static function isIntegerIndexed(array $array, $quick = true)
    {
        return Arrays::isIndexType($array, false, $quick);
    }

    /**
     * Checks if the array keys match the given type
     *
     * @param array $array
     * @param $isString
     * @param bool $quick True to check only the first and last position keys, false to check all keys.
     * @return bool
     */
    private static function isIndexType(array $array, $isString, $quick)
    {
        $checkType = $isString ? '\is_string' : '\is_int';
        $count = count($array);

        if ($count == 0) {
            // No indexes mean it can be anything
            return true;
        }
        else if ($quick) {
            reset($array);
            $first = $count > 0 ? key($array) : null;
            end($array);
            $last = $count > 1 ? key($array) : null;
            return ($first !== null && $checkType($first)) && ($last === null || $checkType($last));
        }
        else {

            foreach ($array as $key => $value) {
                if (!$checkType($key)) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * Returns true if an array has no elements or if is null.
     *
     * @param array $array
     * @return bool
     */
    public static function isEmpty(array $array = null)
    {
        return $array === null || count($array) == 0;
    }

    /**
     * Asserts that the array elements are indexed by integers (at least the first and last ones).
     * @param array $array
     */
    public static function assertIntegerIndexed(array $array)
    {
        if (!Arrays::isIntegerIndexed($array)) {
            throw new InvalidArgumentException("Array is not integer-indexed");
        }
    }

    /**
     * Asserts that the array elements are indexed by strings (at least the first and last ones).
     *
     * @param array $array
     */
    public static function assertStringIndexed(array $array)
    {
        if (!Arrays::isStringIndexed($array)) {
            throw new InvalidArgumentException("Array is not string-indexed");
        }
    }

    /*************************************************************************
     * Methods designed to work only with numerically indexed arrays
     *************************************************************************/

    /**
     * Extract a subset of an array as another array
     *
     * @param int $start
     * @param int $length
     * @param array $array
     * @return array
     * @throws OutOfBoundsException If the index is out of array limits
     */
    public static function slice(array $array, $start = 0, $length = null)
    {
        $size = count($array);

        if ($start >= $size) {
            return [];
        }

        $start = Arrays::checkArrayIndex($array, $start);

        return array_slice($array, $start, $length);
    }

    /**
     * Inserts one or more items into an array.
     *
     * @param array $array The array to insert items into, passed by reference.
     * @param int $pos Starting insertion position on $array. If negative then is a position from
     * the end of the array, if positive is a position from the start.
     * @param mixed $item Item or array of items to be inserted. If an array each value will be
     * inserted at the given position. If not it will be inserted as a single item.
     * @param int $start If $item is an integer-indexed array this is the position for first
     * element to be inserted.
     * @param int $length If $item is an integer-indexed array this is the maximum number of
     * elements to insert.
     * @return int The inserted element position
     * throws \OutOfBoundsException If the index is out of array limits
     */
    public static function insert(array &$array, $pos, $item, $start = null, $length = null)
    {
        $len = count($array);

        $pos = Arrays::checkArrayIndex($array, $pos, true);

        if (is_array($item)) {
            if ($start !== null) {
                Arrays::checkArrayIndex($item, $start);
            }

            if ($length !== null && !is_int($length) && !is_numeric($length)) {
                throw new InvalidArgumentException('Parameter $length must be numeric if specified');
            }

            $items = array_slice($item, $start, $length);
            array_splice($array, $pos, 0, $items);
        }
        else if ($pos == $len) {
            $array[] = $item;
        }
        else {
            array_splice($array, $pos, 0, [$item]);
        }

        return $pos;
    }

    /**
     * Replaces a portion of an array with items from another array or by a single item.
     *
     * If the replacement is null or an empty array the replaced items are removed from the array and no item is
     * inserted in place.
     *
     * @param array $array Array to modify passed by reference
     * @param int $start Starting position from the beginning of the array to start to remove
     * elements. Supports negative indexes.
     * @param int $length Number of elements to be replaced. If the length is greater than the
     * number of available items then is reduced to that number. If set to null replaces all the elements from the starting
     * position to the end of the array. Defaults to null.
     * @param array $replacement Array of items single item (non-array) to insert in place of the range of elements. If
     * null or an empty array the items will be replaced by nothing effectively removing them from the array.
     * @return array the replaced array elements (removed from the array)
     * throws \OutOfBoundsException If the index is out of array limits
     * @throws OutOfBoundsException If the start index is out of the array bounds or the array is empty
     * @throws InvalidArgumentException If the start index is not an integer.
     */
    public static function replace(array &$array, $start, $length = null,
                                   $replacement = null)
    {
        $arrLen = count($array);

        $start = Arrays::checkArrayIndex($array, $start);

        if ($start > $arrLen) {
            return [];
        }

        if ($length === null) {
            $length = $arrLen - $start;
        }

        return array_splice($array, $start, $length, $replacement);
    }

    /**
     * Displaces elements of the array inserting new elements from the opposite side if specified.
     *
     * @param array $array
     * @param string $dir
     * @param int $count
     * @param mixed $pad Element to add or callback to generate the element or elements to be added
     * @return array the elements removed during the shift operation as an array
     * throws \OutOfBoundsException If the index is out of array limits
     */
    public static function shift(array &$array, $dir = Arrays::SHIFT_LEFT, $count = 1, $pad = null)
    {

        if ($count < 0) {
            throw new InvalidArgumentException('Shift count cannot be less than 1');
        }

        $result = null;
        $isPad = $pad !== null;
        $isPadCall = $pad !== null && is_callable($pad);

        if ($dir == Arrays::SHIFT_LEFT && !$isPad) {
            $result = array_splice($array, 0, $count);
        }
        else if ($dir == Arrays::SHIFT_LEFT && $isPad && !$isPadCall) {
            $result = array_splice($array, 0, $count);
            if (!is_array($pad)) {
                $pad = array_pad([], $count, $pad);
            }

            array_splice($array, count($array), 0, $pad);
        }
        else if ($dir == Arrays::SHIFT_LEFT && $isPad && $isPadCall) {
            $result = array_splice($array, 0, $count);
            array_splice($array, count($array), 0, $pad($count));
        }
        else if ($dir == Arrays::SHIFT_RIGHT && !$isPad) {
            $result = array_splice($array, count($array) - $count);
        }
        else if ($dir == Arrays::SHIFT_RIGHT && $isPad && !$isPadCall) {
            $result = array_splice($array, count($array) - $count, $count);

            if (!is_array($pad)) {
                $pad = array_pad([], $count, $pad);
            }

            array_splice($array, 0, 0, $pad);
        }
        else if ($dir == Arrays::SHIFT_RIGHT && $isPad && $isPadCall) {
            $result = array_splice($array, count($array) - $count, $count);
            array_splice($array, 0, 0, $pad($count));
        }
        else {
            throw new InvalidArgumentException('Invalid shift direction: ' . $dir);
        }

        return $result;
    }

    /**
     * Validates an index against an array. If the index is negative returns the real positive index. If the index is
     * greater than the array size it "wraps" the index reducing by the array size and returns the resulting index.
     *
     * If the flag $allowLastElement is true the length of the array is a valid index.
     * If the flag $fitSize is true the index will be set to the last viable index if is greater than the array size.
     * If the flag $maxIndex is set this will be used as the maximum viable index and set $allowLastElement to false.
     *
     * @param array $array
     * @param int $index
     * @param bool $allowLastElement Whether the size of the array is allowed to insert and increment the array size.
     * @param bool $fitSize Whether the index is fixed to the last allowed position if exceeds the maximum
     * @param int $maxIndex Maximum array size allowed
     * @return int
     * @throws InvalidTypeException If the parameter $index is not numeric
     * @throws OutOfRangeException If the index is out of array limits
     */
    public static function checkArrayIndex(array $array, $index, $allowLastElement = false, $fitSize = false, $maxIndex = null)
    {
        Arrays::assertIntegerIndexed($array);

        $index = Ints::castInt($index);

        if ($maxIndex !== null) {
            $maxIndex = Ints::castInt($maxIndex);

            if ($maxIndex < 0) {
                throw new InvalidOperationException("Maximum index cannot be negative: $maxIndex");
            }

            $len = $maxIndex + 1;
            // When there is a maximum size we cannot expand the array size over it
            $allowLastElement = false;

        }
        else {
            $len = count($array);
        }

        $allowLastElement = Bools::castBool($allowLastElement);
        $fitSize = Bools::castBool($fitSize);
        $index = (int)$index;
        $result = $index;

        if ($index < 0) {
            // Make it positive if negative
            $result = $index + $len;
        }

        if ($fitSize && $index > $len) {
            // Set the index to the last viable index
            if ($len == 0) {
                $result = 0;
            }
            else if ($allowLastElement) {
                $result = $len;
            }
            else {
                $result = $len - 1;
            }
        }

        $isError = ($result == $len && !$allowLastElement) || $result > $len || $result < 0;

        if ($len == 0 && !$allowLastElement && $index >= 0) {
            throw new OutOfRangeException("Invalid index $index. The array is empty");
        }
        else if ($len == 0 && !$allowLastElement && $index < 0) {
            throw new OutOfRangeException("Invalid negative index $index. The array is empty");
        }
        else if ($isError && $result >= 0) {
            throw new OutOfRangeException("Index $index ($result) is out of the range [0, $len" . ($allowLastElement ? ']' : ')'));
        }
        else if ($isError && $result < 0) {
            throw new OutOfRangeException("Negative index $index ($result) is out of the allowed negative range " . ($allowLastElement ? '[' : '(') . "-$len, -1]");
        }

        return $result;
    }

    /**
     * Returns an array element by index supporting negative indexes.
     *
     * Negative indexes are converted to positive indexes. If the resulting positive index is out of the array limits
     * an exception will be thrown.
     *
     * @param array $array
     * @param $index
     * @return mixed The element at the given index
     * @throws OutOfBoundsException When the index is out of array limits
     */
    public static function get(array $array, $index, $maxIndex = null)
    {
        $index = Arrays::checkArrayIndex($array, $index, false, false, $maxIndex);

        return $array[$index];
    }

    /**
     * Sets an item of an array supporting negative indexes.
     *
     * Negative indexes are converted to positive indexes. If the resulting positive index is out of the array limits
     * an exception will be thrown.
     *
     * The last element of the array is not a valid position.
     *
     * @param array $array
     * @param int $index Index to set the element
     * @param mixed $value Element to be set at the given array index.
     * @param int $maxIndex Maximum index to set. Defaults to null.
     * @return int The effective array index
     * @throws OutOfBoundsException When the index is out of array limits
     */
    public static function set(array &$array, $index, $value, $maxIndex = null)
    {
        $index = Arrays::checkArrayIndex($array, $index, false, false, $maxIndex);

        $array[$index] = $value;
        return $index;
    }

    /**
     * Removes an item from an array supporting negative indexes.
     *
     * Convenient wrapper over Arrays::replace for a single item.
     *
     * Negative indexes are converted to positive indexes. If the resulting positive index is out of the array limits
     * an exception will be thrown.
     *
     * @param array $array Array to remove the item from
     * @param int $index Position to remove the item from
     * @return mixed
     * @throws OutOfBoundsException If the index is out of array limits
     */
    public static function remove(array &$array, $index)
    {
        $result = Arrays::replace($array, $index, 1, null);
        return $result[0];
    }

    /**
     * Gets if the given index exists in the array and if the value is not null.
     *
     * Negative indexes are converted to positive indexes. If the resulting positive index is out of the array limits
     * an exception will be thrown.
     *
     * @param array $array
     * @param int $index
     * @return bool
     */
    public static function isDefined(array $array, $index)
    {

        $checkedIndex = Arrays::checkArrayIndex($array, $index, true, true);

        return isset($array[$checkedIndex]);
    }

    /**
     * Checks if both arrays contains the same keys
     *
     * @param array $arr1
     * @param array $arr2
     */
    public static function sameKeys(array $arr1, array $arr2)
    {
        if (count($arr1) != count($arr2)) {
            return false;
        }

        foreach ($arr1 as $key => $value) {
            if (!Arrays::isDefined($arr2, $key)) {
                return false;
            }
        }

        return true;
    }

    /*************************************************************************
     * Methods designed to work only with string indexed arrays
     *************************************************************************/

}
