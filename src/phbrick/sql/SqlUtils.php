<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\sql;

use InvalidArgumentException;
use phbrick\StaticClass;

/**
 * Class SqlUtils
 * @package phbrick\sql
 */
class SqlUtils extends StaticClass
{
    const SAFE_CHARS_REGEX = '/^[0-9.,:_\?%\pL ]+$/u';

    /**
     * Throws an exception if the value is an string and it contains non alphanumeric characters excluding whitespace,
     * punctuation, comma, percentage, underscore, question mark or semicolon.
     *
     * This method can be used to check an string before injecting it into an SQL statement but only if no proper escaping
     * can be done and you are desperate. Another case is that you are testing something and you simply need something
     * that blows up just in case.
     *
     * @param mixed $value
     */
    public static function assertNotUnsafeValue($value)
    {
        if (!is_string($value) || empty($value)) {
            return;
        }

        $value = strtolower($value);

        if (!preg_match(SqlUtils::SAFE_CHARS_REGEX, $value)) {
            throw new InvalidArgumentException("Unsafe contents found in the string: «" . $value . "»");
        }
    }
}
