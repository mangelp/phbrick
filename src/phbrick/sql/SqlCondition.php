<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\sql;

use Closure;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\ICloneable;
use phbrick\string\Renderables;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Types;
use Traversable;

/**
 * Class SqlCondition
 *
 * Builds simple SQL WHERE condition from an array or by using the class API.
 *
 * @package phbrick\sql
 */
class SqlCondition extends BaseStrictClass implements ICloneable
{
    use SafeStringRenderTrait;

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Operators: Conjunction, disjunction and variants (negated, xor, ...)
    ////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Logical OR
     */
    const OP_OR = 'OR';
    /**
     * Logical OR negated
     */
    const OP_NOT_OR = 'NOR';
    /**
     * Logical OR negating contained expressions. Affects at each comparison negating and inverting the sense of it.
     */
    const OP_OR_NOT = 'ORN';
    /**
     * Logical AND
     */
    const OP_AND = 'AND';
    /**
     * Logical AND negated
     */
    const OP_NOT_AND = 'NAND';
    /**
     * Logical OR negating contained expressions. Affects at each comparison negating and inverting the sense of it.
     */
    const OP_AND_NOT = 'ANDN';

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Available comparisons
    ////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Equals
     */
    const CMP_EQ = 'EQ';
    /**
     * Not equals
     */
    const CMP_NOT_EQ = 'NEQ';
    /**
     * Greater than
     */
    const CMP_GT = 'GT';
    /**
     * Not greater than
     */
    const CMP_NOT_GT = 'NGT';
    /**
     * Less than
     */
    const CMP_LT = 'LT';
    /**
     * Not less than
     */
    const CMP_NOT_LT = 'NLT';
    /**
     * Greater or equal
     */
    const CMP_GE = 'GE';
    /**
     * Not greater or equal
     */
    const CMP_NOT_GE = 'NGE';
    /**
     * Less or equal
     */
    const CMP_LE = 'LE';
    /**
     * Not less or equal
     */
    const CMP_NOT_LE = 'NLE';
    /**
     * Like
     */
    const CMP_LIKE = 'LIKE';
    /**
     * Not like
     */
    const CMP_NOT_LIKE = 'NLIKE';
    /**
     * Is null
     */
    const CMP_NULL = 'NULL';
    /**
     * Not is null
     */
    const CMP_NOT_NULL = 'NNULL';
    /**
     * Value is into a finite list
     */
    const CMP_IN = 'IN';
    /**
     * Value is not into a finite list
     */
    const CMP_NOT_IN = 'NIN';
    /**
     * Value is in range
     */
    const CMP_BETWEEN = 'BETWEEN';
    /**
     * Value is not in range
     */
    const CMP_NOT_BETWEEN = 'NBETWEEN';

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Configuration keys
    ////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Operator for the logical expression
     */
    const CFG_OP = '#OP';

    /**
     * @param SqlCondition $sqlCondition
     * @return SqlCondition
     */
    public static function castSqlCondition(SqlCondition $sqlCondition)
    {
        return $sqlCondition;
    }

    /**
     * Condition items
     * @var array
     */
    private $items = [];
    /**
     * Callback that properly scapes the SQL.
     *
     * Should be set by the underlying database.
     *
     * @var Closure|null
     */
    private $escapeCallback = null;

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Gets the callback used to escape values before inserting them into conditions
     *
     * @return callable
     */
    public function getEscapeCallback()
    {
        return $this->escapeCallback;
    }

    /**
     * Sets the callback used to escape values before inserting them into conditions
     *
     * @param callable $escapeCallback
     * @return $this
     */
    public function setEscapeCallback(callable $escapeCallback)
    {
        $this->escapeCallback = $escapeCallback;
        return $this;
    }

    /**
     * Returns the current join operator
     *
     * @return string
     */
    public function getOperator()
    {
        return $this->items['#OP'];
    }

    /**
     * Sets the current join operator
     *
     * @param string $operator
     * @return $this
     */
    public function setOperator($operator)
    {
        $this->items['#OP'] = $operator;
        return $this;
    }

    /**
     * SqlCondition constructor.
     *
     * @param null|Traversable|array $items Initial list of condition items to be added
     * @param null|string $operator Join operator
     * @param callable|null $escapeCallback Callback to apply to escape values inserted into conditions
     */
    public function __construct($items = null, $operator = null, callable $escapeCallback = null)
    {
        if ($operator) {
            $this->setOperator($operator);
        }
        else {
            $this->setOperator(SqlCondition::OP_AND);
        }

        if ($escapeCallback) {
            $this->setEscapeCallback($escapeCallback);
        }

        if ($items) {
            Types::assertIterable($items);
            $this->fromArray($items);
        }
    }

    /**
     * Empties all condition items added but it does not change other properties.
     *
     * @return $this
     */
    public function clear()
    {
        $operator = $this->getOperator();
        $this->items = [SqlCondition::CFG_OP => $operator];
        return $this;
    }

    /**
     * Returns an array with all condition items with the same format they had when they were added
     *
     * @param bool $excludeSpecialKeys
     * @return array
     */
    public function toArray($excludeSpecialKeys = false)
    {
        $result = $this->internalToArray($this->items, $excludeSpecialKeys);

        return $result;
    }

    /**
     * Adds all the array items as condition items.
     *
     * If the flag $excludeSpecialKeys is set to true any key that starts with # is ignored.
     *
     * @param array $params
     * @param bool $excludeSpecialKeys
     * @return array
     */
    protected function internalToArray(array $params, $excludeSpecialKeys = false)
    {
        $result = [];

        foreach ($params as $key => $value) {
            if (is_string($key) && $key[0] == '#' && $excludeSpecialKeys) {
                continue;
            }
            else if (is_string($key)) {
                $result[$key] = $value;
            }
            else {
                $result[] = $value;
            }
        }

        return $result;
    }

    /**
     * Replaces all the items contained by the ones in the $conditions parameter.
     *
     * @param array $conditions
     * @return $this
     */
    public function fromArray(array $conditions)
    {
        $this->clear();
        $this->internalFromArray($conditions);
        return $this;
    }

    /**
     * Adds every condition from the $params array
     *
     * @param array $params
     */
    protected function internalFromArray(array $params)
    {
        foreach ($params as $key => $value) {
            if (is_int($key)) {
                $this->addClause($value);
            }
            else if ($key === SqlCondition::CFG_OP) {
                $this->setOperator($value);
            }
            else {
                $parts = $this->parseConditionAndField($key);

                if (isset($parts['field']) && isset($parts['cmp'])) {
                    $this->add($parts['cmp'], $parts['field'], $value);
                }
                else {
                    throw new InvalidArgumentException("Invalid array key format: $key");
                }
            }
        }
    }

    public function __clone()
    {
        foreach ($this->items as $key => $value) {
            if ($value instanceof SqlCondition) {
                $this->items[$key] = clone $value;
            }
        }
    }

    /**
     * Calls uksort over the internal array of items using the given comparator
     *
     * @param callable $comparator
     * @return $this
     */
    public function sort(callable $comparator = null)
    {

        if ($comparator) {
            uksort($this->items, $comparator);
        }
        else {
            ksort($this->items);
        }

        return $this;
    }

    /**
     * Parses an string that contains both the field and the comparison separated by a semicolon.
     *
     * The string is split by the first semicolon, if it contains more than one they will be returned in the
     * comparison portion of the result.
     *
     * Returns an array with a 'cmp' key for the comparison portion and a 'field' value with the field portion.
     *
     * @param $string
     * @return array
     */
    public function parseConditionAndField($string)
    {
        $pos = strpos($string, ':');

        if ($pos !== false) {
            return ['field' => substr($string, 0, $pos), 'cmp' => substr($string, $pos + 1)];
        }
        else {
            return ['field' => $string, 'cmp' => SqlCondition::CMP_EQ];
        }
    }

    /**
     * Adds another condition as sub-condition.
     *
     * If the condition is an iterable a new SqlCondition instance is created and filled with the items given.
     *
     * @param $clause
     * @return $this
     */
    public function addClause($clause)
    {
        if (!Types::isIterable($clause)) {
            $this->items[] = $clause;
        }
        else {
            $this->items[] = self::instance($clause, $this->getOperator(), $this->getEscapeCallback());
        }

        return $this;
    }

    /**
     * Adds all the elements from the array as condition items
     *
     * @param array $array
     */
    public function addArray(array $array)
    {
        $this->internalFromArray($array);
    }

    /**
     * Adds a condition comparing a field with a value.
     *
     * @param $comparison
     * @param $field
     * @param $value
     * @return $this
     */
    public function add($comparison, $field, $value)
    {
        $this->items[$field . ':' . $comparison] = $value;

        return $this;
    }

    /**
     * The field must be greater than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function greater($field, $value)
    {
        $this->add(self::CMP_GT, $field, $value);
        return $this;
    }

    /**
     * The field must not be greater than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notGreater($field, $value)
    {
        $this->add(self::CMP_NOT_GT, $field, $value);
        return $this;
    }

    /**
     * The field must be greater or equal than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function greaterOrEqual($field, $value)
    {
        $this->add(self::CMP_GE, $field, $value);
        return $this;
    }

    /**
     * The field must not be greater or equal than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notGreaterOrEqual($field, $value)
    {
        $this->add(self::CMP_NOT_GE, $field, $value);
        return $this;
    }

    /**
     * The field must be less than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function less($field, $value)
    {
        $this->add(self::CMP_LT, $field, $value);
        return $this;
    }

    /**
     * The field must not be less than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notLess($field, $value)
    {
        $this->add(self::CMP_NOT_LT, $field, $value);
        return $this;
    }

    /**
     * The field must be less or equal than the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function lessOrEqual($field, $value)
    {
        $this->add(self::CMP_LE, $field, $value);
        return $this;
    }

    /**
     * The field must not be less or equal than the value
     * @param $field
     * @param $value
     * @return $this
     */
    public function notLessOrEqual($field, $value)
    {
        $this->add(self::CMP_NOT_LE, $field, $value);
        return $this;
    }

    /**
     * The field must be equal to the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function equal($field, $value)
    {
        $this->add(self::CMP_EQ, $field, $value);
        return $this;
    }

    /**
     * The field must not be equal to the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notEqual($field, $value)
    {
        $this->add(self::CMP_NOT_EQ, $field, $value);
        return $this;
    }

    /**
     * The field must be like the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function like($field, $value)
    {
        $this->add(self::CMP_LIKE, $field, $value);
        return $this;
    }

    /**
     * The field must not be like the value
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notLike($field, $value)
    {
        $this->add(self::CMP_NOT_LIKE, $field, $value);
        return $this;
    }

    /**
     * The field must be null
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function null($field, $value)
    {
        $this->add(self::CMP_NULL, $field, $value);
        return $this;
    }

    /**
     * The field must not be null
     *
     * @param $field
     * @param $value
     * @return $this
     */
    public function notNull($field, $value)
    {
        $this->add(self::CMP_NOT_NULL, $field, $value);
        return $this;
    }

    /**
     * The field is one of the specified set of values
     *
     * @param $field
     * @param array $values
     * @return $this
     */
    public function into($field, array $values)
    {
        $this->add(self::CMP_IN, $field, $values);
        return $this;
    }

    /**
     * The field is not one of the specified values
     *
     * @param $field
     * @param array $values
     * @return $this
     */
    public function notInto($field, array $values)
    {
        $this->add(self::CMP_NOT_IN, $field, $values);
        return $this;
    }

    /**
     * The field is between the given values
     *
     * @param $field
     * @param $lower
     * @param $upper
     * @return $this
     */
    public function between($field, $lower, $upper)
    {
        $this->add(self::CMP_BETWEEN, $field, ['lower' => $lower, 'upper' => $upper]);
        return $this;
    }

    /**
     * The field is not one of the specified values
     *
     * @param $field
     * @param $lower
     * @param $upper
     * @return $this
     */
    public function notBetween($field, $lower, $upper)
    {
        $this->add(self::CMP_BETWEEN, $field, ['lower' => $lower, 'upper' => $upper]);
        return $this;
    }

    /**
     * Returns an string with the SQL condition built from the current list of items
     *
     * @return string
     */
    public function toString()
    {
        return $this->generateSQLForItems($this->items);
    }

    /**
     * Generates an SQL condition string from all the condition items added.
     *
     * @return string
     */
    public function generateSQL()
    {
        return $this->generateSQLForItems($this->items);
    }

    /**
     * Generates an SQL condition by joining all the given items using the current join operator.
     *
     * @param array|Traversable $items
     * @return string SQL condition
     */
    public function generateSQLForItems($items)
    {
        Types::assertIterable($items);

        $sqlConditions = [];
        $operator = $this->getOperator();

        foreach ($items as $cKey => $cValue) {
            if ($cKey === SqlCondition::CFG_OP) {
                $operator = $cValue;
                continue;
            }
            else if (is_int($cKey)) {
                if ($cValue instanceof SqlCondition) {
                    $sqlConditions[] = '(' . SqlCondition::castSqlCondition($cValue)->generateSQL() . ')';
                }
                else if (Types::isIterable($cValue)) {
                    $sqlConditions[] = '(' . $this->generateSQLForItems($cValue) . ')';
                }
                else {
                    $sqlConditions[] = $cValue;
                }
            }
            else {

                $condition = $this->extractCondition($cKey);
                $field = $this->extractField($cKey);

                $sqlConditions[] = $this->generateSQLCondition($condition, $operator, $field, $cValue);
            }
        }

        $sqlString = $this->joinConditions($operator, $sqlConditions, false);

        return $sqlString;
    }

    private function extractCondition($cKey)
    {
        $parts = explode(':', $cKey);

        if (count($parts) != 2) {
            return SqlCondition::CMP_EQ;
        }

        return $parts[1];
    }

    private function extractField($cKey)
    {
        $parts = explode(':', $cKey);

        return $parts[0];
    }

    /**
     * Joins the conditions using the given operator.
     *
     * It does not alter the sql conditions. In the case of OP_OR_NOT and OP_AND_NOT this method will use OR and AND
     * respectively given that the negation of the condition should have been already done by the method
     * generateSQLCondition().
     *
     * @param string $operator Conjunction or disjunction operator to use
     * @param array $sqlConditions Array of sql condition strings
     * @param bool $applyNegation If true (default) and operator is OP_AND_NOT or OP_OR_NOT every condition will be
     * negated by appending the NOT keyword
     * @return string Resulting condition
     */
    public function joinConditions($operator, array $sqlConditions, $applyNegation = true)
    {

        switch ($operator) {
            case self::OP_AND:
                $result = implode(" AND ", $sqlConditions);
                break;
            case self::OP_OR:
                $result = implode(" OR ", $sqlConditions);
                break;
            case self::OP_NOT_AND:
                $result = "NOT (" . implode(" AND ", $sqlConditions) . ")";
                break;
            case self::OP_NOT_OR:
                $result = "NOT (" . implode(" OR ", $sqlConditions) . ")";
                break;
            case self::OP_AND_NOT:
                if ($applyNegation) {
                    $result = "NOT (" . implode(") AND NOT (", $sqlConditions) . ")";
                }
                else {
                    $result = implode(" AND ", $sqlConditions);
                }
                break;
            case self::OP_OR_NOT:
                if ($applyNegation) {
                    $result = "NOT (" . implode(") OR NOT (", $sqlConditions) . ")";
                }
                else {
                    $result = implode(" OR ", $sqlConditions);
                }
                break;
            default:
                throw new InvalidArgumentException("Invalid operator $operator");
        }

        return $result;
    }

    /**
     * Uses a callback that escapes the value and returns the result.
     *
     * If no callback is set returns the value as is.
     *
     * @param $value
     * @return string|string[]
     */
    public function escapeValue($value)
    {
        if (Types::isIterable($value)) {
            $result = [];

            foreach ($value as $vKey => $vValue) {
                $result[] = $this->escapeValue($vValue);
            }

            /** @var string[] $result */
            return $result;
        }
        else if ($this->escapeCallback) {
            $callback = $this->escapeCallback;
            return $callback($value);
        }
        else if (Renderables::isStringRenderable($value)) {
            if (!is_scalar($value) || is_string($value)) {
                $value = Renderables::toString($value);

                // We do not have escaping and we are desperate, apply an strict filter and pray
                SqlUtils::assertNotUnsafeValue($value);

                // Add quotes around the string now that we know the string does not contains them
                $value = "'$value'";
            }

            return "$value";
        }
        else {
            throw new InvalidArgumentException("Cannot convert to string the type: " . Types::getTypeName($value));
        }
    }

    /**
     * Inverts the current join operator by negating it.
     *
     * This causes the change of the resulting condition logic:
     *
     * + A OR B => NOT A AND NOT B
     * + A OR NOT B => NOT A AND B
     * + NOT A OR B => A AND NOT B
     * + NOT A OR NOT B => A AND B
     * + A AND B => NOT A OR NOT B
     * + A AND NOT B => NOT A OR B
     * + NOT A AND B => A OR NOT B
     * + NOT A AND NOT B => A OR B
     *
     * @return $this
     */
    public function invertOperator()
    {
        switch ($this->getOperator()) {
            case SqlCondition::OP_AND:
                $this->setOperator(SqlCondition::OP_OR_NOT);
                break;
            case SqlCondition::OP_OR:
                $this->setOperator(SqlCondition::OP_AND_NOT);
                break;
            case SqlCondition::OP_NOT_AND:
                $this->setOperator(SqlCondition::OP_AND);
                break;
            case SqlCondition::OP_NOT_OR:
                $this->setOperator(SqlCondition::OP_OR);
                break;
            case SqlCondition::OP_AND_NOT:
                $this->setOperator(SqlCondition::OP_OR);
                break;
            case SqlCondition::OP_OR_NOT:
                $this->setOperator(SqlCondition::OP_AND);
                break;
        }

        return $this;
    }

    /**
     * Returns the logically opposite of the given condition constant.
     *
     * @param $condition
     * @return string
     */
    public function invertCondition($condition)
    {
        // Invert conditions
        switch ($condition) {
            case SqlCondition::CMP_GT:
                $condition = SqlCondition::CMP_LE;
                break;
            case SqlCondition::CMP_NOT_LE:
                $condition = SqlCondition::CMP_LE;
                break;
            case SqlCondition::CMP_GE:
                $condition = SqlCondition::CMP_LT;
                break;
            case SqlCondition::CMP_NOT_LT:
                $condition = SqlCondition::CMP_LT;
                break;
            case SqlCondition::CMP_LT:
                $condition = SqlCondition::CMP_GE;
                break;
            case SqlCondition::CMP_NOT_GE:
                $condition = SqlCondition::CMP_GE;
                break;
            case SqlCondition::CMP_LE:
                $condition = SqlCondition::CMP_GT;
                break;
            case SqlCondition::CMP_NOT_GT:
                $condition = SqlCondition::CMP_GT;
                break;
            case SqlCondition::CMP_EQ:
                $condition = SqlCondition::CMP_NOT_EQ;
                break;
            case SqlCondition::CMP_NOT_EQ:
                $condition = SqlCondition::CMP_EQ;
                break;
            case SqlCondition::CMP_LIKE:
                $condition = SqlCondition::CMP_NOT_LIKE;
                break;
            case SqlCondition::CMP_NOT_LIKE:
                $condition = SqlCondition::CMP_LIKE;
                break;
            case SqlCondition::CMP_NULL:
                $condition = SqlCondition::CMP_NOT_NULL;
                break;
            case SqlCondition::CMP_NOT_NULL:
                $condition = SqlCondition::CMP_NULL;
                break;
            case SqlCondition::CMP_BETWEEN:
                $condition = SqlCondition::CMP_NOT_BETWEEN;
                break;
            case SqlCondition::CMP_NOT_BETWEEN:
                $condition = SqlCondition::CMP_BETWEEN;
                break;
            case SqlCondition::CMP_IN:
                $condition = SqlCondition::CMP_NOT_IN;
                break;
            case SqlCondition::CMP_NOT_IN:
                $condition = SqlCondition::CMP_IN;
                break;
        }

        return $condition;
    }

    /**
     * Generates a valid SQL condition from the given condition, operator, field and value.
     *
     * @param $condition
     * @param $operator
     * @param $field
     * @param $value
     * @return string
     */
    public function generateSQLCondition($condition, $operator, $field, $value)
    {
        $conditionStr = "$field ";
        $invertComparisons = ($operator == SqlCondition::OP_OR_NOT || $operator == SqlCondition::OP_AND_NOT);

        if ($invertComparisons) {
            $condition = $this->invertCondition($condition);
        }

        switch ($condition) {
            case SqlCondition::CMP_GT:
                // fall through
            case SqlCondition::CMP_NOT_LE:
                $conditionStr .= "> " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_GE:
                // fall through
            case SqlCondition::CMP_NOT_LT:
                $conditionStr .= ">= " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_LT:
                // fall through
            case SqlCondition::CMP_NOT_GE:
                $conditionStr .= "< " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_LE:
                // fall through
            case SqlCondition::CMP_NOT_GT:
                $conditionStr .= "<= " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_EQ:
                $conditionStr .= "= " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_NOT_EQ:
                $conditionStr .= "<> " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_LIKE:
                $conditionStr .= "LIKE " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_NOT_LIKE:
                $conditionStr .= "NOT LIKE " . $this->escapeValue($value);
                break;
            case SqlCondition::CMP_NULL:
                // This condition ignores the value
                $conditionStr .= "IS NULL";
                break;
            case SqlCondition::CMP_NOT_NULL:
                // This condition ignores the value
                $conditionStr .= "IS NOT NULL";
                break;
            case SqlCondition::CMP_BETWEEN:
                $conditionStr .= $this->generateBetweenConditionRightPart($value, false);
                break;
            case SqlCondition::CMP_NOT_BETWEEN:
                $conditionStr .= $this->generateBetweenConditionRightPart($value, true);
                break;
            case SqlCondition::CMP_IN:
                $conditionStr .= $this->generateInConditionRightPart($value, false);
                break;
            case SqlCondition::CMP_NOT_IN:
                $conditionStr .= $this->generateInConditionRightPart($value, true);
                break;
            default:
                throw new InvalidArgumentException("Parameter \$condition must be a comparison operator. Provided: $condition");
        }

        return $conditionStr;
    }

    /**
     * Generate the right part of a BETWEEN condition including the keyword.
     *
     * @param $vValue
     * @param $negated
     * @return string
     */
    public function generateBetweenConditionRightPart($vValue, $negated)
    {

        if (Types::isArrayAccess($vValue) && isset($vValue['lower']) && isset($vValue['upper'])) {
            $result = "BETWEEN " . $this->escapeValue($vValue['lower']) . " AND " . $this->escapeValue($vValue['upper']);
        }
        else if (Types::isArrayAccess($vValue) && isset($vValue['start']) && isset($vValue['end'])) {
            $result = "BETWEEN " . $this->escapeValue($vValue['start']) . " AND " . $this->escapeValue($vValue['end']);
        }
        else if (Types::isArrayAccess($vValue) && isset($vValue[0]) && isset($vValue[1])) {
            $result = "BETWEEN " . $this->escapeValue($vValue[0]) . " AND " . $this->escapeValue($vValue[1]);
        }
        else {
            throw new InvalidArgumentException("Invalid value not usable for BETWEEN comparison: " . Types::getTypeName($vValue));
        }

        if ($negated) {
            $result = "NOT $result";
        }

        return $result;
    }

    /**
     * Generate the right part of an IN condition including the keyword.
     *
     * @param mixed $vValue Value to iterate
     * @param bool $negated If true the NOT keyword will be prepended
     * @return string
     */
    public function generateInConditionRightPart($vValue, $negated)
    {

        if (Types::isIterable($vValue)) {
            $result = [];

            foreach ($vValue as $key => $value) {
                $result[] = $this->escapeValue($value);
            }

            $result = "IN (" . implode(', ', $result) . ")";
        }
        else if (Types::isArrayAccess($vValue)) {
            $len = count($vValue);
            $result = [];

            for ($i = 0; $i < $len; $i++) {
                $result[] = $this->escapeValue($vValue[$i]);
            }

            $result = "IN (" . implode(', ', $result) . ")";
        }
        else if (is_scalar($vValue)) {
            $result = "IN ($vValue)";
        }
        else {
            throw new InvalidArgumentException("Invalid value not usable for IN comparison: " . Types::getTypeName($vValue));
        }

        if ($negated) {
            $result = "NOT $result";
        }

        return $result;
    }
}
