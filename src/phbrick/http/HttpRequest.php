<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\http;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\net\IpAddress;
use phbrick\net\Url;
use phbrick\string\Strings;
use phbrick\types\Bools;
use phbrick\types\Floats;
use phbrick\types\Ints;
use phbrick\types\Types;

/**
 * Class HttpRequest
 *
 * Models the minimal information of an HTTP request to serve a request only for GET and POST verbs.
 *
 * Does not models cookies, file uploads, json requests or anything else.
 *
 * The user agent is filtered and limited in length
 *
 * @package phbrick\http
 */
class HttpRequest extends BaseStrictClass
{
    /**
     * Default maximum string parameter size
     */
    const STRING_DEFAULT_SIZE_LIMIT = 1024;
    /**
     * User agent maximum size
     */
    const STRING_USER_AGENT_SIZE_LIMIT = 300;

    private $requestMethod = 'get';
    /**
     * @var string
     */
    private $host = 'localhost';
    /**
     * @var string
     */
    private $scheme = 'http';
    /**
     * @var int
     */
    private $port = 80;
    /**
     * @var IpAddress
     */
    private $serverIp = null;
    /**
     * @var IpAddress
     */
    private $clientIp = null;
    /**
     * @var string
     */
    private $clientAgent = 'unknown';

    /**
     * @var string
     */
    private $prefix = '';

    /**
     * @var array
     */
    private $params = [];

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = Ints::castInt($port);
    }

    /**
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     */
    public function setScheme($scheme)
    {
        $scheme = strtolower(trim($scheme));

        if (!in_array($scheme, ['http', 'https'])) {
            throw new InvalidArgumentException("Unsupported scheme $scheme");
        }

        $this->scheme = $scheme;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @param string $requestMethod
     */
    public function setRequestMethod($requestMethod)
    {
        $requestMethod = strtolower(trim($requestMethod));

        if (!in_array($requestMethod, ['get', 'post'])) {
            throw new InvalidArgumentException("Unsupported request method $requestMethod");
        }

        $this->requestMethod = $requestMethod;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setParam($name, $value)
    {
        $param = $this->filterParamName($name);
        $this->params[$param] = $value;
    }

    /**
     * @return string
     */
    public function getClientAgent()
    {
        return $this->clientAgent;
    }

    /**
     * @param string $clientAgent
     */
    public function setClientAgent($clientAgent)
    {
        $clientAgent = trim($clientAgent);

        if (strlen($clientAgent) > self::STRING_USER_AGENT_SIZE_LIMIT) {
            // Truncate the user agent string. We do not care of the missing part and we add an ellipsis at the end
            $clientAgent = substr(trim($clientAgent), 0, self::STRING_USER_AGENT_SIZE_LIMIT - 6) . ' [...]';
        }

        // If the UA strings does not match this regex of common chars avoid using it
        if (!preg_match('/^[a-zA-Z0-9\/ \(\)\[\];:,\._\-\+]{1,' . self::STRING_USER_AGENT_SIZE_LIMIT . '}$/', $clientAgent)) {
            $clientAgent = 'unknown';
        }

        $this->clientAgent = $clientAgent;
    }

    /**
     * @return IpAddress
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }

    /**
     * @param IpAddress|string $clientIp
     */
    public function setClientIp($clientIp)
    {
        if (is_string($clientIp)) {
            $clientIp = trim($clientIp);
            $this->clientIp = new IpAddress($clientIp);
        }
        else if (is_a($clientIp, IpAddress::class)) {
            $this->clientIp = clone $clientIp;
        }
        else {
            throw new InvalidArgumentException("Invalid parameter type " . Types::getTypeName($clientIp));
        }
    }

    /**
     * @return IpAddress
     */
    public function getServerIp()
    {
        return $this->serverIp;
    }

    /**
     * @param IpAddress|string $serverIp
     */
    public function setServerIp($serverIp)
    {
        if (is_string($serverIp)) {
            $serverIp = trim($serverIp);
            $this->serverIp = new IpAddress($serverIp);
        }
        else if (is_a($serverIp, IpAddress::class)) {
            $this->serverIp = clone $serverIp;
        }
        else {
            throw new InvalidArgumentException("Invalid parameter type " . Types::getTypeName($serverIp));
        }
    }

    public function __construct(array $params = null)
    {
        if ($params) {
            $this->setParams($params);
        }
    }

    /**
     * Gets a new Url instance with the current Url.
     * @return Url
     */
    public function getUrl()
    {
        $currentUrlString = $this->getScheme() . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $url = Url::parseUrl($currentUrlString);
        $url->setHost($this->getHost());
        $url->setPort($this->getPort());
        $url->setScheme($this->getScheme());

        return $url;
    }

    public function isGet()
    {
        return strcasecmp($this->requestMethod, 'get') == 0;
    }

    public function isPost()
    {
        return strcasecmp($this->requestMethod, 'post') == 0;
    }

    public function isSecure()
    {
        return strcasecmp($this->scheme, 'https') == 0;
    }

    /**
     * Ensures the parameter is properly prefixed before being used.
     *
     * @param $name
     * @return string
     */
    protected function filterParamName($name)
    {
        return trim($this->prefix . $name);
    }

    /**
     * @param $name
     * @param null $defaultValue
     * @return mixed|null
     */
    protected function getParam($name, $defaultValue = null)
    {
        $param = $this->filterParamName($name);

        if (!isset($this->params[$param])) {
            return $defaultValue;
        }

        return $this->params[$param];
    }

    /**
     * @param string $name
     * @param int $defaultValue
     * @return int
     */
    public function getParamInt($name, $defaultValue = 0)
    {
        $param = $this->getParam($name, $defaultValue);

        return Ints::castInt($param);
    }

    /**
     * @param string $name
     * @param float $defaultValue
     * @return float
     */
    public function getParamFloat($name, $defaultValue = 0.0)
    {
        $param = $this->getParam($name, $defaultValue);

        return Floats::castFloat($param);
    }

    /**
     * @param string $name
     * @param bool $defaultValue
     * @return bool
     */
    public function getParamBool($name, $defaultValue = false)
    {
        $param = $this->getParam($name, $defaultValue);

        return Bools::castBool($param);
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @param int $sizeLimit
     * @return string
     */
    public function getParamString($name, $defaultValue = "", $sizeLimit = self::STRING_DEFAULT_SIZE_LIMIT)
    {
        $param = $this->getParam($name, $defaultValue);

        $result = Strings::castString($param);

        if ($sizeLimit > 0) {
            $result = substr($result, 0, $sizeLimit);
        }

        return $result;
    }

    /**
     * Creates a new instance using the current global request information
     */
    public static function createFromCurrentRequest()
    {
        $httpRequest = self::instance();

        if (isset($_REQUEST)) {
            $httpRequest->setParams($_REQUEST);
        }
        else if (isset($_GET) && isset($_POST)) {
            $httpRequest->setParams(array_merge($_GET, $_POST));
        }
        else if (isset($_GET)) {
            $httpRequest->setParams($_GET);
        }
        else if (isset($_POST)) {
            $httpRequest->setParams($_POST);
        }

        if (isset($_SERVER)) {
            $httpRequest->setHost($_SERVER["SERVER_HOST"]);
            $httpRequest->setPort($_SERVER["SERVER_PORT"]);
            $httpRequest->setServerIp($_SERVER["SERVER_ADDR"]);
            $httpRequest->setClientIp($_SERVER["REMOTE_ADDR"]);
            $httpRequest->setClientAgent($_SERVER["HTTP_USER_AGENT"]);
            $httpRequest->setRequestMethod($_SERVER["REQUEST_METHOD"]);
            $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], "off") != 0;
            $httpRequest->setScheme($https ? "https" : "http");
        }

        return $httpRequest;
    }
}
