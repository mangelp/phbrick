<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\version;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\collection\Arrays;
use phbrick\exceptions\InvalidFormatException;
use phbrick\exceptions\InvalidTypeException;
use phbrick\IComparable;
use phbrick\types\Ints;
use phbrick\types\Types;
use Traversable;

/**
 * Class Version
 *
 * Implements semantic versions http://semver.org/
 *
 * @package phbrick\version
 */
class Version extends BaseStrictClass implements IComparable
{
    /**
     * @param Version $version
     * @return Version
     */
    public static function cast(Version $version)
    {
        return $version;
    }

    private $major = 0;
    private $minor = 0;
    private $patch = 0;
    private $preRelease = [];
    private $buildMeta = [];

    /**
     * @return int
     */
    public function getMajor()
    {
        return $this->major;
    }

    /**
     * @param int $major
     */
    public function setMajor($major)
    {
        $this->major = Ints::castInt($major);
    }

    /**
     * @return int
     */
    public function getMinor()
    {
        return $this->minor;
    }

    /**
     * @param int $minor
     */
    public function setMinor($minor)
    {
        $this->minor = Ints::castInt($minor);
    }

    /**
     * @return int
     */
    public function getPatch()
    {
        return $this->patch;
    }

    /**
     * @param int $patch
     */
    public function setPatch($patch)
    {
        $this->patch = Ints::castInt($patch);
    }

    /**
     * @return array
     */
    public function getPreRelease()
    {
        return $this->preRelease;
    }

    /**
     * Returns the pre-release data as an string
     * @return string
     */
    public function getPreReleaseAsString()
    {
        return implode('.', $this->preRelease);
    }

    /**
     * @param string|array|Traversable $preRelease
     */
    public function setPreRelease($preRelease)
    {
        $this->preRelease = $this->parseDotSeparatedIdentifierSequence($preRelease);
    }

    /**
     * Appends one or more identifiers to the current pre-release string.
     *
     * The input parameter is parsed into an identifier array and that array is merged with the existing data
     *
     * @param string|array|Traversable $preRelease
     */
    public function appendPreRelease($preRelease)
    {
        $this->preRelease = array_merge($this->preRelease, $this->parseDotSeparatedIdentifierSequence($preRelease));
    }

    /**
     * @return array
     */
    public function getBuildMeta()
    {
        return $this->buildMeta;
    }

    /**
     * Returns the current build metadata as a dot-separated identifier sequence
     *
     * @return string
     */
    public function getBuildMetaAsString()
    {
        return implode(".", $this->buildMeta);
    }

    /**
     * @param string|array|Traversable $buildMeta
     */
    public function setBuildMeta($buildMeta)
    {
        $this->buildMeta = $this->parseDotSeparatedIdentifierSequence($buildMeta);
    }

    /**
     * Appends one or more identifiers to the current build metadata string.
     *
     * The input parameter is parsed into an identifier array and that array is merged with the existing data
     *
     * @param string|array|Traversable $buildMeta
     */
    public function appendBuildMeta($buildMeta)
    {
        $this->buildMeta = array_merge($this->buildMeta, $this->parseDotSeparatedIdentifierSequence($buildMeta));
    }

    protected function parseDotSeparatedIdentifierSequence($data)
    {
        $identifiers = [];

        if (empty($data)) {
            return $identifiers; // -- RETURNS --
        }

        if (is_string($data) && !preg_match("/^[a-zA-Z0-9\.]+\$/", $data)) {
            throw new InvalidFormatException("Invalid identifier sequence `$data`. It must contain only alphanumeric ASCII characters plus dot.");
        }
        else if (is_string($data)) {
            $identifiers = explode(".", $data);
        }
        else if (!Types::isIterable($data)) {
            throw new InvalidArgumentException('The parameter $preRelease must be an string or a collection of valid identifiers');
        }
        else {
            $identifiers = $data;
        }

        $result = [];

        foreach ($identifiers as $identifier) {
            if (!is_int($identifier) && !is_string($identifier)) {
                throw new InvalidFormatException("Invalid identifier type " . Types::getTypeName($identifier) . ". Only string and integer are allowed.");
            }
            else if (!ctype_alnum($identifier)) {
                throw new InvalidFormatException("Invalid identifier `" . $identifier . "`. Identifiers must contain only alphanumeric ASCII characters.");
            }

            if (is_string($identifier) && ctype_digit($identifier)) {
                $identifier = (int)$identifier;
            }

            $result[] = $identifier;
        }

        return $result;
    }

    /**
     * Version constructor.
     * @param int $major
     * @param int $minor
     * @param int $patch
     * @param null $preRelease
     * @param null $buildMeta
     */
    public function __construct($major = 0, $minor = 0, $patch = 0, $preRelease = null, $buildMeta = null)
    {
        $this->setMajor($major);
        $this->setMinor($minor);
        $this->setPatch($patch);
        $this->setPreRelease($preRelease);
        $this->setBuildMeta($buildMeta);
    }

    public function isDevelopment()
    {
        return !$this->isEmpty() && $this->major == 0;
    }

    public function isEmpty()
    {
        return $this->major == 0 && $this->minor == 0 && $this->patch == 0;
    }

    public function hasPreRelease()
    {
        return !empty($this->preRelease);
    }

    public function hasBuildMeta()
    {
        return !empty($this->buildMeta);
    }

    public function incrementMajor()
    {
        $this->major++;
        $this->minor = 0;
        $this->patch = 0;
    }

    public function incrementMinor()
    {
        $this->minor++;
        $this->patch = 0;
    }

    /**
     * @param $other
     * @return int
     */
    public function compareTo($other)
    {
        $version = null;

        if (!is_a($other, Version::class)) {
            $version = Version::parse($other);
        }
        else {
            $version = Version::cast($other);
        }

        if ($this->getMajor() > $version->getMajor()) {
            return 1;
        }
        else if ($this->getMajor() < $version->getMajor()) {
            return -1;
        }

        if ($this->getMinor() > $version->getMinor()) {
            return 1;
        }
        else if ($this->getMinor() < $version->getMinor()) {
            return -1;
        }

        if ($this->getPatch() > $version->getPatch()) {
            return 1;
        }
        else if ($this->getPatch() < $version->getPatch()) {
            return -1;
        }

        return $this->comparePreRelease($version);
    }

    /**
     * Compares the pre-release portion of the current version against another one.
     *
     * If both versions l
     *
     * @param Version $other
     * @return int
     */
    public function comparePreRelease(Version $other)
    {
        if (!$this->hasPreRelease() && !$other->hasPreRelease()) {
            return 0;
        }
        else if (!$this->hasPreRelease()) {
            // Current does not have a pre-release but the other does
            return 1;
        }
        else if (!$other->hasPreRelease()) {
            // Current does not have pre-release but the other does
            return -1;
        }

        return $this->compareIdentifierSequences($this->getPreRelease(), $other->getPreRelease());
    }

    /**
     * Compares two identifier sequences
     *
     * @param array $current
     * @param array $other
     * @return int
     */
    protected function compareIdentifierSequences(array $current, array $other)
    {

        $sizeCurrent = count($current);
        $sizeOther = count($other);

        $size = min($sizeCurrent, $sizeOther);

        for ($index = 0; $index < $size; $index++) {
            $cmp = $this->compareIdentifiers($current[$index], $other[$index]);

            if ($cmp > 0 || $cmp < 0) {
                // The first difference found wins and we return it as the result
                return $cmp;
            }
        }

        // If both sequences are the same, but one is longer than the other and we give precedence to the longer
        if ($sizeCurrent > $sizeOther) {
            return 1;
        }
        else if ($sizeCurrent < $sizeOther) {
            return -1;
        }

        // Otherwise they are equal and we return ZERO
        return 0;
    }

    /**
     * Compares two identifiers.
     *
     * Numeric identifiers are compared as integers and ASCII identifiers are compared lexicographically.
     *
     * @param $identifierA
     * @param $identifierB
     * @return int 1 if A > B, -1 if A < B and 0 if A == B
     */
    private function compareIdentifiers($identifierA, $identifierB)
    {
        if (is_numeric($identifierA) && is_numeric($identifierB)) {
            $identifierA = Ints::castInt($identifierA);
            $identifierB = Ints::castInt($identifierB);

            if ($identifierA > $identifierB) {
                return 1;
            }
            else if ($identifierA < $identifierB) {
                return -1;
            }
            else {
                return 0;
            }
        }
        else {
            return strcmp($identifierA, $identifierB);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * Generates the version as an string
     *
     * @return string
     */
    public function toString()
    {
        $version = sprintf("%d.%d.%d", $this->getMajor(), $this->getMinor(), $this->getPatch());

        if ($this->hasPreRelease()) {
            $version .= '-' . $this->getPreReleaseAsString();
        }

        if ($this->hasBuildMeta()) {
            $version .= '+' . $this->getBuildMetaAsString();
        }

        return $version;
    }

    /**
     * Converts the version to an integer created from the current version values.
     *
     * Two versions that differ in either the major, minor or path components will always have different results.
     *
     * The same version will have a different value if the pre-realease and build-meta are set or not. The concrete value
     * of those strings does not affect the result, only if they are set or not.
     *
     * The returned integer cannot be converted back into the original version as the pre-release and build-meta strings
     * are lost.
     *
     * Returned integers are not guaranteed not keep the ordering criteria of the versions. So if given
     * versionA > versionB the order between the integers of each version is undefined.
     *
     * @return int
     */
    public function toInt()
    {
        $result = pow(11, $this->getMajor())
            * pow(7, $this->getMinor())
            * pow(5, $this->getPatch());

        if ($this->hasPreRelease()) {
            $result *= 3;
        }
        else {
            $result *= pow(3, 2);
        }

        if ($this->hasBuildMeta()) {
            $result *= 2;
        }
        else {
            $result *= pow(2, 2);
        }

        return $result;
    }

    /**
     * @param string|array|Traversable $version
     * @return Version
     */
    public static function parse($version)
    {
        /** @var Version $result */
        $result = self::instance();

        if (empty($version)) {
            return $result;
        }

        if (is_string($version)) {
            $result = Version::parseString($version);
        }
        else if (Types::isIterable($version)) {
            $result = Version::parseIterable($version);
        }
        else {
            throw new InvalidTypeException("string, array, \\Traversable", Types::getTypeName($version));
        }

        return $result;
    }

    /**
     * Parses the version string and returns and object that represents it
     *
     * If the input string is empty will return a 0.0.0 version.
     *
     * @param string $versionString
     * @return Version
     */
    public static function parseString($versionString)
    {
        /** @var Version $version */
        $version = self::instance();

        if (empty($versionString)) {
            return $version;
        }

        $hyphenPos = strpos($versionString, '-');
        $hasMoreHyphens = $hyphenPos !== false && strpos($versionString, '-', $hyphenPos + 1);
        $plusSignPos = strpos($versionString, '+');
        $hasMorePlusSigns = $plusSignPos !== false && strpos($versionString, '+', $plusSignPos + 1);

        if ($hasMoreHyphens && $hasMorePlusSigns) {
            throw new InvalidFormatException("Invalid version string. Contains more than one plus sign and one hyphen when only one of each is allowed");
        }
        else if ($hasMoreHyphens) {
            throw new InvalidFormatException("Invalid version string. Contains more than one hyphen and only one is allowed");
        }
        else if ($hasMorePlusSigns) {
            throw new InvalidFormatException("Invalid version string. Contains more than one plus sign and only one is allowed");
        }

        if ($plusSignPos !== false && $hyphenPos !== false && $plusSignPos < $hyphenPos) {
            throw new InvalidFormatException("Invalid version string. Build metadata delimiter `+` found before pre-release delimiter `-`");
        }

        // There is always at least the part of numeric version

        if ($hyphenPos !== false) {
            $numericParts = substr($versionString, 0, $hyphenPos);
        }
        else if ($plusSignPos !== false) {
            $numericParts = substr($versionString, 0, $plusSignPos);
        }
        else {
            $numericParts = $versionString;
        }

        $numericParts = explode('.', $numericParts);

        if (count($numericParts) != 3 || $numericParts[0] === "" || $numericParts[1] === "" || $numericParts[2] === "") {
            throw new InvalidFormatException("Invalid version string. The version format `x.y.z` is required");
        }

        $version->setMajor($numericParts[0]);
        $version->setMinor($numericParts[1]);
        $version->setPatch($numericParts[2]);

        // The reminder of elements, if any are the pre-release and build metadata portions

        if ($hyphenPos !== false) {
            $preRelease = substr(
                $versionString,
                $hyphenPos + 1,
                $plusSignPos !== false ? $plusSignPos - $hyphenPos - 1 : null);
            $version->setPreRelease($preRelease);
        }

        if ($plusSignPos !== false) {
            $buildMeta = substr($versionString, $plusSignPos + 1);
            $version->setBuildMeta($buildMeta);
        }

        return $version;
    }

    /**
     * Parses an iterable with all the version components
     *
     * The iterable must have between 3 and 5 elements. The first three elements must be the major, minor and path
     * numbers. The last two are the pre-release and the build metadata respectively.
     *
     * Pre-release and/or build metadata items can be empty.
     *
     * @param array|Traversable $versionArray
     * @return Version
     */
    public static function parseIterable($versionArray)
    {
        /** @var Version $version */
        $version = self::instance();

        if (empty($versionArray)) {
            return $version;
        }

        $arr = Arrays::toArray($versionArray);
        $size = count($arr);

        if ($size < 3) {
            throw new InvalidFormatException('Expected at least three elements with major, minor and path numbers.');
        }

        if ($size > 5) {
            throw new InvalidFormatException('Maximum number of elements is 5. Passed $size elements.');
        }

        if (!is_numeric($arr[0]) || !is_numeric($arr[1]) || !is_numeric($arr[2])) {
            throw new InvalidFormatException("The first three elements must be integers");
        }

        $version->setMajor($arr[0]);
        $version->setMinor($arr[1]);
        $version->setPatch($arr[2]);

        if ($size > 3) {
            $version->setPreRelease($arr[3]);
        }

        if ($size == 5) {
            $version->setBuildMeta($arr[4]);
        }

        return $version;
    }
}
