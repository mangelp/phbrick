<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class InvalidKeyException
 *
 * A key was used and it was not valid due to either the format of the value, the type or because the key is used as
 * an array index or hashtable key and it does not exists.
 *
 * @package phbrick\exceptions
 */
class InvalidKeyException extends RuntimeException
{

}
