<?php

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class InvalidCallbackException
 *
 * A non-callable was provided where a callable was expected or the callable throwed an unhandled exception while
 * being called.
 *
 * @package phbrick\exceptions
 */
class InvalidCallbackException extends RuntimeException
{

}
