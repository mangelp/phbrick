<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class TypeException
 *
 * A non conforming type was provided where a different type was required or the provided type does not match expected
 * restrictions.
 *
 * @package phbrick\exceptions
 */
class TypeException extends RuntimeException
{

}
