<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class RenderableException
 *
 * A non-renderable value was provided where a renderable one was expected.
 *
 * @package phbrick\exceptions
 */
class RenderableException extends RuntimeException
{

}
