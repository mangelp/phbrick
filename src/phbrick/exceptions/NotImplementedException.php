<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class NotImplementedException
 *
 * Represents that an operation or feature that is not yet implemented was executed
 *
 * @package phbrick\exceptions
 */
class NotImplementedException extends RuntimeException
{

}
