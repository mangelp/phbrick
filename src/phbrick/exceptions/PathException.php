<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class PathException
 *
 * A path of the wrong type was provided when another type of path was expected.
 *
 * IE: A directory path is provided when a file path is required and vice-versa.
 *
 * @package phbrick\exceptions
 */
class PathException extends RuntimeException
{

}
