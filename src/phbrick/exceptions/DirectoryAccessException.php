<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\exceptions;

use RuntimeException;

/**
 * Class DirectoryAccessException
 *
 * Access to a directory was not possible due to the directory being non-accessible by security policy or simply because
 * the directory or some part of the path does not exists.
 *
 * @package phbrick\exceptions
 */
class DirectoryAccessException extends RuntimeException
{

}
