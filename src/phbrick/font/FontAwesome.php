<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use mangelp\impdesign\designer\properties\FontProperty;
use phbrick\BaseStrictClass;
use phbrick\graphics\RgbaColor;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Ints;

/**
 * Class FontAwesome
 *
 * Models a single FontAwesome icon.
 *
 * @package phbrick\font
 */
class FontAwesome extends BaseStrictClass
{
    use SafeStringRenderTrait;

    /**
     * Font awesome icon code point
     * @var int
     */
    private $value = FontAwesomeIcons::FA_FONT_AWESOME;
    /**
     * Font size in points
     * @var int
     */
    private $size = 10;
    /**
     * @var RgbaColor
     */
    private $color = null;

    /**
     * Gets the FontAwesome icon used
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the FontAwesome icon to use
     * @param int $value
     */
    public function setValue($value)
    {
        FontAwesomeIcons::assertValue($value);
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Font size in points
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = Ints::castInt($size);
    }

    /**
     * @return RGBColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param RGBColor|int|string|array $color
     */
    public function setColor($color)
    {
        $this->color = RGBColor::parse($color);
    }

    public function __construct($icon = null, $size = null, $color = null)
    {
        if ($icon !== null) {
            $this->setValue($icon);
        }

        if ($size !== null) {
            $this->setSize($size);
        }

        $this->color = RGBColor::parse($color);
    }

    public function getName()
    {
        $name = FontAwesomeIcons::nameOf($this->value);

        $name = str_replace('_', '-', strtolower(substr($name, 3)));

        return $name;
    }

    /**
     * Returns the constant name for the icon
     * @return string
     */
    public function toString()
    {
        return FontAwesomeIcons::nameOf($this->value);
    }

    /**
     * Returns an string that is usable to draw the icon over an image.
     * @return string
     */
    public function toDrawableString()
    {
        return sprintf('&#x%x;', $this->value);
    }

    /**
     * Returns an string that is usable to set the content of the icon by css styles.
     * @return string
     */
    public function toStyleString()
    {
        return sprintf('\%x', $this->value);
    }

    /**
     * Returns a new instance of FontProperty pointing to the font.
     *
     * @return FontProperty
     */
    public function getFont()
    {
        $prop = new FontProperty(
            FontManager::FONT_AWESOME,
            $this->size,
            $this->color,
            false, false, false);
        return $prop;
    }
}
