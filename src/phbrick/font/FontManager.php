<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use InvalidArgumentException;
use phbrick\BaseStrictClass;

/**
 * Class FontManager
 *
 * Manages fonts providing access to them by virtual URLs. Delegates the management on special-case management classes.
 *
 * @package phbrick\font
 */
class FontManager extends BaseStrictClass
{
    const FONT_AWESOME = 'font-awesome';
    const FONT_ROBOTO = 'roboto';

    /**
     * @var LocalFontManager
     */
    private $local = null;

    public function __construct()
    {
        $this->local = new LocalFontManager();
    }

    /**
     * Gets the default font size in points.
     *
     * @return float
     */
    public static function getDefaultFontSize()
    {
        return 12;
    }

    public function getFontList()
    {
        return $this->local->getFontList();
    }

    /**
     * Retrieves the full path to the given font
     *
     * @param $font
     * @param $bold
     * @param $italic
     * @return string
     */
    public function getFontFile($font, $bold, $italic)
    {
        $fontFile = $this->local->getFontFile($font, $bold, $italic);

        if (!$fontFile) {
            throw new InvalidArgumentException("Font not found: $font" . ($bold ? " bold" : "") . ($italic ? " italic" : ""));
        }

        return $fontFile;
    }

    /**
     * Gets an instance of the class that abstract usage of FontAwesome iconic font
     * @return FontAwesome
     */
    public function getFontAwesome()
    {
        return new FontAwesome(null, null, null);
    }
}
