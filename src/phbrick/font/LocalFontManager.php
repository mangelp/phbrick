<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use Imagine\Exception\InvalidArgumentException;
use mangelp\impdesign\Extension;
use phbrick\exceptions\FileAccessException;
use phbrick\exceptions\IllegalStateException;
use phbrick\string\Strings;

/**
 * Class LocalFontManager
 *
 * Manages fonts stored locally in the server filesystem.
 *
 * @package phbrick\font
 */
class LocalFontManager
{
    /**
     * @var string
     */
    private $fontsDirectory = '';
    /**
     * @var array
     */
    private $fonts = [];
    /**
     * @var bool
     */
    private $fontsLoaded = false;
    /**
     * @var string
     */
    private $defaultFont = '';

    /**
     * @return string
     */
    public function getFontsDirectory()
    {
        return $this->fontsDirectory;
    }

    /**
     * @param string $fontsDirectory
     */
    public function setFontsDirectory($fontsDirectory)
    {
        $this->fontsDirectory = $fontsDirectory;
    }

    /**
     * @return array
     */
    public function getFonts()
    {
        return $this->fonts;
    }

    /**
     * @param array $fonts
     */
    public function setFonts($fonts)
    {
        $this->fonts = $fonts;
    }

    /**
     * @return bool
     */
    public function isFontsLoaded()
    {
        return $this->fontsLoaded;
    }

    /**
     * @return string
     */
    public function getDefaultFont()
    {
        return $this->defaultFont;
    }

    /**
     * @param string $defaultFont
     */
    public function setDefaultFont($defaultFont)
    {
        if (!isset($this->fonts[$defaultFont])) {
            throw new InvalidArgumentException("Font not defined: $defaultFont");
        }

        $this->defaultFont = $defaultFont;
    }

    /**
     * Checks if the font exists
     *
     * @param $fontName
     * @return bool
     */
    public function existsFont($fontName)
    {
        str_replace(' ', '-', strtolower($fontName));
        return isset($this->fonts[$fontName]);
    }

    /**
     * LocalFontManager constructor initializes the fonts directory with the given folder or uses the extension uri
     * helper to retrieve the virtual path "fonts:/".
     *
     * @param string|null $fontsDirectory The full path to fonts folder or null to use the default one
     * @throws IllegalStateException When the fonts folder is not defined, it does not exists or it does not contains a file named fonts.php
     */
    public function __construct($fontsDirectory = null)
    {

        if (Strings::isNullOrEmpty($fontsDirectory)) {
            $extension = Extension::getInstance();
            // TODO: Use the virtual URI resolver to resolve "fonts:/"
            // Meanwhile we resolve it directly with the well-known path "assets/fonts/"
            $uriHelper = $extension->getUriHelper();
            $fontsDirectory = $uriHelper->getFsPath('assets/fonts/');
        }

        if (Strings::isNullOrEmpty($fontsDirectory)) {
            throw new IllegalStateException("Cannot initialize the local font manager without the fonts folder");
        }

        $this->setFontsDirectory($fontsDirectory);

        if (!is_file($this->fontsDirectory . '/fonts.php')) {
            error_log(__CLASS__ . ': missing font list file at ' . $this->fontsDirectory);
        }
    }

    /**
     * Returns an item from an array
     *
     * @param array $array
     * @param $key
     * @param null $defaultValue
     * @return mixed|null
     */
    private function getArrayItem(array $array, $key, $defaultValue = null)
    {
        if (isset($array[$key]) && !empty($array[$key])) {
            return $array[$key];
        }

        return $defaultValue;
    }

    private function getList($value)
    {
        $result = [];

        if (is_string($value)) {
            $parts = explode(',', $value);

            foreach ($parts as $part) {
                $result[] = trim($part);
            }

        }
        else if (is_array($value)) {
            foreach ($value as $subValue) {

                $sublist = $this->getList($subValue);

                foreach ($sublist as $subListItem) {
                    $result[] = $subListItem;
                }
            }
        }

        return $result;
    }

    /**
     * Loads all the fonts available from a fonts.php file in the fonts folder.
     * @return bool true if the file is loaded or false if not
     * @throws IllegalStateException if the fonts have been already loaded or the current file has an invalid format
     */
    public function loadFonts()
    {
        if ($this->fontsLoaded) {
            throw new IllegalStateException("Fonts have been already loaded");
        }

        ob_start();
        include($this->fontsDirectory . '/fonts.php');
        ob_end_clean();

        if (!isset($fonts) || !is_array($fonts)) {
            return false;
        }

        $extension = Extension::getInstance();
        $resolver = $extension->getUriHelper();
        $defaultFontName = $this->getArrayItem($fonts, 'default-font');
        $fontList = $this->getArrayItem($fonts, 'fonts', []);

        if (empty($fontList)) {
            throw new IllegalStateException("Missing key 'fonts' or empty value");
        }

        foreach ($fontList as $fontName => $fontData) {
            $fontName = $this->sanitizeFontName($fontName);
            $fontInfo = new FontInfo($fontName);
            $this->fonts[$fontName] = $fontInfo;

            if (isset($fontData['family'])) {
                $fontInfo->setFamily($fontData['family']);
            }

            if (isset($fontData['type'])) {
                $fontInfo->setType($fontData['type']);
            }

            if (isset($fontData['formats'])) {
                $formats = $this->getList($fontData['formats']);
                $fontInfo->setFormats($formats);
            }

            if (isset($fontData['styles'])) {
                $styles = $this->getList($fontData['styles']);
                $fontInfo->setStyles($styles);
            }
        }

        $this->setDefaultFont($defaultFontName);
        $this->fontsLoaded = true;
        return true;
    }

    /**
     * Removes unwanted trailing characters from a font name.
     *
     * @param $fontName
     * @return string
     */
    protected function sanitizeFontName($fontName)
    {
        $fontName = trim($fontName, "\r\n\t #.:\\/");

        $fontName = str_replace(' ', '-', strtolower($fontName));

        return $fontName;
    }

    /**
     * Gets the information for a given font subtype by its name and two flags to select the bold, the italic or the
     * bold-italic variation (if it exists).
     *
     * @param $font
     * @param bool $bold
     * @param bool $italic
     * @return array
     */
    protected function getFontName($font, $bold = false, $italic = false)
    {
        $font = $this->sanitizeFontName($font);
        $fontSubType = 'regular';

        if ($bold && $italic) {
            $fontSubType = 'bold-italic';
        }
        else if ($bold) {
            $fontSubType = 'bold';
        }
        else if ($italic) {
            $fontSubType = 'italic';
        }

        return [
            'name' => $font,
            'type' => $fontSubType,
        ];
    }

    /**
     * @param $font
     * @param null|bool $bold
     * @param null|bool $italic
     * @return FontInfo|null
     */
    public function getFontInfo($font, $bold = null, $italic = null)
    {
        if (!$this->fontsLoaded) {
            $this->loadFonts();
        }

        $fontName = $this->sanitizeFontName($font);

        if (!isset($this->fonts[$fontName])) {
            return null;
        }

        /** @var FontInfo $fontInfo */
        $fontInfo = $this->fonts[$fontName];

        if ($fontInfo->getType() == FontTypes::TEXT) {
            $fontStyle = FontStyles::getFontStyle($bold, $italic);

            if (!$fontInfo->hasStyle($fontStyle)) {
                return null;
            }
        }

        return $fontInfo;
    }

    /**
     * @param $font
     * @param bool $bold
     * @param bool $italic
     * @param string $format
     * @return string
     * @throws FileAccessException
     * @throws IllegalStateException
     */
    public function getFontFile($font, $bold = false, $italic = false, $format = FontFormats::TTF)
    {

        $fontInfo = $this->getFontInfo($font, $bold, $italic);

        if (!$fontInfo) {
            throw new \InvalidArgumentException("Font not defined: $font");
        }

        $fontStyle = FontStyles::getFontStyle($bold, $italic);
        $fontFile = $fontInfo->getFile($fontStyle, $format);

        if (!$fontFile) {
            throw new IllegalStateException("No such font style " . FontStyles::nameOf($fontStyle) . " or format " . $format . " for family " . $fontInfo->getFamily());
        }

        $fullFontFilePath = $this->getFontsDirectory() . '/' . $fontFile;

        if (!is_file($fullFontFilePath)) {
            throw new FileAccessException("Font file does not exists or is not readable: " . $fontFile);
        }

        return $fullFontFilePath;
    }

    /**
     * @param $font
     * @param bool $bold
     * @param bool $italic
     * @return bool
     */
    public function hasFont($font, $bold = false, $italic = false)
    {

        if (!$this->fontsLoaded) {
            $this->loadFonts();
        }

        $fontName = $this->getFontName($font, $bold, $italic);
        $name = $fontName['name'];
        $type = $fontName['type'];

        if (!isset($this->fonts[$name]) || !isset($this->fonts[$name][$type])) {
            return false;
        }

        return true;
    }
}
