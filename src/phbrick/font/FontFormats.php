<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use phbrick\EnumerationClass;

/**
 * Class FontFormats
 * @package phbrick\font
 */
class FontFormats extends EnumerationClass
{
    const WOFF = 'woff';
    const TTF = 'ttf';
    const SVG = 'svg';
    const EOT = 'eot';
    const OTF = 'otf';
}
