<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use phbrick\EnumerationClass;
use phbrick\types\Ints;
use phbrick\types\Types;

/**
 * Class FontStyles
 *
 * Enumeration of possible different font styles.
 *
 * The values are flags power of two that can be ORed together.
 *
 * @package phbrick\font
 */
class FontStyles extends EnumerationClass
{
    /**
     * The font does not have different version for each style
     */
    const NONE = 0;
    /**
     * The font has a regular style
     */
    const REGULAR = 2;
    /**
     * The font has a bold style
     */
    const BOLD = 4;
    /**
     * The font has an italic style
     */
    const ITALIC = 8;
    /**
     * The font has a bold-italic style
     */
    const BOLD_ITALIC = 16;

    protected static function getEnumerationValueType()
    {
        return Types::TYPE_INT;
    }

    protected static function isConstantNameCaseSensitive()
    {
        return false;
    }

    /**
     * Converts a comma-separated string of constant names or an integer made up of several constants ORed together into
     * an array with a constant value on each position.
     *
     * @param $styles
     * @return array
     */
    public static function toFlags($styles)
    {
        $values = [];

        if (is_string($styles) && !is_numeric($styles)) {
            $parts = explode(',', $styles);

            foreach ($parts as $part) {
                $part = trim($part);
                if (is_numeric($part)) {
                    array_merge($values, FontStyles::toFlags($part));
                }
                else {
                    $values[] = FontStyles::valueOf(str_replace('-', '_', $part));
                }
            }
        }
        else if (is_numeric($styles)) {
            $styles = Ints::castInt($styles);

            foreach (FontStyles::getConstants() as $name => $value) {
                if ($styles & $value === $value) {
                    $value[] = $value;
                }
            }
        }

        return $values;
    }

    /**
     * Gets if all the checked flags are in the source flags
     *
     * @param $sourceFlags Dash separated list of flag names or numeric ORed flags to check against
     * @param $checkFlags Dash separated list of flag names or numeric ORed flags to check existence
     * @return bool true if all the flags checked are in the source flags or false otherwise
     */
    public static function isFlagsSet($sourceFlags, $checkFlags)
    {
        $flagList = FontStyles::toFlags($sourceFlags);
        $flagCheck = FontStyles::toFlags($checkFlags);

        foreach ($flagCheck as $flag) {
            if (!in_array($flag, $flagList, true)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the appropriate font style constant for the given flags
     * @param $bold
     * @param $italic
     * @return int
     */
    public static function getFontStyle($bold, $italic)
    {
        if (!$bold && !$italic) {
            return FontStyles::REGULAR;
        }
        else if ($bold && !$italic) {
            return FontStyles::BOLD;
        }
        else if (!$bold && $italic) {
            return FontStyles::ITALIC;
        }
        else if ($bold && $italic) {
            return FontStyles::BOLD_ITALIC;
        }

        return FontStyles::NONE;
    }
}
