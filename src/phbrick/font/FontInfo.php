<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\font;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\collection\Arrays;
use phbrick\string\Strings;
use phbrick\types\Types;
use Traversable;

/**
 * Class FontInfo models font information.
 *
 * Encapsulates information for a given font with a given type. For example Roboto-Regular.ttf or Roboto-Bold.ttf.
 *
 * All fonts managed must be stored on disk as is not supported to use a font from an URL or stored in database.
 *
 * @package phbrick\font
 */
class FontInfo extends BaseStrictClass
{
    /**
     * @param FontInfo $fontInfo
     * @return FontInfo
     */
    public static function castFontInfo(FontInfo $fontInfo)
    {
        return $fontInfo;
    }

    /**
     * Internal font name used to load the font and retrieve it
     *
     * @var string
     */
    private $name = '';

    /**
     * Font family display name
     * @var string
     */
    private $family = '';

    /**
     * Font styles flag
     * @see FontStyles enumeration
     * @var int
     */
    private $styles = FontStyles::NONE;

    /**
     * Type of font family. Either iconic or text
     *
     * @var string
     */
    private $type = FontTypes::TEXT;

    /**
     * @var array
     */
    private $formats = [];
    /**
     * @var array
     */
    private $files = [];

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param string $family
     */
    public function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    protected function getFontKey($fontStyle, $fontFormat)
    {
        $key = $fontFormat;

        if ($this->type == FontTypes::TEXT) {
            if (is_numeric($fontStyle)) {
                $fontStyle = FontStyles::nameOf($fontStyle);
            }

            $key = $fontStyle . '_' . $key;
        }

        return strtolower($key);
    }

    /**
     * Sets the relative or absolute file path to use for a given font style and format instead of the generated file
     * pattern.
     *
     * @param int $fontStyle Single font style
     * @param string $fontFormat Single font format
     * @param string $file File to set
     */
    public function setFile($fontStyle, $fontFormat, $file)
    {
        if (is_numeric($fontStyle)) {
            FontStyles::assertValue($fontStyle);
        }
        else if (is_string($fontStyle)) {
            FontStyles::assertName($fontStyle);
        }

        FontFormats::assertValue($fontFormat);

        $this->files[$this->getFontKey($fontStyle, $fontFormat)] = $file;
    }

    /**
     * Returns the base path (folder and file name) of the font file.
     *
     * The caller must obtain the full path prepending the full path to the fonts folder full path.
     *
     * @param $fontStyle
     * @param $fontFormat
     * @return string
     */
    public function getFile($fontStyle, $fontFormat)
    {
        FontStyles::assertValue($fontStyle);
        FontFormats::assertValue($fontFormat);

        $suffix = '';

        if ($this->type == FontTypes::TEXT) {
            $key = $this->getFontKey($fontStyle, $fontFormat);

            if (isset($this->files[$key])) {
                return $this->files[$key];
            }

            $suffix = '-' . str_replace('_', '-', strtolower(FontStyles::nameOf($fontStyle)));

        }
        else if ($this->type == FontTypes::ICONIC) {
            // Iconic fonts does not have variants per style
            if (isset($this->files[$fontFormat])) {
                return $this->files[$fontFormat];
            }
        }

        return $this->name . '/' . $this->name . $suffix . '.' . strtolower($fontFormat);
    }

    /**
     * @return int
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * Checks if a single style is set
     *
     * @param $style
     * @return int
     */
    public function hasStyle($style)
    {
        FontStyles::assertValue($style);

        return ($this->styles & $style) === $style;
    }

    /**
     * Checks if the given font has the given set of styles.
     *
     * You can pass several styles by ORing the constants for each style
     *
     * @param array|Traversable|string|int $styles Dash-separated list of styles or integer with the flags
     * @return bool True if all the style flag are set or false if one it is not set.
     */
    public function hasStyles($styles)
    {
        if (Types::isIterable($styles)) {
            /** @var Traversable $styles */
            foreach ($styles as $style) {
                if (!$this->hasStyle($style)) {
                    return false;
                }
            }
        }
        else if (is_string($styles) || is_numeric($styles)) {

            $flags = FontStyles::toFlags($styles);

            foreach ($flags as $flag) {
                if ($this->styles & $flag !== $flag) {
                    return false;
                }
            }
        }
        else {
            throw new InvalidArgumentException("Invalid styles");
        }

        return true;
    }

    /**
     * Sets the style(s) flag.
     *
     * @param $styles
     */
    public function setStyles($styles)
    {
        $this->styles = 0;
        $this->addStyles($styles);
    }

    /**
     * Sets the style(s) flag.
     *
     * @param $style
     */
    public function setStyle($style)
    {
        if (is_string($style) && !is_numeric($style)) {
            $style = strtoupper(str_replace('-', '_', $style));
            $style = FontStyles::valueOf($style);
        }

        FontStyles::assertValue($style);

        $this->styles |= $style;
    }

    /**
     * @param string|numeric|array|Traversable $styles
     */
    public function addStyles($styles)
    {
        if (Types::isIterable($styles)) {

            foreach ($styles as $style) {
                $this->setStyle($style);
            }
        }
        else if (is_numeric($styles) || is_string($styles)) {
            $flags = FontStyles::toFlags($styles);

            $this->addStyles($flags);
        }
        else {
            throw new InvalidArgumentException("Invalid styles");
        }
    }

    /**
     * @return array
     */
    public function getFormats()
    {
        return $this->formats;
    }

    /**
     * Sets all the formats overwriting the previous ones.
     *
     * @param array $formats
     */
    public function setFormats($formats)
    {
        $this->formats = [];
        $this->addFormats($formats);
    }

    /**
     * Adds a new format checking that is not already added
     *
     * @param $format
     * @return bool true if the format is added or false if it is already added
     */
    public function addFormat($format)
    {
        $format = FontFormats::assertValue($format);

        if (!in_array($format, $this->formats)) {
            $this->formats[] = $format;
            return true;
        }

        return false;
    }

    /**
     * Adds all formats to existing ones
     *
     * @param array|Traversable $formats
     */
    public function addFormats($formats)
    {
        Types::assertIterable($formats);

        foreach ($formats as $format) {
            $this->addFormat($format);
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        FontTypes::assertValue($type);

        $this->type = $type;
    }

    /**
     * FontInfo constructor.
     *
     * Initializes the font family name by capitalizing the font name after splitting it by dashes.
     * Ej: font-awesome font name initializes the font family to FontAwesome.
     *
     * @param string|null $name
     * @param string|null $type
     * @param int $styles
     * @param array $formats
     */
    public function __construct($name = null, $type = null, $styles = 0, $formats = [])
    {

        if (!Strings::isNullOrEmpty($name)) {
            $this->setName($name);

            $parts = explode('-', $name);
            $parts = array_map('ucfirst', $parts);
            $this->setFamily(implode(' ', $parts));
        }

        if (!Strings::isNullOrEmpty($type)) {
            $this->setType($type);
        }

        if ($styles !== null && $styles != 0) {
            $this->setStyles($styles);
        }

        if (!Arrays::isEmpty($formats)) {
            $this->setFormats($formats);
        }
    }
}
