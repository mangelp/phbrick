<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf;

use phbrick\BaseStrictClass;
use RuntimeException;

/**
 * Stores measurements as a CSV file
 */
class MeasureCsvStorage extends BaseStrictClass
{
    /**
     * @var Measurements
     */
    private $measurements = null;

    /**
     * Gets the measurements
     * @return Measurements
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * Sets the measurements
     * @param Measurements $measurements
     */
    public function setMeasurements(Measurements $measurements)
    {
        $this->measurements = $measurements;
    }

    /**
     * @var string
     */
    private $delimiter = ';';

    /**
     * Gets the value delimiter. Defaults to ;
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * Sets the value delimiter. Defaults to ;
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @var string
     */
    private $enclosure = '"';

    /**
     * Gets the string enclosure. Defaults to "
     * @return string
     */
    public function getEnclosure()
    {
        return $this->enclosure;
    }

    /**
     * Sets the string enclosure. Defaults to "
     * @param string $enclosure
     */
    public function setEnclosure($enclosure)
    {
        $this->enclosure = $enclosure;
    }

    /**
     * @var string
     */
    private $file = null;

    /**
     * Gets the file to store the csv data
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets the file to store the csv data
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @var bool
     */
    private $appendToFile = false;

    /**
     * Gets if the contents must be appended or the file will be overwritten
     * @return bool
     */
    public function isAppendToFile()
    {
        return $this->appendToFile;
    }

    /**
     * Sets if the contents must be appended or the file will be overwritten
     * @param bool $appendToFile
     */
    public function setAppendToFile($appendToFile)
    {
        $this->appendToFile = (bool)$appendToFile;
    }

    public function __construct(Measurements $measurements = null, $delimiter = null, $enclosure = null)
    {
        $this->setMeasurements($measurements);

        if ($delimiter) {
            $this->setDelimiter($delimiter);
        }

        if ($enclosure) {
            $this->setEnclosure($enclosure);
        }
    }

    public function save($append = null)
    {
        $exists = file_exists($this->file);

        if (!is_bool($append)) {
            $append = $this->appendToFile;
        }

        $fd = fopen($this->file, $append ? 'a' : 'w');

        if (!$fd) {
            throw new RuntimeException('File not found: ' . $this->file);
        }

        $abort = false;

        if (!$append || !$exists) {
            // When not appending or file didn't existed write headers to the file
            $wrote = fputcsv(
                $fd,
                [
                    'Date',
                    'PHP Ver.',
                    'Test',
                    'Num',
                    'Mem',
                    'Time'],
                $this->getDelimiter(),
                $this->getEnclosure());

            if (!$wrote) {
                $abort = true;
            }
        }

        $groups = $this->measurements->getGroups();
        $date = date('Ymd H:i:s');

        foreach ($groups as $group) {
            if ($abort) {
                break;
            }

            $lineCount = 0;

            foreach ($group['measures'] as $measurement) {
                $measurement = Measurement::cast($measurement);
                ++$lineCount;

                $wrote = fputcsv(
                    $fd,
                    [
                        $date,
                        PHP_VERSION . '(' . PHP_VERSION_ID . ')',
                        $group['label'],
                        $lineCount,
                        $measurement->getMemory(),
                        $measurement->getTime()],
                    $this->getDelimiter(),
                    $this->getEnclosure());

                if (!$wrote) {
                    $abort = true;
                    break;
                }
            }
        }

        fclose($fd);
    }
}
