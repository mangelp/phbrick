<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\perf;

/**
 * Unit-specific formatting utility
 */
class UnitFormats
{
    public static function formatBytes($bytes, $separator = '~')
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;

        $result = [];

        if ($bytes > $megabyte) {
            $megabytes = (int)round((float)$bytes / $megabyte, 0);
            $result[] = sprintf('%dMB', $megabytes);
            $bytes -= $megabytes * $megabyte;
        }

        if ($bytes > $kilobyte) {
            $kilobytes = (int)round((float)$bytes / $kilobyte, 0);
            $result[] = sprintf('%dKB', $kilobytes);
            $bytes -= $kilobytes * $kilobyte;
        }

        if ($bytes >= 0) {
            $result[] = sprintf('%dB', $bytes);
        }

        return implode($separator, $result);
    }

    public static function formatMicroseconds($microseconds, $separator = '~')
    {
        $second = 1000000;
        $minute = $second * 60;
        $hour = $minute * 60;

        $result = [];

        if ($microseconds > $hour) {
            $hours = (int)round((float)$microseconds / $hour, 0);
            $result[] = sprintf('%dh', $hours);
            $microseconds -= $hours * $hour;
        }

        if ($microseconds > $minute) {
            $minutes = (int)round((float)$microseconds / $minute, 0);
            $result[] = sprintf('%dm', $minutes);
            $microseconds -= $minutes * $minute;
        }

        if ($microseconds > $second) {
            $seconds = (int)round((float)$microseconds / $second, 0);
            $result[] = sprintf('%ds', $seconds);
            $microseconds -= $seconds * $second;
        }

        if ($microseconds >= 0) {
            $result[] = sprintf('%du', $microseconds);
        }

        return implode($separator, $result);
    }
}
