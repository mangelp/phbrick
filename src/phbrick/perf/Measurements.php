<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf;

use Exception;
use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\types\Bools;
use RuntimeException;

/**
 * Generates and collects measurements for concrete groups to perform further analysis of collected
 * data.
 *
 * Garbage collection can be forced before doing the measurement and disabled during data acquisition
 * to exclude its effects from the timings.
 *
 */
class Measurements extends BaseStrictClass
{
    /**
     * @var array
     */
    private $groups = [];

    /**
     * Gets measure groups
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Retrieves the stored data for a group
     * @param string $group
     * @return mixed
     */
    public function getGroup($group)
    {
        self::assertGroup($group);
        return $this->groups[$group];
    }

    /**
     * Adds a measure group
     * @param $group
     * @param null $label
     * @internal param array $groups
     */
    public function addGroup($group, $label = null)
    {
        if (!isset($this->groups[$group])) {
            $this->groups[$group] = [
                'label'    => $label,
                'measures' => [],
            ];
        }
    }

    /**
     * Remove a measure group and all the collected data for it
     *
     * @param string $group
     */
    public function removeGroup($group)
    {
        if (isset($this->groups[$group])) {
            unset ($this->groups[$group]);
        }
    }

    /**
     * Gets if the given group exists.
     * @param string $group
     * @return bool
     */
    public function hasGroup($group)
    {
        return isset($this->groups[$group]);
    }

    /**
     * @var bool
     */
    private $realMemoryUse = false;

    /**
     * Gets if the memory must be measured as real use or emalloc use
     * @return bool
     */
    public function isRealMemoryUse()
    {
        return $this->realMemoryUse;
    }

    /**
     * Flag to force a GC cycle before taking a measure and disable GC until done.
     * @var bool
     */
    private $forceGCCycle = false;

    /**
     * Gets if a GC cycle must be forced before creating a measure
     * @return bool
     */
    public function isForceGCCycle()
    {
        return $this->forceGCCycle;
    }

    /**
     * Sets if a GC cycle must be forced before creating a measure
     *
     * This also disables the GC to run until end() is called.
     * @param bool $forceGCCycle If true forces a garbage collection cycle between single test repetitions
     */
    public function setForceGCCycle($forceGCCycle)
    {
        $this->forceGCCycle = Bools::castBool($forceGCCycle);
    }

    /**
     * Microseconds to sleep between measurements
     * @var int
     */
    private $interMeasureSleep = 2500;

    /**
     * Gets the time in microseconds to sleep between measurements
     * @return int
     */
    public function getInterMeasureSleep()
    {
        return $this->interMeasureSleep;
    }

    /**
     * Sets the time in microseconds to sleep between measurements
     * @param int $interMeasureSleep Microseconds
     */
    public function setInterMeasureSleep($interMeasureSleep)
    {
        $this->interMeasureSleep = $interMeasureSleep;
    }

    public function __construct($forceGCCycle = false)
    {
        $this->forceGCCycle = $forceGCCycle;
    }

    protected function assertGroup($group)
    {
        if (!isset($this->groups[$group])) {
            throw new InvalidArgumentException('Cannot measure for undefined group: ' . $group);
        }
    }

    /**
     * Creates a measure and returns it without either starting or ending it.
     *
     * @param string $group
     * @param bool $forceGCCycle Forces a GC cycle before the measure initial values are stored.
     * Defaults to false.
     * @param bool $realMemoryUse If true stores emalloc-ed memory, if false stores total PHP
     * process assigned memory. Defaults to the value set in Measurements instance
     * @return Measurement Created measurement instance
     */
    public function createMeasure($group, $forceGCCycle = null, $realMemoryUse = null)
    {
        self::assertGroup($group);

        if ($realMemoryUse === null) {
            $realMemoryUse = $this->realMemoryUse;
        }

        if ($forceGCCycle === null) {
            $forceGCCycle = false;
        }

        $measurement = new Measurement($group, (bool)(int)$realMemoryUse, (bool)(int)$forceGCCycle);
        $this->groups[$group]['measures'][] = $measurement;

        return $measurement;
    }

    /**
     * Performs the measurement over a callback.
     * If a garbage collection cycle is forced it disables garbage collection while executing the callback.
     *
     * @param $group
     * @param $executionCount
     * @param callable $callback
     * @param bool $doStartAndEnd
     * @return Measurement
     */
    public function measure($group, $executionCount, callable $callback, $doStartAndEnd = true)
    {
        $gc_enabled = gc_enabled();
        $measurement = null;

        try {
            if ($this->forceGCCycle && $gc_enabled) {
                gc_collect_cycles();
                gc_disable();
            }

            for ($i = 0; $i < $executionCount; $i++) {
                $measurement = $this->createMeasure($group, false);

                if ($doStartAndEnd) {
                    $measurement->start();
                }

                $callback($measurement, $this);

                if ($doStartAndEnd) {
                    $measurement->end();
                }

                if (!$doStartAndEnd && !$measurement->isEnded()) {
                    throw new RuntimeException(
                        'Parameter $doStartAndEnd set but measurement was unfinished for group '
                        . $group . '. Make sure that you call both start() and end()');
                }

                if ($i > 1) {
                    usleep($this->interMeasureSleep);
                }
            }

        } catch (Exception $ex) {
            throw new RuntimeException(
                'Measurement aborted by exception: ' . $ex->getMessage(), 0, $ex);
        } finally {
            if ($this->forceGCCycle && $gc_enabled) {
                gc_enable();
            }
        }

        return $measurement;
    }

    /**
     * Returns collected results for a group as an array with the given keys:
     *  + Mid time (ns)
     *  + Max time (ns)
     *  + Min time (ns)
     *  + Mid memory (bytes)
     *  + Max memory (ns)
     *  + Min memory (ns)
     *  + Measure count (int)
     * @param $group
     * @return array
     */
    public function getGroupResume($group)
    {
        $groupData = $this->getGroup($group);

        if (!$groupData) {
            throw new RuntimeException("No such group: $group");
        }

        $count = count($groupData['measures']);

        if ($count < 1) {
            throw new RuntimeException("No measures found in group: $group. " . print_r($groupData, true));
        }

        $result = [
            'label'  => $groupData['label'],
            'count'  => $count,
            'php'    => [
                'ver' => PHP_VERSION,
                'id'  => PHP_VERSION_ID,
            ],
            'time'   => [
                'mid' => 0.0,
                'max' => -1,
                'min' => PHP_INT_MAX,
            ],
            'memory' => [
                'mid' => 0.0,
                'max' => -1,
                'min' => PHP_INT_MAX,
            ],
        ];

        $names = ['time', 'memory'];

        for ($i = 0; $i < $result['count']; $i++) {
            $measurement = $groupData['measures'][$i];
            $measurement = Measurement::cast($measurement);

            foreach ($names as $name) {
                $propName = 'get' . ucfirst($name);
                $amount = $measurement->$propName();

                if ($amount > $result[$name]['max']) {
                    $result[$name]['max'] = $amount;
                }

                if ($amount < $result[$name]['min']) {
                    $result[$name]['min'] = $amount;
                }

                $result[$name]['mid'] += $amount;
            }
        }

        foreach ($names as $name) {
            $mid = $result[$name]['mid'];
            $mid = $mid / $count;
            $result[$name]['mid'] = $mid;
        }

        return $result;
    }

    /**
     * Returns an array with the difference between resumed data of two groups, with the given keys:
     *
     *  + memory:
     *    + max: Maximum value difference
     *    + min: Minimum value difference
     *    + mid: Mid value difference
     *  + time:
     *    + max: Maximum value difference
     *    + min: Minimum value difference
     *    + mid: Mid value difference
     *
     * @param array $groupResumeA
     * @param array $groupResumeB
     * @return array
     */
    public function getGroupResumeDiff(array $groupResumeA, array $groupResumeB)
    {

        $names = ['time', 'memory'];
        $measureNames = ['max', 'min', 'mid'];
        $diff = [
            'memory' => ['max' => 0, 'min' => 0, 'mid' => '0'],
            'time'   => ['max' => 0, 'min' => 0, 'mid' => '0'],
        ];

        foreach ($names as $name) {
            foreach ($measureNames as $measureName) {
                $amountA = (float)$groupResumeA[$name][$measureName];
                $amountB = (float)$groupResumeB[$name][$measureName];

                $diffAmount = (float)$amountA - (float)$amountB;
                $diff[$name][$measureName] = $diffAmount;
            }
        }

        return $diff;
    }
}
