<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf;

use phbrick\BaseStrictClass;
use RuntimeException;

/**
 * Stores some data that might be of use to do some benchmarking or performance measurements.
 *
 * This class stores the current timestamp and the memory usage upon creation. It has a method to
 * re-do all these measurements.
 */
class Measurement extends BaseStrictClass
{

    /**
     *
     * @param Measurement $measurement
     * @return Measurement
     */
    public static function cast(Measurement $measurement)
    {
        return $measurement;
    }

    /**
     * @var string
     */
    private $group = null;

    /**
     * Gets the name of the group this measurement belongs to
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @var float
     */
    private $time = 0;
    private $startTime = 0;

    /**
     * Gets the measured time in microseconds
     *
     * This value is always positive
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @var int
     */
    private $memory = 0;
    private $startMemory = 0;

    /**
     * Gets the amount of memory measured in bytes
     *
     * This value can be negative if the total of allocated memory decreases after the measure
     * is done.
     *
     * @return int
     */
    public function getMemory()
    {
        return $this->memory;
    }

    /**
     * @var bool
     */
    private $realMemoryUse = false;

    /**
     * Gets if the memory amounts are real amounts (true) or only memory used by emalloc
     * @return bool
     */
    public function isRealMemoryUse()
    {
        return $this->realMemoryUse;
    }

    /**
     * @var bool
     */
    private $started = false;

    /**
     * Gets if the measurement was started
     * @return bool
     */
    public function isStarted()
    {
        return $this->started;
    }

    /**
     * @var bool
     */
    private $ended = false;

    /**
     * Gets if the measurement was ended
     * @return bool
     */
    public function isEnded()
    {
        return $this->ended;
    }

    /**
     * Gets if the measurement is valid.
     *
     * Is valid if it have been marked as started and ended.
     */
    public function isValid()
    {
        return $this->started && $this->ended;
    }

    /**
     * @var bool
     */
    private $forceGCCycle = false;
    private $gcDisabled = false;

    /**
     * Gets if a Garbage collection cycle is forced when the measure begin() method is called before
     * any data is stored.
     * @return bool
     */
    public function isForceGCCycle()
    {
        return $this->forceGCCycle;
    }

    /**
     * Sets if a Garbage collection cycle is forced when the measure begin() method is called before
     * any data is stored
     * @param bool $forceGCCycle
     */
    public function setForceGCCycle($forceGCCycle)
    {
        $this->forceGCCycle = (bool)$forceGCCycle;
    }

    public function __construct($group, $realMemoryUse = false, $forceGCCycle = false)
    {
        $this->group = $group;
        $this->realMemoryUse = $realMemoryUse;
        $this->forceGCCycle = $forceGCCycle;
    }

    /**
     * Resets the data if is not valid
     *
     * @throws RuntimeException If the measure have not been completed
     */
    public function reset()
    {
        if ($this->started && !$this->ended) {
            throw new RuntimeException("Cannot reset measure, it was started but not ended");
        }

        $this->started = false;
        $this->ended = false;
        $this->time = 0;
        $this->startTime = 0;
        $this->memory = 0;
        $this->startMemory = 0;

        if ($this->gcDisabled && $this->forceGCCycle) {
            $this->gcDisabled = false;
            gc_enable();
        }
    }

    /**
     * Fills the starting data for the measure.
     *
     * @return Measurement
     * @throws RuntimeException If the measure is already ended or started
     */
    public function start()
    {
        if ($this->ended) {
            throw new RuntimeException("Cannot start measure because it was ended");
        }

        if ($this->started) {
            throw new RuntimeException("Cannot start measure because it was already started");
        }

        // Forcing a cycle implies that we will disable gc collection if enabled
        if ($this->forceGCCycle && gc_enabled()) {
            $this->gcDisabled = true;
            gc_collect_cycles();
            gc_disable();
        }

        $this->started = true;
        $this->startTime = microtime(true);
        $this->startMemory = memory_get_usage($this->realMemoryUse);

        return $this;
    }

    /**
     * Fills again the data and stores the difference with the previous data stored.
     *
     * @return Measurement
     * @throws RuntimeException If the measure is already ended or started
     */
    public function end()
    {
        if (!$this->started) {
            throw new RuntimeException("Cannot end measure because it was not started");
        }

        if ($this->ended) {
            throw new RuntimeException("Cannot end measure because it was already ended");
        }

        $this->ended = true;

        $currentTime = microtime(true);
        // $store this as microseconds
        $this->time = (int)(($currentTime - $this->startTime) * 1000000);

        $currentMemory = memory_get_usage($this->realMemoryUse);
        $this->memory = $currentMemory - $this->startMemory;

        // We disabled garbage collection, enable it again
        if ($this->forceGCCycle && $this->gcDisabled) {
            $this->gcDisabled = false;
            gc_enable();
        }

        return $this;
    }
}
