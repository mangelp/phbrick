<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\perf;

/**
 * Compares two measurement groups showing the difference between the two
 */
class MeasurementGroupComparator
{
    /**
     * @var string
     */
    private $groupA = null;

    /**
     * Gets the name of the first group compared
     * @return string
     */
    public function getGroupA()
    {
        return $this->groupA;
    }

    /**
     * Sets the name of the first group compared
     * @param string $groupA
     */
    public function setGroupA($groupA)
    {
        $this->groupA = $groupA;
    }

    /**
     * @var string
     */
    private $groupB = null;

    /**
     * Gets the name of the group which groupA compares to
     * @return string
     */
    public function getGroupB()
    {
        return $this->groupB;
    }

    /**
     * Sets the name of the group which groupA compares to
     * @param string $groupB
     */
    public function setGroupB($groupB)
    {
        $this->groupB = $groupB;
    }

    /**
     * @var Measurements
     */
    private $measurements = null;

    /**
     * Gets the measurements that contains the given groups to compare
     * @return Measurements
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * Sets the measurements that contains the given groups to compare
     * @param Measurements $measurements
     */
    public function setMeasurements(Measurements $measurements)
    {
        $this->measurements = $measurements;
    }

    public function __construct(Measurements $measurements = null, $groupA = null, $groupB = null)
    {
        $this->setMeasurements($measurements);
        $this->setGroupA($groupA);
        $this->setGroupB($groupB);
    }

    /**
     * Compares the given groups and returns the results as an array with the given fields:
     *  + label: Comparison label (groupA label VS groupB label)
     *  + mem: Memory consumption difference between groups. Positive values show that groupA used
     *    that more memory than groupB. A negative value shows that groupA used that less memory
     *    than groupB.
     *    The percentage calculated accounts the proportion of memory usage from groupA per memory
     *    usage in groupB rounded to two decimals. A number greater than 1 if the value is positive and a number less
     *    than 1 if the value is negative.
     *    + min: Min value diff and Percentage. Integer Bytes.
     *    + mid: Med value diff and Percentage. Float Bytes.
     *    + max: Max value diff and Percentage. Integer Bytes.
     *  + time: Time spent difference between groups. Positive values show that groupA was slower
     *    than groupB. A negative value shows that groupA was faster than group B.
     *    The percentage calculated accounts the proportion of time spent from groupA per time spent
     *    in groupB rounded to two decimals. A number greater than 1 if the value is positive and a number less than 1
     *    if the value is negative.
     *    + min: Min value diff and Percentage. Float seconds with microseconds.
     *    + mid: Med value diff and Percentage. Float seconds with microseconds.
     *    + max: Max value diff and Percentage. Float seconds with microseconds.
     */
    public function compare()
    {
        $groupResumeA = $this->measurements->getGroupResume($this->groupA);
        $groupResumeB = $this->measurements->getGroupResume($this->groupB);

        $result = [
            'label'  => $groupResumeA['label'] . ' VS ' . $groupResumeB['label'],
            'memory' => ['max' => 0, 'max_ratio' => 0.0, 'min' => 0, 'min_ratio' => 0.0, 'mid' => 0.0, 'mid_ratio' => 0.0, 'unit' => 'byte'],
            'time'   => ['max' => 0, 'max_ratio' => 0.0, 'min' => 0, 'min_ratio' => 0.0, 'mid' => 0.0, 'mid_ratio' => 0.0, 'unit' => 'ms'],
        ];

        // Memory

        foreach (['min', 'mid', 'max'] as $valueLabel) {
            $valA = $groupResumeA['memory'][$valueLabel];
            $valB = $groupResumeB['memory'][$valueLabel];
            $dif = $valA - $valB;

            if ($dif == 0 || $valB == 0) {
                $ratio = 0;
            }
            else {
                $ratio = (float)$valA / (float)$valB;
                $ratio = round($ratio * 100, 2);
            }

            $sign = '';

            if ($dif > 0) {
                $sign = '+';
            }
            else if ($dif < 0) {
                $sign = '-';
            }

            $dif = abs($dif);

            $result['memory'][$valueLabel] = $sign . UnitFormats::formatBytes($dif);
            $result['memory'][$valueLabel . '_ratio'] = sprintf('%.2f', $ratio) . '%';
        }

        // Time

        foreach (['min', 'mid', 'max'] as $valueLabel) {
            $valA = $groupResumeA['time'][$valueLabel];
            $valB = $groupResumeB['time'][$valueLabel];
            $dif = $valA - $valB;

            if ($dif == 0 || $valB == 0) {
                $ratio = 0;
            }
            else {
                $ratio = (float)$valA / (float)$valB;
                $ratio = round($ratio * 100, 2);
            }

            $sign = '';

            if ($dif > 0) {
                $sign = '+';
            }
            else if ($dif < 0) {
                $sign = '-';
            }

            $dif = abs($dif);

            $result['time'][$valueLabel] = $sign . UnitFormats::formatMicroseconds($dif);
            $result['time'][$valueLabel . '_ratio'] = sprintf('%.2f', $ratio) . '%';
        }

        return $result;
    }
}
