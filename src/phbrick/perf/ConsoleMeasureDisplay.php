<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf;

use InvalidArgumentException;
use phbrick\BaseStrictClass;

/**
 * Displays measurements to console.
 */
class ConsoleMeasureDisplay extends BaseStrictClass
{

    /**
     * @var Measurements
     */
    private $measurements = null;

    /**
     * Gets the measurements
     * @return Measurements
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * Sets the measurements
     * @param Measurements $measurements
     */
    public function setMeasurements(Measurements $measurements)
    {
        $this->measurements = $measurements;
    }

    public function __construct(Measurements $measurements = null)
    {
        $this->setMeasurements($measurements);
    }

    protected function formatMemory($amount)
    {
        $amount = round((float)$amount / 1024, 4);

        return ($amount > 0 ? '+' : '') . $amount . ' KB';
    }

    protected function formatTime($time)
    {
        $time = round((float)$time * 1000, 4);

        return ($time > 0 ? '+' : '') . $time . ' ms';
    }

    protected function format($name, $measurement)
    {
        if ($name == 'time') {
            return $this->formatTime($measurement);
        }
        else if ($name == 'memory') {
            return $this->formatMemory($measurement);
        }
        else {
            throw new InvalidArgumentException('Invalid unit name: ' . $name);
        }
    }

    protected function formatResume(array $groupResume)
    {
        $result = "\n\t"
            . $groupResume['label'] . ' [';

        $names = ['time' => 'ms', 'memory' => 'MB'];

        foreach ($names as $name => $unit) {
            $result .= "\n\t\t" . strtoupper($name) . ' {'
                . 'Max:' . $this->format($name, $groupResume[$name]['max'])
                . ', Min:' . $this->format($name, $groupResume[$name]['min'])
                . ', Mid:' . $this->format($name, $groupResume[$name]['mid'])
                . '}';
        }

        $result .= "\n\t]";

        return $result;
    }

    public function displayResume($group)
    {
        $groupResume = $this->measurements->getGroupResume($group);
        return $this->formatResume($groupResume);
    }

    public function displayComparison($groupA, $groupB, $label = null)
    {

        $comparator = new MeasurementGroupComparator($this->measurements, $groupA, $groupB);
        $comparisonResult = $comparator->compare();
        $comparisonResult['label'] = $comparisonResult['label'] . (!empty($label) ? " $label" : '');

        $resultStr = $comparisonResult['label'];

        $groupAResume = $this->measurements->getGroupResume($groupA);
        $groupBResume = $this->measurements->getGroupResume($groupB);

        foreach (['time', 'memory'] as $name) {

            $sectionLabel = strtoupper($name);

            $resultStr .= "\n--------------- $sectionLabel ";

            $format = $name == 'time' ?
                function($value) {
                    return UnitFormats::formatMicroseconds($value);
                } :
                function($value) {
                    return UnitFormats::formatBytes($value);
                };

            $resultStr .= sprintf("\n\t %s {MIN: %s; MID: %s; MAX: %s}",
                $groupAResume['label'],
                $format($groupAResume[$name]['min']),
                $format($groupAResume[$name]['mid']),
                $format($groupAResume[$name]['max']));

            $resultStr .= sprintf("\n\t %s {MIN: %s; MID: %s; MAX: %s}",
                $groupBResume['label'],
                $format($groupBResume[$name]['min']),
                $format($groupBResume[$name]['mid']),
                $format($groupBResume[$name]['max']));

            $resultStr .= sprintf("\n\t Difference {MIN: %s, %s; MID: %s, %s; MAX: %s, %s}",
                $comparisonResult[$name]['min'], $comparisonResult[$name]['min_ratio'],
                $comparisonResult[$name]['mid'], $comparisonResult[$name]['mid_ratio'],
                $comparisonResult[$name]['max'], $comparisonResult[$name]['max_ratio']);
        }

        $resultStr .= "\n----------------------------------------------------\n";

        return $resultStr;
    }
}
