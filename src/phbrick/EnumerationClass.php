<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick;

use InvalidArgumentException;
use phbrick\exceptions\TypeException;
use phbrick\types\Types;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Models an enumeration of constants with support for inheritance.
 *
 * Inherited constant override is not supported and internal logic will always use
 * the values of parent constants.
 *
 */
abstract class EnumerationClass extends StaticClass
{

    /**
     * Cache for enumeration constants
     * @var array
     */
    private static $___constantCache = [];

    /**
     * Gets an array with all the constant names (not the constant values).
     *
     * @return array
     * @throws TypeException
     */
    public static function getConstantNames()
    {
        return array_keys(self::getConstants());
    }

    /**
     * Gets an array where the keys are the constant names and the values are the value of each
     * constant.
     *
     * All constant listings are cached in memory for faster access.
     * @return array
     * @throws TypeException When the enumeration constant values does not match the given enumeration type
     */
    public static function getConstants()
    {
        /**
         * @var EnumerationClass
         */
        $cls = self::cls();

        if (isset(self::$___constantCache[$cls])) {
            return self::$___constantCache[$cls];
        }

        try {
            $constants = (new ReflectionClass($cls))->getConstants();
            self::$___constantCache[$cls] = $constants;
        } catch (ReflectionException $rex) {
            throw new RuntimeException("Class does not exists '$cls'", 0, $rex);
        }

        $type = $cls::getEnumerationValueType();

        foreach ($constants as $const => $value) {
            if (!Types::isType($type, $value)) {
                throw new TypeException("Invalid type " . gettype($value)
                    . " for constant $const. Enumeration values of $cls type must be of type $type.");
            }
        }

        return $constants;
    }

    /**
     * @param $value
     * @return bool|int|string
     * @throws TypeException
     */
    protected static function findValueConstant($value)
    {
        $constants = self::getConstants();
        /**
         * @var EnumerationClass
         */
        $cls = self::cls();
        $type = $cls::getEnumerationValueType();
        $caseSensitive = $cls::getEnumerationValueStringTypeIsCaseSensitive();

        foreach ($constants as $cName => $cValue) {
            if (Types::equals($value, $cValue, $type, $caseSensitive)) {
                return $cName;
            }
        }

        return null;
    }

    /**
     * @param $constant
     * @return bool|mixed
     * @throws TypeException
     */
    protected static function findConstantValue($constant)
    {
        $normalizedConstant = (string)$constant;
        $constants = self::getConstants();
        /**
         * @var EnumerationClass
         */
        $cls = self::cls();
        $constantNameCaseSensitive = $cls::isConstantNameCaseSensitive();

        foreach ($constants as $cName => $cValue) {
            if (Types::equals($normalizedConstant, $cName, Types::TYPE_STRING, $constantNameCaseSensitive)) {
                return $cValue;
            }
        }

        return null;
    }

    /**
     * Gets the value for a given constant name.
     *
     * @param string $constant The case-sensitive constant name
     * @return mixed
     * @throws InvalidArgumentException If no constant is named $constant
     * @throws TypeException
     */
    public static function valueOf($constant)
    {
        $cValue = self::findConstantValue($constant);

        if ($cValue === null) {
            $constants = array_keys(self::getConstants());

            throw new InvalidArgumentException("Invalid constant name `$constant`. Must be one of {" . implode(", ", $constants) . "}");
        }

        return $cValue;
    }

    /**
     * Gets the name of a constant given its value.
     *
     * If several constants has the same value the first one found is returned.
     *
     * @param $value
     * @return string The name of the constant
     * @throws InvalidArgumentException If no constant has the value $value
     */
    public static function nameOf($value)
    {

        $cName = self::findValueConstant($value);

        if ($cName === null) {
            throw new InvalidArgumentException("Invalid value `$value`. No constant found for it");
        }

        return $cName;
    }

    /**
     * Gets if the given constant name is one of the constants of the enumeration.
     *
     * @param $constant
     * @return bool
     */
    public static function hasConstant($constant)
    {
        $cValue = self::findConstantValue($constant);

        if ($cValue === null) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Gets if there is at least one constant with the given value set.
     *
     * @param $value
     * @return bool
     */
    public static function hasValue($value)
    {
        $cName = self::findValueConstant($value);

        if ($cName === null) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Asserts that the given value is one of the values of the enumeration.
     *
     * @param mixed $value The value to check
     * @throws TypeException
     */
    public static function assertValue($value)
    {
        if (!self::hasValue($value)) {
            throw new InvalidArgumentException(
                "Value $value is not one of the enumeration values: "
                . implode(", ", array_values(self::getConstants())));
        }
    }

    /**
     * Asserts that the given string is one of the constants of the enumeration.
     *
     * @param mixed $name The value to check
     * @throws TypeException
     */
    public static function assertName($name)
    {
        if (!self::hasConstant($name)) {
            throw new InvalidArgumentException(
                "Constant $name is not one of the enumeration constants: "
                . implode(", ", array_keys(self::getConstants())));
        }
    }

    /**
     * Returns the type of the enumeration values
     *
     * @return string
     */
    protected static function getEnumerationValueType()
    {
        return Types::TYPE_META_MIXED;
    }

    /**
     * Returns true if the strings used as values are case sensitive or not
     *
     * @return bool
     */
    protected static function getEnumerationValueStringTypeIsCaseSensitive()
    {
        return true;
    }

    /**
     * Returns true if the name of the constants is case-sensitive or false if not.
     *
     * Used when looking for a constant value or existence of that constant.
     *
     * @return bool
     */
    protected static function isConstantNameCaseSensitive()
    {
        return true;
    }
}
