<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\net;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\exceptions\InvalidOperationException;

/**
 * IP address.
 *
 * Supports storage and validation of an IP address.
 * @see https://www.ripe.net/participate/member-support/new-lir/ipv6_reference_card.pdf
 */
class IpAddress extends BaseStrictClass
{

    /**
     * @param IpAddress $ipAddress
     * @return IpAddress
     */
    public static function castIpAddress(IpAddress $ipAddress)
    {
        return $ipAddress;
    }

    const IP_V4 = 'ipv4';
    const IP_V6 = 'ipv6';
    const FORMAT_SIMPLIFIED = 'simplified';

    const ADDR_TYPE_BENCHMARKING = 'benchmarking';
    const ADDR_TYPE_DOCUMENTATION = 'documentation';
    const ADDR_TYPE_IPV6_GLOBAL_UNICAST = 'globalunicast';
    const ADDR_TYPE_IPV6_IPV4_MAPPED = 'ipv4mapped';
    const ADDR_TYPE_IPV6_ORCHID = 'orchid';
    const ADDR_TYPE_IPV6_TEREDO = 'teredo';
    const ADDR_TYPE_IPV6_TO_IPV4 = 'toipv4';
    const ADDR_TYPE_LINK_LOCAL = 'linklocal';
    const ADDR_TYPE_LOOPBACK = 'loopback';
    const ADDR_TYPE_MULTICAST = 'multicast';
    const ADDR_TYPE_OTHER = 'other';
    const ADDR_TYPE_PUBLIC = 'public';
    const ADDR_TYPE_PRIVATE = 'private';
    const ADDR_TYPE_UNESPECIFIED = 'unespecified';

    /**
     * @var string
     */
    private $addrType = null;

    /**
     * Gets the IP address type
     * @return string
     */
    public function getAddrType()
    {
        return $this->addrType;
    }

    /**
     * Components of the IP address. An IPv4 has 4 components of two bytes each and an IPv6 has 8
     * components of 4 bytes each. Each component is represented by an integer.
     * @var array
     */
    private $value = null;

    /**
     * Gets address value as an array of digits. 4 per IPv4 and 8 per IPv6.
     * @return array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets address value as an array of bytes.
     * @param array $value
     */
    protected function setValue(array $value)
    {
        if (!$this->version) {
            throw new InvalidOperationException('Cannot set the value if the type is not already set');
        }

        if (count($value) != $this->length) {
            throw new InvalidArgumentException(
                'Invalid IP byte length, must be ' . $this->length . ' but got ' . count($value));
        }

        $this->value = $value;
        $this->addrType = $this->calculateAddressType();
    }

    /**
     * Length of the IP address. 4 for IPv4 and 8 for IPv6
     * @var int
     */
    private $length = 0;

    /**
     * @var string
     */
    private $version = null;

    /**
     * Gets the address type (IPV4 or IPV6)
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets the address type (IPV4 or IPV6)
     * @param string $version
     */
    protected function setVersion($version)
    {
        if ($version == self::IP_V4) {
            $this->length = 4;
        }
        else if ($version == self::IP_V6) {
            $this->length = 8;
        }
        else {
            throw new InvalidArgumentException('Invalid type: ' . $version);
        }

        $this->version = $version;
    }

    /**
     * Creates a new address
     * @param string|array $address
     */
    public function __construct($address)
    {
        $this->initAddress($address);
    }

    /**
     * Sets the address and the type.
     *
     * If you pass an array it must contain either the decimal values of each address component or
     * the hexadecimal value for it prefixed by 0x. Without the 0x prefix numbers will be assumed
     * to be decimal and the wrong address will be set.
     *
     * @param string|array|self $address
     * @throws InvalidArgumentException
     */
    protected function initAddress($address)
    {
        $this->version = null;
        $this->value = null;

        if (is_array($address)) {
            $len = count($address);

            if ($len == 4) {
                $address = self::arrayToIPv4String($address);
            }
            else if ($len == 8) {
                $address = self::arrayToIPv6String($address);
            }
            else {
                throw new InvalidArgumentException('Invalid IP address ' . print_r($address, true));
            }
        }

        if (is_string($address)) {

            if (self::isIPv4($address)) {
                $this->setVersion(self::IP_V4);
                $address = explode('.', $address);

                $address = array_map(function($value) {
                    return (int)$value;
                }, $address);
            }
            else if (self::isIPv6($address)) {
                $this->setVersion(self::IP_V6);
                $address = self::canonicalIPv6String($address);
                $address = explode(':', $address);

                $address = array_map(function($value) {
                    return hexdec($value);
                }, $address);
            }
            else {
                throw new InvalidArgumentException('Invalid IP address ' . $address);
            }

            $this->setValue($address);
        }
        else if (is_a($address, 'phbrick\\net\\IpAddress')) {
            $address = IpAddress::castIpAddress($address);
            $this->setVersion($address->getAddrType());
            $this->setValue($address->getValue());
        }
        else if (!empty($address)) {
            throw new InvalidArgumentException('Invalid parameter $address');
        }
    }

    protected function calculateAddressType()
    {
        if ($this->isBenchmarking()) {
            return self::ADDR_TYPE_BENCHMARKING;
        }
        else if ($this->isDocumentation()) {
            return self::ADDR_TYPE_DOCUMENTATION;
        }
        else if ($this->isLinkLocal()) {
            return self::ADDR_TYPE_LINK_LOCAL;
        }
        else if ($this->isLoopback()) {
            return self::ADDR_TYPE_LOOPBACK;
        }
        else if ($this->isMulticast()) {
            return self::ADDR_TYPE_MULTICAST;
        }
        else if ($this->isPrivate()) {
            return self::ADDR_TYPE_PRIVATE;
        }
        else if ($this->isPublic()) {
            return self::ADDR_TYPE_PUBLIC;
        }
        else if ($this->isUnespecified()) {
            return self::ADDR_TYPE_UNESPECIFIED;
        }
        else if ($this->isIPv6IPv4Mapped()) {
            return self::ADDR_TYPE_IPV6_IPV4_MAPPED;
        }
        else if ($this->isIPv6Teredo()) {
            return self::ADDR_TYPE_IPV6_TEREDO;
        }
        else if ($this->isIPv6To4()) {
            return self::ADDR_TYPE_IPV6_TO_IPV4;
        }
        else if ($this->isIPv6Orchid()) {
            return self::ADDR_TYPE_IPV6_ORCHID;
        }
        else if ($this->isIPv6GlobalUnicast()) {
            return self::ADDR_TYPE_IPV6_GLOBAL_UNICAST;
        }
        else {
            throw new InvalidOperationException(
                'Cannot get address type of '
                . $this->toString() . '(' . print_r($this->value, true) . ')');
        }
    }

    /**
     * Gets if the IP address is a public address assigned to internet devices
     */
    protected function isPublic()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Public();
        }
        else {
            return $this->isIPv6Public();
        }
    }

    /**
     * Gets if the IP address is a private address reserved for private networks.
     * This excludes loopback IP addresses 127.0.0.0\8
     */
    protected function isPrivate()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Private();
        }
        else {
            return $this->isIPv6Private();
        }
    }

    /**
     * Gets if the IP address is a local loopback address used inside a computer.
     */
    protected function isLoopback()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Loopback();
        }
        else {
            return $this->isIPv6Loopback();
        }
    }

    /**
     * Gets if the IP address is link local
     */
    protected function isLinkLocal()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4LinkLocal();
        }
        else {
            return $this->isIPv6LinkLocal();
        }
    }

    /**
     *
     */
    protected function isBenchmarking()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Benchmarking();
        }
        else {
            return $this->isIPv6Benchmarking();
        }
    }

    /**
     *
     */
    protected function isDocumentation()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Documentation();
        }
        else {
            return $this->isIPv6Documentation();
        }
    }

    /**
     *
     */
    protected function isMulticast()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPv4Multicast();
        }
        else {
            return $this->isIPv6Multicast();
        }
    }

    /**
     *
     */
    protected function isUnespecified()
    {
        if ($this->version == self::IP_V4) {
            return $this->isIPV4Unespecified();
        }
        else {
            return $this->isIPv6Unespecified();
        }
    }

    /**
     *
     */
    protected function isOther()
    {
        if ($this->version == self::IP_V4) {
            return false;
        }
        else {
            return $this->isIPv6Other();
        }
    }

    protected function isIPv4Private()
    {
        return $this->value[0] == 10
            || ($this->value[0] == 192 && $this->value[1] == 168)
            || ($this->value[0] == 172 && $this->value[1] >= 16 && $this->value[1] <= 31);
    }

    protected function isIPv4Loopback()
    {
        return $this->value[0] == 127;
    }

    protected function isIPv4LinkLocal()
    {
        return $this->value[0] == 169 && $this->value[1] == 254;
    }

    protected function isIPv4Benchmarking()
    {
        // 198.18.0.0/15
        return $this->value[0] == 198 && $this->value[1] >= 18 && $this->value[1] <= 19;
    }

    protected function isIPv4Documentation()
    {
        // 192.0.2.0/24
        // 198.51.100.0/24
        // 203.0.113.0/24
        return ($this->value[0] == 192 && $this->value[1] == 0 && $this->value[2] == 2)
            || ($this->value[0] == 198 && $this->value[1] == 51 && $this->value[2] == 100)
            || ($this->value[0] == 203 && $this->value[1] == 0 && $this->value[2] == 113);
    }

    protected function isIPv4Multicast()
    {
        // 224.0.0.0/4
        return $this->value[0] >= 224 && $this->value[0] <= 239;
    }

    protected function isIPV4Unespecified()
    {
        return $this->value[0] == 0
            && $this->value[1] == 0
            && $this->value[2] == 0
            && $this->value[3] == 0
            && $this->value[4] == 0;
    }

    protected function isIPv4Public()
    {
        return !$this->isIPv4Private()
            && !$this->isIPv4Loopback()
            && !$this->isIPv4LinkLocal()
            && !$this->isIPv4Benchmarking()
            && !$this->isIPv4Documentation()
            && !$this->isIPv4Multicast()
            && !$this->isIPV4Unespecified();
    }

    protected function isIPv6Private()
    {
        return $this->value[0] >= 0xfc00 && $this->value[0] <= 0xfdff;
    }

    protected function isIPv6Loopback()
    {
        return $this->value[7] == 1
            && $this->value[6] == 0
            && $this->value[5] == 0
            && $this->value[4] == 0
            && $this->value[3] == 0
            && $this->value[2] == 0
            && $this->value[1] == 0
            && $this->value[0] == 0;
    }

    protected function isIPv6LinkLocal()
    {
        return $this->value[0] >= 0xfe80 && $this->value[0] <= 0xfeff;
    }

    protected function isIPv6Benchmarking()
    {
        return $this->value[0] == 0x2001 && $this->value[1] == 0x0002
            && $this->value[2] >= 0x0000 && $this->value[2] <= 0x00ff;
    }

    protected function isIPv6Documentation()
    {
        return $this->value[0] == 0x2001 && $this->value[1] == 0x0db8;
    }

    protected function isIPv6Multicast()
    {
        return $this->value[0] >= 0xff00 && $this->value[0] <= 0xffff;
    }

    protected function isIPv6Unespecified()
    {
        return $this->value[0] == 0
            && $this->value[1] == 0
            && $this->value[2] == 0
            && $this->value[3] == 0
            && $this->value[4] == 0
            && $this->value[5] == 0
            && $this->value[6] == 0
            && $this->value[7] == 0;
    }

    protected function isIPv6IPv4Mapped()
    {
        return $this->value[0] == 0 && $this->value[1] == 0 && $this->value[2] == 0 && $this->value[3] == 0xffff;
    }

    protected function isIPv6Orchid()
    {
        return $this->value[0] == 0x2001 && $this->value[1] >= 0x0010 && $this->value[1] <= 0x001f;
    }

    protected function isIPv6GlobalUnicast()
    {
        return $this->value[0] >= 0x2000 && $this->value[0] <= 0x3ff;
    }

    protected function isIPv6To4()
    {
        return $this->value[0] == 0x2002;
    }

    protected function isIPv6Teredo()
    {
        return $this->value[0] == 0x2001 && $this->value[1] == 0x000;
    }

    protected function isIPv6Other()
    {

        return
            // IPV4-MAPPED
            $this->isIPv6IPv4Mapped()
            // TEREDO
            || $this->isIPv6Teredo()
            // 6to4
            || $this->isIPv6To4()
            // Orchid
            || $this->isIPv6Orchid()
            // Global unicast
            || $this->isIPv6GlobalUnicast();
    }

    protected function isIPv6Public()
    {
        return !$this->isIPv6Private()
            && !$this->isIPv6Loopback()
            && !$this->isIPv6LinkLocal()
            && !$this->isIPv6Benchmarking()
            && !$this->isIPv6Documentation()
            && !$this->isIPv6Multicast()
            && !$this->isIPv6Other()
            && !$this->isIPv6Unespecified();
    }

    /**
     * Returns the IP address value as an string
     * @param string $format Format of IPv6 address, has no effect for IPv4
     * @return string IP address as an string
     */
    public function toString($format = null)
    {
        if ($this->version == self::IP_V4) {
            return $this->arrayToIPv4String($this->value);
        }

        // The rest of the method goes with IPv6 format

        $components = $this->value;
        $ipv4 = [];
        $isIPv4Mapped = $this->isIPv6IPv4Mapped();

        if ($isIPv4Mapped) {
            $ipv4 = array_slice($components, 4);
            $components = array_slice($components, 0, 4);
        }

        $components = array_map(function($component) use ($format) {
            if ($format == IpAddress::FORMAT_SIMPLIFIED && $component != 0) {
                return sprintf("%x", $component);
            }
            else if ($format == IpAddress::FORMAT_SIMPLIFIED && $component == 0) {
                return '';
            }
            else {
                return sprintf("%04x", $component);
            }
        }, $components);

        $result = implode(':', $components);

        if ($format == self::FORMAT_SIMPLIFIED) {
            $zeroSequences = [];
            $lastPos = null;

            for ($i = 0; $i < strlen($result); $i++) {
                if ($result[$i] == ':' && $lastPos === null) {
                    $lastPos = $i;
                }
                else if ($result[$i] != ':' && $lastPos !== null) {

                    if ($i - $lastPos > 1) {
                        $zeroSequences[] = [
                            'length' => $i - $lastPos,
                            'start'  => $lastPos];
                    }

                    $lastPos = null;
                }
            }

            if (!empty($zeroSequences)) {
                usort($zeroSequences, function(array $elemA, array $elemB) {
                    return $elemB['length'] - $elemA['length'];
                });

                $length = $zeroSequences[0]['length'];
                $start = $zeroSequences[0]['start'];

                $result = substr($result, 0, $start)
                    . '::'
                    . substr($result, $start + $length);
            }
        }

        if ($isIPv4Mapped) {
            $result .= ':' . implode('.', $ipv4);
        }

        return $result;
    }

    /**
     * Returns an IPv6 version of an IPv4 address.
     *
     * If the current address is already an IPv6 address this method returns a new instance with
     * the same value.
     *
     * @return IpAddress
     */
    public function toIPv6()
    {
        $value = $this->getValue();

        if ($this->version == self::IP_V4) {
            $value = array_merge([0x0, 0x0, 0x0, 0xffff], $value);
        }

        /** @var IpAddress $new */
        $new = self::instance($value);
        $result = IpAddress::castIpAddress($new);

        return $result;
    }

    /**
     * Checks if $ip parameter is an IPv4 address
     * @param string|array|IpAddress $ip
     * @return bool
     */
    public static function isIPv4($ip)
    {
        if (is_array($ip) && count($ip) == 4) {
            $ip = self::normalizeArrayValues($ip);
            $ip = self::arrayToIPv4String($ip);
        }

        if (is_string($ip)) {
            return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false;
        }
        else if (is_a($ip, 'phbrick\\net\\IpAddress')) {
            return $ip->getVersion() == self::IP_V4;
        }

        return false;
    }

    /**
     * Checks if $ip parameter is an IPv6 address
     * @param string|array|IpAddress $ip
     * @return bool
     */
    public static function isIPv6($ip)
    {
        if (is_array($ip) && count($ip) == 8) {
            $ip = self::normalizeArrayValues($ip);
            $ip = self::arrayToIPv6String($ip);
        }

        if (is_string($ip)) {
            return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false;
        }
        else if (is_a($ip, 'phbrick\\net\\IpAddress')) {
            return $ip->getVersion() == self::IP_V6;
        }

        return false;
    }

    /**
     * Converts IPv4 digits to string
     * @param array $arr
     * @return string
     */
    protected static function arrayToIPv4String(array $arr)
    {
        return implode('.', $arr);
    }

    /**
     * Converts IPv6 digits to string
     * @param array $arr
     * @return string
     */
    protected static function arrayToIPv6String(array $arr)
    {

        $arr = array_map(function($value) {
            return sprintf('%04x', $value);
        }, $arr);

        return implode(':', $arr);
    }

    /**
     * Convert all array values to integers.
     *
     * @param array $addrValue
     * @return array
     * @throws InvalidArgumentException If any value has an invalid type (not numeric).
     */
    protected static function normalizeArrayValues(array $addrValue)
    {
        $result = array_map(function($value) {
            if (is_string($value) && preg_match('/^(0x)?[0-9a-fA-F]$/', $value)) {
                return hexdec($value);
            }
            else if (is_string($value) && is_numeric($value)) {
                return (int)$value;
            }
            else if (is_int($value)) {
                return $value;
            }
            else if (empty($value)) {
                return 0;
            }
            else {
                throw new InvalidArgumentException('Invalid address byte: ' . $value);
            }
        }, $addrValue);

        return $result;
    }

    /**
     * Expands the IPv6 address components separated by semi-colons and represents them in
     * canonical 4-hex digits form.
     * @param string $addr
     * @return string
     */
    public static function canonicalIPv6String($addr)
    {

        if (!is_string($addr)) {
            throw new InvalidArgumentException('Parameter $addr must be an string');
        }

        // First identify simplified zero sequence
        $pos = strpos($addr, '::');

        if ($pos !== false) {
            // We have a simplified sequence, calculate needed semicolons and add them
            $semicolonCount = substr_count($addr, ':') - 2;
            $semicolonsNeeded = 7 - $semicolonCount;

            $addr = substr($addr, 0, $pos)
                . str_repeat(':', $semicolonsNeeded)
                . substr($addr, $pos + 2);
        }

        $parts = explode(':', $addr);

        if (count($parts) != 8) {
            throw new InvalidOperationException('Failed to expand simplified address');
        }

        $parts = array_map(function($value) {
            if (empty($value)) {
                $value = 0x0000;
            }

            return sprintf("%04x", hexdec($value));
        }, $parts);

        $result = implode(':', $parts);

        return $result;
    }

    /**
     * Creates a new IpAddress object instance
     * @param string|array $ipAddressString
     * @return IpAddress
     */
    public function parseIpAddress($ipAddressString)
    {
        /** @var IpAddress $new */
        $new = self::instance($ipAddressString);
        return IpAddress::castIpAddress($new);
    }
}
