<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\net;

use InvalidArgumentException;
use phbrick\collection\ArrayMap;
use phbrick\collection\Arrays;
use phbrick\collection\IMap;
use phbrick\exceptions\InvalidKeyException;
use phbrick\exceptions\NotIterableException;
use phbrick\exceptions\NullValueException;
use phbrick\exceptions\TypeException;
use phbrick\IReadOnly;
use phbrick\ReadOnlyTrait;
use phbrick\string\AbstractStringRenderable;
use phbrick\string\Renderables;
use phbrick\types\Bools;
use phbrick\types\Types;
use RuntimeException;
use Traversable;

/**
 * Models an URL allowing flexible handling of each of its components but without too much
 * complications.
 *
 */
class Url extends AbstractStringRenderable implements IReadOnly
{

    use ReadOnlyTrait;

    /**
     * @param Url $url
     * @return Url
     */
    public static function castUrl(Url $url)
    {
        return $url;
    }

    /**
     * Fragment component. The portion after the # char
     * @var string
     */
    const COMP_FRAGMENT = 'fragment';
    /**
     * Host component. Either a domain or an IP address (v4 or v6)
     * @var string
     */
    const COMP_HOST = 'host';
    /**
     * Password component.
     * @var string
     */
    const COMP_PASS = 'pass';
    /**
     * Path component
     * @var string
     */
    const COMP_PATH = 'path';
    /**
     * Port component.
     * @var string
     */
    const COMP_PORT = 'port';
    /**
     * Query parameters component.
     * @var string
     */
    const COMP_QUERY = 'query';
    /**
     * Scheme component.
     * @var string
     */
    const COMP_SCHEME = 'scheme';
    /**
     * User component.
     * @var string
     */
    const COMP_USER = 'user';
    /**
     * Url encoding following RFC1738 encodes spaces as + character
     * @var string
     */
    const ENCODING_SPACE_PLUS = PHP_QUERY_RFC1738;
    /**
     * Url encoding following RFC3986 encodes spaces as %20 character sequence
     * @var string
     */
    const ENCODING_SPACE_PERCENT = PHP_QUERY_RFC3986;

    private static $urlComponents = [
        self::COMP_FRAGMENT,
        self::COMP_HOST,
        self::COMP_PASS,
        self::COMP_PATH,
        self::COMP_PORT,
        self::COMP_QUERY,
        self::COMP_SCHEME,
        self::COMP_USER,
    ];

    /**
     * @var IMap
     */
    private $parameters = null;

    /**
     * Gets
     * @return IMap
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Sets
     * @param array|Traversable $parameters
     * @throws NotIterableException
     * @throws TypeException
     */
    public function setParameters($parameters)
    {
        $this->assertReadOnly();
        Types::assertIterable($parameters);

        $this->parameters->clear();
        $this->parameters->setAll($parameters);
    }

    /**
     * @var string
     */
    private $fragment = null;

    /**
     * Gets
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * Sets
     * @param string $fragment
     */
    public function setFragment($fragment)
    {
        $this->assertReadOnly();
        $this->fragment = $fragment;
    }

    /**
     * @var string
     */
    private $host = null;

    /**
     * Gets
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets
     * @param string $host
     */
    public function setHost($host)
    {
        $this->assertReadOnly();
        $this->host = $host;
    }

    /**
     * @var int
     */
    private $port = null;

    /**
     * Gets
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Sets
     * @param int $port
     */
    public function setPort($port)
    {
        $this->assertReadOnly();
        $this->port = $port;
    }

    /**
     * @var string
     */
    private $scheme = null;

    /**
     * Gets
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Sets
     * @param string $scheme
     */
    public function setScheme($scheme)
    {
        $this->assertReadOnly();
        $this->scheme = $scheme;
    }

    /**
     * @var string
     */
    private $path = null;

    /**
     * Gets
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets
     * @param string $path
     */
    public function setPath($path)
    {
        $this->assertReadOnly();
        $this->path = $path;
    }

    /**
     * @var string
     */
    private $encoding = self::ENCODING_SPACE_PLUS;

    /**
     * Gets the encoding type for this URL
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Sets the encoding type for this URL
     * @param string $encoding
     */
    public function setEncoding($encoding)
    {
        $this->assertReadOnly();
        $this->encoding = $encoding;
    }

    /**
     * @var string
     */
    private $user = null;

    /**
     * Gets
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets
     * @param string $user
     */
    public function setUser($user)
    {
        $this->assertReadOnly();
        $this->user = $user;
    }

    /**
     * @var string
     */
    private $pass = null;

    /**
     * Gets
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Sets
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->assertReadOnly();
        $this->pass = $pass;
    }

    public function __construct($url = null, $readOnly = false)
    {
        $this->parameters = new ArrayMap();

        if ($url !== null) {
            $this->internalParseUrl($url);
        }

        $this->setReadOnly(Bools::castBool($readOnly));
    }

    public function toString($context = null, $indent = null)
    {
        $parts = [];

        if (!empty($this->scheme)) {
            $parts[] = $this->scheme . '://';
        }

        if (!empty($this->host)) {
            if (!empty($this->user)) {
                $parts[] = $this->user;

                if (!empty($this->pass)) {
                    $parts[] = ':' . $this->pass;
                }

                $parts[] = '@';
            }

            $parts[] = $this->host;
        }

        if (!empty($this->port)) {
            $parts[] = ':' . $this->port;
        }

        if (!empty($this->path)) {
            $parts[] = '/' . ltrim($this->path, '/');
        }

        if (!empty($this->parameters)) {
            $parts[] = '?' . $this->getQueryString();
        }

        if (!empty($this->fragment)) {
            $parts[] = '#' . $this->fragment;
        }

        return implode('', $parts);
    }

    /**
     * Gets an array with all the components of the current object as parse_url returns.
     *
     * @return array of url components
     */
    protected function getComponents()
    {
        $components = [
            self::COMP_FRAGMENT => $this->fragment,
            self::COMP_HOST     => $this->host,
            self::COMP_PASS     => $this->pass,
            self::COMP_PATH     => $this->path,
            self::COMP_PORT     => $this->port,
            self::COMP_QUERY    => $this->getQueryString(),
            self::COMP_SCHEME   => $this->scheme,
            self::COMP_USER     => $this->user,
        ];

        // Remove empty components
        foreach ($components as $key => $value) {
            if (empty($value)) {
                unset($components[$key]);
            }
        }

        return $components;
    }

    protected function setComponents(array $components)
    {
        $this->assertReadOnly();

        foreach ($components as $key => $value) {
            switch ($key) {
                case self::COMP_FRAGMENT:
                    $this->setFragment($value);
                    break;
                case self::COMP_HOST:
                    $this->setHost($value);
                    break;
                case self::COMP_PASS:
                    $this->setPass($value);
                    break;
                case self::COMP_PATH:
                    $this->setPath($value);
                    break;
                case self::COMP_PORT:
                    $this->setPort($value);
                    break;
                case self::COMP_QUERY:
                    $this->setQueryString($value);
                    break;
                case self::COMP_SCHEME:
                    $this->setScheme($value);
                    break;
                case self::COMP_USER:
                    $this->setUser($value);
                    break;
                default:
                    throw new RuntimeException("Unsupported component $key");
            }
        }
    }

    /**
     * Gets a query string with all the current parameters
     * @return string query string
     */
    public function getQueryString()
    {
        return http_build_query(
            $this->parameters->toArray(),
            null,
            '&',
            $this->encoding);
    }

    /**
     * Sets the query string to extract parameters from it.
     * @param string $queryString
     */
    public function setQueryString($queryString)
    {
        $this->assertReadOnly();
        $params = self::parseQueryString($queryString);
        $this->parameters->clear();
        $this->parameters->setAll($params);
    }

    /**
     * Parses and decomposes the url string in his component parts and assign each of them to the
     * current instance.
     *
     * @param string $url
     */
    protected function internalParseUrl($url)
    {
        $components = [];

        if (is_a($url, 'phbrick\\net\\Url')) {
            /** @var Url $url */
            $url = Url::castUrl($url);
            $components = $url->getComponents();
        }
        else if (is_array($url) || is_a($url, '\Traversable')) {
            foreach ($url as $key => $value) {
                if (is_int($key)) {
                    throw new InvalidArgumentException("Invalid URL component with numeric key $key and value $value");
                }

                $key = strtolower($key);

                // If key is not int then is an string
                if (!in_array($key, self::$urlComponents)) {
                    throw new InvalidArgumentException("Invalid URL component name: $key");
                }

                $components[$key] = $value;
            }
        }
        else if (is_string($url)) {
            $components = parse_url($url);
        }
        else if (Renderables::isStringRenderable($url)) {
            $urlString = Renderables::toString($url);
            $components = parse_url($urlString);
        }
        else {
            throw new InvalidArgumentException('Invalid parameter $url type: ' . gettype($url));
        }

        if ($components === false) {
            throw new InvalidArgumentException('Invalid parameter $url, it cannot be parsed');
        }

        foreach ($components as $key => $value) {
            if (empty($components[$key])) {
                continue;
            }

            if (strcasecmp(self::COMP_QUERY, $key) == 0) {
                $params = self::parseQueryString($value);
                $this->parameters->setAll($params);
            }
            else {
                $this->$key = $value;
            }
        }
    }

    /**
     * Returns a new instance with the given parts modified.
     *
     * The read-only flag does not apply to the modification of the new instance.
     *
     * @param array|Traversable $components
     * @return Url
     * @throws NotIterableException
     * @throws InvalidKeyException
     * @throws NullValueException
     * @throws TypeException
     */
    public function withComponents($components)
    {
        Types::assertIterable($components);

        /** @var Url $url */
        $url = self::instance();
        $url->setEncoding($this->getEncoding());
        $url->getParameters()->setAll($this->getParameters());
        $url->setComponents($this->getComponents());
        $url->setComponents($components);
        $url->setReadOnly($this->isReadOnly());

        return $url;
    }

    /**
     * Parses the string as an URL and creates a new Url instance with it.
     * @param string $url
     * @return Url
     */
    public static function parseUrl($url)
    {
        /** @var Url $newUrl */
        $newUrl = self::instance($url);
        return $newUrl;
    }

    /**
     * Parses the query string and returns an array with the found keys.
     * @param string $queryString
     * @param callable $filter
     * @return array
     */
    public static function parseQueryString($queryString, callable $filter = null)
    {
        $queryParams = [];
        $queryString = trim($queryString, "\r\n\t ?#");
        parse_str($queryString, $queryParams);

        if ($filter !== null) {
            $queryParams = Arrays::apply($queryParams, $filter);
        }

        return $queryParams;
    }
}
