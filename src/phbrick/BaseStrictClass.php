<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Base class that throws exceptions when undefined members are accessed or modified and provides
 * convenient methods to get the class definition from a class instance.
 *
 * This class helps building stricter classes that does not allow adding members at run-time and
 * helps detecting when an undefined class member is used.
 *
 * Child classes should call the parent magic method implementation to keep the strict behaviour or
 * do not do it to allow the regular one.
 */
class BaseStrictClass
{

    /**
     * Throws a RuntimeException
     *
     * @param string $memberName Name of the undefined member that caused the error
     * @param string $type Type of access to the member that caused the error
     * @return null
     * @throws RuntimeException
     */
    protected static final function _x__throwUndefinedMemberException($memberName, $type)
    {

        throw new RuntimeException(
            'Undefined ' . $type . ': ' . (string)$memberName . ' in class '
            . get_called_class() . '.');
    }

    /**
     * Returns the fully qualified class name.
     * @return string
     */
    public static final function cls()
    {
        return get_called_class();
    }

    /**
     * Creates a new instance of the concrete class where this method is called instead of the base class that implements
     * this method.
     *
     * @param array ...$args
     * @return object
     */
    public static final function instance(...$args)
    {
        try {
            $reflectionClass = new ReflectionClass(get_called_class());
            return $reflectionClass->newInstanceArgs($args);
        } catch (ReflectionException $rex) {
            throw new RuntimeException("Cannot create instance for class " . get_called_class(), 0, $rex);
        }
    }

    /**
     * Calls an static method with given args
     * @param $method
     * @param array $args
     * @return mixed
     */
    public static final function callStatic($method, ...$args)
    {
        $cls = self::cls();
        return call_user_func_array([$cls, $method], $args);
    }

    public function __call($methodName, $methodArgs)
    {
        self::_x__throwUndefinedMemberException($methodName, 'method');
    }

    public static function __callstatic($methodName, $methodArgs)
    {
        self::_x__throwUndefinedMemberException($methodName, 'static method');
    }

    public function __isset($field)
    {
        self::_x__throwUndefinedMemberException($field, 'field');
    }

    public function __unset($field)
    {
        self::_x__throwUndefinedMemberException($field, 'field');
    }

    /**
     * @param $field
     * @return mixed
     */
    public function __get($field)
    {
        return self::_x__throwUndefinedMemberException($field, 'field');
    }

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function __set($field, $value)
    {
        return self::_x__throwUndefinedMemberException($field, 'field');
    }
}
