<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

/**
 * Class RGBColor
 *
 * @example
 * Black    #000000    (0,0,0)
 * White    #FFFFFF    (255,255,255)
 * Red      #FF0000    (255,0,0)
 * Lime     #00FF00    (0,255,0)
 * Blue     #0000FF    (0,0,255)
 * Yellow   #FFFF00    (255,255,0)
 * Cyan     #00FFFF    (0,255,255)
 * Magenta  #FF00FF    (255,0,255)
 * Silver   #C0C0C0    (192,192,192)
 * Gray     #808080    (128,128,128)
 * Maroon   #800000    (128,0,0)
 * Olive    #808000    (128,128,0)
 * Green    #008000    (0,128,0)
 * Purple   #800080    (128,0,128)
 * Teal     #008080    (0,128,128)
 * Navy     #000080    (0,0,128)
 *
 * @package phbrick\graphics
 * @see phbrick\graphics\RGBColors
 */
class RGBColor extends RgbaColor
{

    /**
     *
     * @param RGBColor $color
     * @return RGBColor
     */
    public static function castRGB(RGBColor $color)
    {
        return $color;
    }

    /**
     * RGBColor constructor.
     *
     * @param $color
     */
    public function __construct($color = null)
    {
        parent::__construct($color);
        $this->setAlpha(self::MAX_BYTE_VALUE);
    }
}
