<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\exceptions\IllegalStateException;
use phbrick\exceptions\TypeException;
use phbrick\ICloneable;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Numbers;
use phbrick\types\Types;

/**
 * Class Point models a point in cartesian bi-dimensional space.
 *
 * Point coordinates can have negative values.
 *
 * @package phbrick\graphics
 */
class Point extends BaseStrictClass implements ICloneable, IMeasured
{
    use SafeStringRenderTrait;
    use MeasuredTrait;

    /**
     * @param Point $point
     * @return Point
     */
    public static function castPoint(Point $point)
    {
        return $point;
    }

    /**
     * @var float
     */
    private $x = 0;

    /**
     * @var  float
     */
    private $y = 0;

    /**
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param float $x
     * @return Point
     */
    public function setX($x)
    {
        $this->x = Numbers::castFloat($x);
        return $this;
    }

    /**
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return Point
     */
    public function setY($y)
    {
        $this->y = Numbers::castFloat($y);
        return $this;
    }

    /**
     * Point constructor initializes the specified x-y components.
     *
     * The default value is 0
     *
     * @param int|null $x
     * @param int|null $y
     * @param null $units
     * @throws TypeException
     */
    public function __construct($x = null, $y = null, $units = null)
    {
        if ($x !== null) {
            $this->setX($x);
        }

        if ($y !== null) {
            $this->setY($y);
        }

        if ($units != null) {
            $this->setUnits($units);
        }
    }

    /**
     * Translates current point coordinates to another origin given the current origin point in the new
     * system.
     *
     * @param Point $origin Point of the current origin of coordinates in the new system.
     * @return Point
     */
    public function translate(Point $origin)
    {
        $this->assertUnits($origin);
        return $this->offset($origin->getX(), $origin->getY());
    }

    /**
     * Returns a new point whose (X,Y) coordinates have been moved applying the offsets to each component.
     *
     * @param int $offsetX Offset to apply to x point
     * @param int $offsetY Offset to apply to y point
     * @return Point
     */
    public function offset($offsetX = null, $offsetY = null)
    {
        if ($offsetX === null) {
            $offsetX = 0;
        }
        else {
            $offsetX = Numbers::castFloat($offsetX);
        }

        if ($offsetY === null) {
            $offsetY = 0;
        }
        else {
            $offsetY = Numbers::castFloat($offsetY);
        }

        /** @var Point $point */
        $point = self::instance($this->x + $offsetX, $this->y + $offsetY);

        return $point;
    }

    /**
     * Returns the current point components into an array with the current point components into an array.
     *
     * @param string|null $keyNames Format for point key names. Allowed values: x-y, left-top, null/none.
     * @return array
     */
    public function toArray($keyNames = null)
    {
        $xKey = 0;
        $yKey = 1;

        switch (strtolower(trim($keyNames))) {
            case 'x-y':
                $xKey = 'x';
                $yKey = 'y';
                break;
            case 'left-top':
                $xKey = 'left';
                $yKey = 'top';
                break;
        }

        $result = [$xKey => $this->getX(), $yKey => $this->getY()];

        return $result;
    }

    /**
     * Gets a new point with the coordinates converted to the given units.
     *
     * If no units are given all the other parameters are not used and the units are copied from the current point.
     *
     * @param string|null $units new point units
     * @param UnitsConverter|null $converter
     * @param string|int|float|null $precision Conversion result precision
     * @param string|int|float|null $dpm Conversion result quality factor
     * @return Point
     * @throws TypeException
     */
    public function toPoint($units = null, UnitsConverter $converter = null, $precision = null, $dpm = null)
    {
        $other = clone $this;

        // Do not call convert with null units
        if ($units != null) {
            $other->setUnits($units);
            $other->setX($this->convert($this->getX(), $other, $converter, $precision, $dpm));
            $other->setY($this->convert($this->getY(), $other, $converter, $precision, $dpm));
        }

        return $other;
    }

    /**
     * Obtains a new point that is the middle point between the current one and the next one
     *
     * @param Point $next
     * @return Point
     */
    public function middle(Point $next)
    {
        $this->assertUnits($next);
        $x = ($this->getX() + $next->getX()) / 2;
        $y = ($this->getY() + $next->getY()) / 2;

        /** @var Point $result */
        $result = self::instance($x, $y);

        return $result;
    }

    /**
     * Gets if the point coordinates are both zero.
     *
     * @return bool
     */
    public function isZero()
    {
        return $this->x == 0 && $this->y == 0;
    }

    /**
     * Gets the location of the point from the axis.
     */
    public function getAxisLocation()
    {
        if ($this->getX() > 0 && $this->getY() > 0) {
            return PointAxisLocation::TOP_RIGHT;
        }
        else if ($this->getX() > 0 && $this->getY() < 0) {
            return PointAxisLocation::BOTTOM_RIGHT;
        }
        else if ($this->getX() < 0 && $this->getY() < 0) {
            return PointAxisLocation::BOTTOM_LEFT;
        }
        else if ($this->getX() < 0 && $this->getY() > 0) {
            return PointAxisLocation::TOP_LEFT;
        }
        else if ($this->getX() == 0 && $this->getY() == 0) {
            return PointAxisLocation::CENTER;
        }
        else if ($this->getX() == 0) {
            return PointAxisLocation::Y_AXIS;
        }
        else if ($this->getY() == 0) {
            return PointAxisLocation::X_AXIS;
        }
        else {
            throw new IllegalStateException("Point outside cartesian space: $this");
        }
    }

    public function toString()
    {
        return sprintf("(%f,%f)", $this->x, $this->y);
    }

    /**
     * Creates a Point instance from an instance of Size, Rectangle, an array, null or a number.
     *
     * @param Point|Rectangle|array|int|float|null $value
     * @return Point
     */
    public static function fromValue($value)
    {
        /** @var Point $result */
        $result = null;

        if ($value === null) {
            $result = self::instance(0.0, 0.0);
        }
        else if (is_numeric($value)) {
            $value = Numbers::castFloat($value);
            $result = self::instance($value, $value);
        }
        else if (is_a($value, Point::class)) {
            $result = clone $value;
        }
        else if (is_a($value, Rectangle::class)) {
            $result = clone $value->getPoint();
        }
        else if (is_array($value) && count($value) == 2) {
            if (isset($value['x']) && isset($value['y'])) {
                $result = self::instance($value['x'], $value['y']);
            }
            else if (isset($value['left']) && isset($value['top'])) {
                $result = self::instance($value['left'], $value['top']);
            }
            else if (isset($value[0]) && isset($value[1])) {
                $result = self::instance($value[0], $value[1]);
            }
            else {
                throw new InvalidArgumentException("Cannot create Point instance from invalid array format");
            }
        }
        else {
            throw new InvalidArgumentException("Cannot create a Point instance from type " . Types::getTypeName($value));
        }

        return $result;
    }
}
