<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\exceptions\TypeException;

trait MeasuredTrait
{

    /**
     * @var string|null
     */
    private $units = null;

    /**
     * @return null|string
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param null|string $units
     * @throws TypeException
     */
    public function setUnits($units)
    {
        if ($units !== null) {
            MeasureUnits::assertValue($units);
        }

        $this->units = $units;
    }

    protected function assertUnits(IMeasured $other)
    {
        if ($this->units != $other->getUnits()) {
            throw new InvalidArgumentException("Cannot operate between different units. Current: " . $this->getUnits() . ", other: " . $other->getUnits());
        }
    }

    /**
     * @param $value
     * @param IMeasured $other
     * @param UnitsConverter|null $converter
     * @param string|int|float|null $precision
     * @param string|int|float|null $dpm
     * @return float
     * @throws TypeException
     */
    protected function convert($value, IMeasured $other, UnitsConverter $converter = null, $precision = null, $dpm = null)
    {
        if ($converter == null) {
            $converter = UnitsConverter::getDefaultConverter();
        }

        return $converter->convert($value, $other->getUnits(), $this->getUnits(), $precision, $dpm);
    }
}
