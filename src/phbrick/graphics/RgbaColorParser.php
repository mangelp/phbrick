<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\collection\Arrays;
use phbrick\string\Strings;
use phbrick\types\Floats;
use phbrick\types\Ints;
use phbrick\types\Numbers;

/**
 * Class RgbaColorParser
 * @package phbrick\graphics
 */
class RgbaColorParser
{
    const TRIM_CHARS = "\r\n\t# ,;)(][";

    /**
     * Gets if the value is a valid byte value.
     *
     * A valid value is an integer in range [0-255]
     * @param $byte
     * @return bool
     */
    public static function validByte($byte)
    {
        return $byte >= AbstractColor::MIN_BYTE_VALUE && $byte <= AbstractColor::MAX_BYTE_VALUE;
    }

    /**
     * Gets if the value is a valid alpha value.
     *
     * A valid alpha value is an floating point value in range [0.0, 1.0]
     *
     * @param $alpha
     * @return bool
     */
    public static function validAlpha($alpha)
    {
        return $alpha >= RgbaColor::MIN_ALPHA && $alpha <= RgbaColor::MAX_ALPHA;
    }

    /**
     * Creates an initialized color from an rgb, rgba, hex or integer value.
     *
     * Numerical input values are converted to 0-padded hex (even number of chars) and those bytes are stored as
     * color components.
     *
     * @param $color
     * @return RGBColor
     */
    public static function parse($color)
    {
        if ($color === null) {
            return AbstractColor::COLOR_BLACK;
        }
        else if ($color instanceof AbstractColor) {
            /**
             * @var AbstractColor $color
             */
            return $color->toArray();
        }
        else if (is_numeric($color)) {
            return self::parseNumber($color);
        }
        else if (is_string($color)) {
            $filteredColor = self::filterColor($color);

            if (Strings::startsWith($filteredColor, 'rgb')) {
                return self::parseCSS($filteredColor);
            }
            else if (preg_match('/[0-9a-z]{6,8}/', $filteredColor) || $filteredColor[0] == '#') {
                return self::parseHex($color);
            }
            else if (strpos($filteredColor, ',') !== false
                && preg_match('/^[0-9,]+$/', $filteredColor)) {
                return self::parse(explode(',', $filteredColor));
            }
            else {
                throw new InvalidArgumentException("Invalid color $color");
            }
        }
        else if (is_array($color)) {
            return self::parseArray($color);
        }
        else if (is_object($color)) {
            throw new InvalidArgumentException('Unsupported color format: ' . Types::getTypeName($color));
        }
        else if (is_scalar($color)) {
            throw new InvalidArgumentException('Unsupported color format: ' . $color . '(' . Types::getTypeName($color) . ')');
        }
        else {
            throw new InvalidArgumentException("Invalid color $color");
        }

    }

    /**
     * Parses a color and returns an array with RGBA bytes
     *
     * @param $hexColor Color hex notation
     * @return array
     */
    public static function parseHex($hexColor)
    {
        if (is_int($hexColor)) {
            return self::parseNumber($hexColor);
        }

        $hexColor = RgbaColorParser::filterColor($hexColor);

        if (!is_string($hexColor) || empty($hexColor)) {
            throw new InvalidArgumentException("RGBColor must be a hex color string");
        }

        if (!preg_match('/[0-9a-z]{6,8}/', $hexColor)) {
            throw new InvalidArgumentException("Invalid hex color: " . $hexColor);
        }

        $r = substr($hexColor, 0, 2);
        $g = substr($hexColor, 2, 2);
        $b = substr($hexColor, 4, 2);
        // Default alpha is 1, total opacity
        $a = 'ff';

        if (strlen($hexColor) == 8) {
            $a = substr($hexColor, 6, 2);
        }

        return [hexdec($r), hexdec($g), hexdec($b), hexdec($a)];
    }

    /**
     * Parses a color in CSS format and returns an array with RGBA bytes
     *
     * @param $rgbColor
     * @return mixed
     * @example
     *  RgbaColorParser::parseCSS('rgba(0, 1, 2, 0.9)');
     *  RgbaColorParser::parseCSS('rgb(0, 1, 2)');
     */
    public static function parseCSS($rgbColor)
    {
        $rgbColor = RgbaColorParser::filterColor($rgbColor);

        if (!is_string($rgbColor) || empty($rgbColor)) {
            throw new InvalidArgumentException("RGBColor must be an rgb color string");
        }

        if (substr($rgbColor, 0, 4) == 'rgba') {
            $rgbColor = substr($rgbColor, 4);
            $rgbColor = RgbaColorParser::filterColor($rgbColor);
        }
        else if (substr($rgbColor, 0, 3) == 'rgb') {
            $rgbColor = substr($rgbColor, 3);
            $rgbColor = RgbaColorParser::filterColor($rgbColor);
        }

        $explodedColor = explode(',', $rgbColor);
        $bytes = AbstractColor::COLOR_BLACK;
        $numBytes = count($explodedColor);

        if ($numBytes == 3 || $numBytes == 4) {
            $bytes[0] = Ints::castInt($explodedColor[0]);
            $bytes[1] = Ints::castInt($explodedColor[1]);
            $bytes[2] = Ints::castInt($explodedColor[2]);

            if ($numBytes == 4) {
                $float = Floats::toFloat($explodedColor[3]);

                if ($float > RgbaColor::MAX_ALPHA) {

                }

                $bytes[3] = round(Floats::toFloat($explodedColor[3]) * 255, 0);
                $bytes[3] = Ints::toInt($bytes[3]);

                if (!self::validByte($bytes[3])) {
                    throw new InvalidArgumentException("Invalid CSS color alpha value. Must be a floating value between 0.0 an 1.0. Input color: $rgbColor");
                }
            }

            foreach ($bytes as $pos => $byte) {
                if (!self::validByte($bytes[$pos])) {
                    throw new InvalidArgumentException("Invalid CSS color byte $pos ${bytes[$pos]} in color $rgbColor");
                }
            }
        }
        else {
            throw new InvalidArgumentException("Invalid CSS color: $rgbColor");
        }

        return $bytes;
    }

    private static function arrayHasAllKeys(array $input, array $keys)
    {
        if (count($input) != count($keys)) {
            return false;
        }

        foreach ($keys as $pos => $key) {
            if (!Arrays::isDefined($input, $key)) {
                return false;
            }
        }

        return true;
    }

    private static function arrayCopyTo(array $input, $keys, array &$output)
    {
        $index = 0;
        foreach ($keys as $pos => $key) {
            Arrays::set($output, $index, $input[$key]);
            $index++;
        }
    }

    /**
     * Parses an array of color components and returns an array with RGBA bytes
     *
     * The supported array formats are:
     * + A simple array with either three (RGB) or four (RGBA) elements.
     * + An array with keys R,G,B,A.
     * + An array with keys red, green, blue, alpha.
     *
     * returns an array with components R,G,B,A using numeric indexes.
     *
     * @param array $array
     * @return array
     */
    public static function parseArray(array $array)
    {
        $length = count($array);
        $bytes = AbstractColor::COLOR_BLACK;

        if (self::arrayHasAllKeys($array, RgbaColor::RGB_POSITIONS)) {
            self::arrayCopyTo($array, RgbaColor::RGB_POSITIONS, $bytes);
        }
        else if (self::arrayHasAllKeys($array, RgbaColor::RGB_KEYS)) {
            self::arrayCopyTo($array, RgbaColor::RGB_KEYS, $bytes);
        }
        else if (self::arrayHasAllKeys($array, RgbaColor::RGB_NAMES)) {
            self::arrayCopyTo($array, RgbaColor::RGB_NAMES, $bytes);
        }
        else if (self::arrayHasAllKeys($array, RgbaColor::RGBA_POSITIONS)) {
            self::arrayCopyTo($array, RgbaColor::RGBA_POSITIONS, $bytes);
        }
        else if (self::arrayHasAllKeys($array, RgbaColor::RGBA_KEYS)) {
            self::arrayCopyTo($array, RgbaColor::RGBA_KEYS, $bytes);
        }
        else if (self::arrayHasAllKeys($array, RgbaColor::RGBA_NAMES)) {
            self::arrayCopyTo($array, RgbaColor::RGBA_NAMES, $bytes);
        }
        else {
            throw new InvalidArgumentException("Invalid color array");
        }

        if ($length >= 4 && self::validAlpha($bytes[3])) {
            $bytes[3] = Ints::toInt(round($bytes[3] * AbstractColor::MAX_BYTE_VALUE, 0));
        }

        foreach ($bytes as $pos => $byte) {
            if (!self::validByte($bytes[$pos])) {
                throw new InvalidArgumentException('Invalid byte value in color: ' + implode(',', $array));
            }
        }

        return $bytes;
    }

    /**
     * Converts the number into rgba color bytes.
     *
     * If the provided value is in range [0, 255] the returned color will contain that value on each byte.
     *
     * @param int|float|string $number Number that represents the color.
     * @param bool $ignoreAlpha If true the color is parsed ignoring the alpha component (default)
     * @return RGBColor
     * @throws InvalidArgumentException If the value is not a valid RGB/A color value.
     */
    public static function parseNumber($number, $ignoreAlpha = true)
    {
        $number = self::filterColor($number);
        $integer = Numbers::castInt($number);

        if ($number >= AbstractColor::MIN_BYTE_VALUE && $number <= AbstractColor::MAX_BYTE_VALUE) {
            return [$integer, $integer, $integer, AbstractColor::MAX_BYTE_VALUE];
        }

        $hex = '';

        if ($ignoreAlpha) {
            $hex = sprintf("%06x", $number);
        }
        else {
            $hex = sprintf("%08x", $number);
        }

        return self::parseHex($hex);
    }

    protected static function filterColor($color)
    {
        return strtolower(trim($color, self::TRIM_CHARS));
    }
}
