<?php

namespace phbrick\graphics;

/**
 * RGBA color.
 *
 * Models an RGBA color that can also be expressed as an RGB one.
 *
 */
class RgbaColor extends AbstractColor
{
    const RED_POS = 0;
    const RED_NAME = 'red';
    const RED_KEY = 'r';

    const GREEN_POS = 1;
    const GREEN_NAME = 'green';
    const GREEN_KEY = 'g';

    const BLUE_POS = 2;
    const BLUE_NAME = 'blue';
    const BLUE_KEY = 'b';

    const ALPHA_POS = 3;
    const ALPHA_NAME = 'alpha';
    const ALPHA_KEY = 'a';

    const RGB_POSITIONS = [self::RED_POS, self::GREEN_POS, self::BLUE_POS];
    const RGB_KEYS = [self::RED_KEY, self::GREEN_KEY, self::BLUE_KEY];
    const RGB_NAMES = [self::RED_NAME, self::GREEN_NAME, self::BLUE_NAME];

    const RGBA_POSITIONS = [self::RED_POS, self::GREEN_POS, self::BLUE_POS, self::ALPHA_POS];
    const RGBA_KEYS = [self::RED_KEY, self::GREEN_KEY, self::BLUE_KEY, self::ALPHA_KEY];
    const RGBA_NAMES = [self::RED_NAME, self::GREEN_NAME, self::BLUE_NAME, self::ALPHA_NAME];

    const TYPE_RGB = 'rgb';
    const TYPE_RGBA = 'rgba';

    const MIN_ALPHA = 0.0;
    const MAX_ALPHA = 1.0;
    const DEFAULT_ALPHA = self::MAX_ALPHA;

    /**
     *
     * @param RgbaColor $color
     * @return RgbaColor
     */
    public static function castRGBA(RgbaColor $color)
    {
        return $color;
    }

    /**
     * RgbaColor constructor.
     *
     * By default initializes the color to black.
     *
     * @param int|string $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     */
    public function __construct($color = null)
    {
        $bytes = self::COLOR_BLACK;

        if ($color !== null) {
            $bytes = RgbaColorParser::parse($color);
        }

        parent::__construct($bytes);
    }

    /**
     * Sets
     * @param int $red
     */
    public function setRed($red)
    {
        $this->setByte(self::RED_POS, $red);
    }

    /**
     * Gets
     * @return int
     */
    public function getRed()
    {
        return $this->getByte(self::RED_POS);
    }

    /**
     * Sets
     * @param int $green
     */
    public function setGreen($green)
    {
        $this->setByte(self::GREEN_POS, $green);
    }

    /**
     * Gets
     * @return int
     */
    public function getGreen()
    {
        return $this->getByte(self::GREEN_POS);
    }

    /**
     * Sets
     * @param int $blue
     */
    public function setBlue($blue)
    {
        $this->setByte(self::BLUE_POS, $blue);
    }

    /**
     * Gets
     * @return int
     */
    public function getBlue()
    {
        return $this->getByte(self::BLUE_POS);
    }

    /**
     * Gets
     * @return int
     */
    public function getAlpha()
    {
        return $this->getByte(self::ALPHA_POS);
    }

    /**
     * Sets
     * @param int $alpha
     */
    public function setAlpha($alpha)
    {
        $this->setByte(self::ALPHA_POS, $alpha);
    }

    public function getAlphaFloat()
    {
        $alpha = (float)$this->getAlpha() / self::MAX_BYTE_VALUE;
        $alpha = round($alpha, 2);
        return $alpha;
    }

    public function getAlphaInteger()
    {
        $alpha = (float)$this->getAlphaFloat() * 100;
        $alpha = (int)round($alpha, 0);
        return $alpha;
    }

    public function __clone()
    {
    }

    /**
     * Gets the color as an HTML color value with format rgb(r, g, b)
     *
     * @return string
     */
    public function toRgb()
    {
        $bytes = $this->toRgbArray();
        return sprintf(self::TYPE_RGB . "(%d,%d,%d)", ...$bytes);
    }

    /**
     * Gets the color as an HTML color value with format rgb(r, g, b, a)
     *
     * @return string
     */
    public function toRgba()
    {
        $bytes = $this->toRgbaArray();
        return sprintf(self::TYPE_RGBA . "(%d,%d,%d,%d)", ... $bytes);
    }

    /**
     * Gets all color bytes
     * @return array
     */
    public function toArray()
    {
        return $this->toRgbaArray();
    }

    /**
     * Gets only RGB color bytes
     * @return array
     */
    public function toRgbArray()
    {
        return $this->getBytes([self::RED_POS, self::GREEN_POS, self::BLUE_POS]);
    }

    /**
     * Gets all RGBA color bytes.
     *
     * @return array
     */
    public function toRgbaArray()
    {
        return $this->getBytes([self::RED_POS, self::GREEN_POS, self::BLUE_POS, self::ALPHA_POS]);
    }

    /**
     * @param $color
     * @return RgbaColor
     */
    public static function parse($color)
    {
        $cls = self::cls();
        $bytes = RgbaColorParser::parse($color);
        return new $cls($bytes);
    }
}
