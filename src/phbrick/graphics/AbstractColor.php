<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use http\Exception\InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\collection\Arrays;
use phbrick\exceptions\InvalidOperationException;
use phbrick\ICloneable;
use phbrick\types\Ints;

/**
 * Class AbstractColor
 * @package phbrick\graphics
 */
abstract class AbstractColor
    extends BaseStrictClass
    implements ICloneable
{
    const MAX_BYTE_VALUE = 255;
    const MIN_BYTE_VALUE = 0;
    const COLOR_WHITE = [self::MAX_BYTE_VALUE, self::MAX_BYTE_VALUE, self::MAX_BYTE_VALUE, self::MAX_BYTE_VALUE];
    const COLOR_BLACK = [self::MIN_BYTE_VALUE, self::MIN_BYTE_VALUE, self::MIN_BYTE_VALUE, self::MAX_BYTE_VALUE];

    protected $bytes = [];
    private $maxIndex = 0;

    protected function __construct(array $bytes, $maxSize = null)
    {
        if (count($bytes) == 0) {
            throw new InvalidArgumentException("Parameter \$bytes cannot be empty");
        }

        $this->bytes = [];

        if ($maxSize === null) {
            $this->maxIndex = count($bytes) - 1;
        }
        else {
            $this->maxIndex = Ints::castInt($maxSize);
        }

        foreach ($bytes as $index => $byte) {
            if (is_int($index)) {
                $this->setByte($index, $byte);
            }
            else {
                throw new \InvalidArgumentException("Invalid key '$index', only numeric keys are allowed");
            }
        }
    }

    public function __toString()
    {
        return $this->toHex(true);
    }

    /**
     * gets the color as hexadecimal
     *
     * @param bool $prependHash
     * @return string
     */
    public function toHex($prependHash = false)
    {
        $hexString = $prependHash ? '#' : '';

        for ($index = 0; $index < count($this->bytes); $index++) {
            $hexString .= sprintf('%02x', $this->bytes[$index]);
        }

        return $hexString;
    }

    /**
     * Gets the color as an integer
     *
     * @return float|int
     */
    public function toInt()
    {
        if (count($this->bytes) > Ints::getByteCount()) {
            throw new InvalidOperationException('Cannot convert Color to integer, number of bytes exceeds platform integer size');
        }

        return hexdec($this->toHex(false));
    }

    /**
     * Gets an array with all the bytes used to represent the color
     *
     * @return array
     */
    public function toArray()
    {
        return array_slice($this->bytes, 0, $this->maxIndex);
    }

    /**
     * Sets a byte
     * @param $pos
     * @param $byte
     * @return AbstractColor
     */
    protected function setByte($pos, $byte)
    {
        Ints::assertRange($byte, self::MIN_BYTE_VALUE, self::MAX_BYTE_VALUE);
        Arrays::set($this->bytes, $pos, $byte, $this->maxIndex);
        return $this;
    }

    /**
     * Iterates the input bytes and assign it over current ones
     *
     * @param array $bytes
     */
    protected function setBytes(array $bytes)
    {
        foreach ($bytes as $pos => $byte) {
            $this->setByte($pos, $byte);
        }
    }

    /**
     * Gets a byte
     *
     * @param $pos
     * @return int
     */
    protected function getByte($pos)
    {
        return Arrays::get($this->bytes, $pos, $this->maxIndex);
    }

    /**
     * Gets the given byte positions and returns them into a new array
     *
     * The returned array contains the bytes in the same order they were specified.
     *
     * @param array $positions Optional array of positions to get
     * @return array
     */
    protected function getBytes(array $positions = null)
    {
        $result = [];

        if ($positions === null) {
            $result = $this->bytes;
        }
        else {

            foreach ($positions as $pos) {
                $result[] = $this->getByte($pos);
            }
        }

        return $result;
    }

    /**
     * Copies all color bytes from given color.
     *
     * The maximum byte index is also copied
     * @param AbstractColor $color
     */
    public function copy(AbstractColor $color)
    {
        $this->setBytes($color->getBytes());
        $this->maxIndex = $color->maxIndex;
    }

    private function __clone()
    {
        // Nothing to do
    }
}
