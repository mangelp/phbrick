<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\ICloneable;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Numbers;
use phbrick\types\Types;

/**
 * Class Rectangle
 *
 * Defines a rectangle as the position of the top-left corner and the width/height of the rectangle.
 *
 * Access to the position of a corner other than the top-left one creates a new point object from the original one
 * increasing the coordinates using the size to get the corner position.
 *
 * To manipulate the rectangle use the size and point objects to alter the size and location of the rectangle.
 *
 * @package phbrick\graphics
 */
class Rectangle extends BaseStrictClass implements ICloneable
{
    use SafeStringRenderTrait;

    /**
     * @param Rectangle $rectangle
     * @return Rectangle
     */
    public static function castRectangle(Rectangle $rectangle)
    {
        return $rectangle;
    }

    /**
     * @var Size
     */
    private $size = null;
    /**
     * @var Point
     */
    private $point = null;

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Size $size
     * @return Rectangle
     */
    public function setSize(Size $size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Sets the reference point for the rectangle top-left corner.
     *
     * @param Point $point
     * @return Rectangle
     */
    public function setPoint(Point $point)
    {
        $this->point = $point;
        return $this;
    }

    /**
     * Returns the reference point for the rectangle.
     *
     * @return Point
     */
    public function getTopLeft()
    {
        return clone $this->point;
    }

    /**
     * Gets a new Point instance that represents the top-right corner of the rectangle.
     *
     * @return Point
     */
    public function getTopRight()
    {
        return $this->point->offset($this->size->getWidth() - 1, 0);
    }

    /**
     * Gets a new Point instance that represents the bottom-left corner of the rectangle.
     *
     * @return Point
     */
    public function getBottomLeft()
    {
        return $this->point->offset(0, $this->size->getHeight() - 1);
    }

    /**
     * Gets a new Point instance that represents the bottom-right corner of the rectangle.
     *
     * @return Point
     */
    public function getBottomRight()
    {
        return $this->point->offset($this->size->getWidth() - 1, $this->size->getHeight() - 1);
    }

    /**
     * Rectangle constructor.
     *
     * If a size is not provided the rectangle will have 0 size.
     * If a point is not provided the rectangle will have 0,0 position.
     *
     * @param Point|array|Rectangle|null $point
     * @param Size|array|Rectangle|null $size
     *
     */
    public function __construct($size = null, $point = null)
    {
        $this->setSize(Size::fromValue($size));
        $this->setPoint(Point::fromValue($point));
    }

    /**
     * Returns true if the current rectangle contains any point from the second area
     *
     * @param Rectangle $area
     * @return bool
     */
    public function intersects(Rectangle $area)
    {
        return $this->containsPoint($area->getTopLeft())
            || $this->containsPoint($area->getTopRight())
            || $this->containsPoint($area->getBottomLeft())
            || $this->containsPoint($area->getBottomRight());
    }

    /**
     * Checks if the given point is contained in the current area
     *
     * @param Point $point
     * @return bool
     */
    public function containsPoint(Point $point)
    {
        $x = $this->getPoint()->getX();
        $y = $this->getPoint()->getY();
        $w = $this->getSize()->getWidth();
        $h = $this->getSize()->getHeight();
        $x2 = $x + $w;
        $y2 = $y + $h;

        $a = $point->getX();
        $b = $point->getY();

        return $x <= $a && $y <= $b && $x2 >= $a && $y2 >= $b;
    }

    /**
     * Checks of the current area contains all the points from another area
     *
     * @param Rectangle $area
     * @return bool
     */
    public function containsRectangle(Rectangle $area)
    {
        return $this->containsPoint($area->getTopLeft())
            && $this->containsPoint($area->getTopRight())
            && $this->containsPoint($area->getBottomLeft())
            && $this->containsPoint($area->getBottomRight());
    }

    /**
     * Returns a Rectangle with all the points in the intersection of the current area with the given one
     *
     * @param Rectangle $area
     * @return Rectangle
     */
    public function clip(Rectangle $area)
    {
        $x1a = $this->getPoint()->getX();
        $y1a = $this->getPoint()->getY();
        $x2a = $x1a + $this->getSize()->getWidth();
        $y2a = $y1a + $this->getSize()->getHeight();

        $x1b = $area->getPoint()->getX();
        $y1b = $area->getPoint()->getY();
        $x2b = $x1b + $area->getSize()->getWidth();
        $y2b = $y1b + $area->getSize()->getHeight();

        $x1c = $x1a < $x1b ? $x1b : $x1a;
        $y1c = $y1a < $y1b ? $y1b : $y1a;
        $x2c = $x2a < $x2b ? $x2a : $x2b;
        $y2c = $y2a < $y2b ? $y2a : $y2b;

        /** @var Rectangle $result */
        $result = self::instance([$x1c, $y1c], [$x2c - $x1c, $y2c - $y1c]);
        return $result;
    }

    /**
     * Returns an array with the points that define the area using the specified names for the keys
     *
     * @param string|null $keyNames Names for the point keys as explained in Point::toArray()
     * @return array with the area points
     * @see Point::toArray() for a description of the allowed key names
     */
    public function toArray($keyNames = null)
    {
        return [
            $this->getTopLeft()->toArray($keyNames),
            $this->getTopRight()->toArray($keyNames),
            $this->getBottomRight()->toArray($keyNames),
            $this->getBottomLeft()->toArray($keyNames),
        ];
    }

    /**
     * Returns an array with the points that define the area
     *
     * @return array with the area points
     */
    public function toPointArray()
    {
        return [
            $this->getTopLeft(),
            $this->getTopRight(),
            $this->getBottomRight(),
            $this->getBottomLeft(),
        ];
    }

    /**
     * Returns the point of the center in the area
     *
     * @return Point
     */
    public function getCenter()
    {
        $x = $this->getPoint()->getX() + $this->getSize()->getWidth() / 2;
        $y = $this->getPoint()->getY() + $this->getSize()->getHeight() / 2;

        $center = $this->createPoint($x, $y);

        return $center;
    }

    public function toString()
    {
        return implode("|", $this->toPointArray());
    }

    /**
     * @param float $x
     * @param float $y
     * @return Point
     */
    protected function createPoint($x, $y)
    {
        return new Point($x, $y);
    }

    private function __clone()
    {
        $this->size = clone $this->size;
        $this->point = clone $this->point;
    }

    /**
     * Gets if the rectangle has zero size
     * @return bool
     */
    public function isZero()
    {
        return $this->size == null || $this->size->isZero();
    }

    /**
     * Creates a new Rectangle instance from the given value
     *
     * @param mixed $value
     * @return Rectangle|null
     */
    public static function fromValue($value)
    {
        /**
         * @var Rectangle
         */
        $result = null;

        if ($value == null) {
            $result = self::instance();
        }
        else if (is_numeric($value)) {
            $value = Numbers::castFloat($value);
            $result = self::instance($value, $value);
        }
        else if ($value instanceof Size) {
            $result = self::instance($value, null);
        }
        else if ($value instanceof Point) {
            $result = self::instance(null, $value);
        }
        else if ($value instanceof Rectangle) {
            $result = clone $value;
        }
        else if (is_array($value) && in_array(count($value), [1, 2, 4])) {
            $num = count($value);

            if ($num == 1 && isset($value[0])) {
                $result = Rectangle::fromValue($value[0]);
            }
            else if ($num == 1 && isset($value['size'])) {
                $result = self::instance($value['size']);
            }
            else if ($num == 1 && isset($value['point'])) {
                $result = self::instance(null, $value['point']);
            }
            else if ($num == 2 && isset($value[0]) && isset($value[1])) {
                $result = self::instance($value[0], $value[1]);
            }
            else if ($num == 2 && isset($value['size']) && isset($value['point'])) {
                $result = self::instance($value['size'], $value['point']);
            }
            else if ($num == 4 && isset($value[0]) && isset($value[1]) && isset($value[2]) && isset($value[3])) {
                $result = self::instance([$value[0], $value[1]], [$value[2], $value[3]]);
            }
            else if ($num == 4 && isset($value['top']) && isset($value['left']) && isset($value['width']) && isset($value['height'])) {
                $result = self::instance([$value['width'], $value['height']], [$value['top'], $value['left']]);
            }
            else if ($num == 4 && isset($value['x']) && isset($value['y']) && isset($value['w']) && isset($value['h'])) {
                $result = self::instance([$value['w'], $value['h']], [$value['x'], $value['y']]);
            }
            else {
                throw new InvalidArgumentException("Cannot create Rectangle instance from invalid array format");
            }
        }
        else {
            throw new InvalidArgumentException("Cannot create a Rectangle instance from type " . Types::getTypeName($value));
        }

        if ($result !== null) {
            return Rectangle::castRectangle($result);
        }
        else {
            return null;
        }
    }
}
