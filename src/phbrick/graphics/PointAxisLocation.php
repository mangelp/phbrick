<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\EnumerationClass;

/**
 * Class PointAxisLocation
 * @package phbrick\graphics
 */
class PointAxisLocation extends EnumerationClass
{
    const TOP_RIGHT = 1;
    const BOTTOM_RIGHT = 2;
    const BOTTOM_LEFT = 3;
    const TOP_LEFT = 4;
    const CENTER = 0;
    const X_AXIS = 5;
    const Y_AXIS = 6;
}
