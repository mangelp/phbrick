<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use phbrick\ICloneable;
use phbrick\string\SafeStringRenderTrait;
use phbrick\types\Bools;
use phbrick\types\Floats;
use phbrick\types\Numbers;
use phbrick\types\Types;

/**
 * Class Size models a distance in bi-dimensional cartesian space as one value per each axis.
 *
 * This class supports scaling using a factor and aspect ratio.
 *
 * Sizes cannot have negative values.
 *
 * @package phbrick\graphics
 */
class Size extends BaseStrictClass implements ICloneable, IMeasured
{
    use SafeStringRenderTrait;
    use MeasuredTrait;

    /**
     * @param Size $size
     * @return Size
     */
    public static function castSize(Size $size)
    {
        return $size;
    }

    /**
     * @var float
     */
    private $width = 0.0;

    /**
     * @var float
     */
    private $height = 0.0;

    /**
     * @var float
     */
    private $scale = 0.0;

    /**
     * @var bool
     */
    private $keepAspectRatio = false;

    /**
     * @var float
     */
    private $aspectRatio = 1.0;

    /**
     * Gets the width
     * @return float
     */
    public function getWidth()
    {
        return $this->scaleValue($this->width);
    }

    /**
     * Gets the width without scaling
     *
     * @return float
     */
    public function getRealWidth()
    {
        return $this->width;
    }

    /**
     * Sets the width
     * @param int|float $width
     * @return Size
     */
    public function setWidth($width)
    {
        $width = (float)Numbers::castNumber($width);

        if ($width < 0) {
            throw new InvalidArgumentException("Width cannot be negative: $width");
        }

        $width = $this->unScaleValue($width);

        $this->width = $width;

        if ($this->keepAspectRatio && ($this->aspectRatio == 0.0 || $this->width == 0.0 || $this->height == 0.0)) {
            $this->width = 0;
            $this->height = 0;
        }
        else if ($this->keepAspectRatio) {
            $this->height = round($width / $this->aspectRatio, 0, PHP_ROUND_HALF_DOWN);
        }
        else if ($this->width == 0.0 || $this->height == 0.0) {
            $this->aspectRatio = 0.0;
        }
        else {
            $this->aspectRatio = $this->width / $this->height;
        }

        return $this;
    }

    /**
     * Gets the height
     * @return float
     */
    public function getHeight()
    {
        return $this->scaleValue($this->height);
    }

    /**
     * Gets the height without scale
     *
     * @return float
     */
    public function getRealHeight()
    {
        return $this->height;
    }

    /**
     * Sets the height
     *
     * @param int|float $height
     * @return Size
     */
    public function setHeight($height)
    {
        $height = (float)Numbers::castNumber($height);

        if ($height < 0) {
            throw new InvalidArgumentException("Height cannot be negative: $height");
        }

        $height = $this->unScaleValue($height);

        $this->height = $height;

        if ($this->keepAspectRatio && ($this->aspectRatio == 0.0 || $this->height == 0.0 || $this->width == 0.0)) {
            $this->height = 0.0;
            $this->width = 0.0;
        }
        else if ($this->keepAspectRatio) {
            $this->width = round($height * $this->aspectRatio, 0, PHP_ROUND_HALF_DOWN);
        }
        else if ($this->width == 0.0 || $this->height == 0.0) {
            $this->aspectRatio = 0.0;
        }
        else {
            $this->aspectRatio = $this->width / $this->height;
        }

        return $this;
    }

    /**
     * Gets scale percentage as a float value.
     *
     * A value of 1.0 or 0.0 means that no scale is applied.
     *
     * @return float
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Sets scale percentage as a float value
     *
     * A value of 1.0 or 0.0 means that no scale is applied.
     *
     * @param float $scale
     * @return Size
     */
    public function setScale($scale)
    {
        $scale = (float)Numbers::castNumber($scale);

        if ($scale < 0.0) {
            throw new InvalidArgumentException("Invalid scale $scale. Must be a float greater or equal than 0");
        }

        $this->scale = (float)$scale;

        return $this;
    }

    /**
     * Gets if the aspect ratio is kept
     * @return bool
     */
    public function isKeepAspectRatio()
    {
        return $this->keepAspectRatio;
    }

    /**
     * Sets if the aspect ratio is kept or not.
     *
     * When this flag is set to true whenever the width or height are set the opposite value is updated to keep the
     * previous aspect ratio if both are non-zero. Otherwise they are both set to 0.
     *
     * @param bool $keepAspectRatio
     * @return Size
     * @throws InvalidArgumentException When any of the current width or height is zero.
     */
    public function setKeepAspectRatio($keepAspectRatio)
    {
        $this->keepAspectRatio = Bools::castBool($keepAspectRatio);

        if ($this->keepAspectRatio && ($this->width == 0.0 || $this->height == 0.0)) {
            throw new InvalidArgumentException("Cannot enable keepAspectRatio flag when the current width or height are 0");
        }

        return $this;
    }

    /**
     * Returns ratio between the width and height if both are set to non-zero values.
     *
     * If the width or the height are set to 0 the ratio returned will be 0 too.
     *
     * @return float
     */
    public function getAspectRatio()
    {
        return $this->aspectRatio;
    }

    /**
     * SizeProperty constructor initializes the properties with the provided values or default ones.
     *
     * @param float $width
     * @param float $height
     * @param float $scale A floating point number greater or equal than 0
     * @param bool $keepAspectRatio If true changing the width or height updates the other value to keep the aspect ratio.
     * @param null $units
     */
    public function __construct($width = null, $height = null, $scale = null, $keepAspectRatio = null, $units = null)
    {
        if ($width !== null) {
            $this->setWidth($width);
        }

        if ($height !== null) {
            $this->setHeight($height);
        }

        if ($scale !== null) {
            $this->setScale($scale);
        }

        if ($keepAspectRatio !== null) {
            $this->setKeepAspectRatio($keepAspectRatio);
        }

        if ($units !== null) {
            $this->setUnits($units);
        }
    }

    /**
     * Applies the scale to the given value
     *
     * @param float $value
     * @return float
     */
    protected function scaleValue($value)
    {
        if ($this->scale == 0.0 || $this->scale == 1.0) {
            return $value;
        }

        $value *= $this->scale;

        return $value;
    }

    /**
     * Un-applies the scale to the given value
     *
     * @param float $value
     * @return float
     */
    protected function unScaleValue($value)
    {
        if ($this->scale == 0.0 || $this->scale == 1.0) {
            return $value;
        }

        $value /= $this->scale;

        return $value;
    }

    /**
     * Returns a new Size object whose width and height have been scaled by applying the given factor.
     *
     * Given that the same factor is applied to both the width and height the aspect ratio is kept.
     *
     * @param float $factor
     * @return Size
     */
    public function scale($factor)
    {
        $factor = Floats::castFloat($factor);
        /** @var Size $size */
        $size = self::instance($this->width * $factor, $this->height * $factor);

        return $size;
    }

    /**
     * Returns a new instance with the width/height added with the specified parameters.
     *
     * @param float $width
     * @param float $height
     * @return Size
     * @throws InvalidArgumentException If the resulting width or height is less than 0.
     */
    public function delta($width = 0.0, $height = 0.0)
    {
        /** @var Size $size */
        $size = self::instance($this->width + $width, $this->height + $height);

        return $size;
    }

    /**
     * Returns the current position components into an array.
     *
     * @param string|null $keyNames Format for position key names. Allowed values: w-h, width-height, null/none.
     * @return array The array with two elements with the width-height size components.
     */
    public function toArray($keyNames = null)
    {
        $wKey = 0;
        $hKey = 1;

        switch (strtolower(trim($keyNames))) {
            case 'w-h':
                $wKey = 'w';
                $hKey = 'h';
                break;
            case 'width-height':
                $wKey = 'width';
                $hKey = 'height';
                break;
        }

        $result = [$wKey => $this->getWidth(), $hKey => $this->getHeight()];

        return $result;
    }

    /**
     * Returns the size with the coordinates converted using the given units
     *
     * @param string|null $units
     * @param UnitsConverter|null $converter
     * @param string|int|float|null $precision
     * @param string|int|float|null $dpm
     * @return Size
     */
    public function toSize($units = null, UnitsConverter $converter = null, $precision = null, $dpm = null)
    {
        $other = clone $this;

        if ($units != null) {
            $other->setUnits($units);
            $other->setWidth($this->convert($this->getWidth(), $other, $converter, $precision, $dpm));
            $other->setHeight($this->convert($this->getHeight(), $other, $converter, $precision, $dpm));
        }

        return $other;
    }

    /**
     * Gets if the current size is zero
     *
     * @return bool
     */
    public function isZero()
    {
        return $this->width == 0 && $this->height == 0;
    }

    public function toString()
    {
        return sprintf("%fx%f", $this->width, $this->height);
    }

    /**
     * Gets the current area of the box using the scale if set.
     *
     * @return float
     */
    public function getArea()
    {
        return $this->getWidth() * $this->getHeight();
    }

    /**
     * Returns a new size from the current one with the given width and the height calculated to keep the aspect
     * ratio.
     *
     * @param float $width
     * @return Size
     */
    public function widen($width)
    {
        $width = Numbers::castFloat($width);
        $size = clone $this;
        $size->setKeepAspectRatio(true);
        $size->setWidth($width);

        return $size;
    }

    /**
     * Returns a new size from the current one with the given height and the width calculated to keep the aspect
     * ratio.
     *
     * @param float $height
     *
     * @return Size
     */
    public function heighten($height)
    {
        $height = Numbers::castFloat($height);
        $size = clone $this;
        $size->setKeepAspectRatio(true);
        $size->setHeight($height);

        return $size;
    }

    /**
     * Creates a Size instance from an instance of Size, Rectangle, an array, null or a number.
     *
     * @param Size|Rectangle|array|int|float|null $value
     * @return Size
     */
    public static function fromValue($value)
    {
        /** @var Size $size */
        $size = null;

        if ($value === null) {
            $size = self::instance(0.0, 0.0);
        }
        else if (is_numeric($value)) {
            $value = Numbers::castFloat($value);
            $size = self::instance($value, $value);
        }
        else if (is_a($value, Size::class)) {
            $size = clone $value;
        }
        else if (is_a($value, Rectangle::class)) {
            $size = clone $value->getSize();
        }
        else if (is_array($value) && count($value) == 2) {
            if (isset($value['width']) && isset($value['height'])) {
                $size = self::instance($value['width'], $value['height']);
            }
            else if (isset($value['w']) && isset($value['h'])) {
                $size = self::instance($value['w'], $value['h']);
            }
            else if (isset($value[0]) && isset($value[1])) {
                $size = self::instance($value[0], $value[1]);
            }
            else {
                throw new InvalidArgumentException("Cannot create Size instance from invalid array format");
            }
        }
        else {
            throw new InvalidArgumentException("Cannot create a Size instance from type " . Types::getTypeName($value));
        }

        return $size;
    }

    /**
     * Returns true if the size is null or an empty size instance and false otherwise.
     *
     * @param mixed $size
     * @return bool
     */
    public static function isEmpty($size)
    {
        return $size === null || ($size instanceof Size && $size->isZero());
    }
}
