<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\EnumerationClass;

/**
 * Class MeasureUnits
 * @package phbrick\graphics
 */
class MeasureUnits extends EnumerationClass
{
    const MILLIMETER = 'mm';
    const PIXEL = 'px';
    const POINT = 'pt';
}
