<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\exceptions\TypeException;
use phbrick\types\Ints;
use phbrick\types\Numbers;

/**
 * Class UnitsConverter models the conversion between units using configurable factors.
 *
 * @package phbrick\graphics
 */
class UnitsConverter
{
    /**
     * @var UnitsConverter
     */
    private static $defaultConverter = null;

    /**
     * Gets a default converter.
     *
     * @return UnitsConverter
     */
    public static function getDefaultConverter()
    {
        if (self::$defaultConverter === null) {
            self::$defaultConverter = new UnitsConverter();
        }

        return self::$defaultConverter;
    }

    const MILLIMETERS_PER_INCH = 25.4;
    const POINTS_PER_INCH = 72;
    const POINTS_PER_MILLIMETER = 2.834645669;
    const DEFAULT_PIXELS_PER_POINT = 1.33554437418806;
    const DEFAULT_DPI = 96;
    const DEFAULT_DPM = 3.779527559;
    const DEFAULT_PRECISION = 16;

    /**
     * @var int
     */
    private $precision = self::DEFAULT_PRECISION;

    /**
     * Rounding mode.
     *
     * The default one is the one that makes most sense.
     *
     * @var int
     */
    private $roundMode = PHP_ROUND_HALF_UP;
    /**
     * @var int
     */
    private $dpm = self::DEFAULT_DPM;

    /**
     * @return int
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * @param int $precision
     */
    public function setPrecision($precision)
    {
        $this->precision = Ints::castInt($precision);
    }

    /**
     * Gets the rounding mode.
     *
     * Defaults to rounding half up.
     *
     * @return int
     */
    public function getRoundMode()
    {
        return $this->roundMode;
    }

    /**
     * Sets the rounding mode.
     *
     * Must be one of PHP_ROUND_* constants.
     *
     * @param int $roundMode
     */
    public function setRoundMode($roundMode)
    {
        $this->roundMode = $roundMode;
    }

    /**
     * @return int
     */
    public function getDpm()
    {
        return $this->dpm;
    }

    /**
     * @param int|float $dpm
     */
    public function setDpm($dpm)
    {
        $dpm = (float)Numbers::castNumber($dpm);
        $dpm = round($dpm, $this->precision, $this->roundMode);

        if ($dpm <= 0) {
            throw new InvalidArgumentException("DPM cannot be less or equal than 0");
        }

        $this->dpm = (int)$dpm;
    }

    public function setDpi($dpi)
    {
        $dpm = $this->convertDpiToDpm($dpi);
        $this->setDpm($dpm);
    }

    public function getDpi()
    {
        $dpi = $this->convertDpiToDpm($this->dpm);

        return $dpi;
    }

    /**
     *
     * @param int|float|null $precision
     * @param int|float|null $dpm
     * @return float
     */
    public function getPixelsPerPoint($precision = null, $dpm = null)
    {
        $dpm = $this->prepareDpm($dpm);
        $precision = $this->preparePrecision($precision);
        $ppp = $dpm / self::POINTS_PER_MILLIMETER;
        $ppp = (float)round($ppp, $precision, $this->roundMode);

        return $ppp;
    }

    /**
     * @param null|float|int $dpm
     * @return float|int
     */
    protected function prepareDpm($dpm = null)
    {
        if ($dpm !== null) {
            $dpm = Numbers::castNumber($dpm);
        }
        else {
            $dpm = $this->dpm;
        }

        return $dpm;
    }

    /**
     * @param null|float|int $precision
     * @return int
     */
    protected function preparePrecision($precision = null)
    {
        if ($precision !== null) {
            $precision = Numbers::castNumber($precision);
        }
        else {
            $precision = $this->precision;
        }

        return (int)$precision;
    }

    /**
     * @param int|float $dpi
     * @param null|int|float $precision
     * @return float
     */
    public function convertDpiToDpm($dpi, $precision = null)
    {
        $precision = $this->preparePrecision($precision);
        $dpi = (float)Numbers::castNumber($dpi);
        $dpm = $dpi / self::MILLIMETERS_PER_INCH;
        $dpm = (float)round($dpm, $precision, $this->roundMode);

        return $dpm;
    }

    /**
     * @param int|float|string $dpi
     * @return int
     */
    public function fromDpiToDpm($dpi)
    {
        return $this->convertDpiToDpm($dpi, 0);
    }

    /**
     * @param int|float $dpm
     * @param null|int|float $precision
     * @return float
     */
    public function convertDpmToDpi($dpm, $precision = null)
    {
        $precision = $this->preparePrecision($precision);
        $dpm = (float)Numbers::castNumber($dpm);
        $dpi = $dpm * self::MILLIMETERS_PER_INCH;
        $dpi = (float)round($dpi, $precision, $this->roundMode);

        return $dpi;
    }

    /**
     * @param int|float|string $dpm
     * @return float
     */
    public function fromDpmToDpi($dpm)
    {
        return (int)$this->convertDpmToDpi($dpm, 0);
    }

    /**
     * @param int|float $pixels
     * @param null|int|float $precision
     * @param null|int|float $dpm
     * @return float
     */
    public function convertPixelsToPoints($pixels, $precision = null, $dpm = null)
    {
        $pixels = Numbers::castNumber($pixels);
        $points = $pixels / $this->getPixelsPerPoint($dpm);
        $precision = $this->preparePrecision($precision);
        $points = (float)round($points, $precision, $this->roundMode);

        return $points;
    }

    /**
     * @param int|float|string $px
     * @return float
     */
    public function fromPxToPt($px)
    {
        return (float)$this->convertPixelsToPoints($px, 0);
    }

    /**
     * @param int|float $points
     * @param null $precision
     * @param null|int|float $dpm
     * @return float
     */
    public function convertPointsToPixels($points, $precision = null, $dpm = null)
    {
        $points = Numbers::castNumber($points);
        $pixels = $points * $this->getPixelsPerPoint($dpm);
        $precision = $this->preparePrecision($precision);
        $pixels = (float)round($pixels, $precision, $this->roundMode);

        return $pixels;
    }

    /**
     * @param int|float|string $pt
     * @return int
     */
    public function fromPtToPx($pt)
    {
        return (int)$this->convertPointsToPixels($pt, 0);
    }

    /**
     * @param int|float $millimeters
     * @param null $precision
     * @param int|float|null $dpm
     * @return float
     */
    public function convertMillimetersToPixels($millimeters, $precision = null, $dpm = null)
    {
        $millimeters = Numbers::castNumber($millimeters);
        $dpm = $this->prepareDpm($dpm);
        $pixels = $millimeters * $dpm;
        $precision = $this->preparePrecision($precision);
        $pixels = (float)round($pixels, $precision, $this->roundMode);

        return $pixels;
    }

    /**
     * @param int|float|string $mm
     * @return int
     */
    public function fromMmToPx($mm)
    {
        return (int)$this->convertMillimetersToPixels($mm, 0);
    }

    /**
     * Converts pixels to millimeters
     *
     * @param int $pixels
     * @param int|float $dpm
     * @param int $precision
     * @return float
     */
    public function convertPixelsToMillimeters($pixels, $precision = null, $dpm = null)
    {
        $pixels = Numbers::castNumber($pixels);
        $dpm = $this->prepareDpm($dpm);
        $millimeters = $pixels / $dpm;
        $precision = $this->preparePrecision($precision);
        $millimeters = (float)round($millimeters, $precision, $this->roundMode);

        return $millimeters;
    }

    /**
     * @param $px
     * @return float
     */
    public function fromPxToMm($px)
    {
        return (float)$this->convertPixelsToMillimeters($px, 2);
    }

    /**
     * @param int|float $millimeters
     * @param null $precision
     * @return float
     */
    public function convertMillimetersToPoints($millimeters, $precision = null)
    {
        $millimeters = Numbers::castNumber($millimeters);
        $points = $millimeters * self::POINTS_PER_MILLIMETER;
        $precision = $this->preparePrecision($precision);
        $points = (float)round($points, $precision, $this->roundMode);

        return $points;
    }

    /**
     * @param int|float|string $mm
     * @return int
     */
    public function fromMmToPt($mm)
    {
        return (int)$this->convertMillimetersToPoints($mm, 0);
    }

    /**
     * Converts pixels to millimeters
     *
     * @param int $points
     * @param int $precision
     * @return float
     */
    public function convertPointsToMillimeters($points, $precision = null)
    {
        $points = Numbers::castNumber($points);
        $millimeters = $points / self::POINTS_PER_MILLIMETER;
        $precision = $this->preparePrecision($precision);
        $millimeters = (float)round($millimeters, $precision, $this->roundMode);

        return $millimeters;
    }

    /**
     * @param $pt
     * @return float
     */
    public function fromPtToMm($pt)
    {
        return (float)$this->convertPointsToMillimeters($pt, 0);
    }

    /**
     * Alters the given points amount by assuming they are specified for 96 dpi and recalculating for the
     * current dpi setting.
     *
     * @param int|float $pt
     * @param int|float|null $precision
     * @param int|float|null $dpm DPM to apply.
     * @param int|float|null $pointDpm DPM used to calculate the given points amount.
     * @return float
     */
    public function fromPtToPt($pt, $precision = null, $dpm = null, $pointDpm = null)
    {
        $pt = Numbers::castFloat($pt);
        $precision = $this->preparePrecision($precision);
        $dpm = $this->prepareDpm($dpm);

        if ($pointDpm === null) {
            $pointDpm = self::DEFAULT_DPM;
        }
        else {
            $pointDpm = Numbers::castFloat($pointDpm);
        }

        $pt = ($pt / $pointDpm) * $dpm;
        $pt = (float)round($pt, $precision, $this->roundMode);

        return $pt;
    }

    /**
     * @param string|int|float $value
     * @param string $fromUnits
     * @param string $toUnits
     * @param string|int|float|null $precision
     * @param string|int|float|null $dpm
     * @return float
     * @throws TypeException
     * @internal param UnitsConverter $converter
     */
    public function convert($value, $fromUnits, $toUnits, $precision = null, $dpm = null)
    {
        MeasureUnits::assertValue($fromUnits);
        MeasureUnits::assertValue($toUnits);

        $value = Numbers::castFloat($value);

        switch ($fromUnits) {
            case MeasureUnits::PIXEL:
                switch ($toUnits) {
                    case MeasureUnits::PIXEL:
                        return $value;
                        break;
                    case MeasureUnits::POINT:
                        return $this->convertPixelsToPoints($value, $precision, $dpm);
                        break;
                    case MeasureUnits::MILLIMETER:
                        return $this->convertPixelsToMillimeters($value, $precision, $dpm);
                        break;
                    default:
                        throw new InvalidArgumentException("Unsupported units $fromUnits");
                }
                break;
            case MeasureUnits::POINT:
                switch ($toUnits) {
                    case MeasureUnits::PIXEL:
                        return $this->convertPointsToPixels($value, $precision, $dpm);
                        break;
                    case MeasureUnits::POINT:
                        return $value;
                        break;
                    case MeasureUnits::MILLIMETER:
                        return $this->convertPointsToMillimeters($value, $precision);
                        break;
                    default:
                        throw new InvalidArgumentException("Unsupported units $fromUnits");
                }
                break;
            case MeasureUnits::MILLIMETER:
                switch ($toUnits) {
                    case MeasureUnits::PIXEL:
                        return $this->convertMillimetersToPixels($value, $precision, $dpm);
                        break;
                    case MeasureUnits::POINT:
                        return $this->convertMillimetersToPoints($value, $precision);
                        break;
                    case MeasureUnits::MILLIMETER:
                        return $value;
                        break;
                    default:
                        throw new InvalidArgumentException("Unsupported units $fromUnits");
                }
                break;
            default:
                throw new InvalidArgumentException("Unsupported units $fromUnits");
        }
    }
}
