<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use InvalidArgumentException;
use phbrick\types\Floats;
use phbrick\types\Ints;
use phbrick\types\Numbers;

/**
 * Trait OpacityTrait models a floating-point value between 0.0 and 1.0 inclusive that models the opacity
 *
 * @package phbrick\graphics
 */
trait OpacityTrait
{
    private $opacity = 1.0;

    /**
     * @return float
     */
    public function getOpacity()
    {
        return $this->opacity;
    }

    /**
     * Gets the opacity as a value in range [0, 100]
     * @return int
     */
    public function getOpacityInteger()
    {
        return Numbers::castInt($this->opacity * 100);
    }

    /**
     * Sets the opacity
     *
     * If given a positive integer in range [0,100] it will divide it by 100 to get a floating point opacity value.
     * If given a positive floating point value in range [0.0, 1.0] it will use it as the opacity value.
     *
     * Any other value will throw an exception.
     *
     * @param string|int|float $opacity
     */
    public function setOpacity($opacity)
    {
        Numbers::assertNumber($opacity);
        $floatval = -1;

        if (Ints::isInt($opacity)) {
            $floatval = Ints::castInt($opacity) / 100;

        }
        else if (Floats::isFloat($opacity)) {
            $floatval = Floats::castFloat($opacity);
        }

        if ($floatval < 0.0 || $floatval > 1.0) {
            throw new InvalidArgumentException("Invalid value '$opacity'. Expected float in range [0.0, 1.0] or integer in range [0, 100]");
        }

        $this->opacity = $floatval;
    }
}
