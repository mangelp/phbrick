<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use phbrick\exceptions\ReadOnlyException;
use phbrick\types\Bools;

/**
 * Class ReadOnlyTrait
 * @package phbrick
 */
trait ReadOnlyTrait
{
    /**
     * @var bool
     */
    private $readOnly = false;

    public function isReadOnly()
    {
        return $this->readOnly;
    }

    protected function setReadOnly($readOnly)
    {
        $readOnly = Bools::castBool($readOnly);
        $this->readOnly = $readOnly;
    }

    protected function assertReadOnly()
    {
        if ($this->isReadOnly()) {
            throw new ReadOnlyException("Invalid operation. The object is read-only");
        }
    }
}
