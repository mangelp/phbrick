<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use Exception;
use phbrick\collection\CallableArray;
use Throwable;

/**
 * Manager of unhandled exceptions that provides a way to use more than one
 * handler.
 *
 * It keeps the previous handler callable so it can call it before or after
 * executing all registered exception handlers.
 */
class ExceptionHandler
{

    const CHAIN_BEFORE = 'before';
    const CHAIN_AFTER = 'after';
    const CHAIN_NONE = false;

    /**
     * @var string
     */
    private $id = null;

    /**
     * Gets identity
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets identity
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @var string|bool
     */
    private $chainPreviousHandler = self::CHAIN_AFTER;

    /**
     * Gets if the previous handler will be called. Values are: before, after, false
     * @return string|bool
     */
    public function getChainPreviousHandler()
    {
        return $this->chainPreviousHandler;
    }

    /**
     * Sets if the previous handler will be called. Values are: before, after, false
     * @param string|bool $chainPreviousHandler
     */
    public function setChainPreviousHandler($chainPreviousHandler)
    {
        $this->chainPreviousHandler = $chainPreviousHandler;
    }

    /**
     * An instance of this class is registered to handle uncaught exception?
     * @var bool
     */
    private $registered = false;

    /**
     * Gets if this handler is registered to handle uncaught exceptions
     * @return bool
     */
    public function isRegistered()
    {
        return $this->registered;
    }

    /**
     * Sets if this handler is registered to handle uncaught exceptions
     * @param bool $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @var callable
     */
    private $previousHandler = null;

    /**
     * Gets the previously registered exception handler
     * @return callable
     */
    public function getPreviousHandler()
    {
        return $this->previousHandler;
    }

    /**
     * Sets the previously registered exception handler that should get called
     * if it is chained before or after the registered ones.
     *
     * Setting this handler does not affects the stack of registered exception
     * handlers.
     *
     * @param callable $previousHandler
     */
    public function setPreviousHandler(callable $previousHandler)
    {
        $this->previousHandler = $previousHandler;
    }

    /**
     * @var CallableArray
     */
    private $handlers = null;

    /**
     * Gets stack of exception handlers
     * @return CallableArray
     */
    public function getHandlers()
    {
        return $this->handlers;
    }

    public function __construct()
    {
        $this->handlers = new CallableArray();
    }

    /**
     * Adds a handler to the stack of exception handlers
     * @param callable $exceptionHandler
     * @param bool $first
     * @return int handler insertion position
     * @throws Exception
     */
    public function addHandler(callable $exceptionHandler, $first = false)
    {
        if (!is_callable($exceptionHandler)) {
            throw new Exception(
                'Invalid parameter $exceptionHandler, it must be a callable');
        }

        return $this->handlers->add(
            $exceptionHandler,
            $first ?
                CallableArray::POSITION_FIRST :
                CallableArray::POSITION_LAST);
    }

    /**
     * Removes an exception handler.
     *
     * @param callable $exceptionHandler
     * @return boolean True if it is found and removed or false otherwise
     */
    public function removeHandler(callable $exceptionHandler)
    {

        return $this->handlers->removeIfFound($exceptionHandler);
    }

    /**
     * Handles an exception executing all the registered handlers plus the
     * previous handler, if configured to do so.
     *
     * @param Throwable $t
     */
    public function handleException(Throwable $t)
    {

        if ($this->previousHandler
            && $this->chainPreviousHandler == self::CHAIN_BEFORE) {

            call_user_func_array($this->previousHandler, [$t]);
        }

        $this->handlers->call([$t]);

        if ($this->previousHandler
            && $this->chainPreviousHandler == self::CHAIN_AFTER) {

            call_user_func_array($this->previousHandler, [$t]);
        }
    }

    /**
     * All existing exception handlers set used set_exception_handler will be
     * popped and added as handlers in this class, so all of them will get called
     * when an unhandled exception happens. This effectively empties PHP's
     * exception handlers stack.
     *
     * If this class is already registered this won't do nothing.
     *
     * @return boolean True if handlers were loaded or false if not due to this
     * class being already registered as handler.
     */
    public function loadAllRegisteredHandlers()
    {

        if ($this->registered) {
            return false;
        }

        $fooCallable = function() {
        };
        $handler = set_exception_handler($fooCallable);

        while ($handler) {
            // Keep insertion order
            $this->handlers->add($handler, CallableArray::POSITION_FIRST);
            //pop foo
            restore_exception_handler();
            // pop previous
            restore_exception_handler();
            // Get the next one
            $handler = set_exception_handler($fooCallable);
        }

        // pop foo
        restore_exception_handler();

        return true;
    }

    /**
     * Registers all existing handlers directly as exception handlers.
     * Only does this if this class is not registered as exception handler.
     * After registering the collection of handlers is emptied.
     *
     * @return boolean
     */
    public function registerAllHandlers()
    {
        if ($this->registered) {
            return false;
        }

        for ($i = 0; $i < $this->handlers->getSize(); ++$i) {
            set_exception_handler($this->handlers[$i]);
        }

        $this->handlers->clear();

        return true;
    }

    /**
     * Registers this instance as the exception handler for unhandled exceptions
     * keeping a copy of the previous handler callable to eventually call it
     * if configured to do so.
     * @return bool True when it is registered or false if it was already
     * registered.
     * @throws Exception If the exception handler is registered twice
     */
    public function registerAsExceptionHandler()
    {
        if ($this->registered) {
            return false;
        }

        $previousErrorHandler =
            set_exception_handler([$this, 'handleException']);

        if (is_array($previousErrorHandler)
            && !empty($previousErrorHandler)
            && $previousErrorHandler[0] === $this) {

            // Undo registration
            restore_exception_handler();
            throw new Exception("Exception handler registered twice!");
        }

        $this->previousHandler = $previousErrorHandler;
        $this->registered = true;

        return true;
    }

    /**
     * Unregisters this instance as the exception handler for unhandled exceptions.
     *
     * @return boolean True when it is unregistered or false if was not registered.
     */
    public function unregisterAsExceptionHandler()
    {
        if (!$this->registered) {
            return false;
        }

        // WARNING: IF someone else registered another exception handler after
        // we did we will be popping out that handler and not ours :(
        restore_exception_handler();
        $this->previousHandler = null;
        $this->registered = false;

        return true;
    }
}
