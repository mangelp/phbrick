<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\classLoader;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use RuntimeException;

/**
 * Models a configured set of namespace prefixes for the same vendor.
 * Each prefix is a sub-namespace of the vendor and might have it's own configuration for autoloading
 * under it.
 *
 * @package phbrick
 */
class NsPrefixGroup extends BaseStrictClass
{
    /**
     * @type string
     */
    private $vendor;

    /**
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        if (!empty($this->vendor) && !empty($this->prefixes)) {
            throw new InvalidArgumentException(
                'Cannot change the vendor to ' . $vendor
                . ' as there are some configured prefixes with vendor ' . $this->vendor);
        }
    }

    /**
     * Contains each registered prefix for the given vendor along the options to handle it.
     *
     * Prefixes are sorted to keep then in the preference order. Prefixes are sorted as strings ascending but those
     * that share the same prefix are kept in descending order between them. This ordering allows to iterate the
     * prefixes by its preference.
     *
     *
     * @type array
     */
    private $prefixes = [];

    /**
     * @var bool
     */
    private $invertSort = false;

    /**
     * Gets if prefix sorting is inverted or not.
     * @return bool
     */
    public function isInvertSort()
    {
        return $this->invertSort;
    }

    /**
     * Sets if prefix sorting is inverted or not.
     * @param bool $invertSort
     */
    protected function setInvertSort($invertSort)
    {
        $this->invertSort = (bool)$invertSort;
    }

    /**
     * @var callback
     */
    private $prefixComparator = null;

    /**
     * Gets the comparator for prefixes to use instead of the default one
     * @return callback
     */
    public function getPrefixComparator()
    {
        return $this->prefixComparator;
    }

    /**
     * Sets the comparator for prefixes to use instead of the default one
     * @param callable $prefixComparator
     */
    protected function setPrefixComparator($prefixComparator)
    {
        $this->prefixComparator = $prefixComparator;
    }

    public function __construct($vendorName = null, callable $prefixComparator = null, $invertSort = false)
    {
        if ($vendorName) {
            $this->setVendor($vendorName);
        }

        if ($prefixComparator !== null) {
            $this->setPrefixComparator($prefixComparator);
        }

        $this->setInvertSort($invertSort);
    }

    protected function sort()
    {
        $prefixComparator = $this->prefixComparator;
        $comparator = null;
        $invert = $this->invertSort;

        if (!$prefixComparator) {
            $comparator = function(NsObject $nsA, NsObject $nsB) use ($invert) {
                $result = $nsA->compareTo($nsB);
                if ($invert) {
                    $result *= -1;
                }
                return $result;
            };
        }
        else if ($invert) {
            $comparator = function(NsObject $nsA, NsObject $nsB) use ($invert, $prefixComparator) {
                $result = $prefixComparator($nsA, $nsB);
                if ($invert) {
                    $result *= -1;
                }
                return $result;
            };
        }
        else {
            $comparator = $prefixComparator;
        }

        usort($this->prefixes, $comparator);
    }

    /**
     * @return array<int, NsPrefix>
     */
    public function getPrefixes()
    {
        return $this->prefixes;
    }

    /**
     * @return mixed
     */
    public function getAllPrefixesAsString()
    {
        $result = [];

        foreach ($this->prefixes as $prefix) {
            /* @type NsPrefix $prefix */
            $result[] = $prefix->toString();
        }

        return $result;
    }

    /**
     * @param NsObject $nsObject
     * @param NsPrefixOptions $options
     * @throws RuntimeException
     */
    public function add(NsObject $nsObject, NsPrefixOptions $options)
    {

        $thisVendor = $this->getVendor();

        if (empty($thisVendor)) {
            throw new RuntimeException('Invalid State: This prefix group does not have a vendor set');
        }

        if (!$nsObject->getVendor()) {
            $nsObject->setVendor($thisVendor);
        }
        else if ($nsObject->getVendor() != $thisVendor) {
            throw new InvalidArgumentException('Parameter $prefix vendor is ' . $nsObject->getVendor() . ' but it must be ' . $thisVendor);
        }

        if ($nsObject->isFullyQualifiedName()) {
            throw new InvalidArgumentException('Cannot add a fully qualified name as a prefix, it would not work for autoload');
        }

        $nsPrefix = new NsPrefix($nsObject, $options, $this);

        /** @type NsPrefix $nsPrefix */
        $this->prefixes[] = $nsPrefix;
        $this->sort();
    }

    /**
     * Removes a prefix from this group
     * @param $nsPrefix
     * @return bool
     */
    public function remove($nsPrefix)
    {
        $nsObject = NsPrefix::convert($nsPrefix);

        foreach ($this->prefixes as $pos => $prefix) {
            /* @type NsPrefix $prefix */
            if ($prefix->matches($nsObject)) {
                array_splice($this->prefixes, $pos, 1);
                return true;
            }
        }

        return false;
    }

    /**
     * Gets if this group contains the given prefix.
     *
     * @param NsObject|string $nsPrefix
     * @return bool
     */
    public function has($nsPrefix)
    {
        $nsObject = NsPrefix::convert($nsPrefix);

        foreach ($this->prefixes as $nsPrefix) {
            /* @type NsPrefix $nsPrefix */
            if ($nsObject->matches($nsPrefix)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the object for the given prefix if it exists in this group.
     *
     * @param NsObject|string $nsPrefix
     * @return NsPrefix|null
     */
    public function get($nsPrefix)
    {

        $nsObject = $nsObject = NsPrefix::convert($nsPrefix);

        foreach ($this->prefixes as $prefix) {
            /* @type NsPrefix $prefix */
            if ($prefix->matches($nsObject)) {
                return $prefix;
            }
        }

        return null;
    }
}
