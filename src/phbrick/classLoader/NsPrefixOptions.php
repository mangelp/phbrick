<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\classLoader;

use ArrayAccess;
use InvalidArgumentException;
use phbrick\BaseStrictClass;

/**
 * Class NsPrefixOptions holds all configuration options for classloading from a namespace.
 *
 * @package phbrick\classLoader
 */
class NsPrefixOptions extends BaseStrictClass
{
    /**
     * Array of loaders that will be used to try to load any class or interface that matches the prefix they are set to.
     * @type array<callback>
     */
    private $loaders = [];

    /**
     * Array of paths to look for the file(s) that might contain the desired class, interface or trait.
     * @type array<string>
     */
    private $paths = [];

    /**
     * Array of file extensions for the file names generated from the class/trait/interface FQN.
     * @type array<string>
     */
    private $extensions = [];

    /**
     * Mapper that returns a set of paths given a FQN.
     * @type callback
     */
    private $mapper = null;

    /**
     * Whether we might remove a portion of the path generated when the class/interface FQN is converted to a file path
     * by replacing namespace delimiters (backslashes or underscores) with path separators.
     *
     * This is needed to comply with PSR-4 loading as the registered prefix must be removed from the namespace before
     * converting it to a path. But in this case you have to configure it beforehand.
     *
     * The available values for this field are:
     *  + bool: If true the registered prefix will be removed from the beginning of the fully qualified class name.
     *  + int: If greater than 0 that is the number of components to be removed from the beginning of the fully
     *    qualified class name.
     *  + string: If not empty this must be a namespace portion to remove from the beginning of the namespaces if it
     *    matches it.
     * In any other case nothing is removed from the namespace.
     *
     * @type int|string|bool
     */
    private $removePrefix = false;

    /**
     * Whether the options are for pseudo-namespaced names or not.
     * Defaults to false.
     * @type bool
     */
    private $pseudoNamespaced = false;

    /**
     * Concrete order value.
     *
     * This is set and used by the class loader to keep added prefixes sorted in some specific way.
     *
     * @type int
     */
    private $order = 0;

    /**
     * @return array<callback>
     */
    public function getLoaders()
    {
        return $this->loaders;
    }

    public function hasLoaders()
    {
        return !empty($this->loaders);
    }

    /**
     * Sets the loaders at once. Existing loaders are removed.
     * @param callback|array<callback> $loaders
     */
    public function setLoaders($loaders)
    {
        $this->loaders = [];

        if (!$loaders) {
            return;
        }

        if (is_array($loaders) || is_a($loaders, '\\Traversable')) {
            foreach ($loaders as $loader) {
                $this->addLoader($loader);
            }
        }
        else if (is_callable($loaders)) {
            $this->addLoader($loaders);
        }
        else {
            throw new InvalidArgumentException('Parameter $loaders must be a callback or a Traversable of callbacks');
        }
    }

    public function addLoader(callable $loader)
    {
        $this->loaders[] = $loader;
    }

    /**
     * @return mixed
     */
    public function getPaths()
    {
        return $this->paths;
    }

    /**
     * @param array|string $paths
     */
    public function setPaths($paths)
    {

        $this->paths = [];

        if (!$paths) {
            return;
        }

        if (is_string($paths)) {
            $this->paths = explode(';', $paths);
        }
        else if (is_array($paths) || is_a($paths, '\\Traversable')) {
            $this->paths = $paths;

            $numPaths = count($this->paths);

            // Remove trailing / from paths
            for ($pos = 0; $pos < $numPaths; ++$pos) {
                $this->paths[$pos] = rtrim(trim($this->paths[$pos]), '/');
            }
        }
        else {
            throw new InvalidArgumentException("Invalid option format for 'paths'");
        }
    }

    public function addPath($path)
    {
        if (!is_string($path)) {
            throw new InvalidArgumentException('$path parameter must be an string');
        }

        $paths = explode(';', $path);
        foreach ($paths as $candidatePath) {
            if (!in_array($candidatePath, $this->paths)) {
                $this->paths[] = $candidatePath;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getExtensions()
    {
        return $this->extensions;
    }

    /**
     * @param mixed $extensions
     */
    public function setExtensions($extensions)
    {
        $this->extensions = [];

        if (!$extensions) {
            return;
        }

        if (is_string($extensions)) {
            $this->extensions = explode(';', $extensions);
        }
        else if (is_array($extensions)) {
            $this->extensions = $extensions;
        }
        else {
            throw new InvalidArgumentException("Invalid option format for 'extensions'");
        }
    }

    /**
     * @return callable
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * @param callable $mapper
     */
    public function setMapper(callable $mapper = null)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return mixed
     */
    public function getRemovePrefix()
    {
        return $this->removePrefix;
    }

    /**
     * @param $removePrefix
     */
    public function setRemovePrefix($removePrefix = null)
    {
        if (!is_int($removePrefix) && !is_string($removePrefix) && !is_bool($removePrefix)) {
            throw new InvalidArgumentException('Param $removePrefix must be a boolean, an integer or an string');
        }

        $this->removePrefix = $removePrefix;
    }

    /**
     * @return mixed
     */
    public function isPseudoNamespaced()
    {
        return $this->pseudoNamespaced;
    }

    /**
     * @param int $pseudoNamespaced
     */
    public function setPseudoNamespaced($pseudoNamespaced)
    {
        $this->pseudoNamespaced = (bool)(int)$pseudoNamespaced;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = (int)$order;
    }

    private $optionsSet = [];

    public function isOptionSet($name)
    {
        return isset($this->optionsSet[strtolower($name)])
            || isset($this->optionsSet[$name]);
    }

    public function __construct($order = null, $options = null, NsPrefixOptions $defaultOptions = null)
    {
        if ($defaultOptions) {
            $this->copy($defaultOptions);
        }

        if ($options) {
            $this->setOptions($options);
        }

        if (is_numeric($order)) {
            $this->setOrder($order);
        }
        else if (!is_int($this->getOrder())) {
            // Ensure there is a default value of 0
            $this->setOrder(0);
        }
    }

    /**
     * Sets prefix classloading options
     *
     * @param array|ArrayAccess $options
     * @throws InvalidArgumentException
     */
    public function setOptions($options)
    {

        if (is_a($options, '\phbrick\classLoader\NsPrefixOptions')) {
            /** @var NsPrefixOptions $options */
            $this->copy($options);
        }
        else if (is_string($options)) {
            $this->setPaths($options);
        } // If not an string then it is an array
        else if (is_array($options) || is_a($options, '\\Traversable')) {

            foreach ($options as $optKey => $optValue) {

                if (is_int($optKey)) {
                    continue;
                }

                $optionWasSet = false;

                if (strcasecmp($optKey, 'loaders') == 0) {
                    $optionWasSet = true;
                    $this->setLoaders($optValue);
                }

                if (strcasecmp($optKey, 'paths') == 0) {
                    $optionWasSet = true;
                    $this->setPaths($optValue);
                }

                if (strcasecmp($optKey, 'extensions') == 0) {
                    $optionWasSet = true;
                    $this->setExtensions($optValue);
                }

                if (strcasecmp($optKey, 'mapper') == 0) {
                    $optionWasSet = true;
                    $this->setMapper($optValue);
                }

                if (strcasecmp($optKey, 'removePrefix') == 0) {
                    $optionWasSet = true;
                    $this->setRemovePrefix($optValue);
                }

                if (strcasecmp($optKey, 'pseudoNamespaced') == 0) {
                    $optionWasSet = true;
                    $this->setPseudoNamespaced($optValue);
                }

                if (strcasecmp($optKey, 'order') == 0) {
                    $optionWasSet = true;
                    $this->setOrder($optValue);
                }

                if ($optionWasSet) {
                    $this->optionsSet[$optKey] = true;
                }
            }

        }
        else {
            throw new InvalidArgumentException(
                'Invalid argument type for $options. "
                    . "Must be an string or an array, "
                    . "but found it to be of type '
                . gettype($options));
        }
    }

    public function copy(NsPrefixOptions $nsOptions)
    {
        $this->setOrder($nsOptions->getOrder());
        $this->setPaths($nsOptions->getPaths());
        $this->setLoaders($nsOptions->getLoaders());
        $this->setPseudoNamespaced($nsOptions->isPseudoNamespaced());
        $this->setRemovePrefix($nsOptions->getRemovePrefix());
        $this->setExtensions($nsOptions->getExtensions());
        $this->setMapper($nsOptions->getMapper());
    }

    public function __clone()
    {
        $clone = self::instance();
        $clone->copy($this);
        return $clone;
    }
}
