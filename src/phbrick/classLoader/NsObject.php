<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\classLoader;

use InvalidArgumentException;
use phbrick\BaseStrictClass;
use RuntimeException;

/**
 * Models a Namespace as an object.
 *
 * @package phbrick
 */
class NsObject extends BaseStrictClass
{

    /**
     * Namespace separator character constant
     * @var string
     */
    const NS_SEP = '\\';

    /**
     * Default namespace delimiters replaced by namespace separators if the
     * registered prefix or class name seems to be a PSR0 pseudo-namespace.
     * @var string
     */
    const NS_PSEUDO_SEP = '_';

    /**
     * Pseudo-namespaced class name like \Zend_Mail_Transport
     */
    const TYPE_PSEUDO_NAMESPACED = 'pseudo-namespaced';
    /**
     * Namespaced class name like \phbrick\collection\Arrays
     */
    const TYPE_NAMESPACED = 'namespaced';

    /**
     * Fully qualified class name
     * @type string
     */
    private $fqn = null;

    public function getFqn()
    {
        return $this->fqn;
    }

    public function setFqn($fqn)
    {
        $this->fqn = $fqn;
    }

    /**
     * Namespace type. Can be either 'pseudo-namespaced' or 'namespaced'.
     * @type string
     */
    private $type = self::TYPE_NAMESPACED;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Namespace vendor component. Is the first part in the namespace.
     * @type string
     */
    private $vendor = null;

    public function getVendor()
    {
        return $this->vendor;
    }

    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * Namespace sequence of components without vendor or name portion.
     * @type string
     */
    private $path = null;

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Name of a concrete class or namespace.
     * @type string
     */
    private $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Gets if this namespace is made up only of the vendor portion.
     * @return bool
     */
    public function isRoot()
    {
        return !empty($this->vendor)
            && empty($this->path)
            && empty($this->name);
    }

    /**
     * Gets if this namespace is a Fully Qualified Name (either class or interface).
     * @return bool
     */
    public function isFullyQualifiedName()
    {
        return !empty($this->vendor)
            && !empty($this->name);
    }

    /**
     * Initializes the current object with the input namespace.
     *
     * If the input namespace is an NsObject instance it will copy all the properties ignoring $hasName and $nsType
     * parameters.
     *
     * @param string|NsObject|null $namespace
     * @param bool $hasName
     * @param string|null $nsType
     */
    public function __construct($namespace = null, $hasName = false, $nsType = null)
    {
        if (is_string($namespace)) {
            $this->parse($namespace, $hasName, $nsType);
        }
        else if (is_a($namespace, '\phbrick\classLoader\NsObject')) {
            $this->copy($namespace);
        }
        else if ($namespace !== null) {
            throw new InvalidArgumentException('Invalid type for parameter $namespace: ' . gettype($namespace));
        }
    }

    /**
     * Normalizes a namespace string ensuring it begins with a backslash and that it does not ends with it or with an
     * underscore.
     *
     * @param string $namespace Namespace string to be normalized
     * @return string Normalized namespace.
     * @throws RuntimeException If the namespace is not an string.
     */
    public function normalize($namespace)
    {

        if (!is_string($namespace)) {
            throw new RuntimeException("Namespace is not an string");
        }

        // Remove trailing/leading backslashes and underscores, but add a single leading
        // backslash
        $trimChars = "\r\n\t " . self::NS_SEP . self::NS_PSEUDO_SEP;
        $result = self::NS_SEP . trim($namespace, $trimChars);

        return $result;
    }

    /**
     * Parses the namespace and initializes the current instance with it.
     *
     * @param string $namespace Namespace to parse
     * @param bool $hasName Whether it contains a trailing class name or interface
     * @param null|string $nsType Namespace type to force. If not set it will be detected.
     * @throws RuntimeException
     */
    public function parse($namespace, $hasName = false, $nsType = null)
    {
        $namespace = $this->normalize($namespace);

        if (!$nsType) {
            $nsType = $this->detectType($namespace);
        }

        if (!$namespace || !$nsType) {
            throw new InvalidArgumentException('Cannot parse namespace: ' . $namespace);
        }

        $this->vendor = null;
        $this->path = null;
        $this->name = null;
        $this->type = $nsType;
        $this->fqn = null;

        $nsSeparator = ($nsType == self::TYPE_NAMESPACED) ?
            NsObject::NS_SEP :
            NsObject::NS_PSEUDO_SEP;

        if ($namespace == self::NS_SEP) {
            // Empty namespace
            return;
        }

        // Separate the namespace using the separator based on the type and remove leading/trailing chars
        $parts = explode(
            $nsSeparator,
            trim($namespace, NsObject::NS_PSEUDO_SEP . NsObject::NS_SEP));

        // The vendor is always the first part of the namespace
        $this->vendor = $parts[0];
        $this->fqn = $namespace;

        $numParts = count($parts);

        if ($numParts > 1) {
            if ($hasName) {
                $this->name = $parts[$numParts - 1];
                $parts = array_slice($parts, 1, $numParts - 2);
            }
            else {
                $parts = array_slice($parts, 1);
            }

            if (!empty($parts)) {
                $this->path = implode($nsSeparator, $parts);
            }
        }
    }

    /**
     * Detects if the namespace represents a pseudo-namespaced class name or if it is a proper namespace.
     *
     * The input is considered to be a pseudo-namespaced class name if it contains at least one underscore (but not a
     * trailing one) and if it does not contains backslashes or if it contains only one at position 0.
     * If a pseudo-namespaced class name is not detected then it must be a namespace.
     *
     * @param string $namespace Namespace string to be checked
     * @return string Either one of the constants TYPE_NAMESPACED or TYPE_PSEUDO_NAMESPACED.
     * @example
     * $type = $ns->detectType('\\Foo_Bar'); // Result is pseudo-namespaced
     * $type = $ns->detectType('\\Foo\\Bar'); // Result is namespaced
     * $type = $ns->detectType('\\Foo\\Bar_Fop'); // Result is namespaced
     *
     */
    public function detectType($namespace)
    {
        if (!$namespace) {
            return false;
        }

        $backslashPos = strrpos($namespace, self::NS_SEP);
        $underscorePos = strrpos($namespace, self::NS_PSEUDO_SEP);
        $len = strlen($namespace);
        $result = self::TYPE_NAMESPACED;

        if ($underscorePos !== false
            && $underscorePos > 0
            && $underscorePos < $len - 1
            // This means it can be either false (not found) or 0 (found first char).
            && !$backslashPos) {

            $result = self::TYPE_PSEUDO_NAMESPACED;
        }

        return $result;
    }

    /**
     * Compares this instance against another one and returns the order of namespaces.
     *
     * This method does not uses the order field and the comparison is case-sensitive.
     *
     * First the vendor is compared, if it is not the same the comparison between vendors is returned.
     * Then the path is compared. If they have different length we check if one starts by another, if so the longest one
     * determines it goes before. If one does not prefixes the other we compare them to see if they are equal, if not
     * the comparison result is returned.
     * Finally the name is compared (if the paths are equal) and that comparison result is returned.
     *
     * @param NsObject $other
     * @return int A number equal to 0 if they are the same, greater than 0 if this instance goes after the other and
     * less than 0 if this instance order goes before the other.
     */
    public function compareTo(NsObject $other)
    {
        $cmp = strcmp($this->vendor, $other->vendor);

        if ($cmp != 0) {
            return $cmp;
        }

        $lenThis = strlen($this->path);
        $lenOther = strlen($other->path);

        if ($lenThis < $lenOther
            && substr($other->path, 0, $lenThis) == $this->path) {
            return -1;
        }
        else if ($lenThis > $lenOther
            && substr($this->path, 0, $lenOther) == $other->path) {
            return 1;
        }

        $cmp = strcmp($this->path, $other->path);

        if ($cmp != 0) {
            return $cmp;
        }

        return strcmp($this->name, $other->name);
    }

    /**
     * Gets an string that represents the namespace.
     *
     * If the $portion argument is used will return only the given part.
     *
     * @param null $part
     * @param null $separator
     * @return string
     */
    public function toString($part = null, $separator = null)
    {
        $result = [];

        $curSeparator = $this->type == self::TYPE_NAMESPACED ?
            self::NS_SEP :
            self::NS_PSEUDO_SEP;

        if ($this->vendor && (!$part || $part == 'vendor')) {
            $result[] = $this->vendor;
        }

        if ($this->path && (!$part || $part == 'path')) {
            $parts = explode($curSeparator, $this->path);
            $result = array_merge($result, $parts);
        }

        if ($this->name && (!$part || $part == 'name')) {
            $result[] = $this->name;
        }

        if ($separator == null) {
            $separator = $curSeparator;
        }

        $leading = '';

        if ((!$part || $part == 'vendor')
            && ($separator == self::NS_SEP || $separator == self::NS_PSEUDO_SEP)) {

            $leading = self::NS_SEP;
        }
        else if ((!$part || $part == 'vendor')
            && $separator != self::NS_SEP && $separator != self::NS_PSEUDO_SEP) {
            $leading = $separator;
        }

        return $leading . implode($separator, $result);
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function copy(NsObject $other)
    {
        $this->vendor = $other->vendor;
        $this->path = $other->path;
        $this->name = $other->name;
        $this->type = $other->type;
        $this->fqn = $other->fqn;
    }

    public function __clone()
    {
        $clone = self::instance();
        $clone->copy($this);
        return $clone;
    }

    public function matches($prefix)
    {
        $nsObject = NsObject::convert($prefix);

        return $this->vendor == $nsObject->vendor
            && $this->path == $nsObject->path
            && $this->name == $nsObject->name;
    }

    public static function convert($prefix)
    {
        if (is_a($prefix, '\phbrick\classLoader\NsObject')) {
            return $prefix;
        }
        else if (is_string($prefix)) {
            return new NsObject($prefix);
        }
        else {
            throw new InvalidArgumentException('Parameter $prefix must be an string or an instance of NsObject');
        }
    }
}
