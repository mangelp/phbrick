<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\classLoader;

use RuntimeException;
use Traversable;

/**
 * Models a portion of a namespace with a custom configuration for autoloading that belongs to a
 * given vendor and is referenced within the vendor prefix group.
 *
 */
class NsPrefix extends NsObject
{

    /**
     *
     * @param NsPrefix $nsPrefix
     * @return NsPrefix
     */
    public static function cast(NsPrefix $nsPrefix)
    {
        return $nsPrefix;
    }

    /**
     * @var NsPrefixOptions
     */
    private $options = null;

    /**
     * Gets the options for prefix class loading
     * @return NsPrefixOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the options for prefix class loading
     * @param NsPrefixOptions $options
     */
    public function setOptions(NsPrefixOptions $options)
    {
        $this->options = $options;
    }

    /**
     * @type NsPrefixGroup
     */
    private $group;

    /**
     * @return NsPrefixGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param NsPrefixGroup $group
     */
    public function setGroup(NsPrefixGroup $group)
    {
        $this->group = $group;
    }

    /**
     *
     * @param NsObject $nsObject
     * @param array|Traversable|NsPrefixOptions $options
     * @param NsPrefixGroup $group
     */
    public function __construct(NsObject $nsObject = null, $options = null, NsPrefixGroup $group = null)
    {
        parent::__construct($nsObject);
        $this->options = new NsPrefixOptions(0, $options);

        if ($group) {
            $this->setGroup($group);
        }
    }

    /**
     * Copies the group and options from another object.
     *
     * @param NsObject $other
     */
    public function copy(NsObject $other)
    {
        parent::copy($other);

        if (is_a($other, '\phbrick\classLoader\NsPrefix')) {
            /* @type NsPrefix $other */
            $other = NsPrefix::cast($other);
            $this->group = $other->getGroup();
            $this->options->copy($other->getOptions());
        }
    }

    public function matches($nsPrefix)
    {
        $nsObject = NsPRefix::convert($nsPrefix);

        return $this->getVendor() == $nsObject->getVendor()
            && $this->getPath() == $nsObject->getPath();
    }

    /**
     * Returns the paths where a given class should be located using all configured paths for a single prefix.
     *
     * If the prefix has a loader configured it will not be called and will be ignored, thus this method will return,
     * in a best-effort basis, an array of available paths to look the class for.
     *
     * @param NsObject $nsObject
     * @return array <string> Absolute paths to try to load the class from.
     * @throws RuntimeException
     */
    public function generateFilePaths(NsObject $nsObject)
    {

        $paths = [];
        $mapper = $this->getOptions()->getMapper();

        if ($mapper) {
            $paths = $this->getNameMapperPaths($mapper, $nsObject);
        }
        else {
            $rootPaths = $this->getOptions()->getPaths();

            $fileExtensions = $this->getOptions()->getExtensions();
            $classAsPath = $this->convertClassNameToPath($nsObject);

            foreach ($rootPaths as $rootPath) {

                foreach ($fileExtensions as $fileExtension) {
                    $paths[] = $this->createPath($rootPath, $classAsPath, $fileExtension);
                }
            }
        }

        array_unique($paths);

        return $paths;
    }

    /**
     * Gets all the file paths provided by a name mapper, if defined.
     *
     * @param callable $nameMapperCallable
     * @param NsObject $nsObject
     * @return array <string>
     * @throws RuntimeException
     */
    protected function getNameMapperPaths($nameMapperCallable, NsObject $nsObject)
    {

        $mappedPaths = call_user_func_array($nameMapperCallable, [$this->getOptions()->getPaths(), $nsObject]);

        $paths = [];

        if (!is_array($mappedPaths)) {
            $mappedPaths = [$mappedPaths];
        }

        foreach ($mappedPaths as $pos => $mappedPath) {
            if (empty($mappedPath)) {
                continue;
            }

            $hasScheme = strpos($mappedPath, ':');

            if ($mappedPath[0] == '/' || $hasScheme) {
                $paths[] = $mappedPath;
            }
        }

        return $paths;
    }

    protected function createPath($basePath, $classAsPath, $fileExtension)
    {
        $pathSeparator = '/';
        $fullPath = rtrim($basePath, $pathSeparator)
            . $pathSeparator . ltrim($classAsPath, $pathSeparator);

        if ($fileExtension) {
            $fullPath .= '.' . ltrim($fileExtension, '.');
        }

        return $fullPath;
    }

    /**
     * Converts a fully qualified class name into a path.
     *
     * This method takes care of removing the namespace prefix from the path if
     * the option is properly set to do so.
     *
     * The returned value is a relative path without trailing path separators and without file extension, although it
     * includes the file name (the class or interface name is the last component).
     *
     * @param NsObject $nsObject
     * @return string
     */
    protected function convertClassNameToPath(NsObject $nsObject)
    {

        $pathSeparator = '/';
        $classAsPath = $nsObject->toString(null, $pathSeparator);
        $prefixAsPath = $this->toString(null, $pathSeparator);

        $removePrefix = $this->getOptions()->getRemovePrefix();

        if ((is_bool($removePrefix) && $removePrefix) || is_string($removePrefix)) {

            if (is_bool($removePrefix)) {
                $removePrefix = $prefixAsPath;
            }

            $len = strlen($removePrefix);

            if (substr($classAsPath, 0, $len) == $removePrefix) {
                $classAsPath = substr($classAsPath, $len);
            }
        }
        else if (is_int($removePrefix) && $removePrefix > 0) {
            $parts = explode($pathSeparator, trim($classAsPath, $pathSeparator));
            $parts = array_slice($parts, $removePrefix);
            $classAsPath = $pathSeparator . implode($pathSeparator, $parts);
        }

        return ltrim($classAsPath, $pathSeparator);
    }
}
