<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use RuntimeException;

/**
 * Phbrick basic version information and utils.
 *
 * @author Miguel Angel Perez
 * @static
 */
final class Phbrick
{

    use StaticClassTrait;

    const VERSION = '0.1.0';
    const TAG = 'beta';
    const PHP_MIN_VERSION = '5.6.0';
    const PHP_REC_VERSION = '7.1.0';

    /**
     * Checks if the given PHP version meets the minimum required.
     * @param string $version Version to compare to. Defaults to current PHP version.
     * @return bool
     */
    public static function isValidPhpVersion($version = PHP_VERSION)
    {
        return version_compare($version, self::PHP_MIN_VERSION) >= 0;
    }

    /**
     * Asserts that the given PHP version meets the minimum required.
     * @param string $version Version to compare to. Defaults to current PHP version.
     * @throws RuntimeException If PHP version is lower than the minimum required.
     */
    public static function assertValidPhpVersion($version = PHP_VERSION)
    {
        if (!self::isValidPhpVersion($version)) {
            throw new RuntimeException(
                "PHP version $version does not match Phbrick's minimum required version "
                . self::PHP_MIN_VERSION);
        }
    }
}
