[ ![Codeship CI mangelp/phbrick](https://codeship.com/projects/ab25f390-e38f-0133-7af3-0ae7e6ad137a/status?branch=master)](https://codeship.com/projects/146026)
![Supports PHP 5.6 and 7.0](https://img.shields.io/badge/PHP-5.6%2C%207.0-blue.svg)

# README - Phbrick #

## What is this repository for? ##

This repo contains the Phbrick library, a library of bricks for buildings made of PHP.

Phbrick is an experimental small PSR-4 pet library whose main goal is to provide well-written, from
my POV, classes that provide certain features, but that does not pretend to be feature-rich or
production-ready at any time.

I almost follow PSR-2 coding standards, but I deviate in new-line-opening curly braces, i like to
put them at the end of the previous line (separated by an space) than in a new line. I also like to
use Drupal convention for if-else curly braces, that puts a new line before the else making some
space before the else body that provides a clearer view of it:

```
#!php
class MyClass {
    function myFunc() {
        if ($foo) {
            // Nothing
        }
        else if ($bar) {
            // Nothing
        }
        else {
            // Nothing
        }
    }
}
```

## How do I get set up? ##

Clone the repo and configure the lib folder as the root folder to load phbrick namespace into your
preferred PSR-4 class loader. Phbrick is written as a fully OOP library and is compatible with PSR-4
class loaders.

If you haven't got a class loader you could use the provided one at src/phbrick/lib/ClassLoader.php,
load it and configure it:


```
#!php

// Use the class and load it directly
use phbrick\ClassLoader;

$phbrickPath = '/foo/var/blah/phbrick/src/phbrick';

require(realpath($phbrickPath . '/ClassLoader.php'));

// Create a new unconfigured instance that still cannot load nothing
$classLoader = ClassLoader::getInstance();
// Configure to at least load phbrick classes
$classLoader->initDefaults();
// Register for autoloading
$classLoader->registerAutoload();
// Add your own stuff
$classLoader->addPrefix('Zend', '/path/to/ZendFramework/lib');
...
```

The bootstrap.php file in test folder has a working example used to autoload both phbrick classes and
some test classes created.

## Testing ##

I use phpunit for unit tests with a configuration xml file in the tests folder that loads
the bootstrap and runs all tests:

```
#!bash
$ phpunit --config test/phpunit.xml
```

## Composer ##

At the time of this writing Phbrick is not included in Packagist (why I should do that) but a simple
composer.json file is included so you can use phbrick as a dependency with composer.

If you want to include phbrick as a project dependency you must define a repository in your
composer.json file and add the package requirement:

```
#!javascript
{
    "repositories" : [
        {
            "type": "git",
            "url": "https://bitbucket.org/mangelp/phbrick"
        },
    ],
    "require": {
        "mangelp/phbrick": "dev-master",
    }
}
```
## PHP versions supported ##

This library works with PHP 5.6 and 7.x

## LICENSE ##

Phbrick is provided free of charge under the terms of Mozilla Public License 2.0

```
#!text

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this file,
You can obtain one at http://mozilla.org/MPL/2.0/.
```
