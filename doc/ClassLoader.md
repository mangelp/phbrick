# Phrick - Class loader #
The class loader have been designed to be flexible and to support a set of various situations where the sources of the application can be distributed into several packages distributed across several locations.

## Design goals ##

- Flexible enough to load classes from main frameworks and class-based CMS supporting PSR-0 and PSR-4 naming schemes.
- OOP Design. Namespaces, prefixes, options and namespace groups are modeled as classes and used by the class loader.
- Extensibility allowing subclasses to alter internal behaviours.

## Features ##

### Namespace registration ###

The class loader stores registered namespace prefixes (both psr-0 and psr-4) distinguishing them by the first portion, the vendor, and then by the path after it. Every registered prefix is normalized (a leading backslash is added) before being processed.

Added prefixes are grouped by vendor. This way any class that would be loaded by this classloader must have at least a namespace with one component, the vendor name.

For example if your library base namespace is my\pretty\library you could register my\, my\pretty or my\pretty\library as namespace prefixes. Each one with his own configuration, but all would be stored under an unique \my (note the trailing backslash) vendor and grouped/sorted only between them.

Registered prefixes are sorted (first by vendor and then by prefixes for the same vendor) by namespace components giving precedence to the longer ones. This way if you register prefix \foo and then you register prefix \foo\bar the later will be found first than the former. 

Namespace prefixes are processed in this order (longer prefixes first) when looking for classes to be load to provide a way of "overriding" parent namespaces with more specific configurations for paths under it. As an example having registered \phrick prefix we could register \phrick\collections prefix to resolve to another path and provide an alternative implementation of some or all classes under it, so all those classes would load before the original ones.

#### Options ####
 - **loaders**: One or more custom loaders as callables that receive the input class name to be loaded as parameter and returns a boolean with the result of the class loading. If this option is set all the rest are ignored. The return value is used to determine if the loader succeeded or not, assuming that only succeeds if it returns a boolean true value (exact type and value) as any other return value would be taken as a fail. If an array of callables is provided it will be iterated in order just until one loader succeeds, failing only when no loader succeeds.
 - **paths**: One or more absolute filesystem paths to search when loading a class. If is only an string it can have one or more paths separated by semicolons, but if is an array each string value must be only a path.
 - **extensions**: One or more file extensions to be appended to each generated candidate class file name to try to load the class from. If is only an string it can be a semicolon-separated list of file extensions, but if it is an array then each string value must be a single extension. Always without leading dots.
 - **mapper**: Callable that receives the FQCN components and the configured prefix paths and returns one or more paths to try to load the class from. Returned paths must be absolute or start by an scheme (like phar://, file:// and even C:/).
 - **removePrefix**: If set to a non-false value the first part of the class paths generated from the FQCN will be altered by removing a portion of it depending on the concrete type of this option:
   * *string*: If the class path starts by this string then it will be removed. If is empty nothing is done.
   * *int*: Number of path components to remove from the beginning of the class path. If less than 0 nothing is done.
   * *bool*: If true then the registered prefix will be trimmed from the beginning of the class path if found. If false nothing is done.
 - **pseudoNamespaced**: If true then the prefix is pseudo-namespaced (PSR-0, underscore separator) but if set to false then the prefix is namespaced (PSR-4, backslash separator). This is needed to properly handle namespaces. If not set it is guessed checking first for the existence of backslashes and then for the existence of underscores.
 - **order**: Specific order for the prefix configured. This order acts like a grouping factor. Prefixes are first sorted by order and then the default sorting is applied for all prefixes with the same order.
 
### Examples ###
```
    $loader->addPrefix('\My\Prefix', '/class/path1;/class/path2');
         
    $loader->addPrefix('\My\Prefix', array('/class/path1', '/class/path2'));
         
    $loader->addPrefix('My_Prefix_', '/path/to');
         
    $loader->addPrefix('\My\Prefix', array(
       'path' => '/my/lib/',
       'extensions' => 'inc;lib;php',
       'pseudoNamespace' => false,
    ));
         
    $loader->addPrefix('My_Prefix_', array(
       'path' => '/my/lib/',
       'extensions' => 'inc;lib;php',
       'pseudoNamespace' => true,
    ));
```

