<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

use phbrick\ClassLoader;

/**
 * We need to set the autoloader at a minimum.
 */

$phbrickPath = dirname(__DIR__) . '/src/phbrick';
$testPath = dirname(__DIR__) . '/test';

require("$phbrickPath/StaticClassTrait.php");
require("$phbrickPath/Phbrick.php");

phbrick\Phbrick::assertValidPhpVersion();

require("$phbrickPath/ClassLoader.php");

$classLoader = ClassLoader::getInstance();
$classLoader->initDefaults();

// We will be loading classes from both phbrick library path and the test
// library path, so we must add the later path to the list of phbrick
// prefix paths
$testPath = realpath(dirname(__FILE__));
$prefixGroup = $classLoader->getPrefixGroup('phbrick');
$prefix = $prefixGroup->get('phbrick');
$prefix->getOptions()->addPath($testPath);

$classLoader->registerAutoload();
