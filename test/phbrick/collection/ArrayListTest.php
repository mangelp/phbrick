<?php

namespace phbrick\collection;

use ArrayObject;
use StdClass;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-08-01 at 13:12:25.
 */
class ArrayListTest extends IListBaseTestClass
{
    /**
     * @var ArrayList
     */
    protected $arrayList;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        parent::beforeTest();
        $this->arrayList = $this->iList;
    }

    protected function createIListImplementationInstance()
    {
        return new ArrayList();
    }

    /**
     * @covers \phbrick\collection\ArrayList::isSorted
     * @covers \phbrick\collection\ArrayList::sort
     */
    public function testSortAndIsSorted()
    {
        self::assertFalse($this->arrayList->isSorted());
        $this->arrayList->sort();
        self::assertTrue($this->arrayList->isSorted());

        $array = [1, 2, 3, 4];
        $shuffled = [2, 3, 1, 4];

        $this->arrayList->addAll($shuffled);
        $this->arrayList->sort();
        self::assertSame($array, $this->arrayList->toArray());
        self::assertNotSame($array, $shuffled);

        $this->arrayList->sort(null, true);
        $expected = [4, 3, 2, 1];
        self::assertSame($expected, $this->arrayList->toArray());

        $customSort = function($a, $b) {
            $aEven = ($a % 2 == 0);
            $bEven = ($b % 2 == 0);

            if ($aEven == $bEven) {
                return $a - $b;
            }
            else if ($aEven) {
                return -1;
            }
            else if ($bEven) {
                return 1;
            }

            return 0;
        };

        $this->arrayList->sort($customSort);

        $expected = [2, 4, 1, 3];
        self::assertSame($expected, $this->arrayList->toArray());

        $this->arrayList->sort($customSort, true);

        $expected = [3, 1, 4, 2];
        self::assertSame($expected, $this->arrayList->toArray());
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     */
    public function testFilter()
    {
        $this->arrayList->addAll([1, 2, 3, 4]);

        $expected = [1, 3];
        $actual = $this->arrayList->apply(function($index, $value) {
            return $index % 2 == 0;
        });

        self::assertEquals($expected, $actual->toArray());

        $expected = [4, 8];
        $actual = $this->arrayList->apply(function($index, $value) {
            if ($index % 2 != 0) {
                // The index must be ignored and the value appended to the result
                return [$index => $value * 2];
            }
            else {
                return false;
            }
        });

        self::assertEquals($expected, $actual->toArray());

        $expected = [2, 4];
        $actual = $this->arrayList->apply(function($index, $value) {
            return $value % 2 == 0;
        });
        self::assertEquals($expected, $actual->toArray());

        $expected = [1, 3];
        $actual = $this->arrayList->apply(function($index, $value) {
            return $value % 2 != 0;
        });
        self::assertEquals($expected, $actual->toArray());

        $expected = [1, 2, 2, 4, 3, 6, 4, 8];
        $actual = $this->arrayList->apply(function($index, $value) {
            return [$index => $value, ($index + 1) * 2 => $value * 2];
        });
        self::assertEquals($expected, $actual->toArray());

        $expected = [3, 4];
        $actual = $this->arrayList->apply(function($index, $value) {
            $result = false;

            switch ($index) {
                case 0:
                    $result = "0";
                    break;
                case 1:
                    $result = 0;
                    break;
                case 2:
                    $result = 0.0000000001;
                    break;
                case 3:
                    $result = 1.2;
                    break;
            }

            return $result;
        });
        self::assertEquals($expected, $actual->toArray());

        $expected = [1, 2, 3, 4];
        $actual = $this->arrayList->toArray();
        self::assertEquals($expected, $actual);
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     */
    public function testFilterGenerateMoreNewElements()
    {
        $this->arrayList->addAll([1, 2, 3, 4]);
        $expected = [1, "_2", "+0", "+1", 2, 3, "_6", "+2", "+3"];

        $actual = $this->arrayList->apply(function($index, $value) {
            if ($index == 1) {
                return true;
            }
            else if ($value % 2 == 0) {
                return false;
            }

            return [$index => $value, "_$index" => "_" . ($value * 2), "+$index", "+$value"];
        });

        self::assertEquals($expected, $actual->toArray());
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     */
    public function testFilterDonNotThrowsExWhenKeyNotInteger()
    {
        $this->arrayList->addAll([2, 3]);
        $actual = $this->arrayList->apply(function($key, $value) {
            return ['a' => 12];
        });
        self::assertEquals([12, 12], $actual->toArray());
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     *
     */
    public function testFilterThrowsExWhenReturnsString()
    {
        $this->expectException('\phbrick\exceptions\InvalidCallbackException');
        $this->arrayList->add(1);
        $this->arrayList->apply(function($key, $value) {
            return "blow-up";
        });
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     */
    public function testFilterDoNotThrowsExWhenReturnsATraversable()
    {
        $this->arrayList->add(1);
        $this->assertEquals(1, $this->arrayList->getSize());
        $this->arrayList->apply(function($key, $value) {
            return new ArrayObject();
        });
        $this->assertEquals(1, $this->arrayList->getSize());
        $this->arrayList->apply(function($key, $value) {
            return new ArrayList();
        });
        $this->assertEquals(1, $this->arrayList->getSize());
        $this->arrayList->apply(function($key, $value) {
            return [];
        });
        $this->assertEquals(1, $this->arrayList->getSize());
    }

    /**
     * @covers \phbrick\collection\ArrayList::apply
     *
     */
    public function testFilterThrowsExWhenReturnsObject()
    {
        $this->expectException('\phbrick\exceptions\InvalidCallbackException');
        $this->arrayList->add(1);
        $this->arrayList->apply(function($key, $value) {
            return new StdClass();
        });
    }

    /**
     * @covers \phbrick\collection\ArrayList::alter
     */
    public function testApply()
    {
        $init = [1, 2, 3, 4];

        $expected = [1, 3];

        $copy = new ArrayList($init);
        $copy->alter(function($key, $value) {
            return $value % 2 != 0;
        });
        self::assertEquals($expected, $copy->toArray());

        $expected = [2, 2, 6, 4];

        $copy = new ArrayList($init);
        $copy->alter(function($index, $value) {
            if ($value % 2 == 0) {
                return true;
            }

            $value *= 2;

            return [$value];
        });

        self::assertEquals($expected, $copy->toArray());

        $expected = [1, "_2", "+0", "+1", 2, 3, "_6", "+2", "+3"];

        $copy = new ArrayList($init);
        $copy->alter(function($index, $value) {
            if ($index == 1) {
                return true;
            }
            else if ($value % 2 == 0) {
                return false;
            }

            return [$index => $value, "_$index" => "_" . ($value * 2), "+$index", "+$value"];
        });

        self::assertEquals($expected, $copy->toArray());
    }
}
