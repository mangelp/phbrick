<?php

namespace phbrick\collection;

use Iterator;
use phbrick\test\PhbrickTestCase;
use stdClass;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-07-19 at 13:29:51.
 */
abstract class IMapBaseTestClass extends PhbrickTestCase
{
    use ArrayAccessMapTestTrait;

    /**
     * @var IMap
     */
    protected $iMap;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        $this->iMap = $this->createIMapImplementationInstance();

        assert(is_a($this->iMap, 'phbrick\\collection\\IMap'), 'Not an IMap implementation');
    }

    protected abstract function createIMapImplementationInstance();

    public function getArrayAccessCollection()
    {
        return $this->iMap;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function afterTest()
    {
    }

    /**
     * @covers \phbrick\collection\IMap::isEmpty
     * @covers \phbrick\collection\IMap::clear
     * @covers \phbrick\collection\IMap::set
     * @covers \phbrick\collection\IMap::toArray
     */
    public function testSetIncrementSizeAndClearMakesItEmpty()
    {
        self::assertNotNull($this->iMap->getSize());
        self::assertTrue($this->iMap->isEmpty());
        self::assertSame(0, $this->iMap->getSize());
        self::assertEmpty($this->iMap->toArray());

        $this->iMap->set('a', 1);

        self::assertFalse($this->iMap->isEmpty());
        self::assertEquals(1, $this->iMap->getSize());
        self::assertNotEmpty($this->iMap->toArray());

        $this->iMap->clear();

        self::assertTrue($this->iMap->isEmpty());
        self::assertEquals(0, $this->iMap->getSize());
        self::assertEmpty($this->iMap->toArray());

        $this->iMap->set('b', 2);

        self::assertFalse($this->iMap->isEmpty());
        self::assertEquals(1, $this->iMap->getSize());
        self::assertNotEmpty($this->iMap->toArray());

        $this->iMap->set('c', 3);

        self::assertFalse($this->iMap->isEmpty());
        self::assertEquals(2, $this->iMap->getSize());
        self::assertNotEmpty($this->iMap->toArray());

        $this->iMap->clear();

        self::assertTrue($this->iMap->isEmpty());
        self::assertEquals(0, $this->iMap->getSize());
        self::assertEmpty($this->iMap->toArray());
    }

    /**
     * @covers \phbrick\collection\IMap::set
     * @covers \phbrick\collection\IMap::get
     */
    public function testSetAndGet()
    {
        $this->iMap->set("0", 0);
        $this->assertEquals(1, $this->iMap->getSize());

        $this->iMap->set("1", 1);
        $this->assertEquals(2, $this->iMap->getSize());

        $this->iMap->set("2", 2);
        $this->assertEquals(3, $this->iMap->getSize());

        $expected = [0, 1, 2];
        $actual = $this->iMap->values();

        // We sort as map element order is undefined
        sort($actual);
        self::assertEquals($expected, $actual);

        self::assertEquals(0, $this->iMap->get("0"));
        self::assertEquals(1, $this->iMap->get("1"));
        self::assertEquals(2, $this->iMap->get("2"));
    }

    /**
     * @covers \phbrick\collection\IMap::set
     *
     */
    public function testSetNullElementThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\NullValueException');
        $this->iMap->set("a", null);
    }

    /**
     * @covers \phbrick\collection\IMap::set
     *
     */
    public function testSetNullKeyThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->set(null, 12);
    }

    /**
     * @covers \phbrick\collection\IMap::get
     *
     */
    public function testGetNullKeyThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->get(null);
    }

    /**
     * @covers \phbrick\collection\IMap::setAll
     * @covers \phbrick\collection\IMap::toArray
     */
    public function testSetAll()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
        ];

        $this->iMap->setAll($data);
        self::assertEquals($data, $this->iMap->toArray());
        $this->assertEquals(3, $this->iMap->getSize());

        $data2 = [
            'a' => 2,
            'b' => 4,
            'c' => 9,
        ];

        $this->iMap->setAll($data2, false);
        self::assertNotEquals($data2, $this->iMap->toArray());
        self::assertEquals($data, $this->iMap->toArray());
        $this->assertEquals(3, $this->iMap->getSize());

        $this->iMap->setAll($data2);
        self::assertEquals($data2, $this->iMap->toArray());
        self::assertNotEquals($data, $this->iMap->toArray());
        $this->assertEquals(3, $this->iMap->getSize());

        $this->iMap->setAll($data, true);
        self::assertNotEquals($data2, $this->iMap->toArray());
        self::assertEquals($data, $this->iMap->toArray());
        $this->assertEquals(3, $this->iMap->getSize());
    }

    /**
     * @covers \phbrick\collection\IMap::allKeysOf
     */
    public function testAllKeysOf()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'e' => 1,
            'f' => 3,
            'g' => 8,
            'h' => 1,
        ];

        $this->iMap->setAll($data);

        self::assertEmpty($this->iMap->allKeysOf(10));
        self::assertEmpty($this->iMap->allKeysOf(0));

        $actual = $this->iMap->allKeysOf(1);
        sort($actual);

        self::assertEquals(
            ['a', 'e', 'h'],
            $actual);

        $actual = $this->iMap->allKeysOf(3);
        sort($actual);

        self::assertEquals(
            ['b', 'f'],
            $actual);

        $actual = $this->iMap->allKeysOf(8);
        sort($actual);

        self::assertEquals(
            ['c', 'g'],
            $actual);
    }

    /**
     * @covers \phbrick\collection\IMap::allKeysOf
     */
    public function testKeyOf()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'e' => 9,
            'f' => 1,
            'g' => 8,
            'h' => 1,
        ];

        $this->iMap->setAll($data);

        self::assertNull($this->iMap->keyOf(10));
        self::assertNull($this->iMap->keyOf(0));

        $actual1 = $this->iMap->keyOf(1);

        self::assertNotNull($actual1);
        self::assertArrayHasKey($actual1, $data);
        self::assertEquals(1, $data[$actual1]);

        $actual2 = $this->iMap->keyOf(3);

        self::assertNotNull($actual2);
        self::assertArrayHasKey($actual2, $data);
        self::assertEquals(3, $data[$actual2]);

        $actual3 = $this->iMap->keyOf(8);

        self::assertNotNull($actual3);
        self::assertArrayHasKey($actual3, $data);
        self::assertEquals(8, $data[$actual3]);

        $actual4 = $this->iMap->keyOf(9);

        self::assertNotNull($actual4);
        self::assertArrayHasKey($actual4, $data);
        self::assertEquals(9, $data[$actual4]);

        self::assertNotEquals($actual1, $actual2);
        self::assertNotEquals($actual1, $actual3);
        self::assertNotEquals($actual1, $actual4);

        self::assertNotEquals($actual2, $actual3);
        self::assertNotEquals($actual2, $actual4);

        self::assertNotEquals($actual3, $actual4);
    }

    /**
     * @covers \phbrick\collection\IMap::has
     */
    public function testHas()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'e' => 9,
            'f' => 1,
            'g' => 8,
            'h' => 1,
        ];

        $this->iMap->setAll($data);

        self::assertFalse($this->iMap->has('j'));
        self::assertTrue($this->iMap->has('a'));
    }

    /**
     * @covers \phbrick\collection\IMap::has
     *
     */
    public function testHasThrowsExWhenKeyNull()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->has(null);
    }

    /**
     * @covers \phbrick\collection\IMap::has
     *
     */
    public function testHasThrowsExWhenKeyBool()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->has(true);
    }

    /**
     * @covers \phbrick\collection\IMap::has
     *
     */
    public function testHasThrowsExWhenKeyObject()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->has(new stdClass());
    }

    /**
     * @covers \phbrick\collection\IMap::has
     *
     */
    public function testHasThrowsExWhenKeyInteger()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $this->iMap->has(12);
    }

    /**
     * @covers \phbrick\collection\IMap::remove
     */
    public function testRemove()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'd' => 9,
        ];

        $this->iMap->setAll($data);

        self::assertTrue($this->iMap->has('c'));
        $this->iMap->remove('c');
        self::assertEquals(3, $this->iMap->getSize());
        self::assertFalse($this->iMap->has('c'));

        self::assertTrue($this->iMap->has('a'));
        $this->iMap->remove('a');
        self::assertEquals(2, $this->iMap->getSize());
        self::assertFalse($this->iMap->has('a'));

        self::assertTrue($this->iMap->has('b'));
        $this->iMap->remove('b');
        self::assertEquals(1, $this->iMap->getSize());
        self::assertFalse($this->iMap->has('b'));

        self::assertTrue($this->iMap->has('d'));
        $this->iMap->remove('d');
        self::assertEquals(0, $this->iMap->getSize());
        self::assertFalse($this->iMap->has('d'));
    }

    /**
     * @covers \phbrick\collection\IMap::removeAll
     */
    public function testRemoveAll()
    {
        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'e' => 9,
        ];

        $this->iMap->setAll($data);

        $this->iMap->removeAll(['c', 'b']);

        $expected = [
            'a' => 1,
            'e' => 9];
        $actual = $this->iMap->toArray();
        self::assertEquals($expected, $actual);
    }

    /**
     * @covers \phbrick\collection\IMap::count
     */
    public function testCount()
    {
        self::assertEquals(0, count($this->iMap));

        $this->iMap->set('a', 1);
        self::assertEquals(1, count($this->iMap));

        $this->iMap->set('b', 2);
        self::assertEquals(2, count($this->iMap));

        $this->iMap->clear();
        self::assertEquals(0, count($this->iMap));
    }

    /**
     * @covers \phbrick\collection\IMap::getIterator
     */
    public function testGetIterator()
    {
        /** @var Iterator $iterator */
        $iterator = $this->iMap->getIterator();
        self::assertNotNull($iterator);

        $count = 0;

        $iterator->rewind();

        while ($iterator->valid()) {
            $count++;

            $iterator->next();
        }

        self::assertEquals(0, $count);

        $data = [
            'a' => 1,
            'b' => 3,
            'c' => 8,
            'e' => 9,
            'f' => 1,
            'g' => 8,
            'h' => 1,
        ];

        $this->iMap->setAll($data);

        $count = 0;
        $iterator = $this->iMap->getIterator();

        $iterator->rewind();
        $items = [];

        while ($iterator->valid()) {
            $count++;
            $items[$iterator->key()] = $iterator->current();

            $iterator->next();
        }

        self::assertEquals(7, $count);
        self::assertEquals($data, $items);
    }
}
