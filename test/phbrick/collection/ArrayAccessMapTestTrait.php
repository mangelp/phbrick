<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;

/**
 * Trait for testing \ArrayAccess method with List-like implementations and Array-like ones, but not
 * maps.
 */
trait ArrayAccessMapTestTrait
{

    /**
     * @return ArrayAccess
     */
    public abstract function getArrayAccessCollection();

    /**
     * @covers \ArrayAccess::offsetGet
     * @covers \ArrayAccess::offsetSet
     * @covers \ArrayAccess::offsetExists
     */
    public function testOffsetGetAndSet()
    {
        $arrayAccess = $this->getArrayAccessCollection();

        self::assertFalse($arrayAccess->offsetExists('a'));

        $arrayAccess->offsetSet('a', 12);
        self::assertEquals(12, $arrayAccess->offsetGet('a'));

        self::assertTrue($arrayAccess->offsetExists('a'));
        self::assertFalse($arrayAccess->offsetExists('b'));

        $arrayAccess->offsetSet('b', 13);
        self::assertEquals(13, $arrayAccess->offsetGet('b'));

        self::assertTrue($arrayAccess->offsetExists('b'));
        self::assertFalse($arrayAccess->offsetExists('c'));

        $arrayAccess->offsetSet('c', 14);
        self::assertEquals(14, $arrayAccess->offsetGet('c'));

        self::assertTrue($arrayAccess->offsetExists('c'));

        $arrayAccess->offsetSet('a', 15);
        self::assertEquals(15, $arrayAccess->offsetGet('a'));

        $arrayAccess->offsetSet('c', 16);
        self::assertEquals(16, $arrayAccess->offsetGet('c'));
    }

    public function testOffsetSetToEndAppendsAndIsSameAsUsingNull()
    {
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet('a', 12);

        self::assertEquals(12, $arrayAccess['a']);

        $arrayAccess->offsetSet('b', 13);
        self::assertEquals(13, $arrayAccess['b']);

        $arrayAccess->offsetSet('c', 14);
        self::assertEquals(14, $arrayAccess['c']);
    }

    /**
     *
     */
    public function testOffsetSetNullValueThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\NullValueException');
        $arrayAccess = $this->getArrayAccessCollection();

        $arrayAccess->offsetSet('a', null);
    }

    /**
     *
     */
    public function testOffsetSetNullKeyThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidKeyException');
        $arrayAccess = $this->getArrayAccessCollection();

        $arrayAccess->offsetSet(null, 1);
    }

    /**
     * @covers \ArrayAccess::offsetUnset
     * @covers \ArrayAccess::offsetGet
     * @covers \ArrayAccess::offsetSet
     * @covers \ArrayAccess::offsetExists
     */
    public function testOffsetAllOperations()
    {
        $arrayAccess = $this->getArrayAccessCollection();

        self::assertFalse($arrayAccess->offsetExists('a'));

        $arrayAccess->offsetSet('a', 12);

        self::assertTrue($arrayAccess->offsetExists('a'));

        $arrayAccess->offsetUnset('a');

        self::assertFalse($arrayAccess->offsetExists('a'));
    }
}
