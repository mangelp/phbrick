<?php

namespace phbrick\collection;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-08-03 at 19:44:44.
 */
class SortableArrayMapTest extends ArrayMapTest
{
    /**
     * @var SortableArrayMap
     */
    protected $sortableArrayMap;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        parent::beforeTest();
        $this->sortableArrayMap = $this->iMap;
    }

    protected function createIMapImplementationInstance()
    {
        return new SortableArrayMap();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function afterTest()
    {
    }

    /**
     * @covers \phbrick\collection\SortableArrayMap::isSorted
     * @covers \phbrick\collection\SortableArrayMap::sort
     */
    public function testSortAndIsSorted()
    {
        self::assertFalse($this->sortableArrayMap->isSorted());
        $this->sortableArrayMap->sort();
        self::assertTrue($this->sortableArrayMap->isSorted());

        $array = [
            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 4,
        ];

        $shuffled = [
            'c' => 3,
            'b' => 2,
            'd' => 4,
            'a' => 1,
        ];

        $this->sortableArrayMap->setAll($shuffled);
        $this->sortableArrayMap->sort();
        self::assertSame($array, $this->sortableArrayMap->toArray());
        self::assertNotSame($array, $shuffled);

        $expected = [
            'd' => 4,
            'c' => 3,
            'b' => 2,
            'a' => 1,
        ];

        $this->sortableArrayMap->sort(null, true);
        self::assertSame($expected, $this->sortableArrayMap->toArray());

        $customSort = function($a, $b) use ($array) {
            $aEven = ($array[$a] % 2 == 0);
            $bEven = ($array[$b] % 2 == 0);

            if ($aEven == $bEven) {
                return $array[$a] - $array[$b];
            }
            else if ($aEven) {
                return -1;
            }
            else if ($bEven) {
                return 1;
            }

            return 0;
        };

        $expected = [
            'b' => 2,
            'd' => 4,
            'a' => 1,
            'c' => 3,
        ];

        $this->sortableArrayMap->sort($customSort);
        self::assertSame($expected, $this->sortableArrayMap->toArray());

        $expected = [
            'c' => 3,
            'a' => 1,
            'd' => 4,
            'b' => 2,
        ];

        $this->sortableArrayMap->sort($customSort, true);
        self::assertSame($expected, $this->sortableArrayMap->toArray());
    }
}
