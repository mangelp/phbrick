<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\collection;

use phbrick\test\PhbrickTestCase;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-10-04 at 17:11:09.
 */
class ConfigTest extends PhbrickTestCase
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        $this->config = new Config();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function afterTest()
    {
    }

    /**
     * @covers \phbrick\collection\Config::getWrappedConfig
     */
    public function testGetWrappedConfig()
    {
        $config = new Config($this->config);
        self::assertEquals($config->getWrappedConfig(), $this->config);
    }

    /**
     * @covers \phbrick\collection\Config::setReadOnly
     */
    public function testSetReadOnly()
    {
        self::assertFalse($this->config->isReadOnly());

        $this->config->setReadOnly();

        self::assertTrue($this->config->isReadOnly());
    }

    /**
     * @covers \phbrick\collection\Config::isReadOnly
     */
    public function testIsReadOnly()
    {
        self::assertFalse($this->config->isReadOnly());

        $this->config->setReadOnly();

        self::assertTrue($this->config->isReadOnly());
    }

    /**
     * @covers \phbrick\collection\Config::setStrict
     */
    public function testSetStrict()
    {
        self::assertTrue($this->config->isStrict());

        $this->config->setStrict(false);

        self::assertFalse($this->config->isStrict());
    }

    /**
     * @covers \phbrick\collection\Config::isStrict
     */
    public function testIsStrict()
    {
        self::assertTrue($this->config->isStrict());

        $this->config->setStrict(false);

        self::assertFalse($this->config->isStrict());
    }

    /**
     * @covers \phbrick\collection\Config::getKeyRoot
     */
    public function testGetKeyRoot()
    {
        self::assertEquals('', $this->config->getKeyRoot());

        $config = new Config(null, 'test');

        self::assertEquals('test', $config->getKeyRoot());
    }

    /**
     * @covers \phbrick\collection\Config::offsetExists
     */
    public function testOffsetExists()
    {
        $exists = $this->config->offsetExists('some.thing');

        self::assertFalse($exists);

        $this->config->setConfig('some.thing', 'blah');

        $exists = $this->config->offsetExists('some.thing');

        self::assertTrue($exists);
    }

    /**
     * @covers \phbrick\collection\Config::offsetGet
     *
     */
    public function testOffsetGetThrowsExOnUndefinedKey()
    {
        $this->expectException('\Exception');
        $this->config->offsetGet('foo.bar');
    }

    /**
     * @covers \phbrick\collection\Config::offsetGet
     */
    public function testOffsetGet()
    {
        $this->config->setConfig('foo.bar', 'baz');

        $value = $this->config->offsetGet('foo.bar');

        self::assertEquals('baz', $value);
    }

    /**
     * @covers \phbrick\collection\Config::offsetSet
     */
    public function testOffsetSet()
    {
        $this->config->offsetSet('foo.bar', 'baz');

        $value = $this->config->offsetGet('foo.bar');

        self::assertEquals('baz', $value);
    }

    /**
     * @covers \phbrick\collection\Config::offsetUnset
     *
     */
    public function testOffsetUnsetThrowsExOnUndefinedKey()
    {
        $this->expectException('\Exception');
        $this->config->offsetUnset('foo.bar');
    }

    /**
     * @covers \phbrick\collection\Config::offsetUnset
     */
    public function testOffsetUnset()
    {
        $this->config->setConfig('foo.bar', 'baz');
        $this->config->offsetUnset('foo.bar');
        self::assertFalse($this->config->hasConfig('foo.bar'));
    }

    /**
     * @covers \phbrick\collection\Config::count
     */
    public function testCount()
    {
        self::assertEquals(0, $this->config->count());

        $this->config->setConfig('foo.bar', 'baz');
        self::assertEquals(1, $this->config->count());

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fooi', 'bazi');

        self::assertEquals(2, $this->config->count());
        self::assertEquals(1, $wrap->count());

        $wrap2 = new Config($this->config, 'buzi');
        $wrap2->setConfig('foob', 'bazb');

        self::assertEquals(3, $this->config->count());
        self::assertEquals(1, $wrap->count());
        self::assertEquals(1, $wrap2->count());

        $wrap3 = new Config($wrap, 'foobar');
        self::assertEquals(0, $wrap3->count());

        $wrap3->setConfig('bazoon', 'foobarzik');

        self::assertEquals(4, $this->config->count());
        self::assertEquals(2, $wrap->count());
        self::assertEquals(1, $wrap2->count());
        self::assertEquals(1, $wrap3->count());
    }

    /**
     * @covers \phbrick\collection\Config::getSize
     */
    public function testGetTotalSize()
    {
        self::assertEquals(0, $this->config->getTotalSize());

        $this->config->setConfig('foo.bar', 'baz');
        self::assertEquals(1, $this->config->getTotalSize());

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fooi', 'bazi');

        self::assertEquals(2, $this->config->getTotalSize());
        self::assertEquals(2, $wrap->getTotalSize());

        $wrap2 = new Config($this->config, 'buzi');
        $wrap2->setConfig('foob', 'bazb');

        self::assertEquals(3, $this->config->getTotalSize());
        self::assertEquals(3, $wrap->getTotalSize());
        self::assertEquals(3, $wrap2->getTotalSize());

        $wrap3 = new Config($wrap, 'foobar');
        $wrap3->setConfig('bazoon', 'foobarzik');

        self::assertEquals(4, $this->config->getTotalSize());
        self::assertEquals(4, $wrap->getTotalSize());
        self::assertEquals(4, $wrap2->getTotalSize());
        self::assertEquals(4, $wrap3->getTotalSize());
    }

    /**
     * @covers \phbrick\collection\Config::getKeys
     */
    public function testGetKeys()
    {
        self::assertEmpty($this->config->getKeys());

        $this->config->setConfig('foo', 'bar');

        self::assertEquals([
            'foo',
        ],
            $this->config->getKeys());

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fooi', 'bazi');

        self::assertEquals([
            'foo',
            'baz.fooi',
        ],
            $this->config->getKeys());

        self::assertEquals([
            'fooi',
        ],
            $wrap->getKeys());

        $wrap2 = new Config($this->config, 'blob');
        $wrap2->setConfig('foob', 'bazb');

        self::assertEquals([
            'foo',
            'baz.fooi',
            'blob.foob',
        ],
            $this->config->getKeys());

        self::assertEquals([
            'fooi',
        ],
            $wrap->getKeys());

        self::assertEquals([
            'foob',
        ],
            $wrap2->getKeys());

        $wrap3 = new Config($wrap, 'oop');
        $wrap3->setConfig('fooc', 'bazc');

        self::assertEquals([
            'foo',
            'baz.fooi',
            'blob.foob',
            'baz.oop.fooc',
        ],
            $this->config->getKeys());

        self::assertEquals([
            'fooi',
            'oop.fooc',
        ],
            $wrap->getKeys());

        self::assertEquals([
            'foob',
        ],
            $wrap2->getKeys());

        self::assertEquals([
            'fooc',
        ],
            $wrap3->getKeys());
    }

    /**
     * @covers \phbrick\collection\Config::getValues
     */
    public function testGetValues()
    {
        self::assertEmpty($this->config->getKeys());

        $this->config->setConfig('foo', 'bar');

        self::assertEquals([
            'bar',
        ],
            $this->config->getValues());

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fooi', 'bazi');

        self::assertEquals([
            'bar',
            'bazi',
        ],
            $this->config->getValues());

        self::assertEquals([
            'bazi',
        ],
            $wrap->getValues());

        $wrap2 = new Config($this->config, 'blob');
        $wrap2->setConfig('foob', 'bazb');

        self::assertEquals([
            'bar',
            'bazi',
            'bazb',
        ],
            $this->config->getValues());

        self::assertEquals([
            'bazi',
        ],
            $wrap->getValues());

        self::assertEquals([
            'bazb',
        ],
            $wrap2->getValues());

        $wrap3 = new Config($wrap, 'oop');
        $wrap3->setConfig('fooc', 'bazc');

        self::assertEquals([
            'bar',
            'bazi',
            'bazb',
            'bazc',
        ],
            $this->config->getValues());

        self::assertEquals([
            'bazi',
            'bazc',
        ],
            $wrap->getValues());

        self::assertEquals([
            'bazb',
        ],
            $wrap2->getValues());

        self::assertEquals([
            'bazc',
        ],
            $wrap3->getValues());
    }

    /**
     * @covers \phbrick\collection\Config::toArray
     */
    public function testGetAsArray()
    {
        self::assertEmpty($this->config->toArray());

        $this->config->setConfig('foo', 'bar');

        self::assertEquals([
            'foo' => 'bar',
        ],
            $this->config->toArray());

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fooi', 'bazi');

        self::assertEquals([
            'foo'      => 'bar',
            'baz.fooi' => 'bazi',
        ],
            $this->config->toArray());

        self::assertEquals([
            'fooi' => 'bazi',
        ],
            $wrap->toArray());

        $wrap2 = new Config($this->config, 'blob');
        $wrap2->setConfig('foob', 'bazb');

        self::assertEquals([
            'foo'       => 'bar',
            'baz.fooi'  => 'bazi',
            'blob.foob' => 'bazb',
        ],
            $this->config->toArray());

        self::assertEquals([
            'fooi' => 'bazi',
        ],
            $wrap->toArray());

        self::assertEquals([
            'foob' => 'bazb',
        ],
            $wrap2->toArray());

        $wrap3 = new Config($wrap, 'oop');
        $wrap3->setConfig('fooc', 'bazc');

        self::assertEquals([
            'foo'          => 'bar',
            'baz.fooi'     => 'bazi',
            'blob.foob'    => 'bazb',
            'baz.oop.fooc' => 'bazc',
        ],
            $this->config->toArray());

        self::assertEquals([
            'fooi'     => 'bazi',
            'oop.fooc' => 'bazc',
        ],
            $wrap->toArray());

        self::assertEquals([
            'foob' => 'bazb',
        ],
            $wrap2->toArray());

        self::assertEquals([
            'fooc' => 'bazc',
        ],
            $wrap3->toArray());
    }

    /**
     * @covers \phbrick\collection\Config::getConfig
     *
     */
    public function testGetConfigNotExistingThrowsEx()
    {
        $this->expectException('\Exception');
        $this->config->getConfig('not.set');
    }

    /**
     * @covers \phbrick\collection\Config::getConfig
     */
    public function testGetConfig()
    {
        $value = $this->config->getConfig('not.set', $this);
        self::assertEquals($this, $value);

        $this->config->setConfig('foo', 'bar');
        $value = $this->config->getConfig('foo');
        self::assertEquals('bar', $value);

        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fui', 'bari');
        $value = $wrap->getConfig('fui');
        self::assertEquals('bari', $value);

        $value = $this->config->getConfig('baz.fui');
        self::assertEquals('bari', $value);
    }

    /**
     * @covers \phbrick\collection\Config::setConfig
     */
    public function testSetConfig()
    {
        $this->config->setConfig('foo', 'bar');
        $wrap = new Config($this->config, 'baz');
        $wrap->setConfig('fui', 'bari');

        self::assertEquals([
            'foo'     => 'bar',
            'baz.fui' => 'bari',
        ], $this->config->toArray());
    }

    /**
     * @covers \phbrick\collection\Config::hasConfig
     */
    public function testHasConfig()
    {
        self::assertFalse($this->config->hasConfig('not.set'));
        $this->config->setConfig('foo', 'bar');
        self::assertTrue($this->config->hasConfig('foo'));
    }

    /**
     * @covers \phbrick\collection\Config::loadFromPHPFile
     */
    public function testLoadFromPHPFile()
    {
        $this->config->loadFromPHPFile(dirname(__FILE__) . '/test.config.php');
        self::assertTrue($this->config->hasConfig('test.config'));
        self::assertEquals('yeah', $this->config->getConfig('test.config'));
    }

    /**
     * @covers \phbrick\collection\Config::getConfigKeyReplacement
     */
    public function testGetConfigKeyReplacement()
    {
        $replacement = $this->config->getConfigKeyReplacement('{foo}');
        self::assertEquals('{foo}', $replacement);

        $this->config->setConfig('foo', 'bar');
        $replacement = $this->config->getConfigKeyReplacement('{foo}');
        self::assertEquals('bar', $replacement);

        $wrap = new Config($this->config, 'boo');
        $replacement = $wrap->getConfigKeyReplacement('{foo}');
        self::assertEquals('{foo}', $replacement);

        $wrap->setConfig('foo', 'bar2');
        $replacement = $wrap->getConfigKeyReplacement('{foo}');
        self::assertEquals('bar2', $replacement);

        $replacement = $this->config->getConfigKeyReplacement('{boo.foo}');
        self::assertEquals('bar2', $replacement);
    }

    /**
     * @covers \phbrick\collection\Config::replaceConfigKeys
     */
    public function testReplaceConfigKeys()
    {
        $str = "{foo} is not {bar} but is {foo} always.";

        $replaced = $this->config->replaceConfigKeys($str);
        self::assertEquals($str, $replaced);

        $replaced = $this->config->replaceConfigKeys($str, ['{foo}' => 'a', '{bar}' => 'b']);
        self::assertEquals("a is not b but is a always.", $replaced);

        $this->config->setConfig('foo', 'c');
        $this->config->setConfig('bar', 'd');

        $replaced = $this->config->replaceConfigKeys($str, ['{foo}' => 'a', '{bar}' => 'b']);
        self::assertEquals("c is not d but is c always.", $replaced);
    }

    /**
     * @covers \phbrick\collection\Config::isEmpty
     */
    public function testIsEmpty()
    {
        self::assertTrue($this->config->isEmpty());
        $this->config->setConfig('foo', 'bar');
        self::assertFalse($this->config->isEmpty());
    }

    /**
     * @covers \phbrick\collection\Config::clear
     */
    public function testClear()
    {
        $this->config->clear();

        $this->config->setConfig('foo', 'bar');
        self::assertFalse($this->config->isEmpty());
        $this->config->clear();
        self::assertTrue($this->config->isEmpty());
    }

    /**
     * @covers \phbrick\collection\Config::wrap
     */
    public function testWrap()
    {
        $wrap = $this->config->wrap('child', true, true);

        self::assertTrue($wrap->isReadOnly());
        self::assertTrue($wrap->isStrict());
        self::assertEquals('child', $wrap->getKeyRoot());
        self::assertEquals($wrap->getWrappedConfig(), $this->config);
    }
}
