<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick\collection;

use ArrayAccess;

/**
 * Trait for testing \ArrayAccess method with List-like implementations and Array-like ones, but not
 * maps.
 */
trait ArrayAccessTestTrait
{

    /**
     * @return ArrayAccess
     */
    public abstract function getArrayAccessCollection();

    /**
     * @covers \ArrayAccess::offsetGet
     * @covers \ArrayAccess::offsetSet
     * @covers \ArrayAccess::offsetExists
     */
    public function testOffsetGetAndSet()
    {
        $arrayAccess = $this->getArrayAccessCollection();

        self::assertFalse($arrayAccess->offsetExists(0));

        $arrayAccess->offsetSet(null, 12);
        self::assertEquals(12, $arrayAccess->offsetGet(0));

        self::assertTrue($arrayAccess->offsetExists(0));
        self::assertFalse($arrayAccess->offsetExists(1));

        $arrayAccess->offsetSet(null, 13);
        self::assertEquals(13, $arrayAccess->offsetGet(1));

        self::assertTrue($arrayAccess->offsetExists(1));
        self::assertFalse($arrayAccess->offsetExists(2));

        $arrayAccess->offsetSet(null, 14);
        self::assertEquals(14, $arrayAccess->offsetGet(2));

        self::assertTrue($arrayAccess->offsetExists(2));

        $arrayAccess->offsetSet(0, 15);
        self::assertEquals(15, $arrayAccess->offsetGet(0));

        $arrayAccess->offsetSet(2, 16);
        self::assertEquals(16, $arrayAccess->offsetGet(2));
    }

    public function testOffsetSetToEndAppendsAndIsSameAsUsingNull()
    {
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet(0, 12);

        self::assertEquals(12, $arrayAccess[0]);

        $arrayAccess->offsetSet(1, 13);
        self::assertEquals(13, $arrayAccess[1]);

        $arrayAccess->offsetSet(null, 14);
        self::assertEquals(14, $arrayAccess[2]);
    }

    /**
     *
     * @expectedExceptionMessage Invalid index -1, must be in the range [0, 1]
     */
    public function testOffsetSetThrowsExLower()
    {
        $this->expectException('\OutOfBoundsException');
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet(null, 12);

        $arrayAccess->offsetSet(-1, 12);
    }

    /**
     *
     * @expectedExceptionMessage Invalid index 2, must be in the range [0, 1]
     */
    public function testOffsetSetThrowsExUpper()
    {
        $this->expectException('\OutOfBoundsException');
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet(null, 12);

        $arrayAccess->offsetSet(2, 12);
    }

    /**
     *
     * @expectedExceptionMessage The value cannot be null
     */
    public function testOffsetSetNullThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\NullValueException');
        $arrayAccess = $this->getArrayAccessCollection();

        $arrayAccess->offsetSet(null, null);
    }

    /**
     *
     * @expectedExceptionMessage The value cannot be null
     */
    public function testOffsetSetIndexNullThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\NullValueException');
        $arrayAccess = $this->getArrayAccessCollection();

        $arrayAccess->offsetSet(null, 12);

        $arrayAccess->offsetSet(0, null);
    }

    /**
     * @covers \ArrayAccess::offsetUnset
     * @covers \ArrayAccess::offsetGet
     * @covers \ArrayAccess::offsetSet
     * @covers \ArrayAccess::offsetExists
     */
    public function testOffsetAllOperations()
    {
        $arrayAccess = $this->getArrayAccessCollection();

        self::assertFalse($arrayAccess->offsetExists(0));

        $arrayAccess->offsetSet(null, 12);

        self::assertTrue($arrayAccess->offsetExists(0));

        $arrayAccess->offsetUnset(0);

        self::assertFalse($arrayAccess->offsetExists(0));
    }

    /**
     *
     * @expectedExceptionMessage Invalid index -1, must be in the range [0, 0]
     */
    public function testOffsetUnsetThrowsExLower()
    {
        $this->expectException('\OutOfBoundsException');
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet(null, 12);

        $arrayAccess->offsetUnset(-1);
    }

    /**
     *
     * @expectedExceptionMessage Invalid index 1, must be in the range [0, 0]
     */
    public function testOffsetUnsetThrowsExUpper()
    {
        $this->expectException('\OutOfBoundsException');
        $arrayAccess = $this->getArrayAccessCollection();
        $arrayAccess->offsetSet(null, 12);

        $arrayAccess->offsetUnset(1);
    }
}
