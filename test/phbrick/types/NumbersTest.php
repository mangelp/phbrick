<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use phbrick\test\PhbrickTestCase;
use stdClass;

class NumbersTest extends PhbrickTestCase
{
    public function testScalarToIntWithoutExAndWithDefaultIntValue()
    {
        $testValuesAndExpectedResults = [
            [10, "10"],
            [10.10, "10.10"],
            [10, 10],
            [10.20, 10.20],
            [-9999, "hello"],
            [-9999, new stdClass()],
            [-9999, ["10"]],
        ];

        foreach ($testValuesAndExpectedResults as $test) {
            $value = $test[1];
            $expected = $test[0];
            $actual = Numbers::toNumber($value, false, -9999);
            self::assertEquals($expected, $actual, "Failed conversion to number from " . print_r($value, true));
        }
    }

    public function testIsNumber()
    {
        $testValuesAndExpectedResults = [
            [10, true],
            [10.10, true],
            ["10", true],
            ["10.10", true],
            [[10], false],
            [0x10, true],
            [new stdClass(), false],
        ];

        foreach ($testValuesAndExpectedResults as $item) {
            $actual = Numbers::isNumber($item[0]);
            self::assertEquals($item[1], $actual);
        }
    }

    /**
     *
     * @expectedExceptionMessage
     */
    public function testArrayToNumberProducesEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidTypeException');
        Numbers::toNumber([10], true);
    }

    /**
     *
     * @expectedExceptionMessage
     */
    public function testObjectToNumberProducesEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidTypeException');
        Numbers::toNumber(new stdClass(), true);
    }

    /**
     *
     * @expectedExceptionMessage
     */
    public function testNonNumericStringToIntProducesEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Numbers::toNumber("1500HelloWorld200", true);
    }

    /**
     *
     * @expectedExceptionMessage
     */
    public function testNonNumberDefaultValuesThrowsEx()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Numbers::toNumber("Foo", false, "NonNumber");
    }

    public function testCast()
    {
        self::assertSame(12, Numbers::castNumber("12"));
        self::assertSame(12.24, Numbers::castNumber("12.24"));
        self::assertSame(12, Numbers::castNumber(12));
        self::assertSame(12.24, Numbers::castNumber(12.24));

        self::assertSame(12, Numbers::castInt("12"));
        self::assertSame(12, Numbers::castInt("12.54"));
        self::assertSame(12, Numbers::castInt(12));
        self::assertSame(12, Numbers::castInt(12.54));

        self::assertSame(12.0, Numbers::castFloat("12"));
        self::assertSame(12.54, Numbers::castFloat("12.54"));
        self::assertSame(12.0, Numbers::castFloat(12));
        self::assertSame(12.54, Numbers::castFloat(12.54));
    }
}
