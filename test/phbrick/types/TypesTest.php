<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use ArrayObject;
use phbrick\BaseStrictClass;
use phbrick\collection\ArrayList;
use phbrick\markup\Tag;
use phbrick\string\AbstractStringRenderable;
use phbrick\test\PhbrickTestCase;
use stdClass;

/**
 * Class TypesTest
 * @package phbrick\types
 */
class TypesTest extends PhbrickTestCase
{
    public function testIsType()
    {
        $stdClass = new stdClass();
        $tag = new Tag();

        $checks = [
            'int1'    => ['type' => Types::TYPE_INT, 'result' => true, 'value' => 12],
            'int2'    => ['type' => Types::TYPE_INT, 'result' => false, 'value' => 12.5],
            'int3'    => ['type' => Types::TYPE_INT, 'result' => true, 'value' => '12'],
            'int4'    => ['type' => Types::TYPE_INT, 'result' => false, 'value' => '12.5'],
            'int5'    => ['type' => Types::TYPE_INT, 'result' => false, 'value' => [12]],
            'int6'    => ['type' => Types::TYPE_INT, 'result' => false, 'value' => $stdClass],
            'float1'  => ['type' => Types::TYPE_FLOAT, 'result' => true, 'value' => 12.5],
            'float2'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'value' => 12],
            'float3'  => ['type' => Types::TYPE_FLOAT, 'result' => true, 'value' => '12.5'],
            'float4'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'value' => '12'],
            'float5'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'value' => [12.5]],
            'float6'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'value' => $stdClass],
            'string1' => ['type' => Types::TYPE_STRING, 'result' => true, 'value' => 'a'],
            'string2' => ['type' => Types::TYPE_STRING, 'result' => true, 'value' => 12],
            'string3' => ['type' => Types::TYPE_STRING, 'result' => true, 'value' => 12.5],
            'string4' => ['type' => Types::TYPE_STRING, 'result' => false, 'value' => ['a']],
            'string5' => ['type' => Types::TYPE_STRING, 'result' => false, 'value' => $stdClass],
            'class1'  => ['type' => '\\stdClass', 'result' => true, 'value' => $stdClass],
            'class2'  => ['type' => BaseStrictClass::cls(), 'result' => true, 'value' => $tag],
            'class3'  => ['type' => BaseStrictClass::cls(), 'result' => false, 'value' => $stdClass],
            'class4'  => ['type' => AbstractStringRenderable::cls(), 'result' => true, 'value' => $tag],
            'class5'  => ['type' => AbstractStringRenderable::cls(), 'result' => false, 'value' => $stdClass],
            'class6'  => ['type' => 'phbrick\\string\\SafeStringRenderTrait', 'result' => true, 'value' => $tag],
            'class7'  => ['type' => 'phbrick\\string\\SafeStringRenderTrait', 'result' => false, 'value' => $stdClass],
            'class8'  => ['type' => 'phbrick\\string\\IStringRenderable', 'result' => true, 'value' => $tag],
            'class9'  => ['type' => 'phbrick\\string\\IStringRenderable', 'result' => false, 'value' => $stdClass],
        ];

        foreach ($checks as $pos => $check) {
            $expected = $check['result'];
            $type = $check['type'];
            $value = $check['value'];

            $actual = Types::isType($type, $value);
            self::assertEquals($expected, $actual, $expected ?
                "Expected type $type does not match in test " . (string)$pos :
                "Expected type $type matches in test " . (string)$pos);
        }
    }

    /**
     *
     * @expectedExceptionMessage Unknown type \foo\bar\baz\NonDefinedExampleClass
     */
    public function testThrowsExOnUnknownType()
    {
        $this->expectException('\phbrick\exceptions\TypeException');
        Types::isType('\\foo\\bar\\baz\\NonDefinedExampleClass', new stdClass());
    }

    public function testEquals()
    {

        $checks = [
            'int1'    => ['type' => Types::TYPE_INT, 'result' => true, 'a' => 1, 'b' => 1],
            'int2'    => ['type' => Types::TYPE_INT, 'result' => false, 'a' => 1, 'b' => 2],
            'int3'    => ['type' => Types::TYPE_INT, 'result' => true, 'a' => 1, 'b' => '1'],
            'int4'    => ['type' => Types::TYPE_INT, 'result' => false, 'a' => 1, 'b' => '1.0'],
            'int5'    => ['type' => Types::TYPE_INT, 'result' => false, 'a' => 1, 'b' => '1.1'],
            'float1'  => ['type' => Types::TYPE_FLOAT, 'result' => true, 'a' => 1.1, 'b' => 1.1],
            'float2'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'a' => 1.1, 'b' => 1],
            'float3'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'a' => 1, 'b' => 1.0],
            'float4'  => ['type' => Types::TYPE_FLOAT, 'result' => true, 'a' => 1.1, 'b' => '1.1'],
            'float5'  => ['type' => Types::TYPE_FLOAT, 'result' => false, 'a' => 1.1, 'b' => '1'],
            'string1' => ['type' => Types::TYPE_STRING, 'result' => true, 'a' => 1, 'b' => 1],
            'string2' => ['type' => Types::TYPE_STRING, 'result' => true, 'a' => 1.1, 'b' => 1.1],
            'string3' => ['type' => Types::TYPE_STRING, 'result' => true, 'a' => 1, 'b' => 1.0],
            'string4' => ['type' => Types::TYPE_STRING, 'result' => true, 'a' => 'a', 'b' => 'a'],
            'string5' => ['type' => Types::TYPE_STRING, 'result' => false, 'a' => 'A', 'b' => 'a'],
        ];

        foreach ($checks as $pos => $check) {
            $expected = $check['result'];
            $type = $check['type'];
            $a = $check['a'];
            $b = $check['b'];

            $actual = Types::equals($a, $b, $type);
            self::assertEquals($expected, $actual, $expected ?
                "Expected type $type does not match in test " . (string)$pos :
                "Expected type $type matches in test " . (string)$pos);
        }

        self::assertTrue(Types::equals('foo', 'FOO', Types::TYPE_STRING, false));
    }

    public function testIsIterableAndAssert()
    {
        $this->assertTrue(Types::isIterable([]));
        $this->assertTrue(Types::isIterable(new ArrayList()));
        $this->assertTrue(Types::isIterable(new ArrayObject()));

        Types::assertIterable([]);
        Types::assertIterable(new ArrayObject());
        Types::assertIterable(new ArrayList());

        $list = new ArrayList();
        $list->add(new stdClass());
        $list->add(new stdClass());
        $list->add(new stdClass());
        $list->add(new stdClass());

        Types::assertIterable([], '\\stdClass');
        Types::assertIterable($list, '\\stdClass');
    }
}
