<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\types;

use phbrick\test\PhbrickTestCase;

class ConstantsTest extends PhbrickTestCase
{
    public function testAccessors()
    {
        self::assertFalse(Constants::isDefined("FOO_MY_CONST"));
        self::assertFalse(Constants::isReDefined("FOO_MY_CONST"));
        $oldValue = Constants::set("FOO_MY_CONST", "bar");
        self::assertTrue(is_null($oldValue));
        self::assertTrue(Constants::isDefined("FOO_MY_CONST"));
        self::assertTrue(Constants::isReDefined("FOO_MY_CONST"));
        self::assertEquals("bar", Constants::get("FOO_MY_CONST", "baz"));
        self::assertFalse(defined("FOO_MY_CONST"));
        self::assertEquals(["FOO_MY_CONST"], Constants::getReDefinedConstantNames());
        self::assertTrue(Constants::isDefined("PHP_ROUND_HALF_DOWN"));
        self::assertFalse(Constants::isReDefined("PHP_ROUND_HALF_DOWN"));
        self::assertEquals(PHP_ROUND_HALF_DOWN, Constants::get("PHP_ROUND_HALF_DOWN", "baz"));
        $oldValue = Constants::set("PHP_ROUND_HALF_DOWN", 1542);
        self::assertEquals(PHP_ROUND_HALF_DOWN, $oldValue);
        self::assertEquals(1542, Constants::get("PHP_ROUND_HALF_DOWN"));
        self::assertEquals("boo", Constants::get("XXXXX_NOT_DEFINED", "boo"));
        $oldValue = Constants::set("FOO_MY_CONST", "baz");
        self::assertEquals("bar", $oldValue);
        Constants::remove("FOO_MY_CONST");
        self::assertFalse(Constants::isDefined("FOO_MY_CONST"));
        self::assertFalse(Constants::isReDefined("FOO_MY_CONST"));
    }
}
