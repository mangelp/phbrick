<?php /** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpUnhandledExceptionInspection */

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use Exception;
use phbrick\test\PhbrickTestCase;
use RuntimeException;

/**
 * Test ClassLoader protected stuff
 */
class ExceptionHandlerTest extends PhbrickTestCase
{
    /**
     * @var ExceptionHandler
     */
    protected $exceptionHandler;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function afterTest()
    {
    }

    /**
     *
     *
     */
    public function testThrowedException()
    {
        $this->expectException('\RuntimeException');

        $exHandler1 = function(Exception $ex) {
            throw new RuntimeException('foo', 0, $ex);
        };

        $this->exceptionHandler->addHandler($exHandler1);

        $ex = new Exception();

        $this->exceptionHandler->handleException($ex);
    }

    /**
     *
     */
    public function testCallableHandling()
    {

        $exHandler1 = function() {
            throw new RuntimeException('exHandler1');
        };

        $exHandler2 = function() {
            throw new RuntimeException('exHandler2');
        };

        $this->exceptionHandler->addHandler($exHandler1);
        self::assertEquals(1, $this->exceptionHandler->getHandlers()->getSize());
        $found = $this->exceptionHandler->getHandlers()
            ->contains($exHandler1);

        self::assertTrue($found);

        $this->exceptionHandler->addHandler($exHandler2);
        self::assertEquals(2, $this->exceptionHandler->getHandlers()->getSize());
        $found = $this->exceptionHandler->getHandlers()
            ->contains($exHandler2);

        self::assertTrue($found);

        $this->exceptionHandler->removeHandler($exHandler1);
        self::assertEquals(1, $this->exceptionHandler->getHandlers()->getSize());
        $found = $this->exceptionHandler->getHandlers()
            ->contains($exHandler1);

        self::assertFalse($found);

        $found = $this->exceptionHandler->getHandlers()
            ->contains($exHandler2);

        self::assertTrue($found);
        self::assertFalse($this->exceptionHandler->getHandlers()->isEmpty());

        $this->exceptionHandler->removeHandler($exHandler2);

        $found = $this->exceptionHandler->getHandlers()
            ->contains($exHandler2);

        self::assertFalse($found);
        self::assertTrue($this->exceptionHandler->getHandlers()->isEmpty());
    }

    /**
     *
     *
     */
    public function testCallOnHandlers()
    {
        $this->expectException('\RuntimeException');

        $exHandler1 = function() {
            throw new RuntimeException('exHandler1 throws this exception');
        };

        $this->exceptionHandler->addHandler($exHandler1);

        $this->exceptionHandler->getHandlers()->call();
    }
}
