<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick;

class _x__BaseStrictClassChild extends BaseStrictClass
{

    public $propA = 'a';

    private $propB = 'b';

    public function getPropB()
    {
        return $this->propB;
    }

    protected $propC = 'c';

    public function getPropC()
    {
        return $this->propC;
    }

    public function __construct($a = 'a', $b = 'b', $c = 'c')
    {
        $this->propA = $a;
        $this->propB = $b;
        $this->propC = $c;
    }
}
