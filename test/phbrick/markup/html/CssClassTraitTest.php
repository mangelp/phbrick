<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\markup\html;

trait CssClassTraitTest
{
    /**
     * @covers \phbrick\markup\html\HtmlBody::getCssClasses
     * @covers \phbrick\markup\html\HtmlBody::setCssClasses
     */
    public function testGetSetCssClasses()
    {
        /** @var HtmlBody|HtmlDocument $instance */
        $instance = $this->getInstance();
        self::assertEmpty($instance->getCssClasses());
        $instance->setCssClasses(['foo', 'bar']);
        self::assertEquals(['foo', 'bar'], $instance->getCssClasses());

        $instance->setCssClasses('foo bar');
        self::assertEquals(['foo', 'bar'], $instance->getCssClasses());
    }

    /**
     * @covers \phbrick\markup\html\HtmlBody::addCssClass
     */
    public function testAddCssClass()
    {
        /** @var HtmlBody|HtmlDocument $instance */
        $instance = $this->getInstance();
        $instance->setCssClasses(['foo', 'bar']);
        $instance->addCssClass('blah');
        self::assertEquals(['foo', 'bar', 'blah'], $instance->getCssClasses());

        $instance->addCssClass('foo');
        self::assertEquals(['foo', 'bar', 'blah'], $instance->getCssClasses());

        $instance->addCssClass('baz bleh');
        self::assertEquals(['foo', 'bar', 'blah', 'baz', 'bleh'], $instance->getCssClasses());
    }
}
