<?php

namespace phbrick\markup\html;

use phbrick\test\PhbrickTestCase;

class HtmlBuilderTest extends PhbrickTestCase
{
    public function testBuildStaticGeneric()
    {
        /**
         * @type HtmlTag
         */
        $table = HtmlTag::castHtmlTag(HtmlBuilder::table(['class' => 'test-table', 'id' => 'myTable']));

        self::assertTrue($table instanceof HtmlTag);

        self::assertEquals(0, $table->count());
        self::assertEquals([], $table->getContent());
        self::assertEquals('table', $table->getName());
        self::assertEquals(2, count($table->getAttrs()));
    }

    public function testBuildInstanceGeneric()
    {
        $builder = new HtmlBuilder();
        $table = $builder->table();

        self::assertNotNull($builder->getRoot());
        self::assertNotNull($builder->getLast());

        /**
         * @var HtmlTag
         */
        $tag = HtmlTag::castHtmlTag($builder->getRoot());

        self::assertEquals(1, $tag->count());
        $innerTag = $tag->getContent()[0];
        self::assertEquals('table', $innerTag->getName());
    }

    public function testBuildTable()
    {
        /**
         * @var HtmlTag $tag
         */
        $table = HtmlBuilder::table();

        $table->thead()->tr()->children('th,th');
        $table->tbody()->children([['tr' => 'td,td'], ['tr' => 'td,td']]);

        $expected = '<table><thead><tr><th></th><th></th></tr></thead><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table>';
        self::assertEquals($expected, $table->toString());
    }

    public function testBuildWithAttrsAndContent()
    {
        /**
         * @var HtmlTag $tag
         */
        $div = HtmlBuilder::div();

        $div->children([
            ['name' => 'h1', 'attrs' => ['class' => 'title'], 'content' => 'Header'],
            ['name' => 'p', 'attrs' => ['class' => 'text'], 'content' => 'Blah blah blah'],
        ]);

        $expected = '<div><h1 class="title">Header</h1><p class="text">Blah blah blah</p></div>';
        self::assertEquals($expected, $div->toString());
    }

    public function testBuildWithAttrsAndContentAdvanced()
    {
        $div = HtmlBuilder::div();
        $div->children(
            'span', 'span', 'span'
        );

        $expected = '<div><span></span><span></span><span></span></div>';
        self::assertEquals($expected, $div->toString());

        $div = HtmlBuilder::div();
        $div->children(
            ['name' => 'span', 'content' => 'foo'], 'span', 'span'
        );

        $expected = '<div><span>foo</span><span></span><span></span></div>';
        self::assertEquals($expected, $div->toString());
    }

    public function testParseStr()
    {
        $tagStr = '%div|class=block|title=foo title|style=width:20px;height:12px;|foo text node|%span|class=text-label|label text node';
        $expected = '<div class="block" title="foo title" style="width:20px;height:12px;">foo text node<span class="text-label">label text node</span></div>';

        $tag = HtmlBuilder::parseTag($tagStr);
        self::assertEquals($expected, $tag->toString());

        $tagStr = '%hr//|class=separator-element is-hr|content is ignored';
        $tag = HtmlBuilder::parseTag($tagStr);
        $expected = '<hr class="separator-element is-hr"/>';
        self::assertEquals($expected, $tag->toString());

        $tagStr = '%input/|type=button|class=send-button big|Input content is ignored';
        $tag = HtmlBuilder::parseTag($tagStr);
        $expected = '<input type="button" class="send-button big">';
        self::assertEquals($expected, $tag->toString());
    }
}
