<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use phbrick\test\PhbrickTestCase;
use RuntimeException;
use Throwable;

/**
 * Test some PHP's characteristics to ensure how they work
 */
class _PhpTest extends PhbrickTestCase
{
    /**
     * Test set_error_handler.
     *
     * set_error_handler sets a new handler for errors but the old ones are
     * saved in case you call restore_error_handler so they can be restored.
     *
     * There is a way to iterate the error handling list as each time a new
     * error handler is added the previous one is returned and if there is no
     * previous error handler null is returned instead. So for each error handler
     * we must first add a new error handler and then we must remove both to
     * get the previous one.
     */
    public function testSetErrorHandler()
    {
        $defaultErrorHandler = null;
        $errorHandler1 = function() {
        };
        $errorHandler2 = function() {
            throw new RuntimeException("Chained call in error handler 2");
        };

        // Usually this is phpunit's error handler.
        $defaultErrorHandler = set_error_handler($errorHandler1);

        restore_error_handler();

        $defaultErrorHandlerCopy = set_error_handler($errorHandler1);

        self::assertEquals($defaultErrorHandler, $defaultErrorHandlerCopy);

        $errorHandler1Copy = set_error_handler($errorHandler2);

        self::assertEquals($errorHandler1, $errorHandler1Copy);

        restore_error_handler();

        $errorHandler1Copy = set_error_handler($errorHandler2);

        self::assertEquals($errorHandler1, $errorHandler1Copy);

        restore_error_handler();
        restore_error_handler();

        $defaultErrorHandlerCopy = set_error_handler($errorHandler1);

        self::assertEquals($defaultErrorHandler, $defaultErrorHandlerCopy);

        trigger_error("This will not cause an exception", E_USER_NOTICE);

        $errorHandler1Copy = set_error_handler($errorHandler2);

        self::assertEquals($errorHandler1, $errorHandler1Copy);

        try {
            // Finally test that the expected exception is thrown
            trigger_error("Throw the expected exception from the handler", E_USER_NOTICE);
        } catch (RuntimeException $rex) {
            if ($rex->getMessage() != "Chained call in error handler 2") {
                $this->fail("Failed to throw an exception from an error handler");
            }
        }

        restore_error_handler();
        restore_error_handler();
    }

    /**
     * Test set_exception_handler.
     *
     * set_exception_handler sets a new handler for uncatched exceptions but the
     * old ones are saved in case you call restore_exception_handler so they can
     * be restored.
     *
     *
     * @expectedExceptionMessage Uncatched exception but handled
     */
    public function testSetExceptionHandler()
    {
        $this->expectException('\RuntimeException');

        $defaultExceptionHandler = null;
        $exceptionHandler1 = function(Throwable $ex) {
            error_log(" -- Ex handler 1. Empty stuff. Ex: "
                . get_class($ex) . ': ' . $ex->getMessage() . "\n");
        };
        $exceptionHandler2 = function(Throwable $ex) {
            error_log(" -- Ex handler 2. Not empty stuff that tries a cleanup. Ex: "
                . get_class($ex) . ': ' . $ex->getMessage() . "\n");
        };

        // This might be phpunit's exception handler
        $defaultExceptionHandler = set_exception_handler($exceptionHandler1);

        restore_exception_handler();

        $defaultExceptionHandlerCopy = set_exception_handler($exceptionHandler1);

        self::assertEquals($defaultExceptionHandler, $defaultExceptionHandlerCopy);

        $exceptionHandler1Copy = set_exception_handler($exceptionHandler2);

        self::assertEquals($exceptionHandler1, $exceptionHandler1Copy);

        restore_exception_handler();

        $exceptionHandler1Copy = set_exception_handler($exceptionHandler2);

        self::assertEquals($exceptionHandler1, $exceptionHandler1Copy);

        restore_exception_handler();
        restore_exception_handler();

        $defaultExceptionHandlerCopy = set_exception_handler($exceptionHandler1);

        self::assertEquals($defaultExceptionHandler, $defaultExceptionHandlerCopy);

        $exceptionHandler1Copy = set_exception_handler($exceptionHandler2);

        self::assertEquals($exceptionHandler1, $exceptionHandler1Copy);

        throw new RuntimeException("Uncatched exception but handled");
    }
}
