<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\test\PhbrickTestCase;

class UnitsConverterTest extends PhbrickTestCase
{
    public function testConversions()
    {
        $converter = new UnitsConverter();

        self::assertEqualsWithDelta(UnitsConverter::DEFAULT_PIXELS_PER_POINT, $converter->getPixelsPerPoint(), 0.01);

        self::assertEquals(12, $converter->convertDpiToDpm(300, 0));
        self::assertEquals(12, $converter->fromDpiToDpm(300));

        // 12 is a rounded value, so converting it back to dpi shows up the precision loss as a difference of 5 units
        self::assertEquals(305, $converter->convertDpmToDpi(12, 0));
        self::assertEquals(305, $converter->fromDpmToDpi(12));

        self::assertEquals(378, $converter->convertMillimetersToPixels(100, 0));
        self::assertEquals(378, $converter->fromMmToPx(100));
        self::assertEquals(100, (int)$converter->convertPixelsToMillimeters(378, 0));
        self::assertEquals(100, (int)$converter->fromPxToMm(378));

        self::assertEquals(16, $converter->convertPointsToPixels(12, 0));
        self::assertEquals(16, $converter->fromPtToPx(12));
        self::assertEquals(12, $converter->convertPixelsToPoints(16, 0));
        self::assertEquals(12, $converter->fromPxToPt(16));

        self::assertEquals(11, $converter->convertMillimetersToPoints(4, 0));
        self::assertEquals(11, $converter->fromMmToPt(4));
        self::assertEquals(4, $converter->convertPointsToMillimeters(12, 0));
        self::assertEquals(4, $converter->fromPtToMm(12));

        self::assertEquals(12, $converter->fromPtToPt(12));

        $converter->setDpi(150);

        self::assertEquals(12, $converter->convertDpiToDpm(300, 0));
        // 12 is a rounded value, so converting it back to dpi shows up the precision loss as a difference of 5 units
        self::assertEquals(305, $converter->convertDpmToDpi(12, 0));

        self::assertEquals(500, $converter->convertMillimetersToPixels(100, 0));
        self::assertEquals(100, (int)$converter->convertPixelsToMillimeters(500, 0));

        self::assertEquals(21, $converter->convertPointsToPixels(12, 0));
        self::assertEquals(12, $converter->convertPixelsToPoints(21, 0));

        self::assertEquals(16, $converter->fromPtToPt(12, 0));

        $converter->setDpi(300);

        self::assertEquals(12, $converter->convertDpiToDpm(300, 0));
        // 12 is a rounded value, so converting it back to dpi shows up the precision loss as a difference of 5 units
        self::assertEquals(305, $converter->convertDpmToDpi(12, 0));

        self::assertEquals(1100, $converter->convertMillimetersToPixels(100, 0));
        self::assertEquals(100, (int)$converter->convertPixelsToMillimeters(1100, 0));

        self::assertEquals(47, $converter->convertPointsToPixels(12, 0));
        self::assertEquals(12, $converter->convertPixelsToPoints(47, 0));

        self::assertEquals(35, $converter->fromPtToPt(12, 0));
    }
}

