<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\test\PhbrickTestCase;

class PointTest extends PhbrickTestCase
{
    public function testCreate()
    {
        $point = new Point();
        self::assertEquals(0, $point->getX());
        self::assertEquals(0, $point->getY());

        $point2 = $point->offset(5, 5);

        self::assertEquals(5, $point2->getX());
        self::assertEquals(5, $point2->getY());

        $point3 = $point2->offset(-2, 2);

        self::assertEquals(3, $point3->getX());
        self::assertEquals(7, $point3->getY());

        $newOrigin = new Point(5, 10);

        $point4 = $point3->translate($newOrigin);

        self::assertEquals(8, $point4->getX());
        self::assertEquals(17, $point4->getY());
    }

    public function testClone()
    {
        $point = new Point(1, 2, MeasureUnits::PIXEL);
        $pointClone = clone $point;
        self::assertEquals($point, $pointClone);
        self::assertNotSame($point, $pointClone);

        $pointClone->setX(2);
        self::assertNotEquals($point, $pointClone);
        self::assertNotSame($point, $pointClone);
    }

    public function testPointFromValue()
    {
        $point = Point::fromValue(null);
        self::assertTrue($point->isZero());

        $point = Point::fromValue(1);
        self::assertFalse($point->isZero());
        self::assertEquals(1, $point->getX());
        self::assertEquals(1, $point->getY());

        $point = Point::fromValue(new Point(2, 3));
        self::assertFalse($point->isZero());
        self::assertEquals(2, $point->getX());
        self::assertEquals(3, $point->getY());

        $point = Point::fromValue(new Rectangle(null, new Point(4, 2)));
        self::assertFalse($point->isZero());
        self::assertEquals(4, $point->getX());
        self::assertEquals(2, $point->getY());

        $point = Point::fromValue([2, 3]);
        self::assertFalse($point->isZero());
        self::assertEquals(2, $point->getX());
        self::assertEquals(3, $point->getY());

        $point = Point::fromValue(['x' => 2, 'y' => 3]);
        self::assertFalse($point->isZero());
        self::assertEquals(2, $point->getX());
        self::assertEquals(3, $point->getY());

        $point = Point::fromValue(['left' => 2, 'top' => 3]);
        self::assertFalse($point->isZero());
        self::assertEquals(2, $point->getX());
        self::assertEquals(3, $point->getY());

        $point2 = Point::fromValue($point);
        self::assertEquals($point, $point2);
        self::assertNotSame($point, $point2);
    }

    public function testToPoint()
    {
        $point = new Point(378, 378, MeasureUnits::MILLIMETER);

        $pointPx = $point->toPoint();
        self::assertEquals(378, $pointPx->getX());
        self::assertEquals(378, $pointPx->getY());
        self::assertEquals(MeasureUnits::MILLIMETER, $pointPx->getUnits());
        self::assertEquals($point, $pointPx);
        self::assertNotSame($point, $pointPx);

        $pointPx = $point->toPoint(MeasureUnits::MILLIMETER);
        self::assertEquals(378, $pointPx->getX());
        self::assertEquals(378, $pointPx->getY());
        self::assertEquals(MeasureUnits::MILLIMETER, $pointPx->getUnits());
        self::assertEquals($point, $pointPx);
        self::assertNotSame($point, $pointPx);

        $pointPx = $point->toPoint(MeasureUnits::PIXEL, null, 0);

        self::assertEquals(100, $pointPx->getX());
        self::assertEquals(100, $pointPx->getY());
        self::assertNotEquals($point, $pointPx);
        self::assertNotSame($point, $pointPx);

        $pointPx = $point->toPoint(MeasureUnits::PIXEL, null, 2, 12);
        self::assertEquals(31.5, $pointPx->getX());
        self::assertEquals(31.5, $pointPx->getY());
        self::assertEquals(MeasureUnits::PIXEL, $pointPx->getUnits());
        self::assertNotEquals($point, $pointPx);
        self::assertNotSame($point, $pointPx);
    }

    public function testPointAxisLocation()
    {
        $point = new Point(12, 36);

        self::assertEquals(PointAxisLocation::TOP_RIGHT, $point->getAxisLocation());

        $point->setY(-2);
        self::assertEquals(PointAxisLocation::BOTTOM_RIGHT, $point->getAxisLocation());

        $point->setX(-2);
        self::assertEquals(PointAxisLocation::BOTTOM_LEFT, $point->getAxisLocation());

        $point->setY(2);
        self::assertEquals(PointAxisLocation::TOP_LEFT, $point->getAxisLocation());

        $point->setX(0);
        self::assertEquals(PointAxisLocation::Y_AXIS, $point->getAxisLocation());

        $point->setX(12);
        $point->setY(0);
        self::assertEquals(PointAxisLocation::X_AXIS, $point->getAxisLocation());

        $point->setX(0);
        self::assertEquals(PointAxisLocation::CENTER, $point->getAxisLocation());
    }
}
