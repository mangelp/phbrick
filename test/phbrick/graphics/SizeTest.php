<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\test\PhbrickTestCase;
use phbrick\types\Types;

class SizeTest extends PhbrickTestCase
{
    public function testSizeScale()
    {
        $size = new Size(100, 100, 0.0);
        $this->assertEquals(100, $size->getHeight());
        $this->assertEquals(100, $size->getWidth());
        $size->setScale(1.5);
        $this->assertEquals(150, $size->getHeight());
        $this->assertEquals(150, $size->getWidth());
        $size->setScale(0.5);
        $this->assertEquals(50, $size->getHeight());
        $this->assertEquals(50, $size->getWidth());
        $size->setScale(0.25);
        $this->assertEquals(25, $size->getHeight());
        $this->assertEquals(25, $size->getWidth());
        $size->setScale(1.25);
        $this->assertEquals(125, $size->getHeight());
        $this->assertEquals(125, $size->getWidth());
        $size->setScale(1);
        $this->assertEquals(100, $size->getHeight());
        $this->assertEquals(100, $size->getWidth());
    }

    public function testSizeClone()
    {
        $size = new Size(120, 120, 0.5);

        Types::assertCloneable($size);

        $size2 = clone $size;

        self::assertEquals($size->getWidth(), $size2->getWidth());
        self::assertEquals($size->getHeight(), $size2->getHeight());
        self::assertEquals($size->getRealWidth(), $size2->getRealWidth());
        self::assertEquals($size->getRealHeight(), $size2->getRealHeight());
        self::assertEquals($size->getScale(), $size2->getScale());

    }

    public function testSizeAspectRatio()
    {
        $size = new Size(120, 240, 0.5, true);
        self::assertEquals(60, $size->getWidth());
        self::assertEquals(120, $size->getHeight());
        $size->setWidth(30);
        self::assertEquals(30, $size->getWidth());
        self::assertEquals(60, $size->getHeight());
        $size->setScale(1);
        self::assertEquals(60, $size->getWidth());
        self::assertEquals(120, $size->getHeight());
        $size->setScale(2);
        self::assertEquals(120, $size->getWidth());
        self::assertEquals(240, $size->getHeight());
        $size->setHeight(100);
        self::assertEquals(50, $size->getWidth());
        self::assertEquals(100, $size->getHeight());
    }

    public function testIsEmpty()
    {
        self::assertTrue(Size::isEmpty(null));
        self::assertTrue(Size::isEmpty(new Size()));
        self::assertFalse(Size::isEmpty(true));
        self::assertFalse(Size::isEmpty(new Size(2, 12)));
    }

    public function testSizeFromValue()
    {
        $size = Size::fromValue(null);
        self::assertTrue($size->isZero());

        $size = Size::fromValue(1);
        self::assertFalse($size->isZero());
        self::assertEquals(1, $size->getWidth());
        self::assertEquals(1, $size->getHeight());

        $size = Size::fromValue(new Size(2, 3));
        self::assertFalse($size->isZero());
        self::assertEquals(2, $size->getWidth());
        self::assertEquals(3, $size->getHeight());

        $size = Size::fromValue(new Rectangle(new Size(4, 2), null));
        self::assertFalse($size->isZero());
        self::assertEquals(4, $size->getWidth());
        self::assertEquals(2, $size->getHeight());

        $size = Size::fromValue([2, 3]);
        self::assertFalse($size->isZero());
        self::assertEquals(2, $size->getWidth());
        self::assertEquals(3, $size->getHeight());

        $size = Size::fromValue(['width' => 2, 'height' => 3]);
        self::assertFalse($size->isZero());
        self::assertEquals(2, $size->getWidth());
        self::assertEquals(3, $size->getHeight());

        $size = Size::fromValue(['w' => 2, 'h' => 3]);
        self::assertFalse($size->isZero());
        self::assertEquals(2, $size->getWidth());
        self::assertEquals(3, $size->getHeight());

        $size2 = Size::fromValue($size);
        self::assertEquals($size, $size2);
        self::assertNotSame($size, $size2);
    }

    public function testToSize()
    {
        $size = new Size(378, 378, null, false, MeasureUnits::MILLIMETER);

        $sizePx = $size->toSize(MeasureUnits::PIXEL, null, 0);

        self::assertEquals(100, $sizePx->getWidth());
        self::assertEquals(100, $sizePx->getHeight());
    }

    /**
     *
     * @expectedExceptionMessage Width cannot be negative: -2
     */
    public function testSizeWidthCannotBeNegative()
    {
        $this->expectException('\InvalidArgumentException');
        $size = new Size(-2, 10);
    }

    /**
     *
     * @expectedExceptionMessage Height cannot be negative: -10
     */
    public function testSizeHeightCannotBeNegative()
    {
        $this->expectException('\InvalidArgumentException');
        $size = new Size(2, -10);
    }
}

