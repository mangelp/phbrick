<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\graphics;

use phbrick\test\PhbrickTestCase;

class RectangleTest extends PhbrickTestCase
{
    public function testCreate()
    {
        $rectangle = new Rectangle([3, 3], [1, 1]);

        self::assertEquals(1, $rectangle->getPoint()->getX());
        self::assertEquals(1, $rectangle->getPoint()->getY());
        self::assertEquals(3, $rectangle->getSize()->getWidth());
        self::assertEquals(3, $rectangle->getSize()->getHeight());

        self::assertEquals(1, $rectangle->getTopLeft()->getX());
        self::assertEquals(1, $rectangle->getTopLeft()->getY());
        self::assertEquals(3, $rectangle->getTopRight()->getX());
        self::assertEquals(1, $rectangle->getTopRight()->getY());
        self::assertEquals(1, $rectangle->getBottomLeft()->getX());
        self::assertEquals(3, $rectangle->getBottomLeft()->getY());
        self::assertEquals(3, $rectangle->getBottomRight()->getX());
        self::assertEquals(3, $rectangle->getBottomRight()->getY());

        $rectangle2 = new Rectangle([2, 2], [2, 2]);

        self::assertEquals(2, $rectangle2->getPoint()->getX());
        self::assertEquals(2, $rectangle2->getPoint()->getY());
        self::assertEquals(2, $rectangle2->getSize()->getWidth());
        self::assertEquals(2, $rectangle2->getSize()->getHeight());

        self::assertEquals(2, $rectangle2->getTopLeft()->getX());
        self::assertEquals(2, $rectangle2->getTopLeft()->getY());
        self::assertEquals(3, $rectangle2->getTopRight()->getX());
        self::assertEquals(2, $rectangle2->getTopRight()->getY());
        self::assertEquals(2, $rectangle2->getBottomLeft()->getX());
        self::assertEquals(3, $rectangle2->getBottomLeft()->getY());
        self::assertEquals(3, $rectangle2->getBottomRight()->getX());
        self::assertEquals(3, $rectangle2->getBottomRight()->getY());

        self::assertTrue($rectangle->containsRectangle($rectangle2));
        self::assertTrue($rectangle->containsPoint($rectangle2->getTopLeft()));
        self::assertTrue($rectangle->containsPoint($rectangle2->getTopRight()));
        self::assertTrue($rectangle->containsPoint($rectangle2->getBottomLeft()));
        self::assertTrue($rectangle->containsPoint($rectangle2->getBottomRight()));

        $clipRectangle = $rectangle->clip($rectangle2);

        self::assertEquals($clipRectangle, $rectangle2);
    }

    public function testFromValue()
    {
        $rectangle = Rectangle::fromValue(1);
        self::assertEquals([1, 1], $rectangle->getSize()->toArray());
        self::assertEquals([1, 1], $rectangle->getPoint()->toArray());

        $rectangle = Rectangle::fromValue([1, 3]);
        self::assertEquals([1, 1], $rectangle->getSize()->toArray());
        self::assertEquals([3, 3], $rectangle->getPoint()->toArray());

        $rectangle = Rectangle::fromValue([[1, 2], [3, 4]]);
        self::assertEquals([1, 2], $rectangle->getSize()->toArray());
        self::assertEquals([3, 4], $rectangle->getPoint()->toArray());

        $rectangle = Rectangle::fromValue([1, 2, 3, 4]);
        self::assertEquals([1, 2], $rectangle->getSize()->toArray());
        self::assertEquals([3, 4], $rectangle->getPoint()->toArray());

        $rectangle2 = Rectangle::fromValue($rectangle);
        self::assertEquals([1, 2], $rectangle->getSize()->toArray());
        self::assertEquals([3, 4], $rectangle->getPoint()->toArray());
        self::assertEquals($rectangle, $rectangle2);
        self::assertEquals($rectangle->getSize(), $rectangle2->getSize());
        self::assertEquals($rectangle->getPoint(), $rectangle2->getPoint());
        self::assertNotSame($rectangle, $rectangle2);
        self::assertNotSame($rectangle->getSize(), $rectangle2->getSize());
        self::assertNotSame($rectangle->getPoint(), $rectangle2->getPoint());
    }
}
