<?php

namespace phbrick\graphics;

use phbrick\test\PhbrickTestCase;

class RGBColorTest extends PhbrickTestCase
{

    protected function beforeTest()
    {

    }

    public function testCreateSimpleColor()
    {
        $color = new RgbaColor();

        $this->assertEquals(0, $color->getRed());
        $this->assertEquals(0, $color->getGreen());
        $this->assertEquals(0, $color->getBlue());
        $this->assertEquals(RgbaColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = new RgbaColor(1);

        $this->assertEquals(1, $color->getRed());
        $this->assertEquals(1, $color->getGreen());
        $this->assertEquals(1, $color->getBlue());
        $this->assertEquals(RgbaColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = new RgbaColor([1, 2, 3]);

        $this->assertEquals(1, $color->getRed());
        $this->assertEquals(2, $color->getGreen());
        $this->assertEquals(3, $color->getBlue());
        $this->assertEquals(RgbaColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = new RgbaColor([1, 2, 3, 4]);

        $this->assertEquals(1, $color->getRed());
        $this->assertEquals(2, $color->getGreen());
        $this->assertEquals(3, $color->getBlue());
        $this->assertEquals(4, $color->getAlpha());
    }

    public function testConvertTo()
    {
        $color = new RgbaColor([1, 2, 3, 4]);
        $this->assertEquals("#01020304", $color->toHex(true));
        $this->assertEquals("rgb(1,2,3)", $color->toRgb());
        $this->assertEquals("rgba(1,2,3,4)", $color->toRgba());
        $this->assertEquals([1, 2, 3, 4], $color->toArray());
        $this->assertEquals([1, 2, 3], $color->toRgbArray());
        $this->assertEquals(16909060, $color->toInt());

        $color = new RgbaColor([1, 2, 3, 4]);
        $this->assertEquals("#01020304", $color->toHex(true));
        $this->assertEquals("rgb(1,2,3)", $color->toRgb());
        $this->assertEquals("rgba(1,2,3,4)", $color->toRgba());
        $this->assertEquals([1, 2, 3, 4], $color->toArray());
        $this->assertEquals([1, 2, 3], $color->toRgbArray());
        $this->assertEquals(16909060, $color->toInt());
    }

    public function testParseArray()
    {

        $color = RgbaColor::castRGBA(RgbaColor::parse([1, 2, 3]));

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(AbstractColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = RgbaColor::parse(RGBColors::RED);

        self::assertEquals(255, $color->getRed());
        self::assertEquals(0, $color->getGreen());
        self::assertEquals(0, $color->getBlue());
        self::assertEquals(AbstractColor::MAX_BYTE_VALUE, $color->getAlpha());
    }

    public function testParseRgb()
    {
        $color = RgbaColor::parse('rgb(1,2,3)');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(AbstractColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = RgbaColor::parse('1,2,3');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(AbstractColor::MAX_BYTE_VALUE, $color->getAlpha());

        $color = RgbaColor::parse('[1,2,3]');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(AbstractColor::MAX_BYTE_VALUE, $color->getAlpha());
    }

    public function testParseRgba()
    {
        $color = RgbaColor::parse('rgba(1,2,3,0.5)');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(128, $color->getAlpha());
        self::assertEquals(0.5, $color->getAlphaFloat());

        $color = RgbaColor::parse('rgba(1,2,3,1)');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(255, $color->getAlpha());
        self::assertEquals(1.0, $color->getAlphaFloat());

        $color = RgbaColor::parse('rgba(1,2,3,1.0)');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(255, $color->getAlpha());
        self::assertEquals(1.0, $color->getAlphaFloat());

        $color = RgbaColor::parse('rgba(1,2,3,0.25)');

        self::assertEquals(1, $color->getRed());
        self::assertEquals(2, $color->getGreen());
        self::assertEquals(3, $color->getBlue());
        self::assertEquals(64, $color->getAlpha());
        self::assertEquals(0.25, $color->getAlphaFloat());
    }

    public function testParseHex()
    {
        $color = RgbaColor::parse('#f0fca4');

        self::assertEquals(0xf0, $color->getRed());
        self::assertEquals(0xfc, $color->getGreen());
        self::assertEquals(0xa4, $color->getBlue());
        self::assertEquals(255, $color->getAlpha());

        $color = RgbaColor::parse(0xf0fca4);

        self::assertEquals(0xf0, $color->getRed());
        self::assertEquals(0xfc, $color->getGreen());
        self::assertEquals(0xa4, $color->getBlue());
        self::assertEquals(255, $color->getAlpha());

        $color = RgbaColor::parse(0xf0fca41c);

        self::assertEquals(0xf0, $color->getRed());
        self::assertEquals(0xfc, $color->getGreen());
        self::assertEquals(0xa4, $color->getBlue());
        self::assertEquals(0x1c, $color->getAlpha());
    }

    public function testParseInt()
    {

        $color = RgbaColor::parse((int)0xf0fca4);

        self::assertEquals(0xf0, $color->getRed());
        self::assertEquals(0xfc, $color->getGreen());
        self::assertEquals(0xa4, $color->getBlue());
        self::assertEquals(255, $color->getAlpha());
    }

    public function testParse()
    {

        $this->assertEquals("#01020304", RgbaColor::parse('#01020304')->toHex(true, true));
        $this->assertEquals("#01020304", RgbaColor::parse('#01020304')->toHex(true, true));
        $this->assertEquals("#010203ff", RgbaColor::parse('rgb(1,2,3)')->toHex(true, true));
        $this->assertEquals("#010203ff", RgbaColor::parse('rgb(1,2,3)')->toHex(true, true));
        $this->assertEquals("#01020340", RgbaColor::parse('rgba(1,2,3,0.25)')->toHex(true, true));
        $this->assertEquals("#010203bf", RgbaColor::parse('rgba(1,2,3,0.75)')->toHex(true, true));
        $this->assertEquals("#01020304", RgbaColor::parse([1, 2, 3, 4])->toHex(true, true));
        $this->assertEquals("#01020304", RgbaColor::parse([1, 2, 3, 4])->toHex(true, true));
        $this->assertEquals("#102030ff", RgbaColor::parse(16909060)->toHex(true, true));
        $this->assertEquals("#102030ff", RgbaColor::parse("16909060")->toHex(true, true));
        $this->assertEquals("#171717ff", RgbaColor::parse("23")->toHex(true, true));
        $this->assertEquals("#000000ff", RgbaColor::parse(null)->toHex(true, true));
    }

    public function testClone()
    {
        $color = new RgbaColor();
        $cloned = clone $color;

        $this->assertFalse($color === $cloned);
        $this->assertEquals($color->toHex(), $cloned->toHex());
    }
}
