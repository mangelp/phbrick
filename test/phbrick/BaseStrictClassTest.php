<?php /** @noinspection Annotator */

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp[aTT]gmail[DoTT]com>
 */

namespace phbrick;

use phbrick\test\PhbrickTestCase;

class BaseStrictClassTest extends PhbrickTestCase
{

    /**
     *
     */
    public function testCannotAddAField()
    {
        $this->expectException('RuntimeException');
        $strictClass = new _x__BaseStrictClassChild();
        $strictClass->foo = 'bar';
    }

    /**
     *
     */
    public function testCannotGetUndefinedField()
    {
        $this->expectException('RuntimeException');
        $strictClass = new _x__BaseStrictClassChild();
        $strictClass->foo;
    }

    /**
     *
     */
    public function testCannotCallUndefinedMethod()
    {
        $this->expectException('RuntimeException');
        $strictClass = new _x__BaseStrictClassChild();
        $strictClass->foo();
    }

    /**
     *
     */
    public function testCannotCallUndefinedStaticMethod()
    {
        $this->expectException('RuntimeException');
        _x__BaseStrictClassChild::bar();
    }

    /**
     *
     */
    public function testCanUnsetDefinedProperty()
    {

        $instance = new _x__BaseStrictClassChild();
        self::assertTrue(isset($instance->propA));
        unset($instance->propA);
    }

    /**
     *
     */
    public function testCannotUnsetUndefinedProperty()
    {
        $this->expectException('RuntimeException');
        $instance = new _x__BaseStrictClassChild();

        unset($instance->uknownBar);
    }

    /**
     *
     */
    public function testCannotUnsetPrivateProperty()
    {
        $this->expectException('RuntimeException');
        $instance = new _x__BaseStrictClassChild();

        unset($instance->propB);
    }

    /**
     *
     */
    public function testCannotUnsetProtectedProperty()
    {
        $this->expectException('RuntimeException');
        $instance = new _x__BaseStrictClassChild();

        unset($instance->propC);
    }

    /**
     *
     */
    public function testIssetPublicPropertyDoesNotThrowsExceptions()
    {

        $instance = new _x__BaseStrictClassChild('set');

        self::assertTrue(isset($instance->propA));
    }

    public function testClsInstance()
    {
        self::assertEquals('phbrick\\BaseStrictClass', BaseStrictClass::cls());
        $instance = BaseStrictClass::instance();
        self::assertEquals('phbrick\\BaseStrictClass', $instance::cls());
        self::assertEquals('phbrick\\BaseStrictClass', get_class($instance));

        self::assertEquals('phbrick\\_x__BaseStrictClassChild', _x__BaseStrictClassChild::cls());
        $instance = _x__BaseStrictClassChild::instance();
        self::assertEquals('phbrick\\_x__BaseStrictClassChild', $instance::cls());
        self::assertEquals('phbrick\\_x__BaseStrictClassChild', get_class($instance));
    }
}
