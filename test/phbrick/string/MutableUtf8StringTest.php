<?php

namespace phbrick\string;

use phbrick\test\PhbrickTestCase;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2015-03-08 at 16:31:26.
 */
class MutableUtf8StringTest extends PhbrickTestCase
{
    /**
     * @var MutableUtf8String
     */
    protected $string;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function beforeTest()
    {
        $this->string = new MutableUtf8String();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function afterTest()
    {
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::append
     * @covers \phbrick\string\MutableUtf8String::prepend
     * @covers \phbrick\string\MutableUtf8String::toString
     * @covers \phbrick\string\MutableUtf8String::getLength
     * @covers \phbrick\string\MutableUtf8String::clear
     * @covers \phbrick\string\MutableUtf8String::isEmpty
     * @covers \phbrick\string\MutableUtf8String::setValue
     */
    public function testAppendPrependGetLengthToString()
    {
        self::assertEquals(0, $this->string->getLength());
        self::assertTrue($this->string->isEmpty());

        $this->string->append('hello');

        self::assertFalse($this->string->isEmpty());

        self::assertEquals(5, $this->string->getLength());
        self::assertEquals('hello', $this->string->toString());

        $this->string->append(':you');

        self::assertEquals(9, $this->string->getLength());
        self::assertEquals('hello:you', $this->string->toString());

        $this->string->prepend('hi:');

        self::assertEquals('hi:hello:you', $this->string->toString());
        self::assertEquals(12, $this->string->getLength());

        $this->string->append(':ñp');

        self::assertEquals('hi:hello:you:ñp', $this->string->toString());
        self::assertEquals(15, $this->string->getLength());

        $this->string->setValue('argfdeg');
        self::assertEquals('argfdeg', $this->string->toString());
        self::assertEquals(7, $this->string->getLength());

        self::assertFalse($this->string->isEmpty());
        $this->string->clear();
        self::assertTrue($this->string->isEmpty());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::calculateRealIndex
     */
    public function testCalculateRealIndex()
    {
        $this->string->append('12345');

        self::assertEquals(0, $this->string->calculateRealIndex('first', true));
        self::assertEquals(4, $this->string->calculateRealIndex('last', true));
        self::assertEquals(2, $this->string->calculateRealIndex('mid', true));
        self::assertEquals(3, $this->string->calculateRealIndex('before-last', true));
        self::assertEquals(5, $this->string->calculateRealIndex('after-last', true));
        self::assertEquals(1, $this->string->calculateRealIndex('after-first', true));
        self::assertEquals(4, $this->string->calculateRealIndex(-1, true));
        self::assertEquals(3, $this->string->calculateRealIndex(-2, true));
        self::assertEquals(2, $this->string->calculateRealIndex(-3, true));
        self::assertEquals(1, $this->string->calculateRealIndex(-4, true));
        self::assertEquals(0, $this->string->calculateRealIndex(-5, true));
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::getAt
     * @covers \phbrick\string\MutableUtf8String::setAt
     * @covers \phbrick\string\MutableUtf8String::removeAt
     * @covers \phbrick\string\MutableUtf8String::insertAt
     * @covers \phbrick\string\MutableUtf8String::replaceAt
     */
    public function testGetAtSetAtRemoveAtInsertAtReplaceAt()
    {
        $this->string->insertAt(0, '¡Hó!');
        self::assertEquals(4, $this->string->getLength());
        self::assertEquals('¡Hó!', $this->string->toString());

        $this->string->insertAt(0, '<');
        self::assertEquals(5, $this->string->getLength());
        self::assertEquals('<¡Hó!', $this->string->toString());

        $this->string->insertAt(5, '>');
        self::assertEquals(6, $this->string->getLength());
        self::assertEquals('<¡Hó!>', $this->string->toString());

        self::assertEquals('<', $this->string->getAt(0));
        self::assertEquals('¡', $this->string->getAt(1));
        self::assertEquals('H', $this->string->getAt(2));
        self::assertEquals('ó', $this->string->getAt(3));
        self::assertEquals('!', $this->string->getAt(4));
        self::assertEquals('>', $this->string->getAt(5));

        $this->string
            ->replaceAt(1, 1, '[')
            ->replaceAt(-2, 1, ']');

        self::assertEquals(6, $this->string->getLength());
        self::assertEquals("<[Hó]>", $this->string->toString());

        $this->string->replaceAt(1, 4, '(0)');

        self::assertEquals(5, $this->string->getLength());
        self::assertEquals("<(0)>", $this->string->toString());

        $this->string->setAt('first', '[b');
        self::assertEquals('[', $this->string->getAt(0));
        self::assertEquals("[(0)>", $this->string->toString());

        $this->string->setAt('last', ']');
        self::assertEquals(']', $this->string->getAt(4));
        self::assertEquals("[(0)]", $this->string->toString());

        $this->string->setAt('mid', 'XoX');
        self::assertEquals('X', $this->string->getAt(2));
        self::assertEquals("[(X)]", $this->string->toString());

        $this->string->removeAt('after-first', 3);
        self::assertEquals(2, $this->string->getLength());
        self::assertEquals("[]", $this->string->toString());

        $this->string->removeAt(0, 3);
        self::assertEquals(0, $this->string->getLength());
        self::assertEquals("", $this->string->toString());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::split
     */
    public function testSplit()
    {
        $this->string->append("1:2:3:4:5");
        $expected = ["1", "2", "3", "4", "5"];
        $actual = $this->string->split(':');

        self::assertEquals($expected, $actual);

        $expected = ["1", "2", "3", "4", "5"];
        $actual = $this->string->split('[:,\.;]+');

        self::assertEquals($expected, $actual);

        $this->string->setValue('ab12cd34ef56');

        $expected = ["", "12", "34", "56"];
        $actual = $this->string->split('[a-zA-Z]+', false);
        self::assertEquals($expected, $actual);

        $expected = ["12", "34", "56"];
        $actual = $this->string->split('[a-zA-Z]+', true);
        self::assertEquals($expected, $actual);

        $expected = ["ab", "cd", "ef", ""];
        $actual = $this->string->split('[0-9]+', false);
        self::assertEquals($expected, $actual);

        $expected = ["ab", "cd", "ef"];
        $actual = $this->string->split('[0-9]+', true);
        self::assertEquals($expected, $actual);

        $expected = ["ab12", "34ef56"];
        $actual = $this->string->split('cd', true);
        self::assertEquals($expected, $actual);
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::replace
     */
    public function testReplace()
    {
        $original = 'bla1bleh2blah3bleh4';
        $this->string->setValue($original);

        $this->string->replace('blah', '0');
        self::assertEquals('bla1bleh203bleh4', $this->string->toString());

        // Without the third parameter set to true regexes aren't recogniced
        $this->string->replace('[0-9]+', 'Ñ', false);
        self::assertEquals('bla1bleh203bleh4', $this->string->toString());

        $this->string->replace('[0-9]+', 'Ñ', true);
        self::assertEquals('blaÑblehÑblehÑ', $this->string->toString());

        $this->string->replace('Ñ', '(NY)', true);
        self::assertEquals('bla(NY)bleh(NY)bleh(NY)', $this->string->toString());

        $this->string->replace('bleh(NY)', '');
        self::assertEquals('bla(NY)', $this->string->toString());

        $this->string->replace('', 'some');
        self::assertEquals('bla(NY)', $this->string->toString());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::toLower
     * @covers \phbrick\string\MutableUtf8String::toUpper
     * @covers \phbrick\string\MutableUtf8String::ucFirst
     * @covers \phbrick\string\MutableUtf8String::lcFirst
     */
    public function testToLowerToUpperLcFirstUcFirst()
    {
        $this->string->setValue('ñAC');

        $this->string->toLower();
        self::assertEquals('ñac', $this->string->toString());

        $this->string->ucFirst();
        self::assertEquals('Ñac', $this->string->toString());

        $this->string->toUpper();
        self::assertEquals('ÑAC', $this->string->toString());

        $this->string->lcFirst();
        self::assertEquals('ñAC', $this->string->toString());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::reduceTo
     */
    public function testReduceTo()
    {
        $this->string->setValue('qñertYÁsdf');

        $this->string->reduceTo(1, 6);
        self::assertEquals('ñertYÁ', $this->string->toString());

        $this->string->reduceTo(0, 2);
        self::assertEquals('ñe', $this->string->toString());

        $this->string->reduceTo(1, 0);
        self::assertEquals('', $this->string->toString());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::indexOf
     */
    public function testIndexOf()
    {
        $this->string->setValue('qñertYÁsdf');

        self::assertEquals(1, $this->string->indexOf('ñ'));
        self::assertFalse($this->string->indexOf('Ñ'));

        self::assertEquals(1, $this->string->indexOf('ñert'));
        self::assertFalse($this->string->indexOf('Ñert'));
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::toArray
     */
    public function testToArray()
    {
        $this->string->setValue('abc');

        self::assertEquals(['a', 'b', 'c'], $this->string->toArray());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::startsWith
     * @covers \phbrick\string\MutableUtf8String::endsWith
     */
    public function testStartsWithEndsWith()
    {
        $this->string->setValue('ábcdéf');

        self::assertTrue($this->string->startsWith('á'));
        self::assertFalse($this->string->startsWith('a'));

        self::assertTrue($this->string->startsWith('ábc'));

        self::assertTrue($this->string->endsWith('f'));
        self::assertFalse($this->string->endsWith('g'));

        self::assertTrue($this->string->endsWith('déf'));
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::trim
     * @covers \phbrick\string\MutableUtf8String::trimStart
     * @covers \phbrick\string\MutableUtf8String::trimEnd
     */
    public function testTrimTrimStartEnd()
    {
        $orig = '
             ábcdéf
             ';

        $this->string->setValue($orig);
        $this->string->trimEnd();

        self::assertEquals('
             ábcdéf', $this->string->toString());

        $this->string->trimStart();

        self::assertEquals('ábcdéf', $this->string->toString());

        $this->string->setValue($orig);
        $this->string->trim();
        self::assertEquals('ábcdéf', $this->string->toString());

        $this->string->setValue('ábdefáb');
        $this->string->trimStart("áb");
        self::assertEquals('defáb', $this->string->toString());

        $this->string->trimEnd("áb");
        self::assertEquals('def', $this->string->toString());

        $this->string->setValue('ábdefáb');
        $this->string->trim("áb");
        self::assertEquals('def', $this->string->toString());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::count
     */
    public function testCount()
    {
        $this->string->setValue('ábdefáb');
        self::assertEquals(count($this->string), $this->string->getLength());
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::offsetGet
     */
    public function testOffsetGet()
    {
        $this->string->setValue('ábdefáb');
        self::assertEquals('á', $this->string[0]);
        self::assertEquals('b', $this->string[1]);
        self::assertEquals('á', $this->string[5]);
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::offsetSet
     */
    public function testOffsetSet()
    {
        $this->string->setValue('ábdefáb');
        self::assertEquals('á', $this->string[0]);
        self::assertEquals('b', $this->string[1]);
        self::assertEquals('á', $this->string[5]);
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::offsetExists
     */
    public function testOffsetExists()
    {
        $this->string->setValue('ábdefáb');
        self::assertTrue(isset($this->string[0]));
        self::assertTrue(isset($this->string[6]));
        self::assertFalse(isset($this->string[7]));
        self::assertTrue(isset($this->string[-1]));
        self::assertTrue(isset($this->string[-2]));
        self::assertTrue(isset($this->string[-7]));
        self::assertFalse(isset($this->string[-8]));
    }

    /**
     * @covers \phbrick\string\MutableUtf8String::offsetUnset
     */
    public function testOffsetUnset()
    {
        $this->string->setValue('ábdefáb');

        unset($this->string[0]);
        self::assertEquals('bdefáb', $this->string->toString());

        unset($this->string[2]);
        self::assertEquals('bdfáb', $this->string->toString());

        unset($this->string[3]);
        self::assertEquals('bdfb', $this->string->toString());
    }

    public function testExample()
    {
        $str = new MutableUtf8String("My ñ in string");

        $str->toLower()
            ->trimEnd("abcdefg")
            ->lcFirst()->append("!")
            ->prepend("¡")
            ->replace(' in ', ' en ');

        self::assertEquals((string)$str, "¡my ñ en strin!");
    }
}

