<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\string;

use phbrick\test\PhbrickTestCase;

/**
 *
 */
class StringsTest extends PhbrickTestCase
{

    /**
     * @covers \phbrick\string\Strings::getCharsetString
     */
    public function testGetCharsetString()
    {
        $charsetStr = Strings::getCharsetString(Strings::CHARSET_ALPHA_LOWER);
        self::assertEquals(Strings::CHARS_ALPHA_LOWER, $charsetStr);

        $charsetStr = Strings::getCharsetString(Strings::CHARSET_ALPHA_UPPER);
        self::assertEquals(Strings::CHARS_ALPHA_UPPER, $charsetStr);

        $charsetStr = Strings::getCharsetString(Strings::CHARSET_NUM);
        self::assertEquals(Strings::CHARS_NUM, $charsetStr);
    }

    /**
     * @covers \phbrick\string\Strings::random
     */
    public function testRandomGenerateSingleCharsets()
    {
        $string = Strings::random(22, Strings::CHARSET_ALPHA_LOWER);

        self::assertEquals(22, strlen($string));
        self::assertRegExp('/[a-z]{22}/', $string);

        $string = Strings::random(23, Strings::CHARSET_ALPHA_UPPER);

        self::assertEquals(23, strlen($string));
        self::assertRegExp('/[A-Z]{23}/', $string);

        $string = Strings::random(21, Strings::CHARSET_NUM);

        self::assertEquals(21, strlen($string));
        self::assertRegExp('/[0-9]{21}/', $string);

        $string = Strings::random(21, Strings::CHARSET_ARIT);

        self::assertEquals(21, strlen($string));
        self::assertRegExp('/[' . Strings::getCharsetString(Strings::CHARSET_ARIT, true) . ']{21}/', $string);

        $string = Strings::random(21, Strings::CHARSET_PUNCT);

        self::assertEquals(21, strlen($string));
        self::assertRegExp('/[' . Strings::getCharsetString(Strings::CHARSET_PUNCT, true) . ']{21}/', $string);

        $string = Strings::random(21, Strings::CHARSET_SEP);

        self::assertEquals(21, strlen($string));
        self::assertRegExp('/[' . Strings::getCharsetString(Strings::CHARSET_SEP, true) . ']{21}/', $string);

        $string = Strings::random(21, Strings::CHARSET_EXTRA);

        self::assertEquals(21, strlen($string));
        self::assertRegExp('/[' . Strings::getCharsetString(Strings::CHARSET_EXTRA, true) . ']{21}/', $string);
    }

    /**
     * @covers \phbrick\string\Strings::random
     */
    public function testRandomGenerateMultipleCharsets()
    {
        $strings = Strings::random(19, Strings::CHARSET_ALPHA_LOWER, 3);
        self::assertCount(3, $strings);

        self::assertEquals(19, strlen($strings[0]));
        self::assertRegExp('/[a-z]{19}/', $strings[0]);
        self::assertEquals(19, strlen($strings[1]));
        self::assertRegExp('/[a-z]{19}/', $strings[1]);
        self::assertEquals(19, strlen($strings[2]));
        self::assertRegExp('/[a-z]{19}/', $strings[2]);

        $strings = Strings::random(19, Strings::CHARSET_NUM, 2);
        self::assertCount(2, $strings);

        self::assertEquals(19, strlen($strings[0]));
        self::assertRegExp('/[0-9]{19}/', $strings[0]);
        self::assertEquals(19, strlen($strings[1]));
        self::assertRegExp('/[0-9]{19}/', $strings[1]);
    }

    /**
     * @covers \phbrick\string\Strings::random
     */
    public function testRandomGenerateSingleCombined()
    {
        $charsets = Strings::CHARSET_ARIT | Strings::CHARSET_ALPHA_LOWER;
        $string = Strings::random(21, $charsets);

        self::assertEquals(21, strlen($string));
        self::assertRegExp(
            '/[' . Strings::getCharsetString($charsets, true) . ']{21}/',
            $string);

        $charsets = Strings::CHARSET_ARIT | Strings::CHARSET_ALPHA_UPPER | Strings::CHARSET_NUM | Strings::CHARSET_PUNCT;
        $string = Strings::random(22, $charsets);

        self::assertEquals(22, strlen($string));
        self::assertRegExp(
            '/[' . Strings::getCharsetString($charsets, true) . ']{22}/',
            $string);
    }

    /**
     * @covers \phbrick\string\Strings::sequence
     */
    public function testGenerateSequence()
    {
        $charset = 'xyzu';
        $string = Strings::sequence(4, 0, $charset);
        self::assertEquals('xxxx', $string);

        $string = Strings::sequence(4, 1, $charset);
        self::assertEquals('yxxx', $string);

        $string = Strings::sequence(4, 2, $charset);
        self::assertEquals('zxxx', $string);

        $string = Strings::sequence(4, 3, $charset);
        self::assertEquals('uxxx', $string);

        $string = Strings::sequence(4, 4, $charset);
        self::assertEquals('xyxx', $string);

        $string = Strings::sequence(4, 5, $charset);
        self::assertEquals('yyxx', $string);

        $string = Strings::sequence(4, 6, $charset);
        self::assertEquals('zyxx', $string);

        $string = Strings::sequence(4, 7, $charset);
        self::assertEquals('uyxx', $string);
    }
}
