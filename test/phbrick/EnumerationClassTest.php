<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

namespace phbrick;

use phbrick\test\PhbrickTestCase;

class EnumerationClassTest extends PhbrickTestCase
{

    /**
     *
     */
    public function testCreationFailsBecauseItsStatic()
    {
        $this->expectException('\RuntimeException');
        $enum = new SampleEnumeration();
        self::assertNotNull($enum);
    }

    public function testCanGetItsConstants()
    {
        $constants = SampleEnumeration::getConstantNames();
        self::assertNotEmpty($constants);
        self::assertCount(4, $constants);

        self::assertEquals("GAMMA", $constants[0]);
        self::assertEquals("DELTA", $constants[1]);
        self::assertEquals("ALPHA", $constants[2]);
        self::assertEquals("BETA", $constants[3]);
    }

    /**
     *
     */
    public function testItFailsIfTheConstantDoesNotExists()
    {
        $this->expectException('\InvalidArgumentException');
        SampleEnumeration::valueOf("ZETA");
    }

    public function testValueOf()
    {
        $constants = SampleEnumeration::getConstantNames();
        self::assertEquals(SampleEnumeration::GAMMA, SampleEnumeration::valueOf($constants[0]));
        self::assertEquals(SampleEnumeration::DELTA, SampleEnumeration::valueOf($constants[1]));
        self::assertEquals(SampleEnumeration::ALPHA, SampleEnumeration::valueOf($constants[2]));
        self::assertEquals(SampleEnumeration::BETA, SampleEnumeration::valueOf($constants[3]));
    }

    public function testNameOf()
    {
        self::assertEquals("ALPHA", SampleEnumeration::nameOf("alpha"));
        self::assertEquals("BETA", SampleEnumeration::nameOf("beta"));
    }

    /**
     *
     */
    public function testAssertValue()
    {
        $this->expectException('\InvalidArgumentException');
        SampleEnumeration::assertValue("NotValid");
    }

    /**
     *
     */
    public function testAssertName()
    {
        $this->expectException('\InvalidArgumentException');
        SampleEnumeration::assertName("NotValid");
    }

    /**
     *
     * @expectedExceptionMessage Invalid type string for constant TRES. Enumeration values of phbrick\SampleTypedEnumeration type must be of type int.
     */
    public function testAssertTypedEnumeration()
    {
        $this->expectException('\phbrick\exceptions\TypeException');
        $constants = SampleTypedEnumeration::getConstantNames();
        self::assertEquals(SampleTypedEnumeration::UNO, SampleEnumeration::valueOf($constants[0]));
        self::assertEquals(SampleTypedEnumeration::DOS, SampleEnumeration::valueOf($constants[1]));
        self::assertEquals(SampleTypedEnumeration::TRES, SampleEnumeration::valueOf($constants[2]));
    }

    /**
     *
     */
    public function testValueOfCaseSensitive()
    {
        $this->expectException('\InvalidArgumentException');
        $this->expectExceptionMessage("Invalid constant name `aLphA`. Must be one of {GAMMA, DELTA, ALPHA, BETA}");
        SampleEnumeration::valueOf('aLphA');
    }

    /**
     *
     * @expectedExceptionMessage Invalid value `aLphA`. No constant found for it
     */
    public function testNameOfCaseSensitive()
    {
        $this->expectException('\InvalidArgumentException');
        SampleEnumeration::nameOf('aLphA');
    }

    public function testFindNotCaseSensitive()
    {
        $value = SampleTypedEnumeration::valueOf('uNo');
        self::assertEquals(SampleTypedEnumeration::UNO, $value);

        $value = SampleTypedEnumeration::valueOf('dos');
        self::assertEquals(SampleTypedEnumeration::DOS, $value);

        $value = SampleTypedEnumeration::valueOf('treS');
        self::assertEquals(SampleTypedEnumeration::TRES, $value);
    }
}
