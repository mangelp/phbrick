<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\http;

use phbrick\test\PhbrickTestCase;

class HttpRequestTest extends PhbrickTestCase
{
    public function testCreateBasicAndGetParams()
    {
        $httpRequest = new HttpRequest([
            'foo' => "12",
            'baz' => "true",
            "bar" => "1.24",
            "arg" => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum "]);

        self::assertEquals(4, count($httpRequest->getParams()));
        self::assertEquals(true, $httpRequest->getParamBool("baz", false));
        self::assertEquals(1.24, $httpRequest->getParamFloat("bar", 13.45));
        self::assertEquals("lorem ipsum", $httpRequest->getParamString("arg", "bla bla", 11));
    }

    public function testPopulate()
    {
        global $_REQUEST;

        $oldRequest = $_REQUEST;

        $_REQUEST = [
            'foo' => "12",
            'baz' => "true",
            "bar" => "1.24",
            "arg" => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum "];

        global $_SERVER;

        $oldServer = $_SERVER;

        $_SERVER = [
            "SERVER_NAME"     => "foo.bar",
            "SERVER_HOST"     => "foo.bar",
            "SERVER_PORT"     => "80",
            "SERVER_ADDR"     => "127.0.0.1",
            "REMOTE_ADDR"     => "127.0.0.2",
            "HTTP_USER_AGENT" => "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0",
            "REQUEST_METHOD"  => "GET",
            "HTTPS"           => "ON",
        ];

        try {
            $httpRequest = HttpRequest::createFromCurrentRequest();

            self::assertEquals("foo.bar", $httpRequest->getHost());
            self::assertEquals(80, $httpRequest->getPort());
            self::assertEquals("127.0.0.1", $httpRequest->getServerIp()->toString());
            self::assertEquals("127.0.0.2", $httpRequest->getClientIp()->toString());
            self::assertEquals("Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0", $httpRequest->getClientAgent());
            self::assertEquals(true, $httpRequest->isGet());
            self::assertEquals(false, $httpRequest->isPost());
            self::assertEquals("get", $httpRequest->getRequestMethod());
            self::assertEquals(true, $httpRequest->isSecure());
            self::assertEquals("https", $httpRequest->getScheme());
        } finally {
            $_SERVER = $oldServer;
            $_REQUEST = $oldRequest;
        }
    }

    public function testUserAgentTooBigIsTruncated()
    {
        $httpRequest = new HttpRequest();
        $httpRequest->setClientAgent("Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0");

        $expected = "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0--Mozilla/5.0 (X [...]";

        self::assertEquals($expected, $httpRequest->getClientAgent());
    }
}
