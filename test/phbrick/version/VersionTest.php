<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\version;

use phbrick\test\PhbrickTestCase;

class VersionTest extends PhbrickTestCase
{
    private $version = null;

    protected function beforeTest()
    {
        parent::beforeTest();
        $this->version = new Version();
    }

    public function testParseVersionString()
    {
        $version = Version::parseString("1.2.3-alpha.4+build.5643.x.host.23");

        self::assertNotNull($version);
        self::assertInstanceOf(Version::class, $version);
        self::assertEquals(1, $version->getMajor());
        self::assertEquals(2, $version->getMinor());
        self::assertEquals(3, $version->getPatch());
        self::assertEquals('alpha.4', $version->getPreReleaseAsString());
        self::assertEquals(['alpha', 4], $version->getPreRelease());
        self::assertEquals('build.5643.x.host.23', $version->getBuildMetaAsString());
        self::assertEquals(['build', 5643, 'x', 'host', 23], $version->getBuildMeta());

        $v2 = Version::parse("0.5.1230");
        self::assertNotNull($v2);
        self::assertInstanceOf(Version::class, $v2);
        self::assertEquals(0, $v2->getMajor());
        self::assertEquals(5, $v2->getMinor());
        self::assertEquals(1230, $v2->getPatch());
    }

    public function testParseVersionArray()
    {
        $version = Version::parseIterable([1, 3, 200, 'alpha.5']);
        self::assertNotNull($version);
        self::assertInstanceOf(Version::class, $version);
        self::assertEquals(1, $version->getMajor());
        self::assertEquals(3, $version->getMinor());
        self::assertEquals(200, $version->getPatch());
        self::assertEquals('alpha.5', $version->getPreReleaseAsString());
    }

    /**
     * @expectedExceptionMessage Invalid version string. Build metadata delimiter `+` found before pre-release delimiter `-`
     *
     */
    public function testParseFailsPlusBeforeHyphen()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.2.3+alpha.4-build.5643");
    }

    /**
     * @expectedExceptionMessage Invalid version string. Contains more than one hyphen and only one is allowed
     *
     */
    public function testParseFailsDoubleHyphen()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.2.3-alpha.4-build.5643");
    }

    /**
     * @expectedExceptionMessage Invalid version string. Contains more than one plus sign and only one is allowed
     *
     */
    public function testParseFailsDoublePlus()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.2.3+alpha.4+build.5643");
    }

    /**
     * @expectedExceptionMessage Invalid version string. The version format `x.y.z` is required
     *
     */
    public function testParseFailsNoPatch()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.2");
    }

    /**
     * @expectedExceptionMessage Invalid version string. The version format `x.y.z` is required
     *
     */
    public function testParseFailsNoPatchWithDot()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.2.");
    }

    /**
     * @expectedExceptionMessage Invalid version string. The version format `x.y.z` is required
     *
     */
    public function testParseFailsNoMinor()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1");
    }

    /**
     * @expectedExceptionMessage Invalid version string. The version format `x.y.z` is required
     *
     */
    public function testParseFailsNoMinorWithDot()
    {
        $this->expectException('\phbrick\exceptions\InvalidFormatException');
        Version::parseString("1.");
    }

    public function testCompareVersion()
    {
        $v1 = new Version(1, 1, 0);
        self::assertFalse($v1->isEmpty());
        self::assertFalse($v1->isDevelopment());

        $v2 = new Version(1, 2, 0);
        self::assertFalse($v2->isEmpty());
        self::assertFalse($v2->isDevelopment());

        self::assertEquals(-1, $v1->compareTo($v2));
        self::assertEquals(1, $v2->compareTo($v1));

        $v1->setMinor(2);
        $v1->setPreRelease('alpha.12');
        self::assertFalse($v1->isDevelopment());

        self::assertEquals(-1, $v1->compareTo($v2));
        self::assertEquals(1, $v2->compareTo($v1));

        $v1->setPreRelease(null);
        $v2->setPreRelease("beta.1");

        self::assertEquals(1, $v1->compareTo($v2));
        self::assertEquals(-1, $v2->compareTo($v1));

        $v1->setPreRelease("beta.1");
        self::assertEquals(0, $v1->compareTo($v2));
        self::assertEquals(0, $v2->compareTo($v1));

        $v1->setPreRelease("beta.12");
        self::assertEquals(1, $v1->compareTo($v2));
        self::assertEquals(-1, $v2->compareTo($v1));

        $v3 = Version::parse("0.5.1230");
        self::assertEquals(0, $v3->getMajor());
        self::assertEquals(5, $v3->getMinor());
        self::assertEquals(1230, $v3->getPatch());
        self::assertTrue($v3->isDevelopment());

        self::assertEquals(1, $v1->compareTo($v3));
    }

    public function testToInt()
    {
        $v1 = new Version(1, 1, 0);

        self::assertEquals($v1->toInt(), $v1->toInt());

        $v2 = new Version(1, 1, 0);

        self::assertEquals($v1->toInt(), $v2->toInt());

        $v2->setMinor(2);

        self::assertNotEquals($v1->toInt(), $v2->toInt());

        $v2->setMinor(1);
        $v2->setPatch(1);
        self::assertNotEquals($v1->toInt(), $v2->toInt());

        $v2->setPatch(0);
        $v2->setPreRelease('rc');
        self::assertNotEquals($v1->toInt(), $v2->toInt());

        $v2->setPreRelease(null);
        $v2->setBuildMeta('dev34rup');
        self::assertNotEquals($v1->toInt(), $v2->toInt());

        $v2->setBuildMeta(null);
        self::assertEquals($v1->toInt(), $v2->toInt());
    }
}
