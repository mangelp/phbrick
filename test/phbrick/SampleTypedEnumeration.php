<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2009-2015 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick;

use phbrick\types\Types;

/**
 * Class SampleTypedEnumeration
 * @package phbrick
 */
class SampleTypedEnumeration extends EnumerationClass
{
    const UNO = 1;
    const DOS = "2";
    const TRES = "2+1";

    public static function getEnumerationValueType()
    {
        return Types::TYPE_INT;
    }

    public static function isConstantNameCaseSensitive()
    {
        return false;
    }
}
