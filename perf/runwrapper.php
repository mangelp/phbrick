<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

if (isset($argv)
        && count($argv) == 4
        && is_file($argv[1])
        && substr($argv[1], 0, strlen(__DIR__)) == __DIR__
        && !empty($argv[2])
        && !empty($argv[3])) {
            
    require(__DIR__ . '/bootstrap.php');
    require($argv[1]);
    
    $fqcn = str_replace('/', '\\', $argv[2]);
    $methodName = $argv[3];
    
    $instance = new $fqcn();
    $instance->$methodName();
}