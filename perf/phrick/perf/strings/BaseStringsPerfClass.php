<?php

namespace phbrick\perf\strings;

use phbrick\perf\BasePerfClass;

class BaseStringsPerfClass extends BasePerfClass
{

    protected function doStringConcat()
    {
        $group = 'strcat';
        $strings = $this->getTestRandomStrings();

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'String concat');
        }

        $this->measurements->measure($group, 5, function() use($strings){
            $str = '';

            foreach($strings as $string) {
                $str .= $string;
            }

            unset($str);
        });
    }

    protected function doArrayStringImplode() {
        $group = 'arrayimpl';
        $strings = $this->getTestRandomStrings();

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'Array implode');
        }

        $this->measurements->measure($group, 5, function() use($strings){
            $arr = [];

            foreach($strings as $string) {
                $arr []= $string;
            }

            $str = implode('', $arr);
            unset($str);
            unset($arr);
        });
    }
}
