<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf\strings;

use phbrick\ClassLoader;
use phbrick\string\MutableUtf8String;

class MutableUtf8StringPerf extends BaseStringsPerfClass
{

    /**
     * @var MutableUtf8String
     */
    protected $string;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function init()
    {
        parent::init();
        $this->string = new MutableUtf8String();
    }

    protected function doMutableUtf8StringConcat() {
        $group = 'mustring';
        $strings = $this->getTestRandomStrings();

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'MutableUtf8String');
        }

        // Ensure the class is loaded
        ClassLoader::getInstance()->autoloadClass('phbrick\\string\\MutableUtf8String');

        $this->measurements->measure($group, 5, function() use($strings){
            $str = new MutableUtf8String();
            $result = null;

            foreach($strings as $string) {
                $str->append($string);
            }

            $result = $str->toString();

            unset($str);
            unset($result);
        });
    }

    public function measurePerformanceAgainstConcatenation() {

        $this->doStringConcat();
        $this->doMutableUtf8StringConcat();

        echo $this->measurementsDisplay->displayComparison('strcat', 'mustring');
    }

    public function measurePerformanceAgainstArrayImplode() {

        $this->doArrayStringImplode();
        $this->doMutableUtf8StringConcat();

        echo $this->measurementsDisplay->displayComparison('arrayimpl', 'mustring');
    }
}
