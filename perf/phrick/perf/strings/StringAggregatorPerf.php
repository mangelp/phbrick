<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf\strings;

use phbrick\string\StringAggregator;

class StringAggregatorPerf extends BaseStringsPerfClass
{
    /**
     * @var StringAggregator
     */
    protected $stringAggregator;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function init()
    {
        parent::init();
        $this->stringAggregator = new StringAggregator();
    }

    protected function doStringAggregatorAdd() {
        $group = 'straggregator';
        $strings = $this->getTestRandomStrings();

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'StringAggregator');
        }

        $this->measurements->measure('straggregator', $this->executionCount, function() use ($strings){
            $str = new StringAggregator();
            $result = null;

            foreach($strings as $string) {
                $str->append($string);
            }

            $result = $str->toString();

            unset($str);
            unset($result);
        });
    }

    public function measurePerformanceAgainstConcatenation() {

        $this->doStringConcat();
        $this->doStringAggregatorAdd();

        echo $this->measurementsDisplay->displayComparison('strcat', 'straggregator');
    }

    public function measurePerformanceAgainstArrayImplode() {

        $this->doArrayStringImplode();
        $this->doStringAggregatorAdd();

        echo $this->measurementsDisplay->displayComparison('arrayimpl', 'straggregator');
    }
}
