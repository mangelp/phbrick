<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf\strings;

use phbrick\string\StringBundler;

class StringBundlerPerf extends BaseStringsPerfClass
{
    /**
     * @var StringBundler
     */
    protected $stringBundler;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function init()
    {
        parent::init();
        $this->stringBundler = new StringBundler();
    }

    protected function doStringBundlerAdd() {
        $group = 'strbundler';
        $strings = $this->getTestRandomStrings();

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'StringBundler');
        }

        $this->measurements->measure('strbundler', $this->executionCount, function() use ($strings){
            $str = new StringBundler();
            $result = null;

            foreach($strings as $string) {
                $str->add($string);
            }

            $result = $str->toString();

            unset($str);
            unset($result);
        });
    }

    public function measurePerformanceAgainstConcatenation() {

        $this->doStringConcat();
        $this->doStringBundlerAdd();

        echo $this->measurementsDisplay->displayComparison('strcat', 'strbundler');
    }

    public function measurePerformanceAgainstArrayImplode() {

        $this->doArrayStringImplode();
        $this->doStringBundlerAdd();

        echo $this->measurementsDisplay->displayComparison('arrayimpl', 'strbundler');
    }
}
