<?php

namespace phbrick\perf;

use phbrick\BaseStrictClass;
use phbrick\string\Strings;

abstract class BasePerfClass extends BaseStrictClass
{
    private static $strings = null;

    protected static function getTestRandomStrings()
    {

        if (self::$strings) {
            return self::$strings;
        }

        $strings1 = Strings::random(5000, null, 100);
        $strings2 = Strings::random(1000, null, 100);
        $strings3 = Strings::random(25000, null, 50);
        $strings4 = Strings::random(50000, null, 10);
        $strings5 = Strings::random(600, null, 200);

        self::$strings = array_merge($strings1, $strings2, $strings3, $strings4, $strings5);
        shuffle(self::$strings);

        unset($strings1);
        unset($strings2);
        unset($strings3);
        unset($strings4);
        unset($strings5);

        return self::$strings;
    }

    /**
     *
     * @var Measurements
     */
    protected $measurements;

    /**
     *
     * @var Measurements
     */
    protected $measurementsDisplay;

    protected $executionCount = 5;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function __construct()
    {
        $this->measurements = new Measurements(true);
        $this->measurementsDisplay = new ConsoleMeasureDisplay($this->measurements);

        $this->init();
    }

    protected function init() {

    }
}
