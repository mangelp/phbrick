<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014 Miguel Angel Perez <mangelp[ATT]gmail[DOTT]com>
 */

namespace phbrick\perf\collections;

use phbrick\ClassLoader;
use phbrick\collection\Arrays;
use phbrick\perf\BasePerfClass;

class ArraysPerf extends BasePerfClass
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function doArrayMap(array $data, $callback)
    {
        $group = 'arraymap';

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'Array map');
        }

        $this->measurements->measure($group, $this->executionCount, function() use ($data, $callback){
            $result = null;
            $result = array_map($callback, $data);
            unset($result);
        });
    }

    protected function doArrayApply(array $data, $callback)
    {
        $group = 'Arrays::apply';

        if (!$this->measurements->hasGroup($group)) {
            $this->measurements->addGroup($group, 'Arrays::apply');
        }

        ClassLoader::getInstance()->loadClass('phbrick\\collection\\Arrays');

        $this->measurements->measure($group, 5, function() use ($data, $callback) {
            $resutl = null;
            $result = Arrays::apply($data, $callback);
            unset($result);
        });
    }

    public function measureApplyVsFunc() {
        $data = array();

        for ($i=0; $i<5000; $i++) {
            $data[]= $i;
        }

        $callback = function($key, $value = null){

            if ($value === null) {
                return pow($key, 2);
            }
            else {
                return array($key, pow($value, 2));
            }
        };

        $this->doArrayMap($data, $callback);
        $this->doArrayApply($data, $callback);

        echo $this->measurementsDisplay->displayComparison('arraymap', 'Arrays::apply');
    }
}
