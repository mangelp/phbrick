<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
* (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
*/

use phbrick\ClassLoader;

/**
 * We need to set the autoloader at a minimum.
 */

global $perfPath;
global $phbrickPath;

$perfPath = __DIR__;
$phbrickPath = realpath(dirname($perfPath) . '/src/phbrick');
$classLoaderFile = realpath($phbrickPath . '/ClassLoader.php');

require_once($classLoaderFile);

$classLoader = ClassLoader::getInstance();
$classLoader->initDefaults();

// We will be loading classes from both phbrick library path and the test
// library path.
$perfPath = realpath(dirname(__FILE__));
$prefixGroup = $classLoader->getPrefixGroup('phbrick');
$prefix = $prefixGroup->get('phbrick');
$prefix->getOptions()->addPath($perfPath);

$classLoader->registerAutoload();
