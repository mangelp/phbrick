<?php
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 * (c) 2014-2015 Miguel Angel Perez <mangelp@gmail.com>
 */

include __DIR__ . '/bootstrap.php';

$folders = [
    'phbrick/perf',
    'phbrick/perf/collections',
    'phbrick/perf/strings'];

foreach($folders as $folder) {
    $di = new DirectoryIterator(__DIR__ . '/' . $folder);

    foreach($di as $entry) {
        if (!$entry->isFile() || $entry->getExtension() != 'php') {
            continue;
        }

        $fname = pathinfo($entry->getFilename(), PATHINFO_FILENAME);

        if (substr($fname, -4) != 'Perf') {
            continue;
        }

        include($entry->getRealPath());
        $cname = ucfirst($fname);
        $namespace = str_replace('/', '\\', $folder);
        $fqcn = $namespace . '\\' . $cname;

        try {
            $reflection = new ReflectionClass($fqcn);
            $methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC);
        } catch (ReflectionException $rex) {
            throw new RuntimeException("Class does not exists $fqcn", 0, $rex);
        }

        $measurements = [];

        foreach($methods as $pos => $method) {

            if (substr($method->name, 0, 7) == 'measure') {
                $cmd = "php -f $perfPath/runwrapper.php " . $entry->getRealPath() . " "
                    . str_replace('\\', '/', $fqcn) . " {$method->name}";
                echo "\nRunning $fqcn::{$method->name}\n";
                passthru($cmd);
            }
        }
    }
}

